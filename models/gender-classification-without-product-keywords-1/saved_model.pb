Сж+
иЈ
D
AddV2
x"T
y"T
z"T"
Ttype:
2	
B
AssignVariableOp
resource
value"dtype"
dtypetype
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
8
Const
output"dtype"
valuetensor"
dtypetype
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	
.
Identity

input"T
output"T"	
Ttype
:
Less
x"T
y"T
z
"
Ttype:
2	
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(
?
Mul
x"T
y"T
z"T"
Ttype:
2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype
Ѕ
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
9
Softmax
logits"T
softmax"T"
Ttype:
2
[
Split
	split_dim

value"T
output"T*	num_split"
	num_splitint(0"	
Ttype
О
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
@
StaticRegexFullMatch	
input

output
"
patternstring
і
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
-
Tanh
x"T
y"T"
Ttype:

2

TensorListFromTensor
tensor"element_dtype
element_shape"
shape_type
output_handle"
element_dtypetype"

shape_typetype:
2	

TensorListReserve
element_shape"
shape_type
num_elements

handle"
element_dtypetype"

shape_typetype:
2	

TensorListStack
input_handle
element_shape
tensor"element_dtype"
element_dtypetype" 
num_elementsintџџџџџџџџџ
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	

VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 

While

input2T
output2T"
T
list(type)("
condfunc"
bodyfunc" 
output_shapeslist(shape)
 "
parallel_iterationsint
"serve*2.5.02v2.5.0-rc3-213-ga4dfb8d1a718ии)

embedding_17/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape:
ЈУd*(
shared_nameembedding_17/embeddings

+embedding_17/embeddings/Read/ReadVariableOpReadVariableOpembedding_17/embeddings* 
_output_shapes
:
ЈУd*
dtype0
z
dense_17/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:d* 
shared_namedense_17/kernel
s
#dense_17/kernel/Read/ReadVariableOpReadVariableOpdense_17/kernel*
_output_shapes

:d*
dtype0
r
dense_17/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_17/bias
k
!dense_17/bias/Read/ReadVariableOpReadVariableOpdense_17/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0

lstm_17/lstm_cell_17/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	d*,
shared_namelstm_17/lstm_cell_17/kernel

/lstm_17/lstm_cell_17/kernel/Read/ReadVariableOpReadVariableOplstm_17/lstm_cell_17/kernel*
_output_shapes
:	d*
dtype0
Ї
%lstm_17/lstm_cell_17/recurrent_kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	d*6
shared_name'%lstm_17/lstm_cell_17/recurrent_kernel
 
9lstm_17/lstm_cell_17/recurrent_kernel/Read/ReadVariableOpReadVariableOp%lstm_17/lstm_cell_17/recurrent_kernel*
_output_shapes
:	d*
dtype0

lstm_17/lstm_cell_17/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:**
shared_namelstm_17/lstm_cell_17/bias

-lstm_17/lstm_cell_17/bias/Read/ReadVariableOpReadVariableOplstm_17/lstm_cell_17/bias*
_output_shapes	
:*
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0

Adam/embedding_17/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
ЈУd*/
shared_name Adam/embedding_17/embeddings/m

2Adam/embedding_17/embeddings/m/Read/ReadVariableOpReadVariableOpAdam/embedding_17/embeddings/m* 
_output_shapes
:
ЈУd*
dtype0

Adam/dense_17/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:d*'
shared_nameAdam/dense_17/kernel/m

*Adam/dense_17/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_17/kernel/m*
_output_shapes

:d*
dtype0

Adam/dense_17/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/dense_17/bias/m
y
(Adam/dense_17/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_17/bias/m*
_output_shapes
:*
dtype0
Ё
"Adam/lstm_17/lstm_cell_17/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	d*3
shared_name$"Adam/lstm_17/lstm_cell_17/kernel/m

6Adam/lstm_17/lstm_cell_17/kernel/m/Read/ReadVariableOpReadVariableOp"Adam/lstm_17/lstm_cell_17/kernel/m*
_output_shapes
:	d*
dtype0
Е
,Adam/lstm_17/lstm_cell_17/recurrent_kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	d*=
shared_name.,Adam/lstm_17/lstm_cell_17/recurrent_kernel/m
Ў
@Adam/lstm_17/lstm_cell_17/recurrent_kernel/m/Read/ReadVariableOpReadVariableOp,Adam/lstm_17/lstm_cell_17/recurrent_kernel/m*
_output_shapes
:	d*
dtype0

 Adam/lstm_17/lstm_cell_17/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*1
shared_name" Adam/lstm_17/lstm_cell_17/bias/m

4Adam/lstm_17/lstm_cell_17/bias/m/Read/ReadVariableOpReadVariableOp Adam/lstm_17/lstm_cell_17/bias/m*
_output_shapes	
:*
dtype0

Adam/embedding_17/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
ЈУd*/
shared_name Adam/embedding_17/embeddings/v

2Adam/embedding_17/embeddings/v/Read/ReadVariableOpReadVariableOpAdam/embedding_17/embeddings/v* 
_output_shapes
:
ЈУd*
dtype0

Adam/dense_17/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:d*'
shared_nameAdam/dense_17/kernel/v

*Adam/dense_17/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_17/kernel/v*
_output_shapes

:d*
dtype0

Adam/dense_17/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/dense_17/bias/v
y
(Adam/dense_17/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_17/bias/v*
_output_shapes
:*
dtype0
Ё
"Adam/lstm_17/lstm_cell_17/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	d*3
shared_name$"Adam/lstm_17/lstm_cell_17/kernel/v

6Adam/lstm_17/lstm_cell_17/kernel/v/Read/ReadVariableOpReadVariableOp"Adam/lstm_17/lstm_cell_17/kernel/v*
_output_shapes
:	d*
dtype0
Е
,Adam/lstm_17/lstm_cell_17/recurrent_kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	d*=
shared_name.,Adam/lstm_17/lstm_cell_17/recurrent_kernel/v
Ў
@Adam/lstm_17/lstm_cell_17/recurrent_kernel/v/Read/ReadVariableOpReadVariableOp,Adam/lstm_17/lstm_cell_17/recurrent_kernel/v*
_output_shapes
:	d*
dtype0

 Adam/lstm_17/lstm_cell_17/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*1
shared_name" Adam/lstm_17/lstm_cell_17/bias/v

4Adam/lstm_17/lstm_cell_17/bias/v/Read/ReadVariableOpReadVariableOp Adam/lstm_17/lstm_cell_17/bias/v*
_output_shapes	
:*
dtype0

NoOpNoOp
+
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*Щ*
valueП*BМ* BЕ*
ѓ
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
	optimizer
	variables
regularization_losses
trainable_variables
		keras_api


signatures
b

embeddings
	variables
regularization_losses
trainable_variables
	keras_api
R
	variables
regularization_losses
trainable_variables
	keras_api
l
cell

state_spec
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
Ќ
 iter

!beta_1

"beta_2
	#decay
$learning_ratemWmXmY%mZ&m['m\v]v^v_%v`&va'vb
*
0
%1
&2
'3
4
5
 
*
0
%1
&2
'3
4
5
­
	variables

(layers
)layer_regularization_losses
*metrics
regularization_losses
trainable_variables
+non_trainable_variables
,layer_metrics
 
ge
VARIABLE_VALUEembedding_17/embeddings:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUE

0
 

0
­
	variables

-layers
.layer_regularization_losses
/metrics
regularization_losses
trainable_variables
0non_trainable_variables
1layer_metrics
 
 
 
­
	variables

2layers
3layer_regularization_losses
4metrics
regularization_losses
trainable_variables
5non_trainable_variables
6layer_metrics

7
state_size

%kernel
&recurrent_kernel
'bias
8	variables
9regularization_losses
:trainable_variables
;	keras_api
 

%0
&1
'2
 

%0
&1
'2
Й
	variables

<layers
=layer_regularization_losses
>metrics
regularization_losses

?states
trainable_variables
@non_trainable_variables
Alayer_metrics
[Y
VARIABLE_VALUEdense_17/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_17/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
­
	variables

Blayers
Clayer_regularization_losses
Dmetrics
regularization_losses
trainable_variables
Enon_trainable_variables
Flayer_metrics
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUElstm_17/lstm_cell_17/kernel&variables/1/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUE%lstm_17/lstm_cell_17/recurrent_kernel&variables/2/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUElstm_17/lstm_cell_17/bias&variables/3/.ATTRIBUTES/VARIABLE_VALUE

0
1
2
3
 

G0
H1
 
 
 
 
 
 
 
 
 
 
 
 
 

%0
&1
'2
 

%0
&1
'2
­
8	variables

Ilayers
Jlayer_regularization_losses
Kmetrics
9regularization_losses
:trainable_variables
Lnon_trainable_variables
Mlayer_metrics

0
 
 
 
 
 
 
 
 
 
 
4
	Ntotal
	Ocount
P	variables
Q	keras_api
D
	Rtotal
	Scount
T
_fn_kwargs
U	variables
V	keras_api
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

N0
O1

P	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

R0
S1

U	variables

VARIABLE_VALUEAdam/embedding_17/embeddings/mVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_17/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_17/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUE"Adam/lstm_17/lstm_cell_17/kernel/mBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE

VARIABLE_VALUE,Adam/lstm_17/lstm_cell_17/recurrent_kernel/mBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUE Adam/lstm_17/lstm_cell_17/bias/mBvariables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE

VARIABLE_VALUEAdam/embedding_17/embeddings/vVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_17/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_17/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUE"Adam/lstm_17/lstm_cell_17/kernel/vBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE

VARIABLE_VALUE,Adam/lstm_17/lstm_cell_17/recurrent_kernel/vBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUE Adam/lstm_17/lstm_cell_17/bias/vBvariables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE

"serving_default_embedding_17_inputPlaceholder*(
_output_shapes
:џџџџџџџџџ*
dtype0*
shape:џџџџџџџџџ
т
StatefulPartitionedCallStatefulPartitionedCall"serving_default_embedding_17_inputembedding_17/embeddingslstm_17/lstm_cell_17/kernellstm_17/lstm_cell_17/bias%lstm_17/lstm_cell_17/recurrent_kerneldense_17/kerneldense_17/bias*
Tin
	2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*(
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *.
f)R'
%__inference_signature_wrapper_1217151
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
д
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename+embedding_17/embeddings/Read/ReadVariableOp#dense_17/kernel/Read/ReadVariableOp!dense_17/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOp/lstm_17/lstm_cell_17/kernel/Read/ReadVariableOp9lstm_17/lstm_cell_17/recurrent_kernel/Read/ReadVariableOp-lstm_17/lstm_cell_17/bias/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp2Adam/embedding_17/embeddings/m/Read/ReadVariableOp*Adam/dense_17/kernel/m/Read/ReadVariableOp(Adam/dense_17/bias/m/Read/ReadVariableOp6Adam/lstm_17/lstm_cell_17/kernel/m/Read/ReadVariableOp@Adam/lstm_17/lstm_cell_17/recurrent_kernel/m/Read/ReadVariableOp4Adam/lstm_17/lstm_cell_17/bias/m/Read/ReadVariableOp2Adam/embedding_17/embeddings/v/Read/ReadVariableOp*Adam/dense_17/kernel/v/Read/ReadVariableOp(Adam/dense_17/bias/v/Read/ReadVariableOp6Adam/lstm_17/lstm_cell_17/kernel/v/Read/ReadVariableOp@Adam/lstm_17/lstm_cell_17/recurrent_kernel/v/Read/ReadVariableOp4Adam/lstm_17/lstm_cell_17/bias/v/Read/ReadVariableOpConst*(
Tin!
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *)
f$R"
 __inference__traced_save_1219641
Г
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameembedding_17/embeddingsdense_17/kerneldense_17/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratelstm_17/lstm_cell_17/kernel%lstm_17/lstm_cell_17/recurrent_kernellstm_17/lstm_cell_17/biastotalcounttotal_1count_1Adam/embedding_17/embeddings/mAdam/dense_17/kernel/mAdam/dense_17/bias/m"Adam/lstm_17/lstm_cell_17/kernel/m,Adam/lstm_17/lstm_cell_17/recurrent_kernel/m Adam/lstm_17/lstm_cell_17/bias/mAdam/embedding_17/embeddings/vAdam/dense_17/kernel/vAdam/dense_17/bias/v"Adam/lstm_17/lstm_cell_17/kernel/v,Adam/lstm_17/lstm_cell_17/recurrent_kernel/v Adam/lstm_17/lstm_cell_17/bias/v*'
Tin 
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *,
f'R%
#__inference__traced_restore_1219732Ђн(
д
Ж
)__inference_lstm_17_layer_call_fn_1217984

inputs
unknown:	d
	unknown_0:	
	unknown_1:	d
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12165232
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:џџџџџџџџџd: : : 22
StatefulPartitionedCallStatefulPartitionedCall:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
Ћ
ш
D__inference_lstm_17_layer_call_and_return_conditional_losses_1216523

inputs=
*lstm_cell_17_split_readvariableop_resource:	d;
,lstm_cell_17_split_1_readvariableop_resource:	7
$lstm_cell_17_readvariableop_resource:	d
identityЂlstm_cell_17/ReadVariableOpЂlstm_cell_17/ReadVariableOp_1Ђlstm_cell_17/ReadVariableOp_2Ђlstm_cell_17/ReadVariableOp_3Ђ!lstm_cell_17/split/ReadVariableOpЂ#lstm_cell_17/split_1/ReadVariableOpЂwhileD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm{
	transpose	Transposeinputstranspose/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2
lstm_cell_17/ones_like/ShapeShapestrided_slice_2:output:0*
T0*
_output_shapes
:2
lstm_cell_17/ones_like/Shape
lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
lstm_cell_17/ones_like/ConstИ
lstm_cell_17/ones_likeFill%lstm_cell_17/ones_like/Shape:output:0%lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like~
lstm_cell_17/ones_like_1/ShapeShapezeros:output:0*
T0*
_output_shapes
:2 
lstm_cell_17/ones_like_1/Shape
lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2 
lstm_cell_17/ones_like_1/ConstР
lstm_cell_17/ones_like_1Fill'lstm_cell_17/ones_like_1/Shape:output:0'lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like_1
lstm_cell_17/mulMulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul
lstm_cell_17/mul_1Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_1
lstm_cell_17/mul_2Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_2
lstm_cell_17/mul_3Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_3~
lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_cell_17/split/split_dimВ
!lstm_cell_17/split/ReadVariableOpReadVariableOp*lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02#
!lstm_cell_17/split/ReadVariableOpл
lstm_cell_17/splitSplit%lstm_cell_17/split/split_dim:output:0)lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_cell_17/split
lstm_cell_17/MatMulMatMullstm_cell_17/mul:z:0lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul
lstm_cell_17/MatMul_1MatMullstm_cell_17/mul_1:z:0lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_1
lstm_cell_17/MatMul_2MatMullstm_cell_17/mul_2:z:0lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_2
lstm_cell_17/MatMul_3MatMullstm_cell_17/mul_3:z:0lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_3
lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2 
lstm_cell_17/split_1/split_dimД
#lstm_cell_17/split_1/ReadVariableOpReadVariableOp,lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02%
#lstm_cell_17/split_1/ReadVariableOpг
lstm_cell_17/split_1Split'lstm_cell_17/split_1/split_dim:output:0+lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_cell_17/split_1Ї
lstm_cell_17/BiasAddBiasAddlstm_cell_17/MatMul:product:0lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd­
lstm_cell_17/BiasAdd_1BiasAddlstm_cell_17/MatMul_1:product:0lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_1­
lstm_cell_17/BiasAdd_2BiasAddlstm_cell_17/MatMul_2:product:0lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_2­
lstm_cell_17/BiasAdd_3BiasAddlstm_cell_17/MatMul_3:product:0lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_3
lstm_cell_17/mul_4Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_4
lstm_cell_17/mul_5Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_5
lstm_cell_17/mul_6Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_6
lstm_cell_17/mul_7Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_7 
lstm_cell_17/ReadVariableOpReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp
 lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2"
 lstm_cell_17/strided_slice/stack
"lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice/stack_1
"lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2$
"lstm_cell_17/strided_slice/stack_2Ъ
lstm_cell_17/strided_sliceStridedSlice#lstm_cell_17/ReadVariableOp:value:0)lstm_cell_17/strided_slice/stack:output:0+lstm_cell_17/strided_slice/stack_1:output:0+lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_sliceЇ
lstm_cell_17/MatMul_4MatMullstm_cell_17/mul_4:z:0#lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_4
lstm_cell_17/addAddV2lstm_cell_17/BiasAdd:output:0lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add
lstm_cell_17/SigmoidSigmoidlstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/SigmoidЄ
lstm_cell_17/ReadVariableOp_1ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_1
"lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice_1/stack
$lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2&
$lstm_cell_17/strided_slice_1/stack_1
$lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_1/stack_2ж
lstm_cell_17/strided_slice_1StridedSlice%lstm_cell_17/ReadVariableOp_1:value:0+lstm_cell_17/strided_slice_1/stack:output:0-lstm_cell_17/strided_slice_1/stack_1:output:0-lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_1Љ
lstm_cell_17/MatMul_5MatMullstm_cell_17/mul_5:z:0%lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_5Ѕ
lstm_cell_17/add_1AddV2lstm_cell_17/BiasAdd_1:output:0lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_1
lstm_cell_17/Sigmoid_1Sigmoidlstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_1
lstm_cell_17/mul_8Mullstm_cell_17/Sigmoid_1:y:0zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_8Є
lstm_cell_17/ReadVariableOp_2ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_2
"lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2$
"lstm_cell_17/strided_slice_2/stack
$lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2&
$lstm_cell_17/strided_slice_2/stack_1
$lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_2/stack_2ж
lstm_cell_17/strided_slice_2StridedSlice%lstm_cell_17/ReadVariableOp_2:value:0+lstm_cell_17/strided_slice_2/stack:output:0-lstm_cell_17/strided_slice_2/stack_1:output:0-lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_2Љ
lstm_cell_17/MatMul_6MatMullstm_cell_17/mul_6:z:0%lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_6Ѕ
lstm_cell_17/add_2AddV2lstm_cell_17/BiasAdd_2:output:0lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_2x
lstm_cell_17/TanhTanhlstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh
lstm_cell_17/mul_9Mullstm_cell_17/Sigmoid:y:0lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_9
lstm_cell_17/add_3AddV2lstm_cell_17/mul_8:z:0lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_3Є
lstm_cell_17/ReadVariableOp_3ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_3
"lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2$
"lstm_cell_17/strided_slice_3/stack
$lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2&
$lstm_cell_17/strided_slice_3/stack_1
$lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_3/stack_2ж
lstm_cell_17/strided_slice_3StridedSlice%lstm_cell_17/ReadVariableOp_3:value:0+lstm_cell_17/strided_slice_3/stack:output:0-lstm_cell_17/strided_slice_3/stack_1:output:0-lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_3Љ
lstm_cell_17/MatMul_7MatMullstm_cell_17/mul_7:z:0%lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_7Ѕ
lstm_cell_17/add_4AddV2lstm_cell_17/BiasAdd_3:output:0lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_4
lstm_cell_17/Sigmoid_2Sigmoidlstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_2|
lstm_cell_17/Tanh_1Tanhlstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh_1
lstm_cell_17/mul_10Mullstm_cell_17/Sigmoid_2:y:0lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_10
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterц
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0*lstm_cell_17_split_readvariableop_resource,lstm_cell_17_split_1_readvariableop_resource$lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1216389*
condR
while_cond_1216388*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeщ
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*,
_output_shapes
:џџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permІ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtimeМ
IdentityIdentitystrided_slice_3:output:0^lstm_cell_17/ReadVariableOp^lstm_cell_17/ReadVariableOp_1^lstm_cell_17/ReadVariableOp_2^lstm_cell_17/ReadVariableOp_3"^lstm_cell_17/split/ReadVariableOp$^lstm_cell_17/split_1/ReadVariableOp^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:џџџџџџџџџd: : : 2:
lstm_cell_17/ReadVariableOplstm_cell_17/ReadVariableOp2>
lstm_cell_17/ReadVariableOp_1lstm_cell_17/ReadVariableOp_12>
lstm_cell_17/ReadVariableOp_2lstm_cell_17/ReadVariableOp_22>
lstm_cell_17/ReadVariableOp_3lstm_cell_17/ReadVariableOp_32F
!lstm_cell_17/split/ReadVariableOp!lstm_cell_17/split/ReadVariableOp2J
#lstm_cell_17/split_1/ReadVariableOp#lstm_cell_17/split_1/ReadVariableOp2
whilewhile:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
м
ъ
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218246
inputs_0=
*lstm_cell_17_split_readvariableop_resource:	d;
,lstm_cell_17_split_1_readvariableop_resource:	7
$lstm_cell_17_readvariableop_resource:	d
identityЂlstm_cell_17/ReadVariableOpЂlstm_cell_17/ReadVariableOp_1Ђlstm_cell_17/ReadVariableOp_2Ђlstm_cell_17/ReadVariableOp_3Ђ!lstm_cell_17/split/ReadVariableOpЂ#lstm_cell_17/split_1/ReadVariableOpЂwhileF
ShapeShapeinputs_0*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm
	transpose	Transposeinputs_0transpose/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2
lstm_cell_17/ones_like/ShapeShapestrided_slice_2:output:0*
T0*
_output_shapes
:2
lstm_cell_17/ones_like/Shape
lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
lstm_cell_17/ones_like/ConstИ
lstm_cell_17/ones_likeFill%lstm_cell_17/ones_like/Shape:output:0%lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like~
lstm_cell_17/ones_like_1/ShapeShapezeros:output:0*
T0*
_output_shapes
:2 
lstm_cell_17/ones_like_1/Shape
lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2 
lstm_cell_17/ones_like_1/ConstР
lstm_cell_17/ones_like_1Fill'lstm_cell_17/ones_like_1/Shape:output:0'lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like_1
lstm_cell_17/mulMulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul
lstm_cell_17/mul_1Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_1
lstm_cell_17/mul_2Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_2
lstm_cell_17/mul_3Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_3~
lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_cell_17/split/split_dimВ
!lstm_cell_17/split/ReadVariableOpReadVariableOp*lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02#
!lstm_cell_17/split/ReadVariableOpл
lstm_cell_17/splitSplit%lstm_cell_17/split/split_dim:output:0)lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_cell_17/split
lstm_cell_17/MatMulMatMullstm_cell_17/mul:z:0lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul
lstm_cell_17/MatMul_1MatMullstm_cell_17/mul_1:z:0lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_1
lstm_cell_17/MatMul_2MatMullstm_cell_17/mul_2:z:0lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_2
lstm_cell_17/MatMul_3MatMullstm_cell_17/mul_3:z:0lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_3
lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2 
lstm_cell_17/split_1/split_dimД
#lstm_cell_17/split_1/ReadVariableOpReadVariableOp,lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02%
#lstm_cell_17/split_1/ReadVariableOpг
lstm_cell_17/split_1Split'lstm_cell_17/split_1/split_dim:output:0+lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_cell_17/split_1Ї
lstm_cell_17/BiasAddBiasAddlstm_cell_17/MatMul:product:0lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd­
lstm_cell_17/BiasAdd_1BiasAddlstm_cell_17/MatMul_1:product:0lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_1­
lstm_cell_17/BiasAdd_2BiasAddlstm_cell_17/MatMul_2:product:0lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_2­
lstm_cell_17/BiasAdd_3BiasAddlstm_cell_17/MatMul_3:product:0lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_3
lstm_cell_17/mul_4Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_4
lstm_cell_17/mul_5Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_5
lstm_cell_17/mul_6Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_6
lstm_cell_17/mul_7Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_7 
lstm_cell_17/ReadVariableOpReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp
 lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2"
 lstm_cell_17/strided_slice/stack
"lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice/stack_1
"lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2$
"lstm_cell_17/strided_slice/stack_2Ъ
lstm_cell_17/strided_sliceStridedSlice#lstm_cell_17/ReadVariableOp:value:0)lstm_cell_17/strided_slice/stack:output:0+lstm_cell_17/strided_slice/stack_1:output:0+lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_sliceЇ
lstm_cell_17/MatMul_4MatMullstm_cell_17/mul_4:z:0#lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_4
lstm_cell_17/addAddV2lstm_cell_17/BiasAdd:output:0lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add
lstm_cell_17/SigmoidSigmoidlstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/SigmoidЄ
lstm_cell_17/ReadVariableOp_1ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_1
"lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice_1/stack
$lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2&
$lstm_cell_17/strided_slice_1/stack_1
$lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_1/stack_2ж
lstm_cell_17/strided_slice_1StridedSlice%lstm_cell_17/ReadVariableOp_1:value:0+lstm_cell_17/strided_slice_1/stack:output:0-lstm_cell_17/strided_slice_1/stack_1:output:0-lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_1Љ
lstm_cell_17/MatMul_5MatMullstm_cell_17/mul_5:z:0%lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_5Ѕ
lstm_cell_17/add_1AddV2lstm_cell_17/BiasAdd_1:output:0lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_1
lstm_cell_17/Sigmoid_1Sigmoidlstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_1
lstm_cell_17/mul_8Mullstm_cell_17/Sigmoid_1:y:0zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_8Є
lstm_cell_17/ReadVariableOp_2ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_2
"lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2$
"lstm_cell_17/strided_slice_2/stack
$lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2&
$lstm_cell_17/strided_slice_2/stack_1
$lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_2/stack_2ж
lstm_cell_17/strided_slice_2StridedSlice%lstm_cell_17/ReadVariableOp_2:value:0+lstm_cell_17/strided_slice_2/stack:output:0-lstm_cell_17/strided_slice_2/stack_1:output:0-lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_2Љ
lstm_cell_17/MatMul_6MatMullstm_cell_17/mul_6:z:0%lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_6Ѕ
lstm_cell_17/add_2AddV2lstm_cell_17/BiasAdd_2:output:0lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_2x
lstm_cell_17/TanhTanhlstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh
lstm_cell_17/mul_9Mullstm_cell_17/Sigmoid:y:0lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_9
lstm_cell_17/add_3AddV2lstm_cell_17/mul_8:z:0lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_3Є
lstm_cell_17/ReadVariableOp_3ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_3
"lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2$
"lstm_cell_17/strided_slice_3/stack
$lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2&
$lstm_cell_17/strided_slice_3/stack_1
$lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_3/stack_2ж
lstm_cell_17/strided_slice_3StridedSlice%lstm_cell_17/ReadVariableOp_3:value:0+lstm_cell_17/strided_slice_3/stack:output:0-lstm_cell_17/strided_slice_3/stack_1:output:0-lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_3Љ
lstm_cell_17/MatMul_7MatMullstm_cell_17/mul_7:z:0%lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_7Ѕ
lstm_cell_17/add_4AddV2lstm_cell_17/BiasAdd_3:output:0lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_4
lstm_cell_17/Sigmoid_2Sigmoidlstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_2|
lstm_cell_17/Tanh_1Tanhlstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh_1
lstm_cell_17/mul_10Mullstm_cell_17/Sigmoid_2:y:0lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_10
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterц
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0*lstm_cell_17_split_readvariableop_resource,lstm_cell_17_split_1_readvariableop_resource$lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1218112*
condR
while_cond_1218111*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeё
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permЎ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtimeМ
IdentityIdentitystrided_slice_3:output:0^lstm_cell_17/ReadVariableOp^lstm_cell_17/ReadVariableOp_1^lstm_cell_17/ReadVariableOp_2^lstm_cell_17/ReadVariableOp_3"^lstm_cell_17/split/ReadVariableOp$^lstm_cell_17/split_1/ReadVariableOp^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:џџџџџџџџџџџџџџџџџџd: : : 2:
lstm_cell_17/ReadVariableOplstm_cell_17/ReadVariableOp2>
lstm_cell_17/ReadVariableOp_1lstm_cell_17/ReadVariableOp_12>
lstm_cell_17/ReadVariableOp_2lstm_cell_17/ReadVariableOp_22>
lstm_cell_17/ReadVariableOp_3lstm_cell_17/ReadVariableOp_32F
!lstm_cell_17/split/ReadVariableOp!lstm_cell_17/split/ReadVariableOp2J
#lstm_cell_17/split_1/ReadVariableOp#lstm_cell_17/split_1/ReadVariableOp2
whilewhile:^ Z
4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd
"
_user_specified_name
inputs/0
їв
Љ
(sequential_17_lstm_17_while_body_1215235H
Dsequential_17_lstm_17_while_sequential_17_lstm_17_while_loop_counterN
Jsequential_17_lstm_17_while_sequential_17_lstm_17_while_maximum_iterations+
'sequential_17_lstm_17_while_placeholder-
)sequential_17_lstm_17_while_placeholder_1-
)sequential_17_lstm_17_while_placeholder_2-
)sequential_17_lstm_17_while_placeholder_3G
Csequential_17_lstm_17_while_sequential_17_lstm_17_strided_slice_1_0
sequential_17_lstm_17_while_tensorarrayv2read_tensorlistgetitem_sequential_17_lstm_17_tensorarrayunstack_tensorlistfromtensor_0[
Hsequential_17_lstm_17_while_lstm_cell_17_split_readvariableop_resource_0:	dY
Jsequential_17_lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0:	U
Bsequential_17_lstm_17_while_lstm_cell_17_readvariableop_resource_0:	d(
$sequential_17_lstm_17_while_identity*
&sequential_17_lstm_17_while_identity_1*
&sequential_17_lstm_17_while_identity_2*
&sequential_17_lstm_17_while_identity_3*
&sequential_17_lstm_17_while_identity_4*
&sequential_17_lstm_17_while_identity_5E
Asequential_17_lstm_17_while_sequential_17_lstm_17_strided_slice_1
}sequential_17_lstm_17_while_tensorarrayv2read_tensorlistgetitem_sequential_17_lstm_17_tensorarrayunstack_tensorlistfromtensorY
Fsequential_17_lstm_17_while_lstm_cell_17_split_readvariableop_resource:	dW
Hsequential_17_lstm_17_while_lstm_cell_17_split_1_readvariableop_resource:	S
@sequential_17_lstm_17_while_lstm_cell_17_readvariableop_resource:	dЂ7sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOpЂ9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1Ђ9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2Ђ9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3Ђ=sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOpЂ?sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOpя
Msequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2O
Msequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shapeз
?sequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemsequential_17_lstm_17_while_tensorarrayv2read_tensorlistgetitem_sequential_17_lstm_17_tensorarrayunstack_tensorlistfromtensor_0'sequential_17_lstm_17_while_placeholderVsequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02A
?sequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItemъ
8sequential_17/lstm_17/while/lstm_cell_17/ones_like/ShapeShapeFsequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2:
8sequential_17/lstm_17/while/lstm_cell_17/ones_like/ShapeЙ
8sequential_17/lstm_17/while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2:
8sequential_17/lstm_17/while/lstm_cell_17/ones_like/ConstЈ
2sequential_17/lstm_17/while/lstm_cell_17/ones_likeFillAsequential_17/lstm_17/while/lstm_cell_17/ones_like/Shape:output:0Asequential_17/lstm_17/while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd24
2sequential_17/lstm_17/while/lstm_cell_17/ones_likeб
:sequential_17/lstm_17/while/lstm_cell_17/ones_like_1/ShapeShape)sequential_17_lstm_17_while_placeholder_2*
T0*
_output_shapes
:2<
:sequential_17/lstm_17/while/lstm_cell_17/ones_like_1/ShapeН
:sequential_17/lstm_17/while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2<
:sequential_17/lstm_17/while/lstm_cell_17/ones_like_1/ConstА
4sequential_17/lstm_17/while/lstm_cell_17/ones_like_1FillCsequential_17/lstm_17/while/lstm_cell_17/ones_like_1/Shape:output:0Csequential_17/lstm_17/while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd26
4sequential_17/lstm_17/while/lstm_cell_17/ones_like_1
,sequential_17/lstm_17/while/lstm_cell_17/mulMulFsequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0;sequential_17/lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/while/lstm_cell_17/mul
.sequential_17/lstm_17/while/lstm_cell_17/mul_1MulFsequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0;sequential_17/lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_1
.sequential_17/lstm_17/while/lstm_cell_17/mul_2MulFsequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0;sequential_17/lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_2
.sequential_17/lstm_17/while/lstm_cell_17/mul_3MulFsequential_17/lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0;sequential_17/lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_3Ж
8sequential_17/lstm_17/while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2:
8sequential_17/lstm_17/while/lstm_cell_17/split/split_dim
=sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOpReadVariableOpHsequential_17_lstm_17_while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype02?
=sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOpЫ
.sequential_17/lstm_17/while/lstm_cell_17/splitSplitAsequential_17/lstm_17/while/lstm_cell_17/split/split_dim:output:0Esequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split20
.sequential_17/lstm_17/while/lstm_cell_17/split
/sequential_17/lstm_17/while/lstm_cell_17/MatMulMatMul0sequential_17/lstm_17/while/lstm_cell_17/mul:z:07sequential_17/lstm_17/while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd21
/sequential_17/lstm_17/while/lstm_cell_17/MatMul
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_1MatMul2sequential_17/lstm_17/while/lstm_cell_17/mul_1:z:07sequential_17/lstm_17/while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd23
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_1
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_2MatMul2sequential_17/lstm_17/while/lstm_cell_17/mul_2:z:07sequential_17/lstm_17/while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd23
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_2
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_3MatMul2sequential_17/lstm_17/while/lstm_cell_17/mul_3:z:07sequential_17/lstm_17/while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd23
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_3К
:sequential_17/lstm_17/while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2<
:sequential_17/lstm_17/while/lstm_cell_17/split_1/split_dim
?sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOpReadVariableOpJsequential_17_lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype02A
?sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOpУ
0sequential_17/lstm_17/while/lstm_cell_17/split_1SplitCsequential_17/lstm_17/while/lstm_cell_17/split_1/split_dim:output:0Gsequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split22
0sequential_17/lstm_17/while/lstm_cell_17/split_1
0sequential_17/lstm_17/while/lstm_cell_17/BiasAddBiasAdd9sequential_17/lstm_17/while/lstm_cell_17/MatMul:product:09sequential_17/lstm_17/while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd22
0sequential_17/lstm_17/while/lstm_cell_17/BiasAdd
2sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_1BiasAdd;sequential_17/lstm_17/while/lstm_cell_17/MatMul_1:product:09sequential_17/lstm_17/while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd24
2sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_1
2sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_2BiasAdd;sequential_17/lstm_17/while/lstm_cell_17/MatMul_2:product:09sequential_17/lstm_17/while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd24
2sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_2
2sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_3BiasAdd;sequential_17/lstm_17/while/lstm_cell_17/MatMul_3:product:09sequential_17/lstm_17/while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd24
2sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_3
.sequential_17/lstm_17/while/lstm_cell_17/mul_4Mul)sequential_17_lstm_17_while_placeholder_2=sequential_17/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_4
.sequential_17/lstm_17/while/lstm_cell_17/mul_5Mul)sequential_17_lstm_17_while_placeholder_2=sequential_17/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_5
.sequential_17/lstm_17/while/lstm_cell_17/mul_6Mul)sequential_17_lstm_17_while_placeholder_2=sequential_17/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_6
.sequential_17/lstm_17/while/lstm_cell_17/mul_7Mul)sequential_17_lstm_17_while_placeholder_2=sequential_17/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_7і
7sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOpReadVariableOpBsequential_17_lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype029
7sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOpЭ
<sequential_17/lstm_17/while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2>
<sequential_17/lstm_17/while/lstm_cell_17/strided_slice/stackб
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2@
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice/stack_1б
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2@
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice/stack_2ђ
6sequential_17/lstm_17/while/lstm_cell_17/strided_sliceStridedSlice?sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp:value:0Esequential_17/lstm_17/while/lstm_cell_17/strided_slice/stack:output:0Gsequential_17/lstm_17/while/lstm_cell_17/strided_slice/stack_1:output:0Gsequential_17/lstm_17/while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask28
6sequential_17/lstm_17/while/lstm_cell_17/strided_slice
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_4MatMul2sequential_17/lstm_17/while/lstm_cell_17/mul_4:z:0?sequential_17/lstm_17/while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_4
,sequential_17/lstm_17/while/lstm_cell_17/addAddV29sequential_17/lstm_17/while/lstm_cell_17/BiasAdd:output:0;sequential_17/lstm_17/while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/while/lstm_cell_17/addг
0sequential_17/lstm_17/while/lstm_cell_17/SigmoidSigmoid0sequential_17/lstm_17/while/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd22
0sequential_17/lstm_17/while/lstm_cell_17/Sigmoidњ
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1ReadVariableOpBsequential_17_lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02;
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1б
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2@
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stackе
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2B
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stack_1е
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2B
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stack_2ў
8sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1StridedSliceAsequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1:value:0Gsequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stack:output:0Isequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stack_1:output:0Isequential_17/lstm_17/while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2:
8sequential_17/lstm_17/while/lstm_cell_17/strided_slice_1
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_5MatMul2sequential_17/lstm_17/while/lstm_cell_17/mul_5:z:0Asequential_17/lstm_17/while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_5
.sequential_17/lstm_17/while/lstm_cell_17/add_1AddV2;sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_1:output:0;sequential_17/lstm_17/while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/add_1й
2sequential_17/lstm_17/while/lstm_cell_17/Sigmoid_1Sigmoid2sequential_17/lstm_17/while/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd24
2sequential_17/lstm_17/while/lstm_cell_17/Sigmoid_1ќ
.sequential_17/lstm_17/while/lstm_cell_17/mul_8Mul6sequential_17/lstm_17/while/lstm_cell_17/Sigmoid_1:y:0)sequential_17_lstm_17_while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_8њ
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2ReadVariableOpBsequential_17_lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02;
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2б
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2@
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stackе
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2B
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stack_1е
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2B
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stack_2ў
8sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2StridedSliceAsequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2:value:0Gsequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stack:output:0Isequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stack_1:output:0Isequential_17/lstm_17/while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2:
8sequential_17/lstm_17/while/lstm_cell_17/strided_slice_2
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_6MatMul2sequential_17/lstm_17/while/lstm_cell_17/mul_6:z:0Asequential_17/lstm_17/while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_6
.sequential_17/lstm_17/while/lstm_cell_17/add_2AddV2;sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_2:output:0;sequential_17/lstm_17/while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/add_2Ь
-sequential_17/lstm_17/while/lstm_cell_17/TanhTanh2sequential_17/lstm_17/while/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2/
-sequential_17/lstm_17/while/lstm_cell_17/Tanh
.sequential_17/lstm_17/while/lstm_cell_17/mul_9Mul4sequential_17/lstm_17/while/lstm_cell_17/Sigmoid:y:01sequential_17/lstm_17/while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/mul_9
.sequential_17/lstm_17/while/lstm_cell_17/add_3AddV22sequential_17/lstm_17/while/lstm_cell_17/mul_8:z:02sequential_17/lstm_17/while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/add_3њ
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3ReadVariableOpBsequential_17_lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02;
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3б
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2@
>sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stackе
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2B
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stack_1е
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2B
@sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stack_2ў
8sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3StridedSliceAsequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3:value:0Gsequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stack:output:0Isequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stack_1:output:0Isequential_17/lstm_17/while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2:
8sequential_17/lstm_17/while/lstm_cell_17/strided_slice_3
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_7MatMul2sequential_17/lstm_17/while/lstm_cell_17/mul_7:z:0Asequential_17/lstm_17/while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1sequential_17/lstm_17/while/lstm_cell_17/MatMul_7
.sequential_17/lstm_17/while/lstm_cell_17/add_4AddV2;sequential_17/lstm_17/while/lstm_cell_17/BiasAdd_3:output:0;sequential_17/lstm_17/while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/while/lstm_cell_17/add_4й
2sequential_17/lstm_17/while/lstm_cell_17/Sigmoid_2Sigmoid2sequential_17/lstm_17/while/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd24
2sequential_17/lstm_17/while/lstm_cell_17/Sigmoid_2а
/sequential_17/lstm_17/while/lstm_cell_17/Tanh_1Tanh2sequential_17/lstm_17/while/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd21
/sequential_17/lstm_17/while/lstm_cell_17/Tanh_1
/sequential_17/lstm_17/while/lstm_cell_17/mul_10Mul6sequential_17/lstm_17/while/lstm_cell_17/Sigmoid_2:y:03sequential_17/lstm_17/while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd21
/sequential_17/lstm_17/while/lstm_cell_17/mul_10Я
@sequential_17/lstm_17/while/TensorArrayV2Write/TensorListSetItemTensorListSetItem)sequential_17_lstm_17_while_placeholder_1'sequential_17_lstm_17_while_placeholder3sequential_17/lstm_17/while/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype02B
@sequential_17/lstm_17/while/TensorArrayV2Write/TensorListSetItem
!sequential_17/lstm_17/while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2#
!sequential_17/lstm_17/while/add/yС
sequential_17/lstm_17/while/addAddV2'sequential_17_lstm_17_while_placeholder*sequential_17/lstm_17/while/add/y:output:0*
T0*
_output_shapes
: 2!
sequential_17/lstm_17/while/add
#sequential_17/lstm_17/while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2%
#sequential_17/lstm_17/while/add_1/yф
!sequential_17/lstm_17/while/add_1AddV2Dsequential_17_lstm_17_while_sequential_17_lstm_17_while_loop_counter,sequential_17/lstm_17/while/add_1/y:output:0*
T0*
_output_shapes
: 2#
!sequential_17/lstm_17/while/add_1
$sequential_17/lstm_17/while/IdentityIdentity%sequential_17/lstm_17/while/add_1:z:08^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3>^sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp@^sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2&
$sequential_17/lstm_17/while/IdentityЙ
&sequential_17/lstm_17/while/Identity_1IdentityJsequential_17_lstm_17_while_sequential_17_lstm_17_while_maximum_iterations8^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3>^sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp@^sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2(
&sequential_17/lstm_17/while/Identity_1
&sequential_17/lstm_17/while/Identity_2Identity#sequential_17/lstm_17/while/add:z:08^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3>^sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp@^sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2(
&sequential_17/lstm_17/while/Identity_2П
&sequential_17/lstm_17/while/Identity_3IdentityPsequential_17/lstm_17/while/TensorArrayV2Write/TensorListSetItem:output_handle:08^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3>^sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp@^sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2(
&sequential_17/lstm_17/while/Identity_3Г
&sequential_17/lstm_17/while/Identity_4Identity3sequential_17/lstm_17/while/lstm_cell_17/mul_10:z:08^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3>^sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp@^sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2(
&sequential_17/lstm_17/while/Identity_4В
&sequential_17/lstm_17/while/Identity_5Identity2sequential_17/lstm_17/while/lstm_cell_17/add_3:z:08^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_1:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_2:^sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_3>^sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp@^sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2(
&sequential_17/lstm_17/while/Identity_5"U
$sequential_17_lstm_17_while_identity-sequential_17/lstm_17/while/Identity:output:0"Y
&sequential_17_lstm_17_while_identity_1/sequential_17/lstm_17/while/Identity_1:output:0"Y
&sequential_17_lstm_17_while_identity_2/sequential_17/lstm_17/while/Identity_2:output:0"Y
&sequential_17_lstm_17_while_identity_3/sequential_17/lstm_17/while/Identity_3:output:0"Y
&sequential_17_lstm_17_while_identity_4/sequential_17/lstm_17/while/Identity_4:output:0"Y
&sequential_17_lstm_17_while_identity_5/sequential_17/lstm_17/while/Identity_5:output:0"
@sequential_17_lstm_17_while_lstm_cell_17_readvariableop_resourceBsequential_17_lstm_17_while_lstm_cell_17_readvariableop_resource_0"
Hsequential_17_lstm_17_while_lstm_cell_17_split_1_readvariableop_resourceJsequential_17_lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0"
Fsequential_17_lstm_17_while_lstm_cell_17_split_readvariableop_resourceHsequential_17_lstm_17_while_lstm_cell_17_split_readvariableop_resource_0"
Asequential_17_lstm_17_while_sequential_17_lstm_17_strided_slice_1Csequential_17_lstm_17_while_sequential_17_lstm_17_strided_slice_1_0"
}sequential_17_lstm_17_while_tensorarrayv2read_tensorlistgetitem_sequential_17_lstm_17_tensorarrayunstack_tensorlistfromtensorsequential_17_lstm_17_while_tensorarrayv2read_tensorlistgetitem_sequential_17_lstm_17_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2r
7sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp7sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp2v
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_19sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_12v
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_29sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_22v
9sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_39sequential_17/lstm_17/while/lstm_cell_17/ReadVariableOp_32~
=sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp=sequential_17/lstm_17/while/lstm_cell_17/split/ReadVariableOp2
?sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp?sequential_17/lstm_17/while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
к
Ш
while_cond_1218426
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1218426___redundant_placeholder05
1while_while_cond_1218426___redundant_placeholder15
1while_while_cond_1218426___redundant_placeholder25
1while_while_cond_1218426___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
к
Ш
while_cond_1216388
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1216388___redundant_placeholder05
1while_while_cond_1216388___redundant_placeholder15
1while_while_cond_1216388___redundant_placeholder25
1while_while_cond_1216388___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:

p
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217951

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ь
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout/Constx
dropout/MulMulinputsdropout/Const:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
dropout/Mul
dropout/random_uniform/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2 
dropout/random_uniform/shape/1Э
dropout/random_uniform/shapePackstrided_slice:output:0'dropout/random_uniform/shape/1:output:0strided_slice_1:output:0*
N*
T0*
_output_shapes
:2
dropout/random_uniform/shapeа
$dropout/random_uniform/RandomUniformRandomUniform%dropout/random_uniform/shape:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout/GreaterEqual/yЫ
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/Cast
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*,
_output_shapes
:џџџџџџџџџd2
dropout/Mul_1j
IdentityIdentitydropout/Mul_1:z:0*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:џџџџџџџџџd:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs

Ѕ	
while_body_1216389
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0E
2while_lstm_cell_17_split_readvariableop_resource_0:	dC
4while_lstm_cell_17_split_1_readvariableop_resource_0:	?
,while_lstm_cell_17_readvariableop_resource_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorC
0while_lstm_cell_17_split_readvariableop_resource:	dA
2while_lstm_cell_17_split_1_readvariableop_resource:	=
*while_lstm_cell_17_readvariableop_resource:	dЂ!while/lstm_cell_17/ReadVariableOpЂ#while/lstm_cell_17/ReadVariableOp_1Ђ#while/lstm_cell_17/ReadVariableOp_2Ђ#while/lstm_cell_17/ReadVariableOp_3Ђ'while/lstm_cell_17/split/ReadVariableOpЂ)while/lstm_cell_17/split_1/ReadVariableOpУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemЈ
"while/lstm_cell_17/ones_like/ShapeShape0while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/ones_like/Shape
"while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2$
"while/lstm_cell_17/ones_like/Constа
while/lstm_cell_17/ones_likeFill+while/lstm_cell_17/ones_like/Shape:output:0+while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/ones_like
$while/lstm_cell_17/ones_like_1/ShapeShapewhile_placeholder_2*
T0*
_output_shapes
:2&
$while/lstm_cell_17/ones_like_1/Shape
$while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$while/lstm_cell_17/ones_like_1/Constи
while/lstm_cell_17/ones_like_1Fill-while/lstm_cell_17/ones_like_1/Shape:output:0-while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/ones_like_1Т
while/lstm_cell_17/mulMul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mulЦ
while/lstm_cell_17/mul_1Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_1Ц
while/lstm_cell_17/mul_2Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_2Ц
while/lstm_cell_17/mul_3Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_3
"while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2$
"while/lstm_cell_17/split/split_dimЦ
'while/lstm_cell_17/split/ReadVariableOpReadVariableOp2while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype02)
'while/lstm_cell_17/split/ReadVariableOpѓ
while/lstm_cell_17/splitSplit+while/lstm_cell_17/split/split_dim:output:0/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
while/lstm_cell_17/splitБ
while/lstm_cell_17/MatMulMatMulwhile/lstm_cell_17/mul:z:0!while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMulЗ
while/lstm_cell_17/MatMul_1MatMulwhile/lstm_cell_17/mul_1:z:0!while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_1З
while/lstm_cell_17/MatMul_2MatMulwhile/lstm_cell_17/mul_2:z:0!while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_2З
while/lstm_cell_17/MatMul_3MatMulwhile/lstm_cell_17/mul_3:z:0!while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_3
$while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2&
$while/lstm_cell_17/split_1/split_dimШ
)while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype02+
)while/lstm_cell_17/split_1/ReadVariableOpы
while/lstm_cell_17/split_1Split-while/lstm_cell_17/split_1/split_dim:output:01while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
while/lstm_cell_17/split_1П
while/lstm_cell_17/BiasAddBiasAdd#while/lstm_cell_17/MatMul:product:0#while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAddХ
while/lstm_cell_17/BiasAdd_1BiasAdd%while/lstm_cell_17/MatMul_1:product:0#while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_1Х
while/lstm_cell_17/BiasAdd_2BiasAdd%while/lstm_cell_17/MatMul_2:product:0#while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_2Х
while/lstm_cell_17/BiasAdd_3BiasAdd%while/lstm_cell_17/MatMul_3:product:0#while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_3Ћ
while/lstm_cell_17/mul_4Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_4Ћ
while/lstm_cell_17/mul_5Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_5Ћ
while/lstm_cell_17/mul_6Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_6Ћ
while/lstm_cell_17/mul_7Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_7Д
!while/lstm_cell_17/ReadVariableOpReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02#
!while/lstm_cell_17/ReadVariableOpЁ
&while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2(
&while/lstm_cell_17/strided_slice/stackЅ
(while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice/stack_1Ѕ
(while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2*
(while/lstm_cell_17/strided_slice/stack_2ю
 while/lstm_cell_17/strided_sliceStridedSlice)while/lstm_cell_17/ReadVariableOp:value:0/while/lstm_cell_17/strided_slice/stack:output:01while/lstm_cell_17/strided_slice/stack_1:output:01while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2"
 while/lstm_cell_17/strided_sliceП
while/lstm_cell_17/MatMul_4MatMulwhile/lstm_cell_17/mul_4:z:0)while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_4З
while/lstm_cell_17/addAddV2#while/lstm_cell_17/BiasAdd:output:0%while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add
while/lstm_cell_17/SigmoidSigmoidwhile/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/SigmoidИ
#while/lstm_cell_17/ReadVariableOp_1ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_1Ѕ
(while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice_1/stackЉ
*while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*while/lstm_cell_17/strided_slice_1/stack_1Љ
*while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_1/stack_2њ
"while/lstm_cell_17/strided_slice_1StridedSlice+while/lstm_cell_17/ReadVariableOp_1:value:01while/lstm_cell_17/strided_slice_1/stack:output:03while/lstm_cell_17/strided_slice_1/stack_1:output:03while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_1С
while/lstm_cell_17/MatMul_5MatMulwhile/lstm_cell_17/mul_5:z:0+while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_5Н
while/lstm_cell_17/add_1AddV2%while/lstm_cell_17/BiasAdd_1:output:0%while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_1
while/lstm_cell_17/Sigmoid_1Sigmoidwhile/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_1Є
while/lstm_cell_17/mul_8Mul while/lstm_cell_17/Sigmoid_1:y:0while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_8И
#while/lstm_cell_17/ReadVariableOp_2ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_2Ѕ
(while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2*
(while/lstm_cell_17/strided_slice_2/stackЉ
*while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*while/lstm_cell_17/strided_slice_2/stack_1Љ
*while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_2/stack_2њ
"while/lstm_cell_17/strided_slice_2StridedSlice+while/lstm_cell_17/ReadVariableOp_2:value:01while/lstm_cell_17/strided_slice_2/stack:output:03while/lstm_cell_17/strided_slice_2/stack_1:output:03while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_2С
while/lstm_cell_17/MatMul_6MatMulwhile/lstm_cell_17/mul_6:z:0+while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_6Н
while/lstm_cell_17/add_2AddV2%while/lstm_cell_17/BiasAdd_2:output:0%while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_2
while/lstm_cell_17/TanhTanhwhile/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/TanhЊ
while/lstm_cell_17/mul_9Mulwhile/lstm_cell_17/Sigmoid:y:0while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_9Ћ
while/lstm_cell_17/add_3AddV2while/lstm_cell_17/mul_8:z:0while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_3И
#while/lstm_cell_17/ReadVariableOp_3ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_3Ѕ
(while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2*
(while/lstm_cell_17/strided_slice_3/stackЉ
*while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2,
*while/lstm_cell_17/strided_slice_3/stack_1Љ
*while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_3/stack_2њ
"while/lstm_cell_17/strided_slice_3StridedSlice+while/lstm_cell_17/ReadVariableOp_3:value:01while/lstm_cell_17/strided_slice_3/stack:output:03while/lstm_cell_17/strided_slice_3/stack_1:output:03while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_3С
while/lstm_cell_17/MatMul_7MatMulwhile/lstm_cell_17/mul_7:z:0+while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_7Н
while/lstm_cell_17/add_4AddV2%while/lstm_cell_17/BiasAdd_3:output:0%while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_4
while/lstm_cell_17/Sigmoid_2Sigmoidwhile/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_2
while/lstm_cell_17/Tanh_1Tanhwhile/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Tanh_1А
while/lstm_cell_17/mul_10Mul while/lstm_cell_17/Sigmoid_2:y:0while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_10с
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1Ъ
while/IdentityIdentitywhile/add_1:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identityн
while/Identity_1Identitywhile_while_maximum_iterations"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_1Ь
while/Identity_2Identitywhile/add:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_2љ
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_3э
while/Identity_4Identitywhile/lstm_cell_17/mul_10:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4ь
while/Identity_5Identitywhile/lstm_cell_17/add_3:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"Z
*while_lstm_cell_17_readvariableop_resource,while_lstm_cell_17_readvariableop_resource_0"j
2while_lstm_cell_17_split_1_readvariableop_resource4while_lstm_cell_17_split_1_readvariableop_resource_0"f
0while_lstm_cell_17_split_readvariableop_resource2while_lstm_cell_17_split_readvariableop_resource_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2F
!while/lstm_cell_17/ReadVariableOp!while/lstm_cell_17/ReadVariableOp2J
#while/lstm_cell_17/ReadVariableOp_1#while/lstm_cell_17/ReadVariableOp_12J
#while/lstm_cell_17/ReadVariableOp_2#while/lstm_cell_17/ReadVariableOp_22J
#while/lstm_cell_17/ReadVariableOp_3#while/lstm_cell_17/ReadVariableOp_32R
'while/lstm_cell_17/split/ReadVariableOp'while/lstm_cell_17/split/ReadVariableOp2V
)while/lstm_cell_17/split_1/ReadVariableOp)while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
Ы

ш
lstm_17_while_cond_1217308,
(lstm_17_while_lstm_17_while_loop_counter2
.lstm_17_while_lstm_17_while_maximum_iterations
lstm_17_while_placeholder
lstm_17_while_placeholder_1
lstm_17_while_placeholder_2
lstm_17_while_placeholder_3.
*lstm_17_while_less_lstm_17_strided_slice_1E
Alstm_17_while_lstm_17_while_cond_1217308___redundant_placeholder0E
Alstm_17_while_lstm_17_while_cond_1217308___redundant_placeholder1E
Alstm_17_while_lstm_17_while_cond_1217308___redundant_placeholder2E
Alstm_17_while_lstm_17_while_cond_1217308___redundant_placeholder3
lstm_17_while_identity

lstm_17/while/LessLesslstm_17_while_placeholder*lstm_17_while_less_lstm_17_strided_slice_1*
T0*
_output_shapes
: 2
lstm_17/while/Lessu
lstm_17/while/IdentityIdentitylstm_17/while/Less:z:0*
T0
*
_output_shapes
: 2
lstm_17/while/Identity"9
lstm_17_while_identitylstm_17/while/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:

p
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217004

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ь
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout/Constx
dropout/MulMulinputsdropout/Const:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
dropout/Mul
dropout/random_uniform/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2 
dropout/random_uniform/shape/1Э
dropout/random_uniform/shapePackstrided_slice:output:0'dropout/random_uniform/shape/1:output:0strided_slice_1:output:0*
N*
T0*
_output_shapes
:2
dropout/random_uniform/shapeа
$dropout/random_uniform/RandomUniformRandomUniform%dropout/random_uniform/shape:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout/GreaterEqual/yЫ
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/Cast
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*,
_output_shapes
:џџџџџџџџџd2
dropout/Mul_1j
IdentityIdentitydropout/Mul_1:z:0*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:џџџџџџџџџd:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs

Ѕ	
while_body_1218742
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0E
2while_lstm_cell_17_split_readvariableop_resource_0:	dC
4while_lstm_cell_17_split_1_readvariableop_resource_0:	?
,while_lstm_cell_17_readvariableop_resource_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorC
0while_lstm_cell_17_split_readvariableop_resource:	dA
2while_lstm_cell_17_split_1_readvariableop_resource:	=
*while_lstm_cell_17_readvariableop_resource:	dЂ!while/lstm_cell_17/ReadVariableOpЂ#while/lstm_cell_17/ReadVariableOp_1Ђ#while/lstm_cell_17/ReadVariableOp_2Ђ#while/lstm_cell_17/ReadVariableOp_3Ђ'while/lstm_cell_17/split/ReadVariableOpЂ)while/lstm_cell_17/split_1/ReadVariableOpУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemЈ
"while/lstm_cell_17/ones_like/ShapeShape0while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/ones_like/Shape
"while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2$
"while/lstm_cell_17/ones_like/Constа
while/lstm_cell_17/ones_likeFill+while/lstm_cell_17/ones_like/Shape:output:0+while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/ones_like
$while/lstm_cell_17/ones_like_1/ShapeShapewhile_placeholder_2*
T0*
_output_shapes
:2&
$while/lstm_cell_17/ones_like_1/Shape
$while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$while/lstm_cell_17/ones_like_1/Constи
while/lstm_cell_17/ones_like_1Fill-while/lstm_cell_17/ones_like_1/Shape:output:0-while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/ones_like_1Т
while/lstm_cell_17/mulMul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mulЦ
while/lstm_cell_17/mul_1Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_1Ц
while/lstm_cell_17/mul_2Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_2Ц
while/lstm_cell_17/mul_3Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_3
"while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2$
"while/lstm_cell_17/split/split_dimЦ
'while/lstm_cell_17/split/ReadVariableOpReadVariableOp2while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype02)
'while/lstm_cell_17/split/ReadVariableOpѓ
while/lstm_cell_17/splitSplit+while/lstm_cell_17/split/split_dim:output:0/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
while/lstm_cell_17/splitБ
while/lstm_cell_17/MatMulMatMulwhile/lstm_cell_17/mul:z:0!while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMulЗ
while/lstm_cell_17/MatMul_1MatMulwhile/lstm_cell_17/mul_1:z:0!while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_1З
while/lstm_cell_17/MatMul_2MatMulwhile/lstm_cell_17/mul_2:z:0!while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_2З
while/lstm_cell_17/MatMul_3MatMulwhile/lstm_cell_17/mul_3:z:0!while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_3
$while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2&
$while/lstm_cell_17/split_1/split_dimШ
)while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype02+
)while/lstm_cell_17/split_1/ReadVariableOpы
while/lstm_cell_17/split_1Split-while/lstm_cell_17/split_1/split_dim:output:01while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
while/lstm_cell_17/split_1П
while/lstm_cell_17/BiasAddBiasAdd#while/lstm_cell_17/MatMul:product:0#while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAddХ
while/lstm_cell_17/BiasAdd_1BiasAdd%while/lstm_cell_17/MatMul_1:product:0#while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_1Х
while/lstm_cell_17/BiasAdd_2BiasAdd%while/lstm_cell_17/MatMul_2:product:0#while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_2Х
while/lstm_cell_17/BiasAdd_3BiasAdd%while/lstm_cell_17/MatMul_3:product:0#while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_3Ћ
while/lstm_cell_17/mul_4Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_4Ћ
while/lstm_cell_17/mul_5Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_5Ћ
while/lstm_cell_17/mul_6Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_6Ћ
while/lstm_cell_17/mul_7Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_7Д
!while/lstm_cell_17/ReadVariableOpReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02#
!while/lstm_cell_17/ReadVariableOpЁ
&while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2(
&while/lstm_cell_17/strided_slice/stackЅ
(while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice/stack_1Ѕ
(while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2*
(while/lstm_cell_17/strided_slice/stack_2ю
 while/lstm_cell_17/strided_sliceStridedSlice)while/lstm_cell_17/ReadVariableOp:value:0/while/lstm_cell_17/strided_slice/stack:output:01while/lstm_cell_17/strided_slice/stack_1:output:01while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2"
 while/lstm_cell_17/strided_sliceП
while/lstm_cell_17/MatMul_4MatMulwhile/lstm_cell_17/mul_4:z:0)while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_4З
while/lstm_cell_17/addAddV2#while/lstm_cell_17/BiasAdd:output:0%while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add
while/lstm_cell_17/SigmoidSigmoidwhile/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/SigmoidИ
#while/lstm_cell_17/ReadVariableOp_1ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_1Ѕ
(while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice_1/stackЉ
*while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*while/lstm_cell_17/strided_slice_1/stack_1Љ
*while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_1/stack_2њ
"while/lstm_cell_17/strided_slice_1StridedSlice+while/lstm_cell_17/ReadVariableOp_1:value:01while/lstm_cell_17/strided_slice_1/stack:output:03while/lstm_cell_17/strided_slice_1/stack_1:output:03while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_1С
while/lstm_cell_17/MatMul_5MatMulwhile/lstm_cell_17/mul_5:z:0+while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_5Н
while/lstm_cell_17/add_1AddV2%while/lstm_cell_17/BiasAdd_1:output:0%while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_1
while/lstm_cell_17/Sigmoid_1Sigmoidwhile/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_1Є
while/lstm_cell_17/mul_8Mul while/lstm_cell_17/Sigmoid_1:y:0while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_8И
#while/lstm_cell_17/ReadVariableOp_2ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_2Ѕ
(while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2*
(while/lstm_cell_17/strided_slice_2/stackЉ
*while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*while/lstm_cell_17/strided_slice_2/stack_1Љ
*while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_2/stack_2њ
"while/lstm_cell_17/strided_slice_2StridedSlice+while/lstm_cell_17/ReadVariableOp_2:value:01while/lstm_cell_17/strided_slice_2/stack:output:03while/lstm_cell_17/strided_slice_2/stack_1:output:03while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_2С
while/lstm_cell_17/MatMul_6MatMulwhile/lstm_cell_17/mul_6:z:0+while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_6Н
while/lstm_cell_17/add_2AddV2%while/lstm_cell_17/BiasAdd_2:output:0%while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_2
while/lstm_cell_17/TanhTanhwhile/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/TanhЊ
while/lstm_cell_17/mul_9Mulwhile/lstm_cell_17/Sigmoid:y:0while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_9Ћ
while/lstm_cell_17/add_3AddV2while/lstm_cell_17/mul_8:z:0while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_3И
#while/lstm_cell_17/ReadVariableOp_3ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_3Ѕ
(while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2*
(while/lstm_cell_17/strided_slice_3/stackЉ
*while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2,
*while/lstm_cell_17/strided_slice_3/stack_1Љ
*while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_3/stack_2њ
"while/lstm_cell_17/strided_slice_3StridedSlice+while/lstm_cell_17/ReadVariableOp_3:value:01while/lstm_cell_17/strided_slice_3/stack:output:03while/lstm_cell_17/strided_slice_3/stack_1:output:03while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_3С
while/lstm_cell_17/MatMul_7MatMulwhile/lstm_cell_17/mul_7:z:0+while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_7Н
while/lstm_cell_17/add_4AddV2%while/lstm_cell_17/BiasAdd_3:output:0%while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_4
while/lstm_cell_17/Sigmoid_2Sigmoidwhile/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_2
while/lstm_cell_17/Tanh_1Tanhwhile/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Tanh_1А
while/lstm_cell_17/mul_10Mul while/lstm_cell_17/Sigmoid_2:y:0while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_10с
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1Ъ
while/IdentityIdentitywhile/add_1:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identityн
while/Identity_1Identitywhile_while_maximum_iterations"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_1Ь
while/Identity_2Identitywhile/add:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_2љ
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_3э
while/Identity_4Identitywhile/lstm_cell_17/mul_10:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4ь
while/Identity_5Identitywhile/lstm_cell_17/add_3:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"Z
*while_lstm_cell_17_readvariableop_resource,while_lstm_cell_17_readvariableop_resource_0"j
2while_lstm_cell_17_split_1_readvariableop_resource4while_lstm_cell_17_split_1_readvariableop_resource_0"f
0while_lstm_cell_17_split_readvariableop_resource2while_lstm_cell_17_split_readvariableop_resource_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2F
!while/lstm_cell_17/ReadVariableOp!while/lstm_cell_17/ReadVariableOp2J
#while/lstm_cell_17/ReadVariableOp_1#while/lstm_cell_17/ReadVariableOp_12J
#while/lstm_cell_17/ReadVariableOp_2#while/lstm_cell_17/ReadVariableOp_22J
#while/lstm_cell_17/ReadVariableOp_3#while/lstm_cell_17/ReadVariableOp_32R
'while/lstm_cell_17/split/ReadVariableOp'while/lstm_cell_17/split/ReadVariableOp2V
)while/lstm_cell_17/split_1/ReadVariableOp)while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
ъ
И
)__inference_lstm_17_layer_call_fn_1217973
inputs_0
unknown:	d
	unknown_0:	
	unknown_1:	d
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputs_0unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12159842
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:џџџџџџџџџџџџџџџџџџd: : : 22
StatefulPartitionedCallStatefulPartitionedCall:^ Z
4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd
"
_user_specified_name
inputs/0
к
Ш
while_cond_1216767
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1216767___redundant_placeholder05
1while_while_cond_1216767___redundant_placeholder15
1while_while_cond_1216767___redundant_placeholder25
1while_while_cond_1216767___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
Ы

ш
lstm_17_while_cond_1217654,
(lstm_17_while_lstm_17_while_loop_counter2
.lstm_17_while_lstm_17_while_maximum_iterations
lstm_17_while_placeholder
lstm_17_while_placeholder_1
lstm_17_while_placeholder_2
lstm_17_while_placeholder_3.
*lstm_17_while_less_lstm_17_strided_slice_1E
Alstm_17_while_lstm_17_while_cond_1217654___redundant_placeholder0E
Alstm_17_while_lstm_17_while_cond_1217654___redundant_placeholder1E
Alstm_17_while_lstm_17_while_cond_1217654___redundant_placeholder2E
Alstm_17_while_lstm_17_while_cond_1217654___redundant_placeholder3
lstm_17_while_identity

lstm_17/while/LessLesslstm_17_while_placeholder*lstm_17_while_less_lstm_17_strided_slice_1*
T0*
_output_shapes
: 2
lstm_17/while/Lessu
lstm_17/while/IdentityIdentitylstm_17/while/Less:z:0*
T0
*
_output_shapes
: 2
lstm_17/while/Identity"9
lstm_17_while_identitylstm_17/while/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
ў

/__inference_sequential_17_layer_call_fn_1217086
embedding_17_input
unknown:
ЈУd
	unknown_0:	d
	unknown_1:	
	unknown_2:	d
	unknown_3:d
	unknown_4:
identityЂStatefulPartitionedCallК
StatefulPartitionedCallStatefulPartitionedCallembedding_17_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4*
Tin
	2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*(
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *S
fNRL
J__inference_sequential_17_layer_call_and_return_conditional_losses_12170542
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:\ X
(
_output_shapes
:џџџџџџџџџ
,
_user_specified_nameembedding_17_input

Ћ
J__inference_sequential_17_layer_call_and_return_conditional_losses_1216549

inputs(
embedding_17_1216264:
ЈУd"
lstm_17_1216524:	d
lstm_17_1216526:	"
lstm_17_1216528:	d"
dense_17_1216543:d
dense_17_1216545:
identityЂ dense_17/StatefulPartitionedCallЂ$embedding_17/StatefulPartitionedCallЂlstm_17/StatefulPartitionedCall
$embedding_17/StatefulPartitionedCallStatefulPartitionedCallinputsembedding_17_1216264*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_embedding_17_layer_call_and_return_conditional_losses_12162632&
$embedding_17/StatefulPartitionedCallЅ
$spatial_dropout1d_17/PartitionedCallPartitionedCall-embedding_17/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12162712&
$spatial_dropout1d_17/PartitionedCallЬ
lstm_17/StatefulPartitionedCallStatefulPartitionedCall-spatial_dropout1d_17/PartitionedCall:output:0lstm_17_1216524lstm_17_1216526lstm_17_1216528*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12165232!
lstm_17/StatefulPartitionedCallЙ
 dense_17/StatefulPartitionedCallStatefulPartitionedCall(lstm_17/StatefulPartitionedCall:output:0dense_17_1216543dense_17_1216545*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12165422"
 dense_17/StatefulPartitionedCallщ
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_17/StatefulPartitionedCall%^embedding_17/StatefulPartitionedCall ^lstm_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2L
$embedding_17/StatefulPartitionedCall$embedding_17/StatefulPartitionedCall2B
lstm_17/StatefulPartitionedCalllstm_17/StatefulPartitionedCall:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs
Бў

"__inference__wrapped_model_1215376
embedding_17_inputG
3sequential_17_embedding_17_embedding_lookup_1215112:
ЈУdS
@sequential_17_lstm_17_lstm_cell_17_split_readvariableop_resource:	dQ
Bsequential_17_lstm_17_lstm_cell_17_split_1_readvariableop_resource:	M
:sequential_17_lstm_17_lstm_cell_17_readvariableop_resource:	dG
5sequential_17_dense_17_matmul_readvariableop_resource:dD
6sequential_17_dense_17_biasadd_readvariableop_resource:
identityЂ-sequential_17/dense_17/BiasAdd/ReadVariableOpЂ,sequential_17/dense_17/MatMul/ReadVariableOpЂ+sequential_17/embedding_17/embedding_lookupЂ1sequential_17/lstm_17/lstm_cell_17/ReadVariableOpЂ3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_1Ђ3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_2Ђ3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_3Ђ7sequential_17/lstm_17/lstm_cell_17/split/ReadVariableOpЂ9sequential_17/lstm_17/lstm_cell_17/split_1/ReadVariableOpЂsequential_17/lstm_17/while 
sequential_17/embedding_17/CastCastembedding_17_input*

DstT0*

SrcT0*(
_output_shapes
:џџџџџџџџџ2!
sequential_17/embedding_17/Cast
+sequential_17/embedding_17/embedding_lookupResourceGather3sequential_17_embedding_17_embedding_lookup_1215112#sequential_17/embedding_17/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*F
_class<
:8loc:@sequential_17/embedding_17/embedding_lookup/1215112*,
_output_shapes
:џџџџџџџџџd*
dtype02-
+sequential_17/embedding_17/embedding_lookupл
4sequential_17/embedding_17/embedding_lookup/IdentityIdentity4sequential_17/embedding_17/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*F
_class<
:8loc:@sequential_17/embedding_17/embedding_lookup/1215112*,
_output_shapes
:џџџџџџџџџd26
4sequential_17/embedding_17/embedding_lookup/Identityђ
6sequential_17/embedding_17/embedding_lookup/Identity_1Identity=sequential_17/embedding_17/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:џџџџџџџџџd28
6sequential_17/embedding_17/embedding_lookup/Identity_1о
+sequential_17/spatial_dropout1d_17/IdentityIdentity?sequential_17/embedding_17/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2-
+sequential_17/spatial_dropout1d_17/Identity
sequential_17/lstm_17/ShapeShape4sequential_17/spatial_dropout1d_17/Identity:output:0*
T0*
_output_shapes
:2
sequential_17/lstm_17/Shape 
)sequential_17/lstm_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)sequential_17/lstm_17/strided_slice/stackЄ
+sequential_17/lstm_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential_17/lstm_17/strided_slice/stack_1Є
+sequential_17/lstm_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential_17/lstm_17/strided_slice/stack_2ц
#sequential_17/lstm_17/strided_sliceStridedSlice$sequential_17/lstm_17/Shape:output:02sequential_17/lstm_17/strided_slice/stack:output:04sequential_17/lstm_17/strided_slice/stack_1:output:04sequential_17/lstm_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential_17/lstm_17/strided_slice
!sequential_17/lstm_17/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2#
!sequential_17/lstm_17/zeros/mul/yФ
sequential_17/lstm_17/zeros/mulMul,sequential_17/lstm_17/strided_slice:output:0*sequential_17/lstm_17/zeros/mul/y:output:0*
T0*
_output_shapes
: 2!
sequential_17/lstm_17/zeros/mul
"sequential_17/lstm_17/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2$
"sequential_17/lstm_17/zeros/Less/yП
 sequential_17/lstm_17/zeros/LessLess#sequential_17/lstm_17/zeros/mul:z:0+sequential_17/lstm_17/zeros/Less/y:output:0*
T0*
_output_shapes
: 2"
 sequential_17/lstm_17/zeros/Less
$sequential_17/lstm_17/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2&
$sequential_17/lstm_17/zeros/packed/1л
"sequential_17/lstm_17/zeros/packedPack,sequential_17/lstm_17/strided_slice:output:0-sequential_17/lstm_17/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2$
"sequential_17/lstm_17/zeros/packed
!sequential_17/lstm_17/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2#
!sequential_17/lstm_17/zeros/ConstЭ
sequential_17/lstm_17/zerosFill+sequential_17/lstm_17/zeros/packed:output:0*sequential_17/lstm_17/zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
sequential_17/lstm_17/zeros
#sequential_17/lstm_17/zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2%
#sequential_17/lstm_17/zeros_1/mul/yЪ
!sequential_17/lstm_17/zeros_1/mulMul,sequential_17/lstm_17/strided_slice:output:0,sequential_17/lstm_17/zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2#
!sequential_17/lstm_17/zeros_1/mul
$sequential_17/lstm_17/zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2&
$sequential_17/lstm_17/zeros_1/Less/yЧ
"sequential_17/lstm_17/zeros_1/LessLess%sequential_17/lstm_17/zeros_1/mul:z:0-sequential_17/lstm_17/zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2$
"sequential_17/lstm_17/zeros_1/Less
&sequential_17/lstm_17/zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2(
&sequential_17/lstm_17/zeros_1/packed/1с
$sequential_17/lstm_17/zeros_1/packedPack,sequential_17/lstm_17/strided_slice:output:0/sequential_17/lstm_17/zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2&
$sequential_17/lstm_17/zeros_1/packed
#sequential_17/lstm_17/zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2%
#sequential_17/lstm_17/zeros_1/Constе
sequential_17/lstm_17/zeros_1Fill-sequential_17/lstm_17/zeros_1/packed:output:0,sequential_17/lstm_17/zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
sequential_17/lstm_17/zeros_1Ё
$sequential_17/lstm_17/transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2&
$sequential_17/lstm_17/transpose/permы
sequential_17/lstm_17/transpose	Transpose4sequential_17/spatial_dropout1d_17/Identity:output:0-sequential_17/lstm_17/transpose/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2!
sequential_17/lstm_17/transpose
sequential_17/lstm_17/Shape_1Shape#sequential_17/lstm_17/transpose:y:0*
T0*
_output_shapes
:2
sequential_17/lstm_17/Shape_1Є
+sequential_17/lstm_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2-
+sequential_17/lstm_17/strided_slice_1/stackЈ
-sequential_17/lstm_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_17/lstm_17/strided_slice_1/stack_1Ј
-sequential_17/lstm_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_17/lstm_17/strided_slice_1/stack_2ђ
%sequential_17/lstm_17/strided_slice_1StridedSlice&sequential_17/lstm_17/Shape_1:output:04sequential_17/lstm_17/strided_slice_1/stack:output:06sequential_17/lstm_17/strided_slice_1/stack_1:output:06sequential_17/lstm_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2'
%sequential_17/lstm_17/strided_slice_1Б
1sequential_17/lstm_17/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ23
1sequential_17/lstm_17/TensorArrayV2/element_shape
#sequential_17/lstm_17/TensorArrayV2TensorListReserve:sequential_17/lstm_17/TensorArrayV2/element_shape:output:0.sequential_17/lstm_17/strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02%
#sequential_17/lstm_17/TensorArrayV2ы
Ksequential_17/lstm_17/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2M
Ksequential_17/lstm_17/TensorArrayUnstack/TensorListFromTensor/element_shapeа
=sequential_17/lstm_17/TensorArrayUnstack/TensorListFromTensorTensorListFromTensor#sequential_17/lstm_17/transpose:y:0Tsequential_17/lstm_17/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02?
=sequential_17/lstm_17/TensorArrayUnstack/TensorListFromTensorЄ
+sequential_17/lstm_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2-
+sequential_17/lstm_17/strided_slice_2/stackЈ
-sequential_17/lstm_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_17/lstm_17/strided_slice_2/stack_1Ј
-sequential_17/lstm_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_17/lstm_17/strided_slice_2/stack_2
%sequential_17/lstm_17/strided_slice_2StridedSlice#sequential_17/lstm_17/transpose:y:04sequential_17/lstm_17/strided_slice_2/stack:output:06sequential_17/lstm_17/strided_slice_2/stack_1:output:06sequential_17/lstm_17/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2'
%sequential_17/lstm_17/strided_slice_2Ц
2sequential_17/lstm_17/lstm_cell_17/ones_like/ShapeShape.sequential_17/lstm_17/strided_slice_2:output:0*
T0*
_output_shapes
:24
2sequential_17/lstm_17/lstm_cell_17/ones_like/Shape­
2sequential_17/lstm_17/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?24
2sequential_17/lstm_17/lstm_cell_17/ones_like/Const
,sequential_17/lstm_17/lstm_cell_17/ones_likeFill;sequential_17/lstm_17/lstm_cell_17/ones_like/Shape:output:0;sequential_17/lstm_17/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/lstm_cell_17/ones_likeР
4sequential_17/lstm_17/lstm_cell_17/ones_like_1/ShapeShape$sequential_17/lstm_17/zeros:output:0*
T0*
_output_shapes
:26
4sequential_17/lstm_17/lstm_cell_17/ones_like_1/ShapeБ
4sequential_17/lstm_17/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?26
4sequential_17/lstm_17/lstm_cell_17/ones_like_1/Const
.sequential_17/lstm_17/lstm_cell_17/ones_like_1Fill=sequential_17/lstm_17/lstm_cell_17/ones_like_1/Shape:output:0=sequential_17/lstm_17/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd20
.sequential_17/lstm_17/lstm_cell_17/ones_like_1№
&sequential_17/lstm_17/lstm_cell_17/mulMul.sequential_17/lstm_17/strided_slice_2:output:05sequential_17/lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2(
&sequential_17/lstm_17/lstm_cell_17/mulє
(sequential_17/lstm_17/lstm_cell_17/mul_1Mul.sequential_17/lstm_17/strided_slice_2:output:05sequential_17/lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_1є
(sequential_17/lstm_17/lstm_cell_17/mul_2Mul.sequential_17/lstm_17/strided_slice_2:output:05sequential_17/lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_2є
(sequential_17/lstm_17/lstm_cell_17/mul_3Mul.sequential_17/lstm_17/strided_slice_2:output:05sequential_17/lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_3Њ
2sequential_17/lstm_17/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :24
2sequential_17/lstm_17/lstm_cell_17/split/split_dimє
7sequential_17/lstm_17/lstm_cell_17/split/ReadVariableOpReadVariableOp@sequential_17_lstm_17_lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype029
7sequential_17/lstm_17/lstm_cell_17/split/ReadVariableOpГ
(sequential_17/lstm_17/lstm_cell_17/splitSplit;sequential_17/lstm_17/lstm_cell_17/split/split_dim:output:0?sequential_17/lstm_17/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2*
(sequential_17/lstm_17/lstm_cell_17/splitё
)sequential_17/lstm_17/lstm_cell_17/MatMulMatMul*sequential_17/lstm_17/lstm_cell_17/mul:z:01sequential_17/lstm_17/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)sequential_17/lstm_17/lstm_cell_17/MatMulї
+sequential_17/lstm_17/lstm_cell_17/MatMul_1MatMul,sequential_17/lstm_17/lstm_cell_17/mul_1:z:01sequential_17/lstm_17/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2-
+sequential_17/lstm_17/lstm_cell_17/MatMul_1ї
+sequential_17/lstm_17/lstm_cell_17/MatMul_2MatMul,sequential_17/lstm_17/lstm_cell_17/mul_2:z:01sequential_17/lstm_17/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2-
+sequential_17/lstm_17/lstm_cell_17/MatMul_2ї
+sequential_17/lstm_17/lstm_cell_17/MatMul_3MatMul,sequential_17/lstm_17/lstm_cell_17/mul_3:z:01sequential_17/lstm_17/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2-
+sequential_17/lstm_17/lstm_cell_17/MatMul_3Ў
4sequential_17/lstm_17/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 26
4sequential_17/lstm_17/lstm_cell_17/split_1/split_dimі
9sequential_17/lstm_17/lstm_cell_17/split_1/ReadVariableOpReadVariableOpBsequential_17_lstm_17_lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02;
9sequential_17/lstm_17/lstm_cell_17/split_1/ReadVariableOpЋ
*sequential_17/lstm_17/lstm_cell_17/split_1Split=sequential_17/lstm_17/lstm_cell_17/split_1/split_dim:output:0Asequential_17/lstm_17/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2,
*sequential_17/lstm_17/lstm_cell_17/split_1џ
*sequential_17/lstm_17/lstm_cell_17/BiasAddBiasAdd3sequential_17/lstm_17/lstm_cell_17/MatMul:product:03sequential_17/lstm_17/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*sequential_17/lstm_17/lstm_cell_17/BiasAdd
,sequential_17/lstm_17/lstm_cell_17/BiasAdd_1BiasAdd5sequential_17/lstm_17/lstm_cell_17/MatMul_1:product:03sequential_17/lstm_17/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/lstm_cell_17/BiasAdd_1
,sequential_17/lstm_17/lstm_cell_17/BiasAdd_2BiasAdd5sequential_17/lstm_17/lstm_cell_17/MatMul_2:product:03sequential_17/lstm_17/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/lstm_cell_17/BiasAdd_2
,sequential_17/lstm_17/lstm_cell_17/BiasAdd_3BiasAdd5sequential_17/lstm_17/lstm_cell_17/MatMul_3:product:03sequential_17/lstm_17/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/lstm_cell_17/BiasAdd_3ь
(sequential_17/lstm_17/lstm_cell_17/mul_4Mul$sequential_17/lstm_17/zeros:output:07sequential_17/lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_4ь
(sequential_17/lstm_17/lstm_cell_17/mul_5Mul$sequential_17/lstm_17/zeros:output:07sequential_17/lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_5ь
(sequential_17/lstm_17/lstm_cell_17/mul_6Mul$sequential_17/lstm_17/zeros:output:07sequential_17/lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_6ь
(sequential_17/lstm_17/lstm_cell_17/mul_7Mul$sequential_17/lstm_17/zeros:output:07sequential_17/lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_7т
1sequential_17/lstm_17/lstm_cell_17/ReadVariableOpReadVariableOp:sequential_17_lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype023
1sequential_17/lstm_17/lstm_cell_17/ReadVariableOpС
6sequential_17/lstm_17/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        28
6sequential_17/lstm_17/lstm_cell_17/strided_slice/stackХ
8sequential_17/lstm_17/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2:
8sequential_17/lstm_17/lstm_cell_17/strided_slice/stack_1Х
8sequential_17/lstm_17/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2:
8sequential_17/lstm_17/lstm_cell_17/strided_slice/stack_2Ю
0sequential_17/lstm_17/lstm_cell_17/strided_sliceStridedSlice9sequential_17/lstm_17/lstm_cell_17/ReadVariableOp:value:0?sequential_17/lstm_17/lstm_cell_17/strided_slice/stack:output:0Asequential_17/lstm_17/lstm_cell_17/strided_slice/stack_1:output:0Asequential_17/lstm_17/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask22
0sequential_17/lstm_17/lstm_cell_17/strided_sliceџ
+sequential_17/lstm_17/lstm_cell_17/MatMul_4MatMul,sequential_17/lstm_17/lstm_cell_17/mul_4:z:09sequential_17/lstm_17/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+sequential_17/lstm_17/lstm_cell_17/MatMul_4ї
&sequential_17/lstm_17/lstm_cell_17/addAddV23sequential_17/lstm_17/lstm_cell_17/BiasAdd:output:05sequential_17/lstm_17/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2(
&sequential_17/lstm_17/lstm_cell_17/addС
*sequential_17/lstm_17/lstm_cell_17/SigmoidSigmoid*sequential_17/lstm_17/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*sequential_17/lstm_17/lstm_cell_17/Sigmoidц
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_1ReadVariableOp:sequential_17_lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype025
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_1Х
8sequential_17/lstm_17/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2:
8sequential_17/lstm_17/lstm_cell_17/strided_slice_1/stackЩ
:sequential_17/lstm_17/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2<
:sequential_17/lstm_17/lstm_cell_17/strided_slice_1/stack_1Щ
:sequential_17/lstm_17/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2<
:sequential_17/lstm_17/lstm_cell_17/strided_slice_1/stack_2к
2sequential_17/lstm_17/lstm_cell_17/strided_slice_1StridedSlice;sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_1:value:0Asequential_17/lstm_17/lstm_cell_17/strided_slice_1/stack:output:0Csequential_17/lstm_17/lstm_cell_17/strided_slice_1/stack_1:output:0Csequential_17/lstm_17/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask24
2sequential_17/lstm_17/lstm_cell_17/strided_slice_1
+sequential_17/lstm_17/lstm_cell_17/MatMul_5MatMul,sequential_17/lstm_17/lstm_cell_17/mul_5:z:0;sequential_17/lstm_17/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+sequential_17/lstm_17/lstm_cell_17/MatMul_5§
(sequential_17/lstm_17/lstm_cell_17/add_1AddV25sequential_17/lstm_17/lstm_cell_17/BiasAdd_1:output:05sequential_17/lstm_17/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/add_1Ч
,sequential_17/lstm_17/lstm_cell_17/Sigmoid_1Sigmoid,sequential_17/lstm_17/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/lstm_cell_17/Sigmoid_1ч
(sequential_17/lstm_17/lstm_cell_17/mul_8Mul0sequential_17/lstm_17/lstm_cell_17/Sigmoid_1:y:0&sequential_17/lstm_17/zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_8ц
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_2ReadVariableOp:sequential_17_lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype025
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_2Х
8sequential_17/lstm_17/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2:
8sequential_17/lstm_17/lstm_cell_17/strided_slice_2/stackЩ
:sequential_17/lstm_17/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2<
:sequential_17/lstm_17/lstm_cell_17/strided_slice_2/stack_1Щ
:sequential_17/lstm_17/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2<
:sequential_17/lstm_17/lstm_cell_17/strided_slice_2/stack_2к
2sequential_17/lstm_17/lstm_cell_17/strided_slice_2StridedSlice;sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_2:value:0Asequential_17/lstm_17/lstm_cell_17/strided_slice_2/stack:output:0Csequential_17/lstm_17/lstm_cell_17/strided_slice_2/stack_1:output:0Csequential_17/lstm_17/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask24
2sequential_17/lstm_17/lstm_cell_17/strided_slice_2
+sequential_17/lstm_17/lstm_cell_17/MatMul_6MatMul,sequential_17/lstm_17/lstm_cell_17/mul_6:z:0;sequential_17/lstm_17/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+sequential_17/lstm_17/lstm_cell_17/MatMul_6§
(sequential_17/lstm_17/lstm_cell_17/add_2AddV25sequential_17/lstm_17/lstm_cell_17/BiasAdd_2:output:05sequential_17/lstm_17/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/add_2К
'sequential_17/lstm_17/lstm_cell_17/TanhTanh,sequential_17/lstm_17/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2)
'sequential_17/lstm_17/lstm_cell_17/Tanhъ
(sequential_17/lstm_17/lstm_cell_17/mul_9Mul.sequential_17/lstm_17/lstm_cell_17/Sigmoid:y:0+sequential_17/lstm_17/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/mul_9ы
(sequential_17/lstm_17/lstm_cell_17/add_3AddV2,sequential_17/lstm_17/lstm_cell_17/mul_8:z:0,sequential_17/lstm_17/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/add_3ц
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_3ReadVariableOp:sequential_17_lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype025
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_3Х
8sequential_17/lstm_17/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2:
8sequential_17/lstm_17/lstm_cell_17/strided_slice_3/stackЩ
:sequential_17/lstm_17/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2<
:sequential_17/lstm_17/lstm_cell_17/strided_slice_3/stack_1Щ
:sequential_17/lstm_17/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2<
:sequential_17/lstm_17/lstm_cell_17/strided_slice_3/stack_2к
2sequential_17/lstm_17/lstm_cell_17/strided_slice_3StridedSlice;sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_3:value:0Asequential_17/lstm_17/lstm_cell_17/strided_slice_3/stack:output:0Csequential_17/lstm_17/lstm_cell_17/strided_slice_3/stack_1:output:0Csequential_17/lstm_17/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask24
2sequential_17/lstm_17/lstm_cell_17/strided_slice_3
+sequential_17/lstm_17/lstm_cell_17/MatMul_7MatMul,sequential_17/lstm_17/lstm_cell_17/mul_7:z:0;sequential_17/lstm_17/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+sequential_17/lstm_17/lstm_cell_17/MatMul_7§
(sequential_17/lstm_17/lstm_cell_17/add_4AddV25sequential_17/lstm_17/lstm_cell_17/BiasAdd_3:output:05sequential_17/lstm_17/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(sequential_17/lstm_17/lstm_cell_17/add_4Ч
,sequential_17/lstm_17/lstm_cell_17/Sigmoid_2Sigmoid,sequential_17/lstm_17/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2.
,sequential_17/lstm_17/lstm_cell_17/Sigmoid_2О
)sequential_17/lstm_17/lstm_cell_17/Tanh_1Tanh,sequential_17/lstm_17/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)sequential_17/lstm_17/lstm_cell_17/Tanh_1№
)sequential_17/lstm_17/lstm_cell_17/mul_10Mul0sequential_17/lstm_17/lstm_cell_17/Sigmoid_2:y:0-sequential_17/lstm_17/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)sequential_17/lstm_17/lstm_cell_17/mul_10Л
3sequential_17/lstm_17/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   25
3sequential_17/lstm_17/TensorArrayV2_1/element_shape
%sequential_17/lstm_17/TensorArrayV2_1TensorListReserve<sequential_17/lstm_17/TensorArrayV2_1/element_shape:output:0.sequential_17/lstm_17/strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02'
%sequential_17/lstm_17/TensorArrayV2_1z
sequential_17/lstm_17/timeConst*
_output_shapes
: *
dtype0*
value	B : 2
sequential_17/lstm_17/timeЋ
.sequential_17/lstm_17/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ20
.sequential_17/lstm_17/while/maximum_iterations
(sequential_17/lstm_17/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2*
(sequential_17/lstm_17/while/loop_counterА
sequential_17/lstm_17/whileWhile1sequential_17/lstm_17/while/loop_counter:output:07sequential_17/lstm_17/while/maximum_iterations:output:0#sequential_17/lstm_17/time:output:0.sequential_17/lstm_17/TensorArrayV2_1:handle:0$sequential_17/lstm_17/zeros:output:0&sequential_17/lstm_17/zeros_1:output:0.sequential_17/lstm_17/strided_slice_1:output:0Msequential_17/lstm_17/TensorArrayUnstack/TensorListFromTensor:output_handle:0@sequential_17_lstm_17_lstm_cell_17_split_readvariableop_resourceBsequential_17_lstm_17_lstm_cell_17_split_1_readvariableop_resource:sequential_17_lstm_17_lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*4
body,R*
(sequential_17_lstm_17_while_body_1215235*4
cond,R*
(sequential_17_lstm_17_while_cond_1215234*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
sequential_17/lstm_17/whileс
Fsequential_17/lstm_17/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2H
Fsequential_17/lstm_17/TensorArrayV2Stack/TensorListStack/element_shapeС
8sequential_17/lstm_17/TensorArrayV2Stack/TensorListStackTensorListStack$sequential_17/lstm_17/while:output:3Osequential_17/lstm_17/TensorArrayV2Stack/TensorListStack/element_shape:output:0*,
_output_shapes
:џџџџџџџџџd*
element_dtype02:
8sequential_17/lstm_17/TensorArrayV2Stack/TensorListStack­
+sequential_17/lstm_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2-
+sequential_17/lstm_17/strided_slice_3/stackЈ
-sequential_17/lstm_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2/
-sequential_17/lstm_17/strided_slice_3/stack_1Ј
-sequential_17/lstm_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_17/lstm_17/strided_slice_3/stack_2
%sequential_17/lstm_17/strided_slice_3StridedSliceAsequential_17/lstm_17/TensorArrayV2Stack/TensorListStack:tensor:04sequential_17/lstm_17/strided_slice_3/stack:output:06sequential_17/lstm_17/strided_slice_3/stack_1:output:06sequential_17/lstm_17/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2'
%sequential_17/lstm_17/strided_slice_3Ѕ
&sequential_17/lstm_17/transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2(
&sequential_17/lstm_17/transpose_1/permў
!sequential_17/lstm_17/transpose_1	TransposeAsequential_17/lstm_17/TensorArrayV2Stack/TensorListStack:tensor:0/sequential_17/lstm_17/transpose_1/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2#
!sequential_17/lstm_17/transpose_1
sequential_17/lstm_17/runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2
sequential_17/lstm_17/runtimeв
,sequential_17/dense_17/MatMul/ReadVariableOpReadVariableOp5sequential_17_dense_17_matmul_readvariableop_resource*
_output_shapes

:d*
dtype02.
,sequential_17/dense_17/MatMul/ReadVariableOpр
sequential_17/dense_17/MatMulMatMul.sequential_17/lstm_17/strided_slice_3:output:04sequential_17/dense_17/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
sequential_17/dense_17/MatMulб
-sequential_17/dense_17/BiasAdd/ReadVariableOpReadVariableOp6sequential_17_dense_17_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-sequential_17/dense_17/BiasAdd/ReadVariableOpн
sequential_17/dense_17/BiasAddBiasAdd'sequential_17/dense_17/MatMul:product:05sequential_17/dense_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2 
sequential_17/dense_17/BiasAddІ
sequential_17/dense_17/SoftmaxSoftmax'sequential_17/dense_17/BiasAdd:output:0*
T0*'
_output_shapes
:џџџџџџџџџ2 
sequential_17/dense_17/Softmaxѓ
IdentityIdentity(sequential_17/dense_17/Softmax:softmax:0.^sequential_17/dense_17/BiasAdd/ReadVariableOp-^sequential_17/dense_17/MatMul/ReadVariableOp,^sequential_17/embedding_17/embedding_lookup2^sequential_17/lstm_17/lstm_cell_17/ReadVariableOp4^sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_14^sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_24^sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_38^sequential_17/lstm_17/lstm_cell_17/split/ReadVariableOp:^sequential_17/lstm_17/lstm_cell_17/split_1/ReadVariableOp^sequential_17/lstm_17/while*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 2^
-sequential_17/dense_17/BiasAdd/ReadVariableOp-sequential_17/dense_17/BiasAdd/ReadVariableOp2\
,sequential_17/dense_17/MatMul/ReadVariableOp,sequential_17/dense_17/MatMul/ReadVariableOp2Z
+sequential_17/embedding_17/embedding_lookup+sequential_17/embedding_17/embedding_lookup2f
1sequential_17/lstm_17/lstm_cell_17/ReadVariableOp1sequential_17/lstm_17/lstm_cell_17/ReadVariableOp2j
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_13sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_12j
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_23sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_22j
3sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_33sequential_17/lstm_17/lstm_cell_17/ReadVariableOp_32r
7sequential_17/lstm_17/lstm_cell_17/split/ReadVariableOp7sequential_17/lstm_17/lstm_cell_17/split/ReadVariableOp2v
9sequential_17/lstm_17/lstm_cell_17/split_1/ReadVariableOp9sequential_17/lstm_17/lstm_cell_17/split_1/ReadVariableOp2:
sequential_17/lstm_17/whilesequential_17/lstm_17/while:\ X
(
_output_shapes
:џџџџџџџџџ
,
_user_specified_nameembedding_17_input
Ђ&
ъ
while_body_1215591
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0/
while_lstm_cell_17_1215615_0:	d+
while_lstm_cell_17_1215617_0:	/
while_lstm_cell_17_1215619_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor-
while_lstm_cell_17_1215615:	d)
while_lstm_cell_17_1215617:	-
while_lstm_cell_17_1215619:	dЂ*while/lstm_cell_17/StatefulPartitionedCallУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemх
*while/lstm_cell_17/StatefulPartitionedCallStatefulPartitionedCall0while/TensorArrayV2Read/TensorListGetItem:item:0while_placeholder_2while_placeholder_3while_lstm_cell_17_1215615_0while_lstm_cell_17_1215617_0while_lstm_cell_17_1215619_0*
Tin

2*
Tout
2*
_collective_manager_ids
 *M
_output_shapes;
9:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_12155772,
*while/lstm_cell_17/StatefulPartitionedCallї
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholder3while/lstm_cell_17/StatefulPartitionedCall:output:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1
while/IdentityIdentitywhile/add_1:z:0+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity
while/Identity_1Identitywhile_while_maximum_iterations+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity_1
while/Identity_2Identitywhile/add:z:0+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity_2К
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity_3Ф
while/Identity_4Identity3while/lstm_cell_17/StatefulPartitionedCall:output:1+^while/lstm_cell_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4Ф
while/Identity_5Identity3while/lstm_cell_17/StatefulPartitionedCall:output:2+^while/lstm_cell_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0":
while_lstm_cell_17_1215615while_lstm_cell_17_1215615_0":
while_lstm_cell_17_1215617while_lstm_cell_17_1215617_0":
while_lstm_cell_17_1215619while_lstm_cell_17_1215619_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2X
*while/lstm_cell_17/StatefulPartitionedCall*while/lstm_cell_17/StatefulPartitionedCall: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
д
Ж
)__inference_lstm_17_layer_call_fn_1217995

inputs
unknown:	d
	unknown_0:	
	unknown_1:	d
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12169662
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:џџџџџџџџџd: : : 22
StatefulPartitionedCallStatefulPartitionedCall:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
Ћ
ш
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218876

inputs=
*lstm_cell_17_split_readvariableop_resource:	d;
,lstm_cell_17_split_1_readvariableop_resource:	7
$lstm_cell_17_readvariableop_resource:	d
identityЂlstm_cell_17/ReadVariableOpЂlstm_cell_17/ReadVariableOp_1Ђlstm_cell_17/ReadVariableOp_2Ђlstm_cell_17/ReadVariableOp_3Ђ!lstm_cell_17/split/ReadVariableOpЂ#lstm_cell_17/split_1/ReadVariableOpЂwhileD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm{
	transpose	Transposeinputstranspose/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2
lstm_cell_17/ones_like/ShapeShapestrided_slice_2:output:0*
T0*
_output_shapes
:2
lstm_cell_17/ones_like/Shape
lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
lstm_cell_17/ones_like/ConstИ
lstm_cell_17/ones_likeFill%lstm_cell_17/ones_like/Shape:output:0%lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like~
lstm_cell_17/ones_like_1/ShapeShapezeros:output:0*
T0*
_output_shapes
:2 
lstm_cell_17/ones_like_1/Shape
lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2 
lstm_cell_17/ones_like_1/ConstР
lstm_cell_17/ones_like_1Fill'lstm_cell_17/ones_like_1/Shape:output:0'lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like_1
lstm_cell_17/mulMulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul
lstm_cell_17/mul_1Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_1
lstm_cell_17/mul_2Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_2
lstm_cell_17/mul_3Mulstrided_slice_2:output:0lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_3~
lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_cell_17/split/split_dimВ
!lstm_cell_17/split/ReadVariableOpReadVariableOp*lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02#
!lstm_cell_17/split/ReadVariableOpл
lstm_cell_17/splitSplit%lstm_cell_17/split/split_dim:output:0)lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_cell_17/split
lstm_cell_17/MatMulMatMullstm_cell_17/mul:z:0lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul
lstm_cell_17/MatMul_1MatMullstm_cell_17/mul_1:z:0lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_1
lstm_cell_17/MatMul_2MatMullstm_cell_17/mul_2:z:0lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_2
lstm_cell_17/MatMul_3MatMullstm_cell_17/mul_3:z:0lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_3
lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2 
lstm_cell_17/split_1/split_dimД
#lstm_cell_17/split_1/ReadVariableOpReadVariableOp,lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02%
#lstm_cell_17/split_1/ReadVariableOpг
lstm_cell_17/split_1Split'lstm_cell_17/split_1/split_dim:output:0+lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_cell_17/split_1Ї
lstm_cell_17/BiasAddBiasAddlstm_cell_17/MatMul:product:0lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd­
lstm_cell_17/BiasAdd_1BiasAddlstm_cell_17/MatMul_1:product:0lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_1­
lstm_cell_17/BiasAdd_2BiasAddlstm_cell_17/MatMul_2:product:0lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_2­
lstm_cell_17/BiasAdd_3BiasAddlstm_cell_17/MatMul_3:product:0lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_3
lstm_cell_17/mul_4Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_4
lstm_cell_17/mul_5Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_5
lstm_cell_17/mul_6Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_6
lstm_cell_17/mul_7Mulzeros:output:0!lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_7 
lstm_cell_17/ReadVariableOpReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp
 lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2"
 lstm_cell_17/strided_slice/stack
"lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice/stack_1
"lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2$
"lstm_cell_17/strided_slice/stack_2Ъ
lstm_cell_17/strided_sliceStridedSlice#lstm_cell_17/ReadVariableOp:value:0)lstm_cell_17/strided_slice/stack:output:0+lstm_cell_17/strided_slice/stack_1:output:0+lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_sliceЇ
lstm_cell_17/MatMul_4MatMullstm_cell_17/mul_4:z:0#lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_4
lstm_cell_17/addAddV2lstm_cell_17/BiasAdd:output:0lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add
lstm_cell_17/SigmoidSigmoidlstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/SigmoidЄ
lstm_cell_17/ReadVariableOp_1ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_1
"lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice_1/stack
$lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2&
$lstm_cell_17/strided_slice_1/stack_1
$lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_1/stack_2ж
lstm_cell_17/strided_slice_1StridedSlice%lstm_cell_17/ReadVariableOp_1:value:0+lstm_cell_17/strided_slice_1/stack:output:0-lstm_cell_17/strided_slice_1/stack_1:output:0-lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_1Љ
lstm_cell_17/MatMul_5MatMullstm_cell_17/mul_5:z:0%lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_5Ѕ
lstm_cell_17/add_1AddV2lstm_cell_17/BiasAdd_1:output:0lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_1
lstm_cell_17/Sigmoid_1Sigmoidlstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_1
lstm_cell_17/mul_8Mullstm_cell_17/Sigmoid_1:y:0zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_8Є
lstm_cell_17/ReadVariableOp_2ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_2
"lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2$
"lstm_cell_17/strided_slice_2/stack
$lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2&
$lstm_cell_17/strided_slice_2/stack_1
$lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_2/stack_2ж
lstm_cell_17/strided_slice_2StridedSlice%lstm_cell_17/ReadVariableOp_2:value:0+lstm_cell_17/strided_slice_2/stack:output:0-lstm_cell_17/strided_slice_2/stack_1:output:0-lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_2Љ
lstm_cell_17/MatMul_6MatMullstm_cell_17/mul_6:z:0%lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_6Ѕ
lstm_cell_17/add_2AddV2lstm_cell_17/BiasAdd_2:output:0lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_2x
lstm_cell_17/TanhTanhlstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh
lstm_cell_17/mul_9Mullstm_cell_17/Sigmoid:y:0lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_9
lstm_cell_17/add_3AddV2lstm_cell_17/mul_8:z:0lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_3Є
lstm_cell_17/ReadVariableOp_3ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_3
"lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2$
"lstm_cell_17/strided_slice_3/stack
$lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2&
$lstm_cell_17/strided_slice_3/stack_1
$lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_3/stack_2ж
lstm_cell_17/strided_slice_3StridedSlice%lstm_cell_17/ReadVariableOp_3:value:0+lstm_cell_17/strided_slice_3/stack:output:0-lstm_cell_17/strided_slice_3/stack_1:output:0-lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_3Љ
lstm_cell_17/MatMul_7MatMullstm_cell_17/mul_7:z:0%lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_7Ѕ
lstm_cell_17/add_4AddV2lstm_cell_17/BiasAdd_3:output:0lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_4
lstm_cell_17/Sigmoid_2Sigmoidlstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_2|
lstm_cell_17/Tanh_1Tanhlstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh_1
lstm_cell_17/mul_10Mullstm_cell_17/Sigmoid_2:y:0lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_10
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterц
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0*lstm_cell_17_split_readvariableop_resource,lstm_cell_17_split_1_readvariableop_resource$lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1218742*
condR
while_cond_1218741*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeщ
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*,
_output_shapes
:џџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permІ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtimeМ
IdentityIdentitystrided_slice_3:output:0^lstm_cell_17/ReadVariableOp^lstm_cell_17/ReadVariableOp_1^lstm_cell_17/ReadVariableOp_2^lstm_cell_17/ReadVariableOp_3"^lstm_cell_17/split/ReadVariableOp$^lstm_cell_17/split_1/ReadVariableOp^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:џџџџџџџџџd: : : 2:
lstm_cell_17/ReadVariableOplstm_cell_17/ReadVariableOp2>
lstm_cell_17/ReadVariableOp_1lstm_cell_17/ReadVariableOp_12>
lstm_cell_17/ReadVariableOp_2lstm_cell_17/ReadVariableOp_22>
lstm_cell_17/ReadVariableOp_3lstm_cell_17/ReadVariableOp_32F
!lstm_cell_17/split/ReadVariableOp!lstm_cell_17/split/ReadVariableOp2J
#lstm_cell_17/split_1/ReadVariableOp#lstm_cell_17/split_1/ReadVariableOp2
whilewhile:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
Пv
Ж
#__inference__traced_restore_1219732
file_prefix<
(assignvariableop_embedding_17_embeddings:
ЈУd4
"assignvariableop_1_dense_17_kernel:d.
 assignvariableop_2_dense_17_bias:&
assignvariableop_3_adam_iter:	 (
assignvariableop_4_adam_beta_1: (
assignvariableop_5_adam_beta_2: '
assignvariableop_6_adam_decay: /
%assignvariableop_7_adam_learning_rate: A
.assignvariableop_8_lstm_17_lstm_cell_17_kernel:	dK
8assignvariableop_9_lstm_17_lstm_cell_17_recurrent_kernel:	d<
-assignvariableop_10_lstm_17_lstm_cell_17_bias:	#
assignvariableop_11_total: #
assignvariableop_12_count: %
assignvariableop_13_total_1: %
assignvariableop_14_count_1: F
2assignvariableop_15_adam_embedding_17_embeddings_m:
ЈУd<
*assignvariableop_16_adam_dense_17_kernel_m:d6
(assignvariableop_17_adam_dense_17_bias_m:I
6assignvariableop_18_adam_lstm_17_lstm_cell_17_kernel_m:	dS
@assignvariableop_19_adam_lstm_17_lstm_cell_17_recurrent_kernel_m:	dC
4assignvariableop_20_adam_lstm_17_lstm_cell_17_bias_m:	F
2assignvariableop_21_adam_embedding_17_embeddings_v:
ЈУd<
*assignvariableop_22_adam_dense_17_kernel_v:d6
(assignvariableop_23_adam_dense_17_bias_v:I
6assignvariableop_24_adam_lstm_17_lstm_cell_17_kernel_v:	dS
@assignvariableop_25_adam_lstm_17_lstm_cell_17_recurrent_kernel_v:	dC
4assignvariableop_26_adam_lstm_17_lstm_cell_17_bias_v:	
identity_28ЂAssignVariableOpЂAssignVariableOp_1ЂAssignVariableOp_10ЂAssignVariableOp_11ЂAssignVariableOp_12ЂAssignVariableOp_13ЂAssignVariableOp_14ЂAssignVariableOp_15ЂAssignVariableOp_16ЂAssignVariableOp_17ЂAssignVariableOp_18ЂAssignVariableOp_19ЂAssignVariableOp_2ЂAssignVariableOp_20ЂAssignVariableOp_21ЂAssignVariableOp_22ЂAssignVariableOp_23ЂAssignVariableOp_24ЂAssignVariableOp_25ЂAssignVariableOp_26ЂAssignVariableOp_3ЂAssignVariableOp_4ЂAssignVariableOp_5ЂAssignVariableOp_6ЂAssignVariableOp_7ЂAssignVariableOp_8ЂAssignVariableOp_9
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*І
valueBB:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesЦ
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*K
valueBB@B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesИ
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*
_output_shapesr
p::::::::::::::::::::::::::::**
dtypes 
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

IdentityЇ
AssignVariableOpAssignVariableOp(assignvariableop_embedding_17_embeddingsIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1Ї
AssignVariableOp_1AssignVariableOp"assignvariableop_1_dense_17_kernelIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2Ѕ
AssignVariableOp_2AssignVariableOp assignvariableop_2_dense_17_biasIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0	*
_output_shapes
:2

Identity_3Ё
AssignVariableOp_3AssignVariableOpassignvariableop_3_adam_iterIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4Ѓ
AssignVariableOp_4AssignVariableOpassignvariableop_4_adam_beta_1Identity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5Ѓ
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_beta_2Identity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6Ђ
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_decayIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7Њ
AssignVariableOp_7AssignVariableOp%assignvariableop_7_adam_learning_rateIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8Г
AssignVariableOp_8AssignVariableOp.assignvariableop_8_lstm_17_lstm_cell_17_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9Н
AssignVariableOp_9AssignVariableOp8assignvariableop_9_lstm_17_lstm_cell_17_recurrent_kernelIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10Е
AssignVariableOp_10AssignVariableOp-assignvariableop_10_lstm_17_lstm_cell_17_biasIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11Ё
AssignVariableOp_11AssignVariableOpassignvariableop_11_totalIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12Ё
AssignVariableOp_12AssignVariableOpassignvariableop_12_countIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13Ѓ
AssignVariableOp_13AssignVariableOpassignvariableop_13_total_1Identity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14Ѓ
AssignVariableOp_14AssignVariableOpassignvariableop_14_count_1Identity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15К
AssignVariableOp_15AssignVariableOp2assignvariableop_15_adam_embedding_17_embeddings_mIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16В
AssignVariableOp_16AssignVariableOp*assignvariableop_16_adam_dense_17_kernel_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17А
AssignVariableOp_17AssignVariableOp(assignvariableop_17_adam_dense_17_bias_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18О
AssignVariableOp_18AssignVariableOp6assignvariableop_18_adam_lstm_17_lstm_cell_17_kernel_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19Ш
AssignVariableOp_19AssignVariableOp@assignvariableop_19_adam_lstm_17_lstm_cell_17_recurrent_kernel_mIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20М
AssignVariableOp_20AssignVariableOp4assignvariableop_20_adam_lstm_17_lstm_cell_17_bias_mIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21К
AssignVariableOp_21AssignVariableOp2assignvariableop_21_adam_embedding_17_embeddings_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22В
AssignVariableOp_22AssignVariableOp*assignvariableop_22_adam_dense_17_kernel_vIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23А
AssignVariableOp_23AssignVariableOp(assignvariableop_23_adam_dense_17_bias_vIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24О
AssignVariableOp_24AssignVariableOp6assignvariableop_24_adam_lstm_17_lstm_cell_17_kernel_vIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25Ш
AssignVariableOp_25AssignVariableOp@assignvariableop_25_adam_lstm_17_lstm_cell_17_recurrent_kernel_vIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26М
AssignVariableOp_26AssignVariableOp4assignvariableop_26_adam_lstm_17_lstm_cell_17_bias_vIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_269
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpА
Identity_27Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_27Ѓ
Identity_28IdentityIdentity_27:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
T0*
_output_shapes
: 2
Identity_28"#
identity_28Identity_28:output:0*K
_input_shapes:
8: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
Е

і
E__inference_dense_17_layer_call_and_return_conditional_losses_1216542

inputs0
matmul_readvariableop_resource:d-
biasadd_readvariableop_resource:
identityЂBiasAdd/ReadVariableOpЂMatMul/ReadVariableOp
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:d*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
MatMul
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2	
BiasAdda
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:џџџџџџџџџ2	
Softmax
IdentityIdentitySoftmax:softmax:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:џџџџџџџџџd: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
Ь

%__inference_signature_wrapper_1217151
embedding_17_input
unknown:
ЈУd
	unknown_0:	d
	unknown_1:	
	unknown_2:	d
	unknown_3:d
	unknown_4:
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallembedding_17_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4*
Tin
	2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*(
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *+
f&R$
"__inference__wrapped_model_12153762
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:\ X
(
_output_shapes
:џџџџџџџџџ
,
_user_specified_nameembedding_17_input
Е

і
E__inference_dense_17_layer_call_and_return_conditional_losses_1219275

inputs0
matmul_readvariableop_resource:d-
biasadd_readvariableop_resource:
identityЂBiasAdd/ReadVariableOpЂMatMul/ReadVariableOp
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:d*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
MatMul
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2	
BiasAdda
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:џџџџџџџџџ2	
Softmax
IdentityIdentitySoftmax:softmax:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:џџџџџџџџџd: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
Џ
R
6__inference_spatial_dropout1d_17_layer_call_fn_1217882

inputs
identityх
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12153852
PartitionedCall
IdentityIdentityPartitionedCall:output:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ:e a
=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 
_user_specified_nameinputs
з
p
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1215417

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ь
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout/Const
dropout/MulMulinputsdropout/Const:output:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
dropout/Mul
dropout/random_uniform/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2 
dropout/random_uniform/shape/1Э
dropout/random_uniform/shapePackstrided_slice:output:0'dropout/random_uniform/shape/1:output:0strided_slice_1:output:0*
N*
T0*
_output_shapes
:2
dropout/random_uniform/shapeа
$dropout/random_uniform/RandomUniformRandomUniform%dropout/random_uniform/shape:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout/GreaterEqual/yЫ
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/Cast
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
dropout/Mul_1{
IdentityIdentitydropout/Mul_1:z:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ:e a
=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 
_user_specified_nameinputs
к

/__inference_sequential_17_layer_call_fn_1217185

inputs
unknown:
ЈУd
	unknown_0:	d
	unknown_1:	
	unknown_2:	d
	unknown_3:d
	unknown_4:
identityЂStatefulPartitionedCallЎ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4*
Tin
	2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*(
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *S
fNRL
J__inference_sequential_17_layer_call_and_return_conditional_losses_12170542
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs
ж
o
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1215385

inputs

identity_1p
IdentityIdentityinputs*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity

Identity_1IdentityIdentity:output:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ:e a
=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 
_user_specified_nameinputs
Є

Љ
I__inference_embedding_17_layer_call_and_return_conditional_losses_1217877

inputs,
embedding_lookup_1217871:
ЈУd
identityЂembedding_lookup^
CastCastinputs*

DstT0*

SrcT0*(
_output_shapes
:џџџџџџџџџ2
Cast
embedding_lookupResourceGatherembedding_lookup_1217871Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*+
_class!
loc:@embedding_lookup/1217871*,
_output_shapes
:џџџџџџџџџd*
dtype02
embedding_lookupя
embedding_lookup/IdentityIdentityembedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*+
_class!
loc:@embedding_lookup/1217871*,
_output_shapes
:џџџџџџџџџd2
embedding_lookup/IdentityЁ
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
embedding_lookup/Identity_1
IdentityIdentity$embedding_lookup/Identity_1:output:0^embedding_lookup*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:џџџџџџџџџ: 2$
embedding_lookupembedding_lookup:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs
ЕE

D__inference_lstm_17_layer_call_and_return_conditional_losses_1215660

inputs'
lstm_cell_17_1215578:	d#
lstm_cell_17_1215580:	'
lstm_cell_17_1215582:	d
identityЂ$lstm_cell_17/StatefulPartitionedCallЂwhileD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm
	transpose	Transposeinputstranspose/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2Ё
$lstm_cell_17/StatefulPartitionedCallStatefulPartitionedCallstrided_slice_2:output:0zeros:output:0zeros_1:output:0lstm_cell_17_1215578lstm_cell_17_1215580lstm_cell_17_1215582*
Tin

2*
Tout
2*
_collective_manager_ids
 *M
_output_shapes;
9:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_12155772&
$lstm_cell_17/StatefulPartitionedCall
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterЈ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0lstm_cell_17_1215578lstm_cell_17_1215580lstm_cell_17_1215582*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1215591*
condR
while_cond_1215590*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeё
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permЎ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtime
IdentityIdentitystrided_slice_3:output:0%^lstm_cell_17/StatefulPartitionedCall^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:џџџџџџџџџџџџџџџџџџd: : : 2L
$lstm_cell_17/StatefulPartitionedCall$lstm_cell_17/StatefulPartitionedCall2
whilewhile:\ X
4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd
 
_user_specified_nameinputs
њ
ъ
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218625
inputs_0=
*lstm_cell_17_split_readvariableop_resource:	d;
,lstm_cell_17_split_1_readvariableop_resource:	7
$lstm_cell_17_readvariableop_resource:	d
identityЂlstm_cell_17/ReadVariableOpЂlstm_cell_17/ReadVariableOp_1Ђlstm_cell_17/ReadVariableOp_2Ђlstm_cell_17/ReadVariableOp_3Ђ!lstm_cell_17/split/ReadVariableOpЂ#lstm_cell_17/split_1/ReadVariableOpЂwhileF
ShapeShapeinputs_0*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm
	transpose	Transposeinputs_0transpose/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2
lstm_cell_17/ones_like/ShapeShapestrided_slice_2:output:0*
T0*
_output_shapes
:2
lstm_cell_17/ones_like/Shape
lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
lstm_cell_17/ones_like/ConstИ
lstm_cell_17/ones_likeFill%lstm_cell_17/ones_like/Shape:output:0%lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like}
lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout/ConstГ
lstm_cell_17/dropout/MulMullstm_cell_17/ones_like:output:0#lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/Mul
lstm_cell_17/dropout/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout/Shapeњ
1lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform#lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ыфб23
1lstm_cell_17/dropout/random_uniform/RandomUniform
#lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2%
#lstm_cell_17/dropout/GreaterEqual/yђ
!lstm_cell_17/dropout/GreaterEqualGreaterEqual:lstm_cell_17/dropout/random_uniform/RandomUniform:output:0,lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_cell_17/dropout/GreaterEqualІ
lstm_cell_17/dropout/CastCast%lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/CastЎ
lstm_cell_17/dropout/Mul_1Mullstm_cell_17/dropout/Mul:z:0lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/Mul_1
lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_1/ConstЙ
lstm_cell_17/dropout_1/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/Mul
lstm_cell_17/dropout_1/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_1/Shapeџ
3lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2чN25
3lstm_cell_17/dropout_1/random_uniform/RandomUniform
%lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_1/GreaterEqual/yњ
#lstm_cell_17/dropout_1/GreaterEqualGreaterEqual<lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_1/GreaterEqualЌ
lstm_cell_17/dropout_1/CastCast'lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/CastЖ
lstm_cell_17/dropout_1/Mul_1Mullstm_cell_17/dropout_1/Mul:z:0lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/Mul_1
lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_2/ConstЙ
lstm_cell_17/dropout_2/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/Mul
lstm_cell_17/dropout_2/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_2/Shapeџ
3lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2УX25
3lstm_cell_17/dropout_2/random_uniform/RandomUniform
%lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_2/GreaterEqual/yњ
#lstm_cell_17/dropout_2/GreaterEqualGreaterEqual<lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_2/GreaterEqualЌ
lstm_cell_17/dropout_2/CastCast'lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/CastЖ
lstm_cell_17/dropout_2/Mul_1Mullstm_cell_17/dropout_2/Mul:z:0lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/Mul_1
lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_3/ConstЙ
lstm_cell_17/dropout_3/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/Mul
lstm_cell_17/dropout_3/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_3/Shape
3lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2АЕ25
3lstm_cell_17/dropout_3/random_uniform/RandomUniform
%lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_3/GreaterEqual/yњ
#lstm_cell_17/dropout_3/GreaterEqualGreaterEqual<lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_3/GreaterEqualЌ
lstm_cell_17/dropout_3/CastCast'lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/CastЖ
lstm_cell_17/dropout_3/Mul_1Mullstm_cell_17/dropout_3/Mul:z:0lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/Mul_1~
lstm_cell_17/ones_like_1/ShapeShapezeros:output:0*
T0*
_output_shapes
:2 
lstm_cell_17/ones_like_1/Shape
lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2 
lstm_cell_17/ones_like_1/ConstР
lstm_cell_17/ones_like_1Fill'lstm_cell_17/ones_like_1/Shape:output:0'lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like_1
lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_4/ConstЛ
lstm_cell_17/dropout_4/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/Mul
lstm_cell_17/dropout_4/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_4/Shape
3lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2хщЖ25
3lstm_cell_17/dropout_4/random_uniform/RandomUniform
%lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_4/GreaterEqual/yњ
#lstm_cell_17/dropout_4/GreaterEqualGreaterEqual<lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_4/GreaterEqualЌ
lstm_cell_17/dropout_4/CastCast'lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/CastЖ
lstm_cell_17/dropout_4/Mul_1Mullstm_cell_17/dropout_4/Mul:z:0lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/Mul_1
lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_5/ConstЛ
lstm_cell_17/dropout_5/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/Mul
lstm_cell_17/dropout_5/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_5/Shapeџ
3lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Т#25
3lstm_cell_17/dropout_5/random_uniform/RandomUniform
%lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_5/GreaterEqual/yњ
#lstm_cell_17/dropout_5/GreaterEqualGreaterEqual<lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_5/GreaterEqualЌ
lstm_cell_17/dropout_5/CastCast'lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/CastЖ
lstm_cell_17/dropout_5/Mul_1Mullstm_cell_17/dropout_5/Mul:z:0lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/Mul_1
lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_6/ConstЛ
lstm_cell_17/dropout_6/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/Mul
lstm_cell_17/dropout_6/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_6/Shape
3lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2мм25
3lstm_cell_17/dropout_6/random_uniform/RandomUniform
%lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_6/GreaterEqual/yњ
#lstm_cell_17/dropout_6/GreaterEqualGreaterEqual<lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_6/GreaterEqualЌ
lstm_cell_17/dropout_6/CastCast'lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/CastЖ
lstm_cell_17/dropout_6/Mul_1Mullstm_cell_17/dropout_6/Mul:z:0lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/Mul_1
lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_7/ConstЛ
lstm_cell_17/dropout_7/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/Mul
lstm_cell_17/dropout_7/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_7/Shape
3lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2НЉЗ25
3lstm_cell_17/dropout_7/random_uniform/RandomUniform
%lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_7/GreaterEqual/yњ
#lstm_cell_17/dropout_7/GreaterEqualGreaterEqual<lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_7/GreaterEqualЌ
lstm_cell_17/dropout_7/CastCast'lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/CastЖ
lstm_cell_17/dropout_7/Mul_1Mullstm_cell_17/dropout_7/Mul:z:0lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/Mul_1
lstm_cell_17/mulMulstrided_slice_2:output:0lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul
lstm_cell_17/mul_1Mulstrided_slice_2:output:0 lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_1
lstm_cell_17/mul_2Mulstrided_slice_2:output:0 lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_2
lstm_cell_17/mul_3Mulstrided_slice_2:output:0 lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_3~
lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_cell_17/split/split_dimВ
!lstm_cell_17/split/ReadVariableOpReadVariableOp*lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02#
!lstm_cell_17/split/ReadVariableOpл
lstm_cell_17/splitSplit%lstm_cell_17/split/split_dim:output:0)lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_cell_17/split
lstm_cell_17/MatMulMatMullstm_cell_17/mul:z:0lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul
lstm_cell_17/MatMul_1MatMullstm_cell_17/mul_1:z:0lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_1
lstm_cell_17/MatMul_2MatMullstm_cell_17/mul_2:z:0lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_2
lstm_cell_17/MatMul_3MatMullstm_cell_17/mul_3:z:0lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_3
lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2 
lstm_cell_17/split_1/split_dimД
#lstm_cell_17/split_1/ReadVariableOpReadVariableOp,lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02%
#lstm_cell_17/split_1/ReadVariableOpг
lstm_cell_17/split_1Split'lstm_cell_17/split_1/split_dim:output:0+lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_cell_17/split_1Ї
lstm_cell_17/BiasAddBiasAddlstm_cell_17/MatMul:product:0lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd­
lstm_cell_17/BiasAdd_1BiasAddlstm_cell_17/MatMul_1:product:0lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_1­
lstm_cell_17/BiasAdd_2BiasAddlstm_cell_17/MatMul_2:product:0lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_2­
lstm_cell_17/BiasAdd_3BiasAddlstm_cell_17/MatMul_3:product:0lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_3
lstm_cell_17/mul_4Mulzeros:output:0 lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_4
lstm_cell_17/mul_5Mulzeros:output:0 lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_5
lstm_cell_17/mul_6Mulzeros:output:0 lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_6
lstm_cell_17/mul_7Mulzeros:output:0 lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_7 
lstm_cell_17/ReadVariableOpReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp
 lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2"
 lstm_cell_17/strided_slice/stack
"lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice/stack_1
"lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2$
"lstm_cell_17/strided_slice/stack_2Ъ
lstm_cell_17/strided_sliceStridedSlice#lstm_cell_17/ReadVariableOp:value:0)lstm_cell_17/strided_slice/stack:output:0+lstm_cell_17/strided_slice/stack_1:output:0+lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_sliceЇ
lstm_cell_17/MatMul_4MatMullstm_cell_17/mul_4:z:0#lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_4
lstm_cell_17/addAddV2lstm_cell_17/BiasAdd:output:0lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add
lstm_cell_17/SigmoidSigmoidlstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/SigmoidЄ
lstm_cell_17/ReadVariableOp_1ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_1
"lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice_1/stack
$lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2&
$lstm_cell_17/strided_slice_1/stack_1
$lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_1/stack_2ж
lstm_cell_17/strided_slice_1StridedSlice%lstm_cell_17/ReadVariableOp_1:value:0+lstm_cell_17/strided_slice_1/stack:output:0-lstm_cell_17/strided_slice_1/stack_1:output:0-lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_1Љ
lstm_cell_17/MatMul_5MatMullstm_cell_17/mul_5:z:0%lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_5Ѕ
lstm_cell_17/add_1AddV2lstm_cell_17/BiasAdd_1:output:0lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_1
lstm_cell_17/Sigmoid_1Sigmoidlstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_1
lstm_cell_17/mul_8Mullstm_cell_17/Sigmoid_1:y:0zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_8Є
lstm_cell_17/ReadVariableOp_2ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_2
"lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2$
"lstm_cell_17/strided_slice_2/stack
$lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2&
$lstm_cell_17/strided_slice_2/stack_1
$lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_2/stack_2ж
lstm_cell_17/strided_slice_2StridedSlice%lstm_cell_17/ReadVariableOp_2:value:0+lstm_cell_17/strided_slice_2/stack:output:0-lstm_cell_17/strided_slice_2/stack_1:output:0-lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_2Љ
lstm_cell_17/MatMul_6MatMullstm_cell_17/mul_6:z:0%lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_6Ѕ
lstm_cell_17/add_2AddV2lstm_cell_17/BiasAdd_2:output:0lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_2x
lstm_cell_17/TanhTanhlstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh
lstm_cell_17/mul_9Mullstm_cell_17/Sigmoid:y:0lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_9
lstm_cell_17/add_3AddV2lstm_cell_17/mul_8:z:0lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_3Є
lstm_cell_17/ReadVariableOp_3ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_3
"lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2$
"lstm_cell_17/strided_slice_3/stack
$lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2&
$lstm_cell_17/strided_slice_3/stack_1
$lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_3/stack_2ж
lstm_cell_17/strided_slice_3StridedSlice%lstm_cell_17/ReadVariableOp_3:value:0+lstm_cell_17/strided_slice_3/stack:output:0-lstm_cell_17/strided_slice_3/stack_1:output:0-lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_3Љ
lstm_cell_17/MatMul_7MatMullstm_cell_17/mul_7:z:0%lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_7Ѕ
lstm_cell_17/add_4AddV2lstm_cell_17/BiasAdd_3:output:0lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_4
lstm_cell_17/Sigmoid_2Sigmoidlstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_2|
lstm_cell_17/Tanh_1Tanhlstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh_1
lstm_cell_17/mul_10Mullstm_cell_17/Sigmoid_2:y:0lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_10
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterц
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0*lstm_cell_17_split_readvariableop_resource,lstm_cell_17_split_1_readvariableop_resource$lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1218427*
condR
while_cond_1218426*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeё
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permЎ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtimeМ
IdentityIdentitystrided_slice_3:output:0^lstm_cell_17/ReadVariableOp^lstm_cell_17/ReadVariableOp_1^lstm_cell_17/ReadVariableOp_2^lstm_cell_17/ReadVariableOp_3"^lstm_cell_17/split/ReadVariableOp$^lstm_cell_17/split_1/ReadVariableOp^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:џџџџџџџџџџџџџџџџџџd: : : 2:
lstm_cell_17/ReadVariableOplstm_cell_17/ReadVariableOp2>
lstm_cell_17/ReadVariableOp_1lstm_cell_17/ReadVariableOp_12>
lstm_cell_17/ReadVariableOp_2lstm_cell_17/ReadVariableOp_22>
lstm_cell_17/ReadVariableOp_3lstm_cell_17/ReadVariableOp_32F
!lstm_cell_17/split/ReadVariableOp!lstm_cell_17/split/ReadVariableOp2J
#lstm_cell_17/split_1/ReadVariableOp#lstm_cell_17/split_1/ReadVariableOp2
whilewhile:^ Z
4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd
"
_user_specified_name
inputs/0
ь
к
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217054

inputs(
embedding_17_1217037:
ЈУd"
lstm_17_1217041:	d
lstm_17_1217043:	"
lstm_17_1217045:	d"
dense_17_1217048:d
dense_17_1217050:
identityЂ dense_17/StatefulPartitionedCallЂ$embedding_17/StatefulPartitionedCallЂlstm_17/StatefulPartitionedCallЂ,spatial_dropout1d_17/StatefulPartitionedCall
$embedding_17/StatefulPartitionedCallStatefulPartitionedCallinputsembedding_17_1217037*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_embedding_17_layer_call_and_return_conditional_losses_12162632&
$embedding_17/StatefulPartitionedCallН
,spatial_dropout1d_17/StatefulPartitionedCallStatefulPartitionedCall-embedding_17/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12170042.
,spatial_dropout1d_17/StatefulPartitionedCallд
lstm_17/StatefulPartitionedCallStatefulPartitionedCall5spatial_dropout1d_17/StatefulPartitionedCall:output:0lstm_17_1217041lstm_17_1217043lstm_17_1217045*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12169662!
lstm_17/StatefulPartitionedCallЙ
 dense_17/StatefulPartitionedCallStatefulPartitionedCall(lstm_17/StatefulPartitionedCall:output:0dense_17_1217048dense_17_1217050*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12165422"
 dense_17/StatefulPartitionedCall
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_17/StatefulPartitionedCall%^embedding_17/StatefulPartitionedCall ^lstm_17/StatefulPartitionedCall-^spatial_dropout1d_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2L
$embedding_17/StatefulPartitionedCall$embedding_17/StatefulPartitionedCall2B
lstm_17/StatefulPartitionedCalllstm_17/StatefulPartitionedCall2\
,spatial_dropout1d_17/StatefulPartitionedCall,spatial_dropout1d_17/StatefulPartitionedCall:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs
шљ
ш
D__inference_lstm_17_layer_call_and_return_conditional_losses_1219255

inputs=
*lstm_cell_17_split_readvariableop_resource:	d;
,lstm_cell_17_split_1_readvariableop_resource:	7
$lstm_cell_17_readvariableop_resource:	d
identityЂlstm_cell_17/ReadVariableOpЂlstm_cell_17/ReadVariableOp_1Ђlstm_cell_17/ReadVariableOp_2Ђlstm_cell_17/ReadVariableOp_3Ђ!lstm_cell_17/split/ReadVariableOpЂ#lstm_cell_17/split_1/ReadVariableOpЂwhileD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm{
	transpose	Transposeinputstranspose/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2
lstm_cell_17/ones_like/ShapeShapestrided_slice_2:output:0*
T0*
_output_shapes
:2
lstm_cell_17/ones_like/Shape
lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
lstm_cell_17/ones_like/ConstИ
lstm_cell_17/ones_likeFill%lstm_cell_17/ones_like/Shape:output:0%lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like}
lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout/ConstГ
lstm_cell_17/dropout/MulMullstm_cell_17/ones_like:output:0#lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/Mul
lstm_cell_17/dropout/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout/Shapeњ
1lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform#lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2бњ23
1lstm_cell_17/dropout/random_uniform/RandomUniform
#lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2%
#lstm_cell_17/dropout/GreaterEqual/yђ
!lstm_cell_17/dropout/GreaterEqualGreaterEqual:lstm_cell_17/dropout/random_uniform/RandomUniform:output:0,lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_cell_17/dropout/GreaterEqualІ
lstm_cell_17/dropout/CastCast%lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/CastЎ
lstm_cell_17/dropout/Mul_1Mullstm_cell_17/dropout/Mul:z:0lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/Mul_1
lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_1/ConstЙ
lstm_cell_17/dropout_1/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/Mul
lstm_cell_17/dropout_1/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_1/Shape
3lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Лф25
3lstm_cell_17/dropout_1/random_uniform/RandomUniform
%lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_1/GreaterEqual/yњ
#lstm_cell_17/dropout_1/GreaterEqualGreaterEqual<lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_1/GreaterEqualЌ
lstm_cell_17/dropout_1/CastCast'lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/CastЖ
lstm_cell_17/dropout_1/Mul_1Mullstm_cell_17/dropout_1/Mul:z:0lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/Mul_1
lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_2/ConstЙ
lstm_cell_17/dropout_2/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/Mul
lstm_cell_17/dropout_2/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_2/Shape
3lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2шш25
3lstm_cell_17/dropout_2/random_uniform/RandomUniform
%lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_2/GreaterEqual/yњ
#lstm_cell_17/dropout_2/GreaterEqualGreaterEqual<lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_2/GreaterEqualЌ
lstm_cell_17/dropout_2/CastCast'lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/CastЖ
lstm_cell_17/dropout_2/Mul_1Mullstm_cell_17/dropout_2/Mul:z:0lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/Mul_1
lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_3/ConstЙ
lstm_cell_17/dropout_3/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/Mul
lstm_cell_17/dropout_3/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_3/Shapeџ
3lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Пдi25
3lstm_cell_17/dropout_3/random_uniform/RandomUniform
%lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_3/GreaterEqual/yњ
#lstm_cell_17/dropout_3/GreaterEqualGreaterEqual<lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_3/GreaterEqualЌ
lstm_cell_17/dropout_3/CastCast'lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/CastЖ
lstm_cell_17/dropout_3/Mul_1Mullstm_cell_17/dropout_3/Mul:z:0lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/Mul_1~
lstm_cell_17/ones_like_1/ShapeShapezeros:output:0*
T0*
_output_shapes
:2 
lstm_cell_17/ones_like_1/Shape
lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2 
lstm_cell_17/ones_like_1/ConstР
lstm_cell_17/ones_like_1Fill'lstm_cell_17/ones_like_1/Shape:output:0'lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like_1
lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_4/ConstЛ
lstm_cell_17/dropout_4/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/Mul
lstm_cell_17/dropout_4/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_4/Shape
3lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2бђЭ25
3lstm_cell_17/dropout_4/random_uniform/RandomUniform
%lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_4/GreaterEqual/yњ
#lstm_cell_17/dropout_4/GreaterEqualGreaterEqual<lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_4/GreaterEqualЌ
lstm_cell_17/dropout_4/CastCast'lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/CastЖ
lstm_cell_17/dropout_4/Mul_1Mullstm_cell_17/dropout_4/Mul:z:0lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/Mul_1
lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_5/ConstЛ
lstm_cell_17/dropout_5/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/Mul
lstm_cell_17/dropout_5/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_5/Shape
3lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2р25
3lstm_cell_17/dropout_5/random_uniform/RandomUniform
%lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_5/GreaterEqual/yњ
#lstm_cell_17/dropout_5/GreaterEqualGreaterEqual<lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_5/GreaterEqualЌ
lstm_cell_17/dropout_5/CastCast'lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/CastЖ
lstm_cell_17/dropout_5/Mul_1Mullstm_cell_17/dropout_5/Mul:z:0lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/Mul_1
lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_6/ConstЛ
lstm_cell_17/dropout_6/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/Mul
lstm_cell_17/dropout_6/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_6/Shape
3lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЋП25
3lstm_cell_17/dropout_6/random_uniform/RandomUniform
%lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_6/GreaterEqual/yњ
#lstm_cell_17/dropout_6/GreaterEqualGreaterEqual<lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_6/GreaterEqualЌ
lstm_cell_17/dropout_6/CastCast'lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/CastЖ
lstm_cell_17/dropout_6/Mul_1Mullstm_cell_17/dropout_6/Mul:z:0lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/Mul_1
lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_7/ConstЛ
lstm_cell_17/dropout_7/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/Mul
lstm_cell_17/dropout_7/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_7/Shapeџ
3lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ТЪ25
3lstm_cell_17/dropout_7/random_uniform/RandomUniform
%lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_7/GreaterEqual/yњ
#lstm_cell_17/dropout_7/GreaterEqualGreaterEqual<lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_7/GreaterEqualЌ
lstm_cell_17/dropout_7/CastCast'lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/CastЖ
lstm_cell_17/dropout_7/Mul_1Mullstm_cell_17/dropout_7/Mul:z:0lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/Mul_1
lstm_cell_17/mulMulstrided_slice_2:output:0lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul
lstm_cell_17/mul_1Mulstrided_slice_2:output:0 lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_1
lstm_cell_17/mul_2Mulstrided_slice_2:output:0 lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_2
lstm_cell_17/mul_3Mulstrided_slice_2:output:0 lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_3~
lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_cell_17/split/split_dimВ
!lstm_cell_17/split/ReadVariableOpReadVariableOp*lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02#
!lstm_cell_17/split/ReadVariableOpл
lstm_cell_17/splitSplit%lstm_cell_17/split/split_dim:output:0)lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_cell_17/split
lstm_cell_17/MatMulMatMullstm_cell_17/mul:z:0lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul
lstm_cell_17/MatMul_1MatMullstm_cell_17/mul_1:z:0lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_1
lstm_cell_17/MatMul_2MatMullstm_cell_17/mul_2:z:0lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_2
lstm_cell_17/MatMul_3MatMullstm_cell_17/mul_3:z:0lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_3
lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2 
lstm_cell_17/split_1/split_dimД
#lstm_cell_17/split_1/ReadVariableOpReadVariableOp,lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02%
#lstm_cell_17/split_1/ReadVariableOpг
lstm_cell_17/split_1Split'lstm_cell_17/split_1/split_dim:output:0+lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_cell_17/split_1Ї
lstm_cell_17/BiasAddBiasAddlstm_cell_17/MatMul:product:0lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd­
lstm_cell_17/BiasAdd_1BiasAddlstm_cell_17/MatMul_1:product:0lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_1­
lstm_cell_17/BiasAdd_2BiasAddlstm_cell_17/MatMul_2:product:0lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_2­
lstm_cell_17/BiasAdd_3BiasAddlstm_cell_17/MatMul_3:product:0lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_3
lstm_cell_17/mul_4Mulzeros:output:0 lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_4
lstm_cell_17/mul_5Mulzeros:output:0 lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_5
lstm_cell_17/mul_6Mulzeros:output:0 lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_6
lstm_cell_17/mul_7Mulzeros:output:0 lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_7 
lstm_cell_17/ReadVariableOpReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp
 lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2"
 lstm_cell_17/strided_slice/stack
"lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice/stack_1
"lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2$
"lstm_cell_17/strided_slice/stack_2Ъ
lstm_cell_17/strided_sliceStridedSlice#lstm_cell_17/ReadVariableOp:value:0)lstm_cell_17/strided_slice/stack:output:0+lstm_cell_17/strided_slice/stack_1:output:0+lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_sliceЇ
lstm_cell_17/MatMul_4MatMullstm_cell_17/mul_4:z:0#lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_4
lstm_cell_17/addAddV2lstm_cell_17/BiasAdd:output:0lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add
lstm_cell_17/SigmoidSigmoidlstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/SigmoidЄ
lstm_cell_17/ReadVariableOp_1ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_1
"lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice_1/stack
$lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2&
$lstm_cell_17/strided_slice_1/stack_1
$lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_1/stack_2ж
lstm_cell_17/strided_slice_1StridedSlice%lstm_cell_17/ReadVariableOp_1:value:0+lstm_cell_17/strided_slice_1/stack:output:0-lstm_cell_17/strided_slice_1/stack_1:output:0-lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_1Љ
lstm_cell_17/MatMul_5MatMullstm_cell_17/mul_5:z:0%lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_5Ѕ
lstm_cell_17/add_1AddV2lstm_cell_17/BiasAdd_1:output:0lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_1
lstm_cell_17/Sigmoid_1Sigmoidlstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_1
lstm_cell_17/mul_8Mullstm_cell_17/Sigmoid_1:y:0zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_8Є
lstm_cell_17/ReadVariableOp_2ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_2
"lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2$
"lstm_cell_17/strided_slice_2/stack
$lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2&
$lstm_cell_17/strided_slice_2/stack_1
$lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_2/stack_2ж
lstm_cell_17/strided_slice_2StridedSlice%lstm_cell_17/ReadVariableOp_2:value:0+lstm_cell_17/strided_slice_2/stack:output:0-lstm_cell_17/strided_slice_2/stack_1:output:0-lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_2Љ
lstm_cell_17/MatMul_6MatMullstm_cell_17/mul_6:z:0%lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_6Ѕ
lstm_cell_17/add_2AddV2lstm_cell_17/BiasAdd_2:output:0lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_2x
lstm_cell_17/TanhTanhlstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh
lstm_cell_17/mul_9Mullstm_cell_17/Sigmoid:y:0lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_9
lstm_cell_17/add_3AddV2lstm_cell_17/mul_8:z:0lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_3Є
lstm_cell_17/ReadVariableOp_3ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_3
"lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2$
"lstm_cell_17/strided_slice_3/stack
$lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2&
$lstm_cell_17/strided_slice_3/stack_1
$lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_3/stack_2ж
lstm_cell_17/strided_slice_3StridedSlice%lstm_cell_17/ReadVariableOp_3:value:0+lstm_cell_17/strided_slice_3/stack:output:0-lstm_cell_17/strided_slice_3/stack_1:output:0-lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_3Љ
lstm_cell_17/MatMul_7MatMullstm_cell_17/mul_7:z:0%lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_7Ѕ
lstm_cell_17/add_4AddV2lstm_cell_17/BiasAdd_3:output:0lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_4
lstm_cell_17/Sigmoid_2Sigmoidlstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_2|
lstm_cell_17/Tanh_1Tanhlstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh_1
lstm_cell_17/mul_10Mullstm_cell_17/Sigmoid_2:y:0lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_10
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterц
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0*lstm_cell_17_split_readvariableop_resource,lstm_cell_17_split_1_readvariableop_resource$lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1219057*
condR
while_cond_1219056*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeщ
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*,
_output_shapes
:џџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permІ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtimeМ
IdentityIdentitystrided_slice_3:output:0^lstm_cell_17/ReadVariableOp^lstm_cell_17/ReadVariableOp_1^lstm_cell_17/ReadVariableOp_2^lstm_cell_17/ReadVariableOp_3"^lstm_cell_17/split/ReadVariableOp$^lstm_cell_17/split_1/ReadVariableOp^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:џџџџџџџџџd: : : 2:
lstm_cell_17/ReadVariableOplstm_cell_17/ReadVariableOp2>
lstm_cell_17/ReadVariableOp_1lstm_cell_17/ReadVariableOp_12>
lstm_cell_17/ReadVariableOp_2lstm_cell_17/ReadVariableOp_22>
lstm_cell_17/ReadVariableOp_3lstm_cell_17/ReadVariableOp_32F
!lstm_cell_17/split/ReadVariableOp!lstm_cell_17/split/ReadVariableOp2J
#lstm_cell_17/split_1/ReadVariableOp#lstm_cell_17/split_1/ReadVariableOp2
whilewhile:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
у
Ќ
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1219537

inputs
states_0
states_10
split_readvariableop_resource:	d.
split_1_readvariableop_resource:	*
readvariableop_resource:	d
identity

identity_1

identity_2ЂReadVariableOpЂReadVariableOp_1ЂReadVariableOp_2ЂReadVariableOp_3Ђsplit/ReadVariableOpЂsplit_1/ReadVariableOpX
ones_like/ShapeShapeinputs*
T0*
_output_shapes
:2
ones_like/Shapeg
ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like/Const
	ones_likeFillones_like/Shape:output:0ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	ones_likec
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout/Const
dropout/MulMulones_like:output:0dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout/Mul`
dropout/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout/Shapeг
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed22&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout/GreaterEqual/yО
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout/Castz
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout/Mul_1g
dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_1/Const
dropout_1/MulMulones_like:output:0dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_1/Muld
dropout_1/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout_1/Shapeй
&dropout_1/random_uniform/RandomUniformRandomUniformdropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2І2(
&dropout_1/random_uniform/RandomUniformy
dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_1/GreaterEqual/yЦ
dropout_1/GreaterEqualGreaterEqual/dropout_1/random_uniform/RandomUniform:output:0!dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_1/GreaterEqual
dropout_1/CastCastdropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_1/Cast
dropout_1/Mul_1Muldropout_1/Mul:z:0dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_1/Mul_1g
dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_2/Const
dropout_2/MulMulones_like:output:0dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_2/Muld
dropout_2/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout_2/Shapeй
&dropout_2/random_uniform/RandomUniformRandomUniformdropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Ќу2(
&dropout_2/random_uniform/RandomUniformy
dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_2/GreaterEqual/yЦ
dropout_2/GreaterEqualGreaterEqual/dropout_2/random_uniform/RandomUniform:output:0!dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_2/GreaterEqual
dropout_2/CastCastdropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_2/Cast
dropout_2/Mul_1Muldropout_2/Mul:z:0dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_2/Mul_1g
dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_3/Const
dropout_3/MulMulones_like:output:0dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_3/Muld
dropout_3/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout_3/Shapeй
&dropout_3/random_uniform/RandomUniformRandomUniformdropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2еж2(
&dropout_3/random_uniform/RandomUniformy
dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_3/GreaterEqual/yЦ
dropout_3/GreaterEqualGreaterEqual/dropout_3/random_uniform/RandomUniform:output:0!dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_3/GreaterEqual
dropout_3/CastCastdropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_3/Cast
dropout_3/Mul_1Muldropout_3/Mul:z:0dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_3/Mul_1^
ones_like_1/ShapeShapestates_0*
T0*
_output_shapes
:2
ones_like_1/Shapek
ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like_1/Const
ones_like_1Fillones_like_1/Shape:output:0ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
ones_like_1g
dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_4/Const
dropout_4/MulMulones_like_1:output:0dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_4/Mulf
dropout_4/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_4/Shapeй
&dropout_4/random_uniform/RandomUniformRandomUniformdropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ыПќ2(
&dropout_4/random_uniform/RandomUniformy
dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_4/GreaterEqual/yЦ
dropout_4/GreaterEqualGreaterEqual/dropout_4/random_uniform/RandomUniform:output:0!dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_4/GreaterEqual
dropout_4/CastCastdropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_4/Cast
dropout_4/Mul_1Muldropout_4/Mul:z:0dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_4/Mul_1g
dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_5/Const
dropout_5/MulMulones_like_1:output:0dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_5/Mulf
dropout_5/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_5/Shapeи
&dropout_5/random_uniform/RandomUniformRandomUniformdropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2б^2(
&dropout_5/random_uniform/RandomUniformy
dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_5/GreaterEqual/yЦ
dropout_5/GreaterEqualGreaterEqual/dropout_5/random_uniform/RandomUniform:output:0!dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_5/GreaterEqual
dropout_5/CastCastdropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_5/Cast
dropout_5/Mul_1Muldropout_5/Mul:z:0dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_5/Mul_1g
dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_6/Const
dropout_6/MulMulones_like_1:output:0dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_6/Mulf
dropout_6/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_6/Shapeй
&dropout_6/random_uniform/RandomUniformRandomUniformdropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ѓХ2(
&dropout_6/random_uniform/RandomUniformy
dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_6/GreaterEqual/yЦ
dropout_6/GreaterEqualGreaterEqual/dropout_6/random_uniform/RandomUniform:output:0!dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_6/GreaterEqual
dropout_6/CastCastdropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_6/Cast
dropout_6/Mul_1Muldropout_6/Mul:z:0dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_6/Mul_1g
dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_7/Const
dropout_7/MulMulones_like_1:output:0dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_7/Mulf
dropout_7/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_7/Shapeи
&dropout_7/random_uniform/RandomUniformRandomUniformdropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2M2(
&dropout_7/random_uniform/RandomUniformy
dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_7/GreaterEqual/yЦ
dropout_7/GreaterEqualGreaterEqual/dropout_7/random_uniform/RandomUniform:output:0!dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_7/GreaterEqual
dropout_7/CastCastdropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_7/Cast
dropout_7/Mul_1Muldropout_7/Mul:z:0dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_7/Mul_1^
mulMulinputsdropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
muld
mul_1Mulinputsdropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_1d
mul_2Mulinputsdropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_2d
mul_3Mulinputsdropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_3d
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
split/split_dim
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*
_output_shapes
:	d*
dtype02
split/ReadVariableOpЇ
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
splite
MatMulMatMulmul:z:0split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
MatMulk
MatMul_1MatMul	mul_1:z:0split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_1k
MatMul_2MatMul	mul_2:z:0split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_2k
MatMul_3MatMul	mul_3:z:0split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_3h
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2
split_1/split_dim
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*
_output_shapes	
:*
dtype02
split_1/ReadVariableOp
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2	
split_1s
BiasAddBiasAddMatMul:product:0split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
BiasAddy
	BiasAdd_1BiasAddMatMul_1:product:0split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_1y
	BiasAdd_2BiasAddMatMul_2:product:0split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_2y
	BiasAdd_3BiasAddMatMul_3:product:0split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_3f
mul_4Mulstates_0dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_4f
mul_5Mulstates_0dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_5f
mul_6Mulstates_0dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_6f
mul_7Mulstates_0dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_7y
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp{
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice/stack
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice/stack_1
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice/stack_2ќ
strided_sliceStridedSliceReadVariableOp:value:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slices
MatMul_4MatMul	mul_4:z:0strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_4k
addAddV2BiasAdd:output:0MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
addX
SigmoidSigmoidadd:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
Sigmoid}
ReadVariableOp_1ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_1
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice_1/stack
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_1/stack_1
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_1/stack_2
strided_slice_1StridedSliceReadVariableOp_1:value:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_1u
MatMul_5MatMul	mul_5:z:0strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_5q
add_1AddV2BiasAdd_1:output:0MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_1^
	Sigmoid_1Sigmoid	add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_1`
mul_8MulSigmoid_1:y:0states_1*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_8}
ReadVariableOp_2ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_2
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_2/stack
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_2/stack_1
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_2/stack_2
strided_slice_2StridedSliceReadVariableOp_2:value:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_2u
MatMul_6MatMul	mul_6:z:0strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_6q
add_2AddV2BiasAdd_2:output:0MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_2Q
TanhTanh	add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh^
mul_9MulSigmoid:y:0Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_9_
add_3AddV2	mul_8:z:0	mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_3}
ReadVariableOp_3ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_3
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_3/stack
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice_3/stack_1
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_3/stack_2
strided_slice_3StridedSliceReadVariableOp_3:value:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_3u
MatMul_7MatMul	mul_7:z:0strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_7q
add_4AddV2BiasAdd_3:output:0MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_4^
	Sigmoid_2Sigmoid	add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_2U
Tanh_1Tanh	add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh_1d
mul_10MulSigmoid_2:y:0
Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_10и
IdentityIdentity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identityм

Identity_1Identity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_1л

Identity_2Identity	add_3:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*R
_input_shapesA
?:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd: : : 2 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_12$
ReadVariableOp_2ReadVariableOp_22$
ReadVariableOp_3ReadVariableOp_32,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/0:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/1
к
Ш
while_cond_1215590
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1215590___redundant_placeholder05
1while_while_cond_1215590___redundant_placeholder15
1while_while_cond_1215590___redundant_placeholder25
1while_while_cond_1215590___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
Д
З
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217106
embedding_17_input(
embedding_17_1217089:
ЈУd"
lstm_17_1217093:	d
lstm_17_1217095:	"
lstm_17_1217097:	d"
dense_17_1217100:d
dense_17_1217102:
identityЂ dense_17/StatefulPartitionedCallЂ$embedding_17/StatefulPartitionedCallЂlstm_17/StatefulPartitionedCallЄ
$embedding_17/StatefulPartitionedCallStatefulPartitionedCallembedding_17_inputembedding_17_1217089*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_embedding_17_layer_call_and_return_conditional_losses_12162632&
$embedding_17/StatefulPartitionedCallЅ
$spatial_dropout1d_17/PartitionedCallPartitionedCall-embedding_17/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12162712&
$spatial_dropout1d_17/PartitionedCallЬ
lstm_17/StatefulPartitionedCallStatefulPartitionedCall-spatial_dropout1d_17/PartitionedCall:output:0lstm_17_1217093lstm_17_1217095lstm_17_1217097*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12165232!
lstm_17/StatefulPartitionedCallЙ
 dense_17/StatefulPartitionedCallStatefulPartitionedCall(lstm_17/StatefulPartitionedCall:output:0dense_17_1217100dense_17_1217102*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12165422"
 dense_17/StatefulPartitionedCallщ
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_17/StatefulPartitionedCall%^embedding_17/StatefulPartitionedCall ^lstm_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2L
$embedding_17/StatefulPartitionedCall$embedding_17/StatefulPartitionedCall2B
lstm_17/StatefulPartitionedCalllstm_17/StatefulPartitionedCall:\ X
(
_output_shapes
:џџџџџџџџџ
,
_user_specified_nameembedding_17_input
ії
Ѕ	
while_body_1216768
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0E
2while_lstm_cell_17_split_readvariableop_resource_0:	dC
4while_lstm_cell_17_split_1_readvariableop_resource_0:	?
,while_lstm_cell_17_readvariableop_resource_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorC
0while_lstm_cell_17_split_readvariableop_resource:	dA
2while_lstm_cell_17_split_1_readvariableop_resource:	=
*while_lstm_cell_17_readvariableop_resource:	dЂ!while/lstm_cell_17/ReadVariableOpЂ#while/lstm_cell_17/ReadVariableOp_1Ђ#while/lstm_cell_17/ReadVariableOp_2Ђ#while/lstm_cell_17/ReadVariableOp_3Ђ'while/lstm_cell_17/split/ReadVariableOpЂ)while/lstm_cell_17/split_1/ReadVariableOpУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemЈ
"while/lstm_cell_17/ones_like/ShapeShape0while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/ones_like/Shape
"while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2$
"while/lstm_cell_17/ones_like/Constа
while/lstm_cell_17/ones_likeFill+while/lstm_cell_17/ones_like/Shape:output:0+while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/ones_like
 while/lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2"
 while/lstm_cell_17/dropout/ConstЫ
while/lstm_cell_17/dropout/MulMul%while/lstm_cell_17/ones_like:output:0)while/lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/dropout/Mul
 while/lstm_cell_17/dropout/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2"
 while/lstm_cell_17/dropout/Shape
7while/lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform)while/lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЩНХ29
7while/lstm_cell_17/dropout/random_uniform/RandomUniform
)while/lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2+
)while/lstm_cell_17/dropout/GreaterEqual/y
'while/lstm_cell_17/dropout/GreaterEqualGreaterEqual@while/lstm_cell_17/dropout/random_uniform/RandomUniform:output:02while/lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2)
'while/lstm_cell_17/dropout/GreaterEqualИ
while/lstm_cell_17/dropout/CastCast+while/lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2!
while/lstm_cell_17/dropout/CastЦ
 while/lstm_cell_17/dropout/Mul_1Mul"while/lstm_cell_17/dropout/Mul:z:0#while/lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout/Mul_1
"while/lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_1/Constб
 while/lstm_cell_17/dropout_1/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_1/Mul
"while/lstm_cell_17/dropout_1/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_1/Shape
9while/lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2К2;
9while/lstm_cell_17/dropout_1/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_1/GreaterEqual/y
)while/lstm_cell_17/dropout_1/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_1/GreaterEqualО
!while/lstm_cell_17/dropout_1/CastCast-while/lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_1/CastЮ
"while/lstm_cell_17/dropout_1/Mul_1Mul$while/lstm_cell_17/dropout_1/Mul:z:0%while/lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_1/Mul_1
"while/lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_2/Constб
 while/lstm_cell_17/dropout_2/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_2/Mul
"while/lstm_cell_17/dropout_2/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_2/Shape
9while/lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Је2;
9while/lstm_cell_17/dropout_2/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_2/GreaterEqual/y
)while/lstm_cell_17/dropout_2/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_2/GreaterEqualО
!while/lstm_cell_17/dropout_2/CastCast-while/lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_2/CastЮ
"while/lstm_cell_17/dropout_2/Mul_1Mul$while/lstm_cell_17/dropout_2/Mul:z:0%while/lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_2/Mul_1
"while/lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_3/Constб
 while/lstm_cell_17/dropout_3/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_3/Mul
"while/lstm_cell_17/dropout_3/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_3/Shape
9while/lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ИСц2;
9while/lstm_cell_17/dropout_3/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_3/GreaterEqual/y
)while/lstm_cell_17/dropout_3/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_3/GreaterEqualО
!while/lstm_cell_17/dropout_3/CastCast-while/lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_3/CastЮ
"while/lstm_cell_17/dropout_3/Mul_1Mul$while/lstm_cell_17/dropout_3/Mul:z:0%while/lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_3/Mul_1
$while/lstm_cell_17/ones_like_1/ShapeShapewhile_placeholder_2*
T0*
_output_shapes
:2&
$while/lstm_cell_17/ones_like_1/Shape
$while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$while/lstm_cell_17/ones_like_1/Constи
while/lstm_cell_17/ones_like_1Fill-while/lstm_cell_17/ones_like_1/Shape:output:0-while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/ones_like_1
"while/lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_4/Constг
 while/lstm_cell_17/dropout_4/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_4/Mul
"while/lstm_cell_17/dropout_4/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_4/Shape
9while/lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ыњІ2;
9while/lstm_cell_17/dropout_4/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_4/GreaterEqual/y
)while/lstm_cell_17/dropout_4/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_4/GreaterEqualО
!while/lstm_cell_17/dropout_4/CastCast-while/lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_4/CastЮ
"while/lstm_cell_17/dropout_4/Mul_1Mul$while/lstm_cell_17/dropout_4/Mul:z:0%while/lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_4/Mul_1
"while/lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_5/Constг
 while/lstm_cell_17/dropout_5/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_5/Mul
"while/lstm_cell_17/dropout_5/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_5/Shape
9while/lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЇќК2;
9while/lstm_cell_17/dropout_5/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_5/GreaterEqual/y
)while/lstm_cell_17/dropout_5/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_5/GreaterEqualО
!while/lstm_cell_17/dropout_5/CastCast-while/lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_5/CastЮ
"while/lstm_cell_17/dropout_5/Mul_1Mul$while/lstm_cell_17/dropout_5/Mul:z:0%while/lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_5/Mul_1
"while/lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_6/Constг
 while/lstm_cell_17/dropout_6/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_6/Mul
"while/lstm_cell_17/dropout_6/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_6/Shape
9while/lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2В2;
9while/lstm_cell_17/dropout_6/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_6/GreaterEqual/y
)while/lstm_cell_17/dropout_6/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_6/GreaterEqualО
!while/lstm_cell_17/dropout_6/CastCast-while/lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_6/CastЮ
"while/lstm_cell_17/dropout_6/Mul_1Mul$while/lstm_cell_17/dropout_6/Mul:z:0%while/lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_6/Mul_1
"while/lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_7/Constг
 while/lstm_cell_17/dropout_7/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_7/Mul
"while/lstm_cell_17/dropout_7/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_7/Shape
9while/lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2еЧ 2;
9while/lstm_cell_17/dropout_7/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_7/GreaterEqual/y
)while/lstm_cell_17/dropout_7/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_7/GreaterEqualО
!while/lstm_cell_17/dropout_7/CastCast-while/lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_7/CastЮ
"while/lstm_cell_17/dropout_7/Mul_1Mul$while/lstm_cell_17/dropout_7/Mul:z:0%while/lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_7/Mul_1С
while/lstm_cell_17/mulMul0while/TensorArrayV2Read/TensorListGetItem:item:0$while/lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mulЧ
while/lstm_cell_17/mul_1Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_1Ч
while/lstm_cell_17/mul_2Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_2Ч
while/lstm_cell_17/mul_3Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_3
"while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2$
"while/lstm_cell_17/split/split_dimЦ
'while/lstm_cell_17/split/ReadVariableOpReadVariableOp2while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype02)
'while/lstm_cell_17/split/ReadVariableOpѓ
while/lstm_cell_17/splitSplit+while/lstm_cell_17/split/split_dim:output:0/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
while/lstm_cell_17/splitБ
while/lstm_cell_17/MatMulMatMulwhile/lstm_cell_17/mul:z:0!while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMulЗ
while/lstm_cell_17/MatMul_1MatMulwhile/lstm_cell_17/mul_1:z:0!while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_1З
while/lstm_cell_17/MatMul_2MatMulwhile/lstm_cell_17/mul_2:z:0!while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_2З
while/lstm_cell_17/MatMul_3MatMulwhile/lstm_cell_17/mul_3:z:0!while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_3
$while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2&
$while/lstm_cell_17/split_1/split_dimШ
)while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype02+
)while/lstm_cell_17/split_1/ReadVariableOpы
while/lstm_cell_17/split_1Split-while/lstm_cell_17/split_1/split_dim:output:01while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
while/lstm_cell_17/split_1П
while/lstm_cell_17/BiasAddBiasAdd#while/lstm_cell_17/MatMul:product:0#while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAddХ
while/lstm_cell_17/BiasAdd_1BiasAdd%while/lstm_cell_17/MatMul_1:product:0#while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_1Х
while/lstm_cell_17/BiasAdd_2BiasAdd%while/lstm_cell_17/MatMul_2:product:0#while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_2Х
while/lstm_cell_17/BiasAdd_3BiasAdd%while/lstm_cell_17/MatMul_3:product:0#while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_3Њ
while/lstm_cell_17/mul_4Mulwhile_placeholder_2&while/lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_4Њ
while/lstm_cell_17/mul_5Mulwhile_placeholder_2&while/lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_5Њ
while/lstm_cell_17/mul_6Mulwhile_placeholder_2&while/lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_6Њ
while/lstm_cell_17/mul_7Mulwhile_placeholder_2&while/lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_7Д
!while/lstm_cell_17/ReadVariableOpReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02#
!while/lstm_cell_17/ReadVariableOpЁ
&while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2(
&while/lstm_cell_17/strided_slice/stackЅ
(while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice/stack_1Ѕ
(while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2*
(while/lstm_cell_17/strided_slice/stack_2ю
 while/lstm_cell_17/strided_sliceStridedSlice)while/lstm_cell_17/ReadVariableOp:value:0/while/lstm_cell_17/strided_slice/stack:output:01while/lstm_cell_17/strided_slice/stack_1:output:01while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2"
 while/lstm_cell_17/strided_sliceП
while/lstm_cell_17/MatMul_4MatMulwhile/lstm_cell_17/mul_4:z:0)while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_4З
while/lstm_cell_17/addAddV2#while/lstm_cell_17/BiasAdd:output:0%while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add
while/lstm_cell_17/SigmoidSigmoidwhile/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/SigmoidИ
#while/lstm_cell_17/ReadVariableOp_1ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_1Ѕ
(while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice_1/stackЉ
*while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*while/lstm_cell_17/strided_slice_1/stack_1Љ
*while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_1/stack_2њ
"while/lstm_cell_17/strided_slice_1StridedSlice+while/lstm_cell_17/ReadVariableOp_1:value:01while/lstm_cell_17/strided_slice_1/stack:output:03while/lstm_cell_17/strided_slice_1/stack_1:output:03while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_1С
while/lstm_cell_17/MatMul_5MatMulwhile/lstm_cell_17/mul_5:z:0+while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_5Н
while/lstm_cell_17/add_1AddV2%while/lstm_cell_17/BiasAdd_1:output:0%while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_1
while/lstm_cell_17/Sigmoid_1Sigmoidwhile/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_1Є
while/lstm_cell_17/mul_8Mul while/lstm_cell_17/Sigmoid_1:y:0while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_8И
#while/lstm_cell_17/ReadVariableOp_2ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_2Ѕ
(while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2*
(while/lstm_cell_17/strided_slice_2/stackЉ
*while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*while/lstm_cell_17/strided_slice_2/stack_1Љ
*while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_2/stack_2њ
"while/lstm_cell_17/strided_slice_2StridedSlice+while/lstm_cell_17/ReadVariableOp_2:value:01while/lstm_cell_17/strided_slice_2/stack:output:03while/lstm_cell_17/strided_slice_2/stack_1:output:03while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_2С
while/lstm_cell_17/MatMul_6MatMulwhile/lstm_cell_17/mul_6:z:0+while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_6Н
while/lstm_cell_17/add_2AddV2%while/lstm_cell_17/BiasAdd_2:output:0%while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_2
while/lstm_cell_17/TanhTanhwhile/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/TanhЊ
while/lstm_cell_17/mul_9Mulwhile/lstm_cell_17/Sigmoid:y:0while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_9Ћ
while/lstm_cell_17/add_3AddV2while/lstm_cell_17/mul_8:z:0while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_3И
#while/lstm_cell_17/ReadVariableOp_3ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_3Ѕ
(while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2*
(while/lstm_cell_17/strided_slice_3/stackЉ
*while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2,
*while/lstm_cell_17/strided_slice_3/stack_1Љ
*while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_3/stack_2њ
"while/lstm_cell_17/strided_slice_3StridedSlice+while/lstm_cell_17/ReadVariableOp_3:value:01while/lstm_cell_17/strided_slice_3/stack:output:03while/lstm_cell_17/strided_slice_3/stack_1:output:03while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_3С
while/lstm_cell_17/MatMul_7MatMulwhile/lstm_cell_17/mul_7:z:0+while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_7Н
while/lstm_cell_17/add_4AddV2%while/lstm_cell_17/BiasAdd_3:output:0%while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_4
while/lstm_cell_17/Sigmoid_2Sigmoidwhile/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_2
while/lstm_cell_17/Tanh_1Tanhwhile/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Tanh_1А
while/lstm_cell_17/mul_10Mul while/lstm_cell_17/Sigmoid_2:y:0while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_10с
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1Ъ
while/IdentityIdentitywhile/add_1:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identityн
while/Identity_1Identitywhile_while_maximum_iterations"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_1Ь
while/Identity_2Identitywhile/add:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_2љ
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_3э
while/Identity_4Identitywhile/lstm_cell_17/mul_10:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4ь
while/Identity_5Identitywhile/lstm_cell_17/add_3:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"Z
*while_lstm_cell_17_readvariableop_resource,while_lstm_cell_17_readvariableop_resource_0"j
2while_lstm_cell_17_split_1_readvariableop_resource4while_lstm_cell_17_split_1_readvariableop_resource_0"f
0while_lstm_cell_17_split_readvariableop_resource2while_lstm_cell_17_split_readvariableop_resource_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2F
!while/lstm_cell_17/ReadVariableOp!while/lstm_cell_17/ReadVariableOp2J
#while/lstm_cell_17/ReadVariableOp_1#while/lstm_cell_17/ReadVariableOp_12J
#while/lstm_cell_17/ReadVariableOp_2#while/lstm_cell_17/ReadVariableOp_22J
#while/lstm_cell_17/ReadVariableOp_3#while/lstm_cell_17/ReadVariableOp_32R
'while/lstm_cell_17/split/ReadVariableOp'while/lstm_cell_17/split/ReadVariableOp2V
)while/lstm_cell_17/split_1/ReadVariableOp)while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 

o
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217929

inputs

identity_1_
IdentityIdentityinputs*
T0*,
_output_shapes
:џџџџџџџџџd2

Identityn

Identity_1IdentityIdentity:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:џџџџџџџџџd:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
иM
Њ
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1215577

inputs

states
states_10
split_readvariableop_resource:	d.
split_1_readvariableop_resource:	*
readvariableop_resource:	d
identity

identity_1

identity_2ЂReadVariableOpЂReadVariableOp_1ЂReadVariableOp_2ЂReadVariableOp_3Ђsplit/ReadVariableOpЂsplit_1/ReadVariableOpX
ones_like/ShapeShapeinputs*
T0*
_output_shapes
:2
ones_like/Shapeg
ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like/Const
	ones_likeFillones_like/Shape:output:0ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	ones_like\
ones_like_1/ShapeShapestates*
T0*
_output_shapes
:2
ones_like_1/Shapek
ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like_1/Const
ones_like_1Fillones_like_1/Shape:output:0ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
ones_like_1_
mulMulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mulc
mul_1Mulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_1c
mul_2Mulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_2c
mul_3Mulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_3d
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
split/split_dim
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*
_output_shapes
:	d*
dtype02
split/ReadVariableOpЇ
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
splite
MatMulMatMulmul:z:0split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
MatMulk
MatMul_1MatMul	mul_1:z:0split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_1k
MatMul_2MatMul	mul_2:z:0split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_2k
MatMul_3MatMul	mul_3:z:0split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_3h
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2
split_1/split_dim
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*
_output_shapes	
:*
dtype02
split_1/ReadVariableOp
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2	
split_1s
BiasAddBiasAddMatMul:product:0split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
BiasAddy
	BiasAdd_1BiasAddMatMul_1:product:0split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_1y
	BiasAdd_2BiasAddMatMul_2:product:0split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_2y
	BiasAdd_3BiasAddMatMul_3:product:0split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_3e
mul_4Mulstatesones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_4e
mul_5Mulstatesones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_5e
mul_6Mulstatesones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_6e
mul_7Mulstatesones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_7y
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp{
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice/stack
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice/stack_1
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice/stack_2ќ
strided_sliceStridedSliceReadVariableOp:value:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slices
MatMul_4MatMul	mul_4:z:0strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_4k
addAddV2BiasAdd:output:0MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
addX
SigmoidSigmoidadd:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
Sigmoid}
ReadVariableOp_1ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_1
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice_1/stack
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_1/stack_1
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_1/stack_2
strided_slice_1StridedSliceReadVariableOp_1:value:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_1u
MatMul_5MatMul	mul_5:z:0strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_5q
add_1AddV2BiasAdd_1:output:0MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_1^
	Sigmoid_1Sigmoid	add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_1`
mul_8MulSigmoid_1:y:0states_1*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_8}
ReadVariableOp_2ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_2
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_2/stack
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_2/stack_1
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_2/stack_2
strided_slice_2StridedSliceReadVariableOp_2:value:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_2u
MatMul_6MatMul	mul_6:z:0strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_6q
add_2AddV2BiasAdd_2:output:0MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_2Q
TanhTanh	add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh^
mul_9MulSigmoid:y:0Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_9_
add_3AddV2	mul_8:z:0	mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_3}
ReadVariableOp_3ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_3
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_3/stack
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice_3/stack_1
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_3/stack_2
strided_slice_3StridedSliceReadVariableOp_3:value:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_3u
MatMul_7MatMul	mul_7:z:0strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_7q
add_4AddV2BiasAdd_3:output:0MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_4^
	Sigmoid_2Sigmoid	add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_2U
Tanh_1Tanh	add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh_1d
mul_10MulSigmoid_2:y:0
Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_10и
IdentityIdentity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identityм

Identity_1Identity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_1л

Identity_2Identity	add_3:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*R
_input_shapesA
?:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd: : : 2 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_12$
ReadVariableOp_2ReadVariableOp_22$
ReadVariableOp_3ReadVariableOp_32,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs:OK
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_namestates:OK
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_namestates
і
o
6__inference_spatial_dropout1d_17_layer_call_fn_1217897

inputs
identityЂStatefulPartitionedCallь
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12170042
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:џџџџџџџџџd22
StatefulPartitionedCallStatefulPartitionedCall:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
аЉ
Н
lstm_17_while_body_1217309,
(lstm_17_while_lstm_17_while_loop_counter2
.lstm_17_while_lstm_17_while_maximum_iterations
lstm_17_while_placeholder
lstm_17_while_placeholder_1
lstm_17_while_placeholder_2
lstm_17_while_placeholder_3+
'lstm_17_while_lstm_17_strided_slice_1_0g
clstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensor_0M
:lstm_17_while_lstm_cell_17_split_readvariableop_resource_0:	dK
<lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0:	G
4lstm_17_while_lstm_cell_17_readvariableop_resource_0:	d
lstm_17_while_identity
lstm_17_while_identity_1
lstm_17_while_identity_2
lstm_17_while_identity_3
lstm_17_while_identity_4
lstm_17_while_identity_5)
%lstm_17_while_lstm_17_strided_slice_1e
alstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensorK
8lstm_17_while_lstm_cell_17_split_readvariableop_resource:	dI
:lstm_17_while_lstm_cell_17_split_1_readvariableop_resource:	E
2lstm_17_while_lstm_cell_17_readvariableop_resource:	dЂ)lstm_17/while/lstm_cell_17/ReadVariableOpЂ+lstm_17/while/lstm_cell_17/ReadVariableOp_1Ђ+lstm_17/while/lstm_cell_17/ReadVariableOp_2Ђ+lstm_17/while/lstm_cell_17/ReadVariableOp_3Ђ/lstm_17/while/lstm_cell_17/split/ReadVariableOpЂ1lstm_17/while/lstm_cell_17/split_1/ReadVariableOpг
?lstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2A
?lstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shape
1lstm_17/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemclstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensor_0lstm_17_while_placeholderHlstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype023
1lstm_17/while/TensorArrayV2Read/TensorListGetItemР
*lstm_17/while/lstm_cell_17/ones_like/ShapeShape8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/ones_like/Shape
*lstm_17/while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2,
*lstm_17/while/lstm_cell_17/ones_like/Const№
$lstm_17/while/lstm_cell_17/ones_likeFill3lstm_17/while/lstm_cell_17/ones_like/Shape:output:03lstm_17/while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/ones_likeЇ
,lstm_17/while/lstm_cell_17/ones_like_1/ShapeShapelstm_17_while_placeholder_2*
T0*
_output_shapes
:2.
,lstm_17/while/lstm_cell_17/ones_like_1/ShapeЁ
,lstm_17/while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2.
,lstm_17/while/lstm_cell_17/ones_like_1/Constј
&lstm_17/while/lstm_cell_17/ones_like_1Fill5lstm_17/while/lstm_cell_17/ones_like_1/Shape:output:05lstm_17/while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2(
&lstm_17/while/lstm_cell_17/ones_like_1т
lstm_17/while/lstm_cell_17/mulMul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/while/lstm_cell_17/mulц
 lstm_17/while/lstm_cell_17/mul_1Mul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_1ц
 lstm_17/while/lstm_cell_17/mul_2Mul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_2ц
 lstm_17/while/lstm_cell_17/mul_3Mul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_3
*lstm_17/while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2,
*lstm_17/while/lstm_cell_17/split/split_dimо
/lstm_17/while/lstm_cell_17/split/ReadVariableOpReadVariableOp:lstm_17_while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype021
/lstm_17/while/lstm_cell_17/split/ReadVariableOp
 lstm_17/while/lstm_cell_17/splitSplit3lstm_17/while/lstm_cell_17/split/split_dim:output:07lstm_17/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2"
 lstm_17/while/lstm_cell_17/splitб
!lstm_17/while/lstm_cell_17/MatMulMatMul"lstm_17/while/lstm_cell_17/mul:z:0)lstm_17/while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_17/while/lstm_cell_17/MatMulз
#lstm_17/while/lstm_cell_17/MatMul_1MatMul$lstm_17/while/lstm_cell_17/mul_1:z:0)lstm_17/while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_1з
#lstm_17/while/lstm_cell_17/MatMul_2MatMul$lstm_17/while/lstm_cell_17/mul_2:z:0)lstm_17/while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_2з
#lstm_17/while/lstm_cell_17/MatMul_3MatMul$lstm_17/while/lstm_cell_17/mul_3:z:0)lstm_17/while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_3
,lstm_17/while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2.
,lstm_17/while/lstm_cell_17/split_1/split_dimр
1lstm_17/while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp<lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype023
1lstm_17/while/lstm_cell_17/split_1/ReadVariableOp
"lstm_17/while/lstm_cell_17/split_1Split5lstm_17/while/lstm_cell_17/split_1/split_dim:output:09lstm_17/while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2$
"lstm_17/while/lstm_cell_17/split_1п
"lstm_17/while/lstm_cell_17/BiasAddBiasAdd+lstm_17/while/lstm_cell_17/MatMul:product:0+lstm_17/while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/while/lstm_cell_17/BiasAddх
$lstm_17/while/lstm_cell_17/BiasAdd_1BiasAdd-lstm_17/while/lstm_cell_17/MatMul_1:product:0+lstm_17/while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/BiasAdd_1х
$lstm_17/while/lstm_cell_17/BiasAdd_2BiasAdd-lstm_17/while/lstm_cell_17/MatMul_2:product:0+lstm_17/while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/BiasAdd_2х
$lstm_17/while/lstm_cell_17/BiasAdd_3BiasAdd-lstm_17/while/lstm_cell_17/MatMul_3:product:0+lstm_17/while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/BiasAdd_3Ы
 lstm_17/while/lstm_cell_17/mul_4Mullstm_17_while_placeholder_2/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_4Ы
 lstm_17/while/lstm_cell_17/mul_5Mullstm_17_while_placeholder_2/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_5Ы
 lstm_17/while/lstm_cell_17/mul_6Mullstm_17_while_placeholder_2/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_6Ы
 lstm_17/while/lstm_cell_17/mul_7Mullstm_17_while_placeholder_2/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_7Ь
)lstm_17/while/lstm_cell_17/ReadVariableOpReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02+
)lstm_17/while/lstm_cell_17/ReadVariableOpБ
.lstm_17/while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        20
.lstm_17/while/lstm_cell_17/strided_slice/stackЕ
0lstm_17/while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   22
0lstm_17/while/lstm_cell_17/strided_slice/stack_1Е
0lstm_17/while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      22
0lstm_17/while/lstm_cell_17/strided_slice/stack_2
(lstm_17/while/lstm_cell_17/strided_sliceStridedSlice1lstm_17/while/lstm_cell_17/ReadVariableOp:value:07lstm_17/while/lstm_cell_17/strided_slice/stack:output:09lstm_17/while/lstm_cell_17/strided_slice/stack_1:output:09lstm_17/while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2*
(lstm_17/while/lstm_cell_17/strided_sliceп
#lstm_17/while/lstm_cell_17/MatMul_4MatMul$lstm_17/while/lstm_cell_17/mul_4:z:01lstm_17/while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_4з
lstm_17/while/lstm_cell_17/addAddV2+lstm_17/while/lstm_cell_17/BiasAdd:output:0-lstm_17/while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/while/lstm_cell_17/addЉ
"lstm_17/while/lstm_cell_17/SigmoidSigmoid"lstm_17/while/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/while/lstm_cell_17/Sigmoidа
+lstm_17/while/lstm_cell_17/ReadVariableOp_1ReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02-
+lstm_17/while/lstm_cell_17/ReadVariableOp_1Е
0lstm_17/while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   22
0lstm_17/while/lstm_cell_17/strided_slice_1/stackЙ
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   24
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_1Й
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      24
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_2Њ
*lstm_17/while/lstm_cell_17/strided_slice_1StridedSlice3lstm_17/while/lstm_cell_17/ReadVariableOp_1:value:09lstm_17/while/lstm_cell_17/strided_slice_1/stack:output:0;lstm_17/while/lstm_cell_17/strided_slice_1/stack_1:output:0;lstm_17/while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2,
*lstm_17/while/lstm_cell_17/strided_slice_1с
#lstm_17/while/lstm_cell_17/MatMul_5MatMul$lstm_17/while/lstm_cell_17/mul_5:z:03lstm_17/while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_5н
 lstm_17/while/lstm_cell_17/add_1AddV2-lstm_17/while/lstm_cell_17/BiasAdd_1:output:0-lstm_17/while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_1Џ
$lstm_17/while/lstm_cell_17/Sigmoid_1Sigmoid$lstm_17/while/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/Sigmoid_1Ф
 lstm_17/while/lstm_cell_17/mul_8Mul(lstm_17/while/lstm_cell_17/Sigmoid_1:y:0lstm_17_while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_8а
+lstm_17/while/lstm_cell_17/ReadVariableOp_2ReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02-
+lstm_17/while/lstm_cell_17/ReadVariableOp_2Е
0lstm_17/while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   22
0lstm_17/while/lstm_cell_17/strided_slice_2/stackЙ
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  24
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_1Й
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      24
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_2Њ
*lstm_17/while/lstm_cell_17/strided_slice_2StridedSlice3lstm_17/while/lstm_cell_17/ReadVariableOp_2:value:09lstm_17/while/lstm_cell_17/strided_slice_2/stack:output:0;lstm_17/while/lstm_cell_17/strided_slice_2/stack_1:output:0;lstm_17/while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2,
*lstm_17/while/lstm_cell_17/strided_slice_2с
#lstm_17/while/lstm_cell_17/MatMul_6MatMul$lstm_17/while/lstm_cell_17/mul_6:z:03lstm_17/while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_6н
 lstm_17/while/lstm_cell_17/add_2AddV2-lstm_17/while/lstm_cell_17/BiasAdd_2:output:0-lstm_17/while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_2Ђ
lstm_17/while/lstm_cell_17/TanhTanh$lstm_17/while/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2!
lstm_17/while/lstm_cell_17/TanhЪ
 lstm_17/while/lstm_cell_17/mul_9Mul&lstm_17/while/lstm_cell_17/Sigmoid:y:0#lstm_17/while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_9Ы
 lstm_17/while/lstm_cell_17/add_3AddV2$lstm_17/while/lstm_cell_17/mul_8:z:0$lstm_17/while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_3а
+lstm_17/while/lstm_cell_17/ReadVariableOp_3ReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02-
+lstm_17/while/lstm_cell_17/ReadVariableOp_3Е
0lstm_17/while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  22
0lstm_17/while/lstm_cell_17/strided_slice_3/stackЙ
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        24
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_1Й
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      24
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_2Њ
*lstm_17/while/lstm_cell_17/strided_slice_3StridedSlice3lstm_17/while/lstm_cell_17/ReadVariableOp_3:value:09lstm_17/while/lstm_cell_17/strided_slice_3/stack:output:0;lstm_17/while/lstm_cell_17/strided_slice_3/stack_1:output:0;lstm_17/while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2,
*lstm_17/while/lstm_cell_17/strided_slice_3с
#lstm_17/while/lstm_cell_17/MatMul_7MatMul$lstm_17/while/lstm_cell_17/mul_7:z:03lstm_17/while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_7н
 lstm_17/while/lstm_cell_17/add_4AddV2-lstm_17/while/lstm_cell_17/BiasAdd_3:output:0-lstm_17/while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_4Џ
$lstm_17/while/lstm_cell_17/Sigmoid_2Sigmoid$lstm_17/while/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/Sigmoid_2І
!lstm_17/while/lstm_cell_17/Tanh_1Tanh$lstm_17/while/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_17/while/lstm_cell_17/Tanh_1а
!lstm_17/while/lstm_cell_17/mul_10Mul(lstm_17/while/lstm_cell_17/Sigmoid_2:y:0%lstm_17/while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_17/while/lstm_cell_17/mul_10
2lstm_17/while/TensorArrayV2Write/TensorListSetItemTensorListSetItemlstm_17_while_placeholder_1lstm_17_while_placeholder%lstm_17/while/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype024
2lstm_17/while/TensorArrayV2Write/TensorListSetIteml
lstm_17/while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_17/while/add/y
lstm_17/while/addAddV2lstm_17_while_placeholderlstm_17/while/add/y:output:0*
T0*
_output_shapes
: 2
lstm_17/while/addp
lstm_17/while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_17/while/add_1/y
lstm_17/while/add_1AddV2(lstm_17_while_lstm_17_while_loop_counterlstm_17/while/add_1/y:output:0*
T0*
_output_shapes
: 2
lstm_17/while/add_1
lstm_17/while/IdentityIdentitylstm_17/while/add_1:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity­
lstm_17/while/Identity_1Identity.lstm_17_while_lstm_17_while_maximum_iterations*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity_1
lstm_17/while/Identity_2Identitylstm_17/while/add:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity_2С
lstm_17/while/Identity_3IdentityBlstm_17/while/TensorArrayV2Write/TensorListSetItem:output_handle:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity_3Е
lstm_17/while/Identity_4Identity%lstm_17/while/lstm_cell_17/mul_10:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/while/Identity_4Д
lstm_17/while/Identity_5Identity$lstm_17/while/lstm_cell_17/add_3:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/while/Identity_5"9
lstm_17_while_identitylstm_17/while/Identity:output:0"=
lstm_17_while_identity_1!lstm_17/while/Identity_1:output:0"=
lstm_17_while_identity_2!lstm_17/while/Identity_2:output:0"=
lstm_17_while_identity_3!lstm_17/while/Identity_3:output:0"=
lstm_17_while_identity_4!lstm_17/while/Identity_4:output:0"=
lstm_17_while_identity_5!lstm_17/while/Identity_5:output:0"P
%lstm_17_while_lstm_17_strided_slice_1'lstm_17_while_lstm_17_strided_slice_1_0"j
2lstm_17_while_lstm_cell_17_readvariableop_resource4lstm_17_while_lstm_cell_17_readvariableop_resource_0"z
:lstm_17_while_lstm_cell_17_split_1_readvariableop_resource<lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0"v
8lstm_17_while_lstm_cell_17_split_readvariableop_resource:lstm_17_while_lstm_cell_17_split_readvariableop_resource_0"Ш
alstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensorclstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2V
)lstm_17/while/lstm_cell_17/ReadVariableOp)lstm_17/while/lstm_cell_17/ReadVariableOp2Z
+lstm_17/while/lstm_cell_17/ReadVariableOp_1+lstm_17/while/lstm_cell_17/ReadVariableOp_12Z
+lstm_17/while/lstm_cell_17/ReadVariableOp_2+lstm_17/while/lstm_cell_17/ReadVariableOp_22Z
+lstm_17/while/lstm_cell_17/ReadVariableOp_3+lstm_17/while/lstm_cell_17/ReadVariableOp_32b
/lstm_17/while/lstm_cell_17/split/ReadVariableOp/lstm_17/while/lstm_cell_17/split/ReadVariableOp2f
1lstm_17/while/lstm_cell_17/split_1/ReadVariableOp1lstm_17/while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
шM
Ќ
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1219391

inputs
states_0
states_10
split_readvariableop_resource:	d.
split_1_readvariableop_resource:	*
readvariableop_resource:	d
identity

identity_1

identity_2ЂReadVariableOpЂReadVariableOp_1ЂReadVariableOp_2ЂReadVariableOp_3Ђsplit/ReadVariableOpЂsplit_1/ReadVariableOpX
ones_like/ShapeShapeinputs*
T0*
_output_shapes
:2
ones_like/Shapeg
ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like/Const
	ones_likeFillones_like/Shape:output:0ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	ones_like^
ones_like_1/ShapeShapestates_0*
T0*
_output_shapes
:2
ones_like_1/Shapek
ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like_1/Const
ones_like_1Fillones_like_1/Shape:output:0ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
ones_like_1_
mulMulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mulc
mul_1Mulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_1c
mul_2Mulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_2c
mul_3Mulinputsones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_3d
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
split/split_dim
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*
_output_shapes
:	d*
dtype02
split/ReadVariableOpЇ
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
splite
MatMulMatMulmul:z:0split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
MatMulk
MatMul_1MatMul	mul_1:z:0split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_1k
MatMul_2MatMul	mul_2:z:0split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_2k
MatMul_3MatMul	mul_3:z:0split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_3h
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2
split_1/split_dim
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*
_output_shapes	
:*
dtype02
split_1/ReadVariableOp
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2	
split_1s
BiasAddBiasAddMatMul:product:0split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
BiasAddy
	BiasAdd_1BiasAddMatMul_1:product:0split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_1y
	BiasAdd_2BiasAddMatMul_2:product:0split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_2y
	BiasAdd_3BiasAddMatMul_3:product:0split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_3g
mul_4Mulstates_0ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_4g
mul_5Mulstates_0ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_5g
mul_6Mulstates_0ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_6g
mul_7Mulstates_0ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_7y
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp{
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice/stack
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice/stack_1
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice/stack_2ќ
strided_sliceStridedSliceReadVariableOp:value:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slices
MatMul_4MatMul	mul_4:z:0strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_4k
addAddV2BiasAdd:output:0MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
addX
SigmoidSigmoidadd:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
Sigmoid}
ReadVariableOp_1ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_1
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice_1/stack
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_1/stack_1
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_1/stack_2
strided_slice_1StridedSliceReadVariableOp_1:value:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_1u
MatMul_5MatMul	mul_5:z:0strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_5q
add_1AddV2BiasAdd_1:output:0MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_1^
	Sigmoid_1Sigmoid	add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_1`
mul_8MulSigmoid_1:y:0states_1*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_8}
ReadVariableOp_2ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_2
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_2/stack
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_2/stack_1
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_2/stack_2
strided_slice_2StridedSliceReadVariableOp_2:value:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_2u
MatMul_6MatMul	mul_6:z:0strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_6q
add_2AddV2BiasAdd_2:output:0MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_2Q
TanhTanh	add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh^
mul_9MulSigmoid:y:0Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_9_
add_3AddV2	mul_8:z:0	mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_3}
ReadVariableOp_3ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_3
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_3/stack
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice_3/stack_1
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_3/stack_2
strided_slice_3StridedSliceReadVariableOp_3:value:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_3u
MatMul_7MatMul	mul_7:z:0strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_7q
add_4AddV2BiasAdd_3:output:0MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_4^
	Sigmoid_2Sigmoid	add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_2U
Tanh_1Tanh	add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh_1d
mul_10MulSigmoid_2:y:0
Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_10и
IdentityIdentity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identityм

Identity_1Identity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_1л

Identity_2Identity	add_3:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*R
_input_shapesA
?:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd: : : 2 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_12$
ReadVariableOp_2ReadVariableOp_22$
ReadVariableOp_3ReadVariableOp_32,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/0:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/1
№

(sequential_17_lstm_17_while_cond_1215234H
Dsequential_17_lstm_17_while_sequential_17_lstm_17_while_loop_counterN
Jsequential_17_lstm_17_while_sequential_17_lstm_17_while_maximum_iterations+
'sequential_17_lstm_17_while_placeholder-
)sequential_17_lstm_17_while_placeholder_1-
)sequential_17_lstm_17_while_placeholder_2-
)sequential_17_lstm_17_while_placeholder_3J
Fsequential_17_lstm_17_while_less_sequential_17_lstm_17_strided_slice_1a
]sequential_17_lstm_17_while_sequential_17_lstm_17_while_cond_1215234___redundant_placeholder0a
]sequential_17_lstm_17_while_sequential_17_lstm_17_while_cond_1215234___redundant_placeholder1a
]sequential_17_lstm_17_while_sequential_17_lstm_17_while_cond_1215234___redundant_placeholder2a
]sequential_17_lstm_17_while_sequential_17_lstm_17_while_cond_1215234___redundant_placeholder3(
$sequential_17_lstm_17_while_identity
о
 sequential_17/lstm_17/while/LessLess'sequential_17_lstm_17_while_placeholderFsequential_17_lstm_17_while_less_sequential_17_lstm_17_strided_slice_1*
T0*
_output_shapes
: 2"
 sequential_17/lstm_17/while/Less
$sequential_17/lstm_17/while/IdentityIdentity$sequential_17/lstm_17/while/Less:z:0*
T0
*
_output_shapes
: 2&
$sequential_17/lstm_17/while/Identity"U
$sequential_17_lstm_17_while_identity-sequential_17/lstm_17/while/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
Ђ&
ъ
while_body_1215915
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0/
while_lstm_cell_17_1215939_0:	d+
while_lstm_cell_17_1215941_0:	/
while_lstm_cell_17_1215943_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor-
while_lstm_cell_17_1215939:	d)
while_lstm_cell_17_1215941:	-
while_lstm_cell_17_1215943:	dЂ*while/lstm_cell_17/StatefulPartitionedCallУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemх
*while/lstm_cell_17/StatefulPartitionedCallStatefulPartitionedCall0while/TensorArrayV2Read/TensorListGetItem:item:0while_placeholder_2while_placeholder_3while_lstm_cell_17_1215939_0while_lstm_cell_17_1215941_0while_lstm_cell_17_1215943_0*
Tin

2*
Tout
2*
_collective_manager_ids
 *M
_output_shapes;
9:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_12158372,
*while/lstm_cell_17/StatefulPartitionedCallї
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholder3while/lstm_cell_17/StatefulPartitionedCall:output:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1
while/IdentityIdentitywhile/add_1:z:0+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity
while/Identity_1Identitywhile_while_maximum_iterations+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity_1
while/Identity_2Identitywhile/add:z:0+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity_2К
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0+^while/lstm_cell_17/StatefulPartitionedCall*
T0*
_output_shapes
: 2
while/Identity_3Ф
while/Identity_4Identity3while/lstm_cell_17/StatefulPartitionedCall:output:1+^while/lstm_cell_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4Ф
while/Identity_5Identity3while/lstm_cell_17/StatefulPartitionedCall:output:2+^while/lstm_cell_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0":
while_lstm_cell_17_1215939while_lstm_cell_17_1215939_0":
while_lstm_cell_17_1215941while_lstm_cell_17_1215941_0":
while_lstm_cell_17_1215943while_lstm_cell_17_1215943_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2X
*while/lstm_cell_17/StatefulPartitionedCall*while/lstm_cell_17/StatefulPartitionedCall: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
в
Њ
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1215837

inputs

states
states_10
split_readvariableop_resource:	d.
split_1_readvariableop_resource:	*
readvariableop_resource:	d
identity

identity_1

identity_2ЂReadVariableOpЂReadVariableOp_1ЂReadVariableOp_2ЂReadVariableOp_3Ђsplit/ReadVariableOpЂsplit_1/ReadVariableOpX
ones_like/ShapeShapeinputs*
T0*
_output_shapes
:2
ones_like/Shapeg
ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like/Const
	ones_likeFillones_like/Shape:output:0ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	ones_likec
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout/Const
dropout/MulMulones_like:output:0dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout/Mul`
dropout/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout/Shapeв
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2еE2&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout/GreaterEqual/yО
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout/Castz
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout/Mul_1g
dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_1/Const
dropout_1/MulMulones_like:output:0dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_1/Muld
dropout_1/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout_1/Shapeй
&dropout_1/random_uniform/RandomUniformRandomUniformdropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ГЧ2(
&dropout_1/random_uniform/RandomUniformy
dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_1/GreaterEqual/yЦ
dropout_1/GreaterEqualGreaterEqual/dropout_1/random_uniform/RandomUniform:output:0!dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_1/GreaterEqual
dropout_1/CastCastdropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_1/Cast
dropout_1/Mul_1Muldropout_1/Mul:z:0dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_1/Mul_1g
dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_2/Const
dropout_2/MulMulones_like:output:0dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_2/Muld
dropout_2/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout_2/Shapeи
&dropout_2/random_uniform/RandomUniformRandomUniformdropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Нє2(
&dropout_2/random_uniform/RandomUniformy
dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_2/GreaterEqual/yЦ
dropout_2/GreaterEqualGreaterEqual/dropout_2/random_uniform/RandomUniform:output:0!dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_2/GreaterEqual
dropout_2/CastCastdropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_2/Cast
dropout_2/Mul_1Muldropout_2/Mul:z:0dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_2/Mul_1g
dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_3/Const
dropout_3/MulMulones_like:output:0dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_3/Muld
dropout_3/ShapeShapeones_like:output:0*
T0*
_output_shapes
:2
dropout_3/Shapeи
&dropout_3/random_uniform/RandomUniformRandomUniformdropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2яЙw2(
&dropout_3/random_uniform/RandomUniformy
dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_3/GreaterEqual/yЦ
dropout_3/GreaterEqualGreaterEqual/dropout_3/random_uniform/RandomUniform:output:0!dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_3/GreaterEqual
dropout_3/CastCastdropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_3/Cast
dropout_3/Mul_1Muldropout_3/Mul:z:0dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_3/Mul_1\
ones_like_1/ShapeShapestates*
T0*
_output_shapes
:2
ones_like_1/Shapek
ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
ones_like_1/Const
ones_like_1Fillones_like_1/Shape:output:0ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
ones_like_1g
dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_4/Const
dropout_4/MulMulones_like_1:output:0dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_4/Mulf
dropout_4/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_4/Shapeй
&dropout_4/random_uniform/RandomUniformRandomUniformdropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2оІз2(
&dropout_4/random_uniform/RandomUniformy
dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_4/GreaterEqual/yЦ
dropout_4/GreaterEqualGreaterEqual/dropout_4/random_uniform/RandomUniform:output:0!dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_4/GreaterEqual
dropout_4/CastCastdropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_4/Cast
dropout_4/Mul_1Muldropout_4/Mul:z:0dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_4/Mul_1g
dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_5/Const
dropout_5/MulMulones_like_1:output:0dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_5/Mulf
dropout_5/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_5/Shapeй
&dropout_5/random_uniform/RandomUniformRandomUniformdropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ѓШ2(
&dropout_5/random_uniform/RandomUniformy
dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_5/GreaterEqual/yЦ
dropout_5/GreaterEqualGreaterEqual/dropout_5/random_uniform/RandomUniform:output:0!dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_5/GreaterEqual
dropout_5/CastCastdropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_5/Cast
dropout_5/Mul_1Muldropout_5/Mul:z:0dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_5/Mul_1g
dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_6/Const
dropout_6/MulMulones_like_1:output:0dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_6/Mulf
dropout_6/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_6/Shapeй
&dropout_6/random_uniform/RandomUniformRandomUniformdropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2юЄ2(
&dropout_6/random_uniform/RandomUniformy
dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_6/GreaterEqual/yЦ
dropout_6/GreaterEqualGreaterEqual/dropout_6/random_uniform/RandomUniform:output:0!dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_6/GreaterEqual
dropout_6/CastCastdropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_6/Cast
dropout_6/Mul_1Muldropout_6/Mul:z:0dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_6/Mul_1g
dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout_7/Const
dropout_7/MulMulones_like_1:output:0dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_7/Mulf
dropout_7/ShapeShapeones_like_1:output:0*
T0*
_output_shapes
:2
dropout_7/Shapeй
&dropout_7/random_uniform/RandomUniformRandomUniformdropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2і§И2(
&dropout_7/random_uniform/RandomUniformy
dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout_7/GreaterEqual/yЦ
dropout_7/GreaterEqualGreaterEqual/dropout_7/random_uniform/RandomUniform:output:0!dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_7/GreaterEqual
dropout_7/CastCastdropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
dropout_7/Cast
dropout_7/Mul_1Muldropout_7/Mul:z:0dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
dropout_7/Mul_1^
mulMulinputsdropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
muld
mul_1Mulinputsdropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_1d
mul_2Mulinputsdropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_2d
mul_3Mulinputsdropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_3d
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
split/split_dim
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*
_output_shapes
:	d*
dtype02
split/ReadVariableOpЇ
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
splite
MatMulMatMulmul:z:0split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
MatMulk
MatMul_1MatMul	mul_1:z:0split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_1k
MatMul_2MatMul	mul_2:z:0split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_2k
MatMul_3MatMul	mul_3:z:0split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_3h
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2
split_1/split_dim
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*
_output_shapes	
:*
dtype02
split_1/ReadVariableOp
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2	
split_1s
BiasAddBiasAddMatMul:product:0split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
BiasAddy
	BiasAdd_1BiasAddMatMul_1:product:0split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_1y
	BiasAdd_2BiasAddMatMul_2:product:0split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_2y
	BiasAdd_3BiasAddMatMul_3:product:0split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
	BiasAdd_3d
mul_4Mulstatesdropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_4d
mul_5Mulstatesdropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_5d
mul_6Mulstatesdropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_6d
mul_7Mulstatesdropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_7y
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp{
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice/stack
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice/stack_1
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice/stack_2ќ
strided_sliceStridedSliceReadVariableOp:value:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slices
MatMul_4MatMul	mul_4:z:0strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_4k
addAddV2BiasAdd:output:0MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
addX
SigmoidSigmoidadd:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
Sigmoid}
ReadVariableOp_1ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_1
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2
strided_slice_1/stack
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_1/stack_1
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_1/stack_2
strided_slice_1StridedSliceReadVariableOp_1:value:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_1u
MatMul_5MatMul	mul_5:z:0strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_5q
add_1AddV2BiasAdd_1:output:0MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_1^
	Sigmoid_1Sigmoid	add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_1`
mul_8MulSigmoid_1:y:0states_1*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_8}
ReadVariableOp_2ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_2
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2
strided_slice_2/stack
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_2/stack_1
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_2/stack_2
strided_slice_2StridedSliceReadVariableOp_2:value:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_2u
MatMul_6MatMul	mul_6:z:0strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_6q
add_2AddV2BiasAdd_2:output:0MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_2Q
TanhTanh	add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh^
mul_9MulSigmoid:y:0Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_9_
add_3AddV2	mul_8:z:0	mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_3}
ReadVariableOp_3ReadVariableOpreadvariableop_resource*
_output_shapes
:	d*
dtype02
ReadVariableOp_3
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2
strided_slice_3/stack
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice_3/stack_1
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_3/stack_2
strided_slice_3StridedSliceReadVariableOp_3:value:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
strided_slice_3u
MatMul_7MatMul	mul_7:z:0strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2

MatMul_7q
add_4AddV2BiasAdd_3:output:0MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
add_4^
	Sigmoid_2Sigmoid	add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
	Sigmoid_2U
Tanh_1Tanh	add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tanh_1d
mul_10MulSigmoid_2:y:0
Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
mul_10и
IdentityIdentity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identityм

Identity_1Identity
mul_10:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_1л

Identity_2Identity	add_3:z:0^ReadVariableOp^ReadVariableOp_1^ReadVariableOp_2^ReadVariableOp_3^split/ReadVariableOp^split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*R
_input_shapesA
?:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd: : : 2 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_12$
ReadVariableOp_2ReadVariableOp_22$
ReadVariableOp_3ReadVariableOp_32,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs:OK
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_namestates:OK
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_namestates

ц
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217126
embedding_17_input(
embedding_17_1217109:
ЈУd"
lstm_17_1217113:	d
lstm_17_1217115:	"
lstm_17_1217117:	d"
dense_17_1217120:d
dense_17_1217122:
identityЂ dense_17/StatefulPartitionedCallЂ$embedding_17/StatefulPartitionedCallЂlstm_17/StatefulPartitionedCallЂ,spatial_dropout1d_17/StatefulPartitionedCallЄ
$embedding_17/StatefulPartitionedCallStatefulPartitionedCallembedding_17_inputembedding_17_1217109*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_embedding_17_layer_call_and_return_conditional_losses_12162632&
$embedding_17/StatefulPartitionedCallН
,spatial_dropout1d_17/StatefulPartitionedCallStatefulPartitionedCall-embedding_17/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12170042.
,spatial_dropout1d_17/StatefulPartitionedCallд
lstm_17/StatefulPartitionedCallStatefulPartitionedCall5spatial_dropout1d_17/StatefulPartitionedCall:output:0lstm_17_1217113lstm_17_1217115lstm_17_1217117*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12169662!
lstm_17/StatefulPartitionedCallЙ
 dense_17/StatefulPartitionedCallStatefulPartitionedCall(lstm_17/StatefulPartitionedCall:output:0dense_17_1217120dense_17_1217122*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12165422"
 dense_17/StatefulPartitionedCall
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_17/StatefulPartitionedCall%^embedding_17/StatefulPartitionedCall ^lstm_17/StatefulPartitionedCall-^spatial_dropout1d_17/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2L
$embedding_17/StatefulPartitionedCall$embedding_17/StatefulPartitionedCall2B
lstm_17/StatefulPartitionedCalllstm_17/StatefulPartitionedCall2\
,spatial_dropout1d_17/StatefulPartitionedCall,spatial_dropout1d_17/StatefulPartitionedCall:\ X
(
_output_shapes
:џџџџџџџџџ
,
_user_specified_nameembedding_17_input

Ѕ	
while_body_1218112
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0E
2while_lstm_cell_17_split_readvariableop_resource_0:	dC
4while_lstm_cell_17_split_1_readvariableop_resource_0:	?
,while_lstm_cell_17_readvariableop_resource_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorC
0while_lstm_cell_17_split_readvariableop_resource:	dA
2while_lstm_cell_17_split_1_readvariableop_resource:	=
*while_lstm_cell_17_readvariableop_resource:	dЂ!while/lstm_cell_17/ReadVariableOpЂ#while/lstm_cell_17/ReadVariableOp_1Ђ#while/lstm_cell_17/ReadVariableOp_2Ђ#while/lstm_cell_17/ReadVariableOp_3Ђ'while/lstm_cell_17/split/ReadVariableOpЂ)while/lstm_cell_17/split_1/ReadVariableOpУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemЈ
"while/lstm_cell_17/ones_like/ShapeShape0while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/ones_like/Shape
"while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2$
"while/lstm_cell_17/ones_like/Constа
while/lstm_cell_17/ones_likeFill+while/lstm_cell_17/ones_like/Shape:output:0+while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/ones_like
$while/lstm_cell_17/ones_like_1/ShapeShapewhile_placeholder_2*
T0*
_output_shapes
:2&
$while/lstm_cell_17/ones_like_1/Shape
$while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$while/lstm_cell_17/ones_like_1/Constи
while/lstm_cell_17/ones_like_1Fill-while/lstm_cell_17/ones_like_1/Shape:output:0-while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/ones_like_1Т
while/lstm_cell_17/mulMul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mulЦ
while/lstm_cell_17/mul_1Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_1Ц
while/lstm_cell_17/mul_2Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_2Ц
while/lstm_cell_17/mul_3Mul0while/TensorArrayV2Read/TensorListGetItem:item:0%while/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_3
"while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2$
"while/lstm_cell_17/split/split_dimЦ
'while/lstm_cell_17/split/ReadVariableOpReadVariableOp2while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype02)
'while/lstm_cell_17/split/ReadVariableOpѓ
while/lstm_cell_17/splitSplit+while/lstm_cell_17/split/split_dim:output:0/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
while/lstm_cell_17/splitБ
while/lstm_cell_17/MatMulMatMulwhile/lstm_cell_17/mul:z:0!while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMulЗ
while/lstm_cell_17/MatMul_1MatMulwhile/lstm_cell_17/mul_1:z:0!while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_1З
while/lstm_cell_17/MatMul_2MatMulwhile/lstm_cell_17/mul_2:z:0!while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_2З
while/lstm_cell_17/MatMul_3MatMulwhile/lstm_cell_17/mul_3:z:0!while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_3
$while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2&
$while/lstm_cell_17/split_1/split_dimШ
)while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype02+
)while/lstm_cell_17/split_1/ReadVariableOpы
while/lstm_cell_17/split_1Split-while/lstm_cell_17/split_1/split_dim:output:01while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
while/lstm_cell_17/split_1П
while/lstm_cell_17/BiasAddBiasAdd#while/lstm_cell_17/MatMul:product:0#while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAddХ
while/lstm_cell_17/BiasAdd_1BiasAdd%while/lstm_cell_17/MatMul_1:product:0#while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_1Х
while/lstm_cell_17/BiasAdd_2BiasAdd%while/lstm_cell_17/MatMul_2:product:0#while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_2Х
while/lstm_cell_17/BiasAdd_3BiasAdd%while/lstm_cell_17/MatMul_3:product:0#while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_3Ћ
while/lstm_cell_17/mul_4Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_4Ћ
while/lstm_cell_17/mul_5Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_5Ћ
while/lstm_cell_17/mul_6Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_6Ћ
while/lstm_cell_17/mul_7Mulwhile_placeholder_2'while/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_7Д
!while/lstm_cell_17/ReadVariableOpReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02#
!while/lstm_cell_17/ReadVariableOpЁ
&while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2(
&while/lstm_cell_17/strided_slice/stackЅ
(while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice/stack_1Ѕ
(while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2*
(while/lstm_cell_17/strided_slice/stack_2ю
 while/lstm_cell_17/strided_sliceStridedSlice)while/lstm_cell_17/ReadVariableOp:value:0/while/lstm_cell_17/strided_slice/stack:output:01while/lstm_cell_17/strided_slice/stack_1:output:01while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2"
 while/lstm_cell_17/strided_sliceП
while/lstm_cell_17/MatMul_4MatMulwhile/lstm_cell_17/mul_4:z:0)while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_4З
while/lstm_cell_17/addAddV2#while/lstm_cell_17/BiasAdd:output:0%while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add
while/lstm_cell_17/SigmoidSigmoidwhile/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/SigmoidИ
#while/lstm_cell_17/ReadVariableOp_1ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_1Ѕ
(while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice_1/stackЉ
*while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*while/lstm_cell_17/strided_slice_1/stack_1Љ
*while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_1/stack_2њ
"while/lstm_cell_17/strided_slice_1StridedSlice+while/lstm_cell_17/ReadVariableOp_1:value:01while/lstm_cell_17/strided_slice_1/stack:output:03while/lstm_cell_17/strided_slice_1/stack_1:output:03while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_1С
while/lstm_cell_17/MatMul_5MatMulwhile/lstm_cell_17/mul_5:z:0+while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_5Н
while/lstm_cell_17/add_1AddV2%while/lstm_cell_17/BiasAdd_1:output:0%while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_1
while/lstm_cell_17/Sigmoid_1Sigmoidwhile/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_1Є
while/lstm_cell_17/mul_8Mul while/lstm_cell_17/Sigmoid_1:y:0while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_8И
#while/lstm_cell_17/ReadVariableOp_2ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_2Ѕ
(while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2*
(while/lstm_cell_17/strided_slice_2/stackЉ
*while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*while/lstm_cell_17/strided_slice_2/stack_1Љ
*while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_2/stack_2њ
"while/lstm_cell_17/strided_slice_2StridedSlice+while/lstm_cell_17/ReadVariableOp_2:value:01while/lstm_cell_17/strided_slice_2/stack:output:03while/lstm_cell_17/strided_slice_2/stack_1:output:03while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_2С
while/lstm_cell_17/MatMul_6MatMulwhile/lstm_cell_17/mul_6:z:0+while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_6Н
while/lstm_cell_17/add_2AddV2%while/lstm_cell_17/BiasAdd_2:output:0%while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_2
while/lstm_cell_17/TanhTanhwhile/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/TanhЊ
while/lstm_cell_17/mul_9Mulwhile/lstm_cell_17/Sigmoid:y:0while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_9Ћ
while/lstm_cell_17/add_3AddV2while/lstm_cell_17/mul_8:z:0while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_3И
#while/lstm_cell_17/ReadVariableOp_3ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_3Ѕ
(while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2*
(while/lstm_cell_17/strided_slice_3/stackЉ
*while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2,
*while/lstm_cell_17/strided_slice_3/stack_1Љ
*while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_3/stack_2њ
"while/lstm_cell_17/strided_slice_3StridedSlice+while/lstm_cell_17/ReadVariableOp_3:value:01while/lstm_cell_17/strided_slice_3/stack:output:03while/lstm_cell_17/strided_slice_3/stack_1:output:03while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_3С
while/lstm_cell_17/MatMul_7MatMulwhile/lstm_cell_17/mul_7:z:0+while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_7Н
while/lstm_cell_17/add_4AddV2%while/lstm_cell_17/BiasAdd_3:output:0%while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_4
while/lstm_cell_17/Sigmoid_2Sigmoidwhile/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_2
while/lstm_cell_17/Tanh_1Tanhwhile/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Tanh_1А
while/lstm_cell_17/mul_10Mul while/lstm_cell_17/Sigmoid_2:y:0while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_10с
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1Ъ
while/IdentityIdentitywhile/add_1:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identityн
while/Identity_1Identitywhile_while_maximum_iterations"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_1Ь
while/Identity_2Identitywhile/add:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_2љ
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_3э
while/Identity_4Identitywhile/lstm_cell_17/mul_10:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4ь
while/Identity_5Identitywhile/lstm_cell_17/add_3:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"Z
*while_lstm_cell_17_readvariableop_resource,while_lstm_cell_17_readvariableop_resource_0"j
2while_lstm_cell_17_split_1_readvariableop_resource4while_lstm_cell_17_split_1_readvariableop_resource_0"f
0while_lstm_cell_17_split_readvariableop_resource2while_lstm_cell_17_split_readvariableop_resource_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2F
!while/lstm_cell_17/ReadVariableOp!while/lstm_cell_17/ReadVariableOp2J
#while/lstm_cell_17/ReadVariableOp_1#while/lstm_cell_17/ReadVariableOp_12J
#while/lstm_cell_17/ReadVariableOp_2#while/lstm_cell_17/ReadVariableOp_22J
#while/lstm_cell_17/ReadVariableOp_3#while/lstm_cell_17/ReadVariableOp_32R
'while/lstm_cell_17/split/ReadVariableOp'while/lstm_cell_17/split/ReadVariableOp2V
)while/lstm_cell_17/split_1/ReadVariableOp)while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
к
Ш
while_cond_1218111
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1218111___redundant_placeholder05
1while_while_cond_1218111___redundant_placeholder15
1while_while_cond_1218111___redundant_placeholder25
1while_while_cond_1218111___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
з
p
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217924

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ь
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
dropout/Const
dropout/MulMulinputsdropout/Const:output:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
dropout/Mul
dropout/random_uniform/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2 
dropout/random_uniform/shape/1Э
dropout/random_uniform/shapePackstrided_slice:output:0'dropout/random_uniform/shape/1:output:0strided_slice_1:output:0*
N*
T0*
_output_shapes
:2
dropout/random_uniform/shapeа
$dropout/random_uniform/RandomUniformRandomUniform%dropout/random_uniform/shape:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2
dropout/GreaterEqual/yЫ
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2
dropout/Cast
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
dropout/Mul_1{
IdentityIdentitydropout/Mul_1:z:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ:e a
=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 
_user_specified_nameinputs
ў

/__inference_sequential_17_layer_call_fn_1216564
embedding_17_input
unknown:
ЈУd
	unknown_0:	d
	unknown_1:	
	unknown_2:	d
	unknown_3:d
	unknown_4:
identityЂStatefulPartitionedCallК
StatefulPartitionedCallStatefulPartitionedCallembedding_17_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4*
Tin
	2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*(
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *S
fNRL
J__inference_sequential_17_layer_call_and_return_conditional_losses_12165492
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:\ X
(
_output_shapes
:џџџџџџџџџ
,
_user_specified_nameembedding_17_input
к

/__inference_sequential_17_layer_call_fn_1217168

inputs
unknown:
ЈУd
	unknown_0:	d
	unknown_1:	
	unknown_2:	d
	unknown_3:d
	unknown_4:
identityЂStatefulPartitionedCallЎ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4*
Tin
	2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*(
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *S
fNRL
J__inference_sequential_17_layer_call_and_return_conditional_losses_12165492
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs
к
Ш
while_cond_1219056
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1219056___redundant_placeholder05
1while_while_cond_1219056___redundant_placeholder15
1while_while_cond_1219056___redundant_placeholder25
1while_while_cond_1219056___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
єї
Ѕ	
while_body_1219057
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0E
2while_lstm_cell_17_split_readvariableop_resource_0:	dC
4while_lstm_cell_17_split_1_readvariableop_resource_0:	?
,while_lstm_cell_17_readvariableop_resource_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorC
0while_lstm_cell_17_split_readvariableop_resource:	dA
2while_lstm_cell_17_split_1_readvariableop_resource:	=
*while_lstm_cell_17_readvariableop_resource:	dЂ!while/lstm_cell_17/ReadVariableOpЂ#while/lstm_cell_17/ReadVariableOp_1Ђ#while/lstm_cell_17/ReadVariableOp_2Ђ#while/lstm_cell_17/ReadVariableOp_3Ђ'while/lstm_cell_17/split/ReadVariableOpЂ)while/lstm_cell_17/split_1/ReadVariableOpУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemЈ
"while/lstm_cell_17/ones_like/ShapeShape0while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/ones_like/Shape
"while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2$
"while/lstm_cell_17/ones_like/Constа
while/lstm_cell_17/ones_likeFill+while/lstm_cell_17/ones_like/Shape:output:0+while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/ones_like
 while/lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2"
 while/lstm_cell_17/dropout/ConstЫ
while/lstm_cell_17/dropout/MulMul%while/lstm_cell_17/ones_like:output:0)while/lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/dropout/Mul
 while/lstm_cell_17/dropout/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2"
 while/lstm_cell_17/dropout/Shape
7while/lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform)while/lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЄИэ29
7while/lstm_cell_17/dropout/random_uniform/RandomUniform
)while/lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2+
)while/lstm_cell_17/dropout/GreaterEqual/y
'while/lstm_cell_17/dropout/GreaterEqualGreaterEqual@while/lstm_cell_17/dropout/random_uniform/RandomUniform:output:02while/lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2)
'while/lstm_cell_17/dropout/GreaterEqualИ
while/lstm_cell_17/dropout/CastCast+while/lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2!
while/lstm_cell_17/dropout/CastЦ
 while/lstm_cell_17/dropout/Mul_1Mul"while/lstm_cell_17/dropout/Mul:z:0#while/lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout/Mul_1
"while/lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_1/Constб
 while/lstm_cell_17/dropout_1/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_1/Mul
"while/lstm_cell_17/dropout_1/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_1/Shape
9while/lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЈЇ2;
9while/lstm_cell_17/dropout_1/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_1/GreaterEqual/y
)while/lstm_cell_17/dropout_1/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_1/GreaterEqualО
!while/lstm_cell_17/dropout_1/CastCast-while/lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_1/CastЮ
"while/lstm_cell_17/dropout_1/Mul_1Mul$while/lstm_cell_17/dropout_1/Mul:z:0%while/lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_1/Mul_1
"while/lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_2/Constб
 while/lstm_cell_17/dropout_2/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_2/Mul
"while/lstm_cell_17/dropout_2/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_2/Shape
9while/lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ќ2;
9while/lstm_cell_17/dropout_2/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_2/GreaterEqual/y
)while/lstm_cell_17/dropout_2/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_2/GreaterEqualО
!while/lstm_cell_17/dropout_2/CastCast-while/lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_2/CastЮ
"while/lstm_cell_17/dropout_2/Mul_1Mul$while/lstm_cell_17/dropout_2/Mul:z:0%while/lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_2/Mul_1
"while/lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_3/Constб
 while/lstm_cell_17/dropout_3/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_3/Mul
"while/lstm_cell_17/dropout_3/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_3/Shape
9while/lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЬОШ2;
9while/lstm_cell_17/dropout_3/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_3/GreaterEqual/y
)while/lstm_cell_17/dropout_3/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_3/GreaterEqualО
!while/lstm_cell_17/dropout_3/CastCast-while/lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_3/CastЮ
"while/lstm_cell_17/dropout_3/Mul_1Mul$while/lstm_cell_17/dropout_3/Mul:z:0%while/lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_3/Mul_1
$while/lstm_cell_17/ones_like_1/ShapeShapewhile_placeholder_2*
T0*
_output_shapes
:2&
$while/lstm_cell_17/ones_like_1/Shape
$while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$while/lstm_cell_17/ones_like_1/Constи
while/lstm_cell_17/ones_like_1Fill-while/lstm_cell_17/ones_like_1/Shape:output:0-while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/ones_like_1
"while/lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_4/Constг
 while/lstm_cell_17/dropout_4/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_4/Mul
"while/lstm_cell_17/dropout_4/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_4/Shape
9while/lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2 §82;
9while/lstm_cell_17/dropout_4/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_4/GreaterEqual/y
)while/lstm_cell_17/dropout_4/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_4/GreaterEqualО
!while/lstm_cell_17/dropout_4/CastCast-while/lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_4/CastЮ
"while/lstm_cell_17/dropout_4/Mul_1Mul$while/lstm_cell_17/dropout_4/Mul:z:0%while/lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_4/Mul_1
"while/lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_5/Constг
 while/lstm_cell_17/dropout_5/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_5/Mul
"while/lstm_cell_17/dropout_5/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_5/Shape
9while/lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2КЋМ2;
9while/lstm_cell_17/dropout_5/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_5/GreaterEqual/y
)while/lstm_cell_17/dropout_5/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_5/GreaterEqualО
!while/lstm_cell_17/dropout_5/CastCast-while/lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_5/CastЮ
"while/lstm_cell_17/dropout_5/Mul_1Mul$while/lstm_cell_17/dropout_5/Mul:z:0%while/lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_5/Mul_1
"while/lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_6/Constг
 while/lstm_cell_17/dropout_6/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_6/Mul
"while/lstm_cell_17/dropout_6/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_6/Shape
9while/lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЧўА2;
9while/lstm_cell_17/dropout_6/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_6/GreaterEqual/y
)while/lstm_cell_17/dropout_6/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_6/GreaterEqualО
!while/lstm_cell_17/dropout_6/CastCast-while/lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_6/CastЮ
"while/lstm_cell_17/dropout_6/Mul_1Mul$while/lstm_cell_17/dropout_6/Mul:z:0%while/lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_6/Mul_1
"while/lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_7/Constг
 while/lstm_cell_17/dropout_7/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_7/Mul
"while/lstm_cell_17/dropout_7/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_7/Shape
9while/lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2№њ2;
9while/lstm_cell_17/dropout_7/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_7/GreaterEqual/y
)while/lstm_cell_17/dropout_7/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_7/GreaterEqualО
!while/lstm_cell_17/dropout_7/CastCast-while/lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_7/CastЮ
"while/lstm_cell_17/dropout_7/Mul_1Mul$while/lstm_cell_17/dropout_7/Mul:z:0%while/lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_7/Mul_1С
while/lstm_cell_17/mulMul0while/TensorArrayV2Read/TensorListGetItem:item:0$while/lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mulЧ
while/lstm_cell_17/mul_1Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_1Ч
while/lstm_cell_17/mul_2Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_2Ч
while/lstm_cell_17/mul_3Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_3
"while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2$
"while/lstm_cell_17/split/split_dimЦ
'while/lstm_cell_17/split/ReadVariableOpReadVariableOp2while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype02)
'while/lstm_cell_17/split/ReadVariableOpѓ
while/lstm_cell_17/splitSplit+while/lstm_cell_17/split/split_dim:output:0/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
while/lstm_cell_17/splitБ
while/lstm_cell_17/MatMulMatMulwhile/lstm_cell_17/mul:z:0!while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMulЗ
while/lstm_cell_17/MatMul_1MatMulwhile/lstm_cell_17/mul_1:z:0!while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_1З
while/lstm_cell_17/MatMul_2MatMulwhile/lstm_cell_17/mul_2:z:0!while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_2З
while/lstm_cell_17/MatMul_3MatMulwhile/lstm_cell_17/mul_3:z:0!while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_3
$while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2&
$while/lstm_cell_17/split_1/split_dimШ
)while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype02+
)while/lstm_cell_17/split_1/ReadVariableOpы
while/lstm_cell_17/split_1Split-while/lstm_cell_17/split_1/split_dim:output:01while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
while/lstm_cell_17/split_1П
while/lstm_cell_17/BiasAddBiasAdd#while/lstm_cell_17/MatMul:product:0#while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAddХ
while/lstm_cell_17/BiasAdd_1BiasAdd%while/lstm_cell_17/MatMul_1:product:0#while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_1Х
while/lstm_cell_17/BiasAdd_2BiasAdd%while/lstm_cell_17/MatMul_2:product:0#while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_2Х
while/lstm_cell_17/BiasAdd_3BiasAdd%while/lstm_cell_17/MatMul_3:product:0#while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_3Њ
while/lstm_cell_17/mul_4Mulwhile_placeholder_2&while/lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_4Њ
while/lstm_cell_17/mul_5Mulwhile_placeholder_2&while/lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_5Њ
while/lstm_cell_17/mul_6Mulwhile_placeholder_2&while/lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_6Њ
while/lstm_cell_17/mul_7Mulwhile_placeholder_2&while/lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_7Д
!while/lstm_cell_17/ReadVariableOpReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02#
!while/lstm_cell_17/ReadVariableOpЁ
&while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2(
&while/lstm_cell_17/strided_slice/stackЅ
(while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice/stack_1Ѕ
(while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2*
(while/lstm_cell_17/strided_slice/stack_2ю
 while/lstm_cell_17/strided_sliceStridedSlice)while/lstm_cell_17/ReadVariableOp:value:0/while/lstm_cell_17/strided_slice/stack:output:01while/lstm_cell_17/strided_slice/stack_1:output:01while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2"
 while/lstm_cell_17/strided_sliceП
while/lstm_cell_17/MatMul_4MatMulwhile/lstm_cell_17/mul_4:z:0)while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_4З
while/lstm_cell_17/addAddV2#while/lstm_cell_17/BiasAdd:output:0%while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add
while/lstm_cell_17/SigmoidSigmoidwhile/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/SigmoidИ
#while/lstm_cell_17/ReadVariableOp_1ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_1Ѕ
(while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice_1/stackЉ
*while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*while/lstm_cell_17/strided_slice_1/stack_1Љ
*while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_1/stack_2њ
"while/lstm_cell_17/strided_slice_1StridedSlice+while/lstm_cell_17/ReadVariableOp_1:value:01while/lstm_cell_17/strided_slice_1/stack:output:03while/lstm_cell_17/strided_slice_1/stack_1:output:03while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_1С
while/lstm_cell_17/MatMul_5MatMulwhile/lstm_cell_17/mul_5:z:0+while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_5Н
while/lstm_cell_17/add_1AddV2%while/lstm_cell_17/BiasAdd_1:output:0%while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_1
while/lstm_cell_17/Sigmoid_1Sigmoidwhile/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_1Є
while/lstm_cell_17/mul_8Mul while/lstm_cell_17/Sigmoid_1:y:0while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_8И
#while/lstm_cell_17/ReadVariableOp_2ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_2Ѕ
(while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2*
(while/lstm_cell_17/strided_slice_2/stackЉ
*while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*while/lstm_cell_17/strided_slice_2/stack_1Љ
*while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_2/stack_2њ
"while/lstm_cell_17/strided_slice_2StridedSlice+while/lstm_cell_17/ReadVariableOp_2:value:01while/lstm_cell_17/strided_slice_2/stack:output:03while/lstm_cell_17/strided_slice_2/stack_1:output:03while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_2С
while/lstm_cell_17/MatMul_6MatMulwhile/lstm_cell_17/mul_6:z:0+while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_6Н
while/lstm_cell_17/add_2AddV2%while/lstm_cell_17/BiasAdd_2:output:0%while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_2
while/lstm_cell_17/TanhTanhwhile/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/TanhЊ
while/lstm_cell_17/mul_9Mulwhile/lstm_cell_17/Sigmoid:y:0while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_9Ћ
while/lstm_cell_17/add_3AddV2while/lstm_cell_17/mul_8:z:0while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_3И
#while/lstm_cell_17/ReadVariableOp_3ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_3Ѕ
(while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2*
(while/lstm_cell_17/strided_slice_3/stackЉ
*while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2,
*while/lstm_cell_17/strided_slice_3/stack_1Љ
*while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_3/stack_2њ
"while/lstm_cell_17/strided_slice_3StridedSlice+while/lstm_cell_17/ReadVariableOp_3:value:01while/lstm_cell_17/strided_slice_3/stack:output:03while/lstm_cell_17/strided_slice_3/stack_1:output:03while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_3С
while/lstm_cell_17/MatMul_7MatMulwhile/lstm_cell_17/mul_7:z:0+while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_7Н
while/lstm_cell_17/add_4AddV2%while/lstm_cell_17/BiasAdd_3:output:0%while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_4
while/lstm_cell_17/Sigmoid_2Sigmoidwhile/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_2
while/lstm_cell_17/Tanh_1Tanhwhile/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Tanh_1А
while/lstm_cell_17/mul_10Mul while/lstm_cell_17/Sigmoid_2:y:0while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_10с
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1Ъ
while/IdentityIdentitywhile/add_1:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identityн
while/Identity_1Identitywhile_while_maximum_iterations"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_1Ь
while/Identity_2Identitywhile/add:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_2љ
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_3э
while/Identity_4Identitywhile/lstm_cell_17/mul_10:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4ь
while/Identity_5Identitywhile/lstm_cell_17/add_3:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"Z
*while_lstm_cell_17_readvariableop_resource,while_lstm_cell_17_readvariableop_resource_0"j
2while_lstm_cell_17_split_1_readvariableop_resource4while_lstm_cell_17_split_1_readvariableop_resource_0"f
0while_lstm_cell_17_split_readvariableop_resource2while_lstm_cell_17_split_readvariableop_resource_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2F
!while/lstm_cell_17/ReadVariableOp!while/lstm_cell_17/ReadVariableOp2J
#while/lstm_cell_17/ReadVariableOp_1#while/lstm_cell_17/ReadVariableOp_12J
#while/lstm_cell_17/ReadVariableOp_2#while/lstm_cell_17/ReadVariableOp_22J
#while/lstm_cell_17/ReadVariableOp_3#while/lstm_cell_17/ReadVariableOp_32R
'while/lstm_cell_17/split/ReadVariableOp'while/lstm_cell_17/split/ReadVariableOp2V
)while/lstm_cell_17/split_1/ReadVariableOp)while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 

o
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1216271

inputs

identity_1_
IdentityIdentityinputs*
T0*,
_output_shapes
:џџџџџџџџџd2

Identityn

Identity_1IdentityIdentity:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:џџџџџџџџџd:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
ЕE

D__inference_lstm_17_layer_call_and_return_conditional_losses_1215984

inputs'
lstm_cell_17_1215902:	d#
lstm_cell_17_1215904:	'
lstm_cell_17_1215906:	d
identityЂ$lstm_cell_17/StatefulPartitionedCallЂwhileD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm
	transpose	Transposeinputstranspose/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2Ё
$lstm_cell_17/StatefulPartitionedCallStatefulPartitionedCallstrided_slice_2:output:0zeros:output:0zeros_1:output:0lstm_cell_17_1215902lstm_cell_17_1215904lstm_cell_17_1215906*
Tin

2*
Tout
2*
_collective_manager_ids
 *M
_output_shapes;
9:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_12158372&
$lstm_cell_17/StatefulPartitionedCall
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterЈ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0lstm_cell_17_1215902lstm_cell_17_1215904lstm_cell_17_1215906*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1215915*
condR
while_cond_1215914*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeё
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permЎ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtime
IdentityIdentitystrided_slice_3:output:0%^lstm_cell_17/StatefulPartitionedCall^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:џџџџџџџџџџџџџџџџџџd: : : 2L
$lstm_cell_17/StatefulPartitionedCall$lstm_cell_17/StatefulPartitionedCall2
whilewhile:\ X
4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd
 
_user_specified_nameinputs
ѕї
Ѕ	
while_body_1218427
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_1_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0E
2while_lstm_cell_17_split_readvariableop_resource_0:	dC
4while_lstm_cell_17_split_1_readvariableop_resource_0:	?
,while_lstm_cell_17_readvariableop_resource_0:	d
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_slice_1U
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorC
0while_lstm_cell_17_split_readvariableop_resource:	dA
2while_lstm_cell_17_split_1_readvariableop_resource:	=
*while_lstm_cell_17_readvariableop_resource:	dЂ!while/lstm_cell_17/ReadVariableOpЂ#while/lstm_cell_17/ReadVariableOp_1Ђ#while/lstm_cell_17/ReadVariableOp_2Ђ#while/lstm_cell_17/ReadVariableOp_3Ђ'while/lstm_cell_17/split/ReadVariableOpЂ)while/lstm_cell_17/split_1/ReadVariableOpУ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   29
7while/TensorArrayV2Read/TensorListGetItem/element_shapeг
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype02+
)while/TensorArrayV2Read/TensorListGetItemЈ
"while/lstm_cell_17/ones_like/ShapeShape0while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/ones_like/Shape
"while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2$
"while/lstm_cell_17/ones_like/Constа
while/lstm_cell_17/ones_likeFill+while/lstm_cell_17/ones_like/Shape:output:0+while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/ones_like
 while/lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2"
 while/lstm_cell_17/dropout/ConstЫ
while/lstm_cell_17/dropout/MulMul%while/lstm_cell_17/ones_like:output:0)while/lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/dropout/Mul
 while/lstm_cell_17/dropout/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2"
 while/lstm_cell_17/dropout/Shape
7while/lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform)while/lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЕЄг29
7while/lstm_cell_17/dropout/random_uniform/RandomUniform
)while/lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2+
)while/lstm_cell_17/dropout/GreaterEqual/y
'while/lstm_cell_17/dropout/GreaterEqualGreaterEqual@while/lstm_cell_17/dropout/random_uniform/RandomUniform:output:02while/lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2)
'while/lstm_cell_17/dropout/GreaterEqualИ
while/lstm_cell_17/dropout/CastCast+while/lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2!
while/lstm_cell_17/dropout/CastЦ
 while/lstm_cell_17/dropout/Mul_1Mul"while/lstm_cell_17/dropout/Mul:z:0#while/lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout/Mul_1
"while/lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_1/Constб
 while/lstm_cell_17/dropout_1/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_1/Mul
"while/lstm_cell_17/dropout_1/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_1/Shape
9while/lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЄЂг2;
9while/lstm_cell_17/dropout_1/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_1/GreaterEqual/y
)while/lstm_cell_17/dropout_1/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_1/GreaterEqualО
!while/lstm_cell_17/dropout_1/CastCast-while/lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_1/CastЮ
"while/lstm_cell_17/dropout_1/Mul_1Mul$while/lstm_cell_17/dropout_1/Mul:z:0%while/lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_1/Mul_1
"while/lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_2/Constб
 while/lstm_cell_17/dropout_2/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_2/Mul
"while/lstm_cell_17/dropout_2/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_2/Shape
9while/lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ћДр2;
9while/lstm_cell_17/dropout_2/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_2/GreaterEqual/y
)while/lstm_cell_17/dropout_2/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_2/GreaterEqualО
!while/lstm_cell_17/dropout_2/CastCast-while/lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_2/CastЮ
"while/lstm_cell_17/dropout_2/Mul_1Mul$while/lstm_cell_17/dropout_2/Mul:z:0%while/lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_2/Mul_1
"while/lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_3/Constб
 while/lstm_cell_17/dropout_3/MulMul%while/lstm_cell_17/ones_like:output:0+while/lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_3/Mul
"while/lstm_cell_17/dropout_3/ShapeShape%while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_3/Shape
9while/lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2йЭЛ2;
9while/lstm_cell_17/dropout_3/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_3/GreaterEqual/y
)while/lstm_cell_17/dropout_3/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_3/GreaterEqualО
!while/lstm_cell_17/dropout_3/CastCast-while/lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_3/CastЮ
"while/lstm_cell_17/dropout_3/Mul_1Mul$while/lstm_cell_17/dropout_3/Mul:z:0%while/lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_3/Mul_1
$while/lstm_cell_17/ones_like_1/ShapeShapewhile_placeholder_2*
T0*
_output_shapes
:2&
$while/lstm_cell_17/ones_like_1/Shape
$while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$while/lstm_cell_17/ones_like_1/Constи
while/lstm_cell_17/ones_like_1Fill-while/lstm_cell_17/ones_like_1/Shape:output:0-while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
while/lstm_cell_17/ones_like_1
"while/lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_4/Constг
 while/lstm_cell_17/dropout_4/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_4/Mul
"while/lstm_cell_17/dropout_4/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_4/Shape
9while/lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ПрУ2;
9while/lstm_cell_17/dropout_4/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_4/GreaterEqual/y
)while/lstm_cell_17/dropout_4/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_4/GreaterEqualО
!while/lstm_cell_17/dropout_4/CastCast-while/lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_4/CastЮ
"while/lstm_cell_17/dropout_4/Mul_1Mul$while/lstm_cell_17/dropout_4/Mul:z:0%while/lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_4/Mul_1
"while/lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_5/Constг
 while/lstm_cell_17/dropout_5/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_5/Mul
"while/lstm_cell_17/dropout_5/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_5/Shape
9while/lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЅЕ
2;
9while/lstm_cell_17/dropout_5/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_5/GreaterEqual/y
)while/lstm_cell_17/dropout_5/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_5/GreaterEqualО
!while/lstm_cell_17/dropout_5/CastCast-while/lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_5/CastЮ
"while/lstm_cell_17/dropout_5/Mul_1Mul$while/lstm_cell_17/dropout_5/Mul:z:0%while/lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_5/Mul_1
"while/lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_6/Constг
 while/lstm_cell_17/dropout_6/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_6/Mul
"while/lstm_cell_17/dropout_6/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_6/Shape
9while/lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2сЃ2;
9while/lstm_cell_17/dropout_6/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_6/GreaterEqual/y
)while/lstm_cell_17/dropout_6/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_6/GreaterEqualО
!while/lstm_cell_17/dropout_6/CastCast-while/lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_6/CastЮ
"while/lstm_cell_17/dropout_6/Mul_1Mul$while/lstm_cell_17/dropout_6/Mul:z:0%while/lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_6/Mul_1
"while/lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"while/lstm_cell_17/dropout_7/Constг
 while/lstm_cell_17/dropout_7/MulMul'while/lstm_cell_17/ones_like_1:output:0+while/lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 while/lstm_cell_17/dropout_7/Mul
"while/lstm_cell_17/dropout_7/ShapeShape'while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2$
"while/lstm_cell_17/dropout_7/Shape
9while/lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform+while/lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ъЅв2;
9while/lstm_cell_17/dropout_7/random_uniform/RandomUniform
+while/lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+while/lstm_cell_17/dropout_7/GreaterEqual/y
)while/lstm_cell_17/dropout_7/GreaterEqualGreaterEqualBwhile/lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:04while/lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)while/lstm_cell_17/dropout_7/GreaterEqualО
!while/lstm_cell_17/dropout_7/CastCast-while/lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!while/lstm_cell_17/dropout_7/CastЮ
"while/lstm_cell_17/dropout_7/Mul_1Mul$while/lstm_cell_17/dropout_7/Mul:z:0%while/lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"while/lstm_cell_17/dropout_7/Mul_1С
while/lstm_cell_17/mulMul0while/TensorArrayV2Read/TensorListGetItem:item:0$while/lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mulЧ
while/lstm_cell_17/mul_1Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_1Ч
while/lstm_cell_17/mul_2Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_2Ч
while/lstm_cell_17/mul_3Mul0while/TensorArrayV2Read/TensorListGetItem:item:0&while/lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_3
"while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2$
"while/lstm_cell_17/split/split_dimЦ
'while/lstm_cell_17/split/ReadVariableOpReadVariableOp2while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype02)
'while/lstm_cell_17/split/ReadVariableOpѓ
while/lstm_cell_17/splitSplit+while/lstm_cell_17/split/split_dim:output:0/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
while/lstm_cell_17/splitБ
while/lstm_cell_17/MatMulMatMulwhile/lstm_cell_17/mul:z:0!while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMulЗ
while/lstm_cell_17/MatMul_1MatMulwhile/lstm_cell_17/mul_1:z:0!while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_1З
while/lstm_cell_17/MatMul_2MatMulwhile/lstm_cell_17/mul_2:z:0!while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_2З
while/lstm_cell_17/MatMul_3MatMulwhile/lstm_cell_17/mul_3:z:0!while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_3
$while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2&
$while/lstm_cell_17/split_1/split_dimШ
)while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype02+
)while/lstm_cell_17/split_1/ReadVariableOpы
while/lstm_cell_17/split_1Split-while/lstm_cell_17/split_1/split_dim:output:01while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
while/lstm_cell_17/split_1П
while/lstm_cell_17/BiasAddBiasAdd#while/lstm_cell_17/MatMul:product:0#while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAddХ
while/lstm_cell_17/BiasAdd_1BiasAdd%while/lstm_cell_17/MatMul_1:product:0#while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_1Х
while/lstm_cell_17/BiasAdd_2BiasAdd%while/lstm_cell_17/MatMul_2:product:0#while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_2Х
while/lstm_cell_17/BiasAdd_3BiasAdd%while/lstm_cell_17/MatMul_3:product:0#while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/BiasAdd_3Њ
while/lstm_cell_17/mul_4Mulwhile_placeholder_2&while/lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_4Њ
while/lstm_cell_17/mul_5Mulwhile_placeholder_2&while/lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_5Њ
while/lstm_cell_17/mul_6Mulwhile_placeholder_2&while/lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_6Њ
while/lstm_cell_17/mul_7Mulwhile_placeholder_2&while/lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_7Д
!while/lstm_cell_17/ReadVariableOpReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02#
!while/lstm_cell_17/ReadVariableOpЁ
&while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2(
&while/lstm_cell_17/strided_slice/stackЅ
(while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice/stack_1Ѕ
(while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2*
(while/lstm_cell_17/strided_slice/stack_2ю
 while/lstm_cell_17/strided_sliceStridedSlice)while/lstm_cell_17/ReadVariableOp:value:0/while/lstm_cell_17/strided_slice/stack:output:01while/lstm_cell_17/strided_slice/stack_1:output:01while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2"
 while/lstm_cell_17/strided_sliceП
while/lstm_cell_17/MatMul_4MatMulwhile/lstm_cell_17/mul_4:z:0)while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_4З
while/lstm_cell_17/addAddV2#while/lstm_cell_17/BiasAdd:output:0%while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add
while/lstm_cell_17/SigmoidSigmoidwhile/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/SigmoidИ
#while/lstm_cell_17/ReadVariableOp_1ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_1Ѕ
(while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2*
(while/lstm_cell_17/strided_slice_1/stackЉ
*while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*while/lstm_cell_17/strided_slice_1/stack_1Љ
*while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_1/stack_2њ
"while/lstm_cell_17/strided_slice_1StridedSlice+while/lstm_cell_17/ReadVariableOp_1:value:01while/lstm_cell_17/strided_slice_1/stack:output:03while/lstm_cell_17/strided_slice_1/stack_1:output:03while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_1С
while/lstm_cell_17/MatMul_5MatMulwhile/lstm_cell_17/mul_5:z:0+while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_5Н
while/lstm_cell_17/add_1AddV2%while/lstm_cell_17/BiasAdd_1:output:0%while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_1
while/lstm_cell_17/Sigmoid_1Sigmoidwhile/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_1Є
while/lstm_cell_17/mul_8Mul while/lstm_cell_17/Sigmoid_1:y:0while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_8И
#while/lstm_cell_17/ReadVariableOp_2ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_2Ѕ
(while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2*
(while/lstm_cell_17/strided_slice_2/stackЉ
*while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*while/lstm_cell_17/strided_slice_2/stack_1Љ
*while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_2/stack_2њ
"while/lstm_cell_17/strided_slice_2StridedSlice+while/lstm_cell_17/ReadVariableOp_2:value:01while/lstm_cell_17/strided_slice_2/stack:output:03while/lstm_cell_17/strided_slice_2/stack_1:output:03while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_2С
while/lstm_cell_17/MatMul_6MatMulwhile/lstm_cell_17/mul_6:z:0+while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_6Н
while/lstm_cell_17/add_2AddV2%while/lstm_cell_17/BiasAdd_2:output:0%while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_2
while/lstm_cell_17/TanhTanhwhile/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/TanhЊ
while/lstm_cell_17/mul_9Mulwhile/lstm_cell_17/Sigmoid:y:0while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_9Ћ
while/lstm_cell_17/add_3AddV2while/lstm_cell_17/mul_8:z:0while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_3И
#while/lstm_cell_17/ReadVariableOp_3ReadVariableOp,while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02%
#while/lstm_cell_17/ReadVariableOp_3Ѕ
(while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2*
(while/lstm_cell_17/strided_slice_3/stackЉ
*while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2,
*while/lstm_cell_17/strided_slice_3/stack_1Љ
*while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*while/lstm_cell_17/strided_slice_3/stack_2њ
"while/lstm_cell_17/strided_slice_3StridedSlice+while/lstm_cell_17/ReadVariableOp_3:value:01while/lstm_cell_17/strided_slice_3/stack:output:03while/lstm_cell_17/strided_slice_3/stack_1:output:03while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"while/lstm_cell_17/strided_slice_3С
while/lstm_cell_17/MatMul_7MatMulwhile/lstm_cell_17/mul_7:z:0+while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/MatMul_7Н
while/lstm_cell_17/add_4AddV2%while/lstm_cell_17/BiasAdd_3:output:0%while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/add_4
while/lstm_cell_17/Sigmoid_2Sigmoidwhile/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Sigmoid_2
while/lstm_cell_17/Tanh_1Tanhwhile/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/Tanh_1А
while/lstm_cell_17/mul_10Mul while/lstm_cell_17/Sigmoid_2:y:0while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
while/lstm_cell_17/mul_10с
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype02,
*while/TensorArrayV2Write/TensorListSetItem\
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add/yi
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: 2
	while/add`
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
while/add_1/yv
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: 2
while/add_1Ъ
while/IdentityIdentitywhile/add_1:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identityн
while/Identity_1Identitywhile_while_maximum_iterations"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_1Ь
while/Identity_2Identitywhile/add:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_2љ
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
while/Identity_3э
while/Identity_4Identitywhile/lstm_cell_17/mul_10:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_4ь
while/Identity_5Identitywhile/lstm_cell_17/add_3:z:0"^while/lstm_cell_17/ReadVariableOp$^while/lstm_cell_17/ReadVariableOp_1$^while/lstm_cell_17/ReadVariableOp_2$^while/lstm_cell_17/ReadVariableOp_3(^while/lstm_cell_17/split/ReadVariableOp*^while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
while/Identity_5")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"Z
*while_lstm_cell_17_readvariableop_resource,while_lstm_cell_17_readvariableop_resource_0"j
2while_lstm_cell_17_split_1_readvariableop_resource4while_lstm_cell_17_split_1_readvariableop_resource_0"f
0while_lstm_cell_17_split_readvariableop_resource2while_lstm_cell_17_split_readvariableop_resource_0"0
while_strided_slice_1while_strided_slice_1_0"Ј
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2F
!while/lstm_cell_17/ReadVariableOp!while/lstm_cell_17/ReadVariableOp2J
#while/lstm_cell_17/ReadVariableOp_1#while/lstm_cell_17/ReadVariableOp_12J
#while/lstm_cell_17/ReadVariableOp_2#while/lstm_cell_17/ReadVariableOp_22J
#while/lstm_cell_17/ReadVariableOp_3#while/lstm_cell_17/ReadVariableOp_32R
'while/lstm_cell_17/split/ReadVariableOp'while/lstm_cell_17/split/ReadVariableOp2V
)while/lstm_cell_17/split_1/ReadVariableOp)while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: 
К
o
6__inference_spatial_dropout1d_17_layer_call_fn_1217887

inputs
identityЂStatefulPartitionedCall§
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12154172
StatefulPartitionedCallЄ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ22
StatefulPartitionedCallStatefulPartitionedCall:e a
=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 
_user_specified_nameinputs
ж
o
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217902

inputs

identity_1p
IdentityIdentityinputs*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity

Identity_1IdentityIdentity:output:0*
T0*=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ:e a
=
_output_shapes+
):'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 
_user_specified_nameinputs
шљ
ш
D__inference_lstm_17_layer_call_and_return_conditional_losses_1216966

inputs=
*lstm_cell_17_split_readvariableop_resource:	d;
,lstm_cell_17_split_1_readvariableop_resource:	7
$lstm_cell_17_readvariableop_resource:	d
identityЂlstm_cell_17/ReadVariableOpЂlstm_cell_17/ReadVariableOp_1Ђlstm_cell_17/ReadVariableOp_2Ђlstm_cell_17/ReadVariableOp_3Ђ!lstm_cell_17/split/ReadVariableOpЂ#lstm_cell_17/split_1/ReadVariableOpЂwhileD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros/mul/yl
	zeros/mulMulstrided_slice:output:0zeros/mul/y:output:0*
T0*
_output_shapes
: 2
	zeros/mul_
zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros/Less/yg

zeros/LessLesszeros/mul:z:0zeros/Less/y:output:0*
T0*
_output_shapes
: 2

zeros/Lessb
zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros/packed/1
zeros/packedPackstrided_slice:output:0zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros/packed_
zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros/Constu
zerosFillzeros/packed:output:0zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
zeros`
zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/mul/yr
zeros_1/mulMulstrided_slice:output:0zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
zeros_1/mulc
zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
zeros_1/Less/yo
zeros_1/LessLesszeros_1/mul:z:0zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
zeros_1/Lessf
zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
zeros_1/packed/1
zeros_1/packedPackstrided_slice:output:0zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
zeros_1/packedc
zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
zeros_1/Const}
zeros_1Fillzeros_1/packed:output:0zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2	
zeros_1u
transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose/perm{
	transpose	Transposeinputstranspose/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
	transposeO
Shape_1Shapetranspose:y:0*
T0*
_output_shapes
:2	
Shape_1x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
TensorArrayV2/element_shapeВ
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2П
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   27
5TensorArrayUnstack/TensorListFromTensor/element_shapeј
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type02)
'TensorArrayUnstack/TensorListFromTensorx
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ќ
strided_slice_2StridedSlicetranspose:y:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_2
lstm_cell_17/ones_like/ShapeShapestrided_slice_2:output:0*
T0*
_output_shapes
:2
lstm_cell_17/ones_like/Shape
lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2
lstm_cell_17/ones_like/ConstИ
lstm_cell_17/ones_likeFill%lstm_cell_17/ones_like/Shape:output:0%lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like}
lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout/ConstГ
lstm_cell_17/dropout/MulMullstm_cell_17/ones_like:output:0#lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/Mul
lstm_cell_17/dropout/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout/Shapeњ
1lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform#lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Яц23
1lstm_cell_17/dropout/random_uniform/RandomUniform
#lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2%
#lstm_cell_17/dropout/GreaterEqual/yђ
!lstm_cell_17/dropout/GreaterEqualGreaterEqual:lstm_cell_17/dropout/random_uniform/RandomUniform:output:0,lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_cell_17/dropout/GreaterEqualІ
lstm_cell_17/dropout/CastCast%lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/CastЎ
lstm_cell_17/dropout/Mul_1Mullstm_cell_17/dropout/Mul:z:0lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout/Mul_1
lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_1/ConstЙ
lstm_cell_17/dropout_1/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/Mul
lstm_cell_17/dropout_1/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_1/Shapeџ
3lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЖЊw25
3lstm_cell_17/dropout_1/random_uniform/RandomUniform
%lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_1/GreaterEqual/yњ
#lstm_cell_17/dropout_1/GreaterEqualGreaterEqual<lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_1/GreaterEqualЌ
lstm_cell_17/dropout_1/CastCast'lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/CastЖ
lstm_cell_17/dropout_1/Mul_1Mullstm_cell_17/dropout_1/Mul:z:0lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_1/Mul_1
lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_2/ConstЙ
lstm_cell_17/dropout_2/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/Mul
lstm_cell_17/dropout_2/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_2/Shape
3lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Ё25
3lstm_cell_17/dropout_2/random_uniform/RandomUniform
%lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_2/GreaterEqual/yњ
#lstm_cell_17/dropout_2/GreaterEqualGreaterEqual<lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_2/GreaterEqualЌ
lstm_cell_17/dropout_2/CastCast'lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/CastЖ
lstm_cell_17/dropout_2/Mul_1Mullstm_cell_17/dropout_2/Mul:z:0lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_2/Mul_1
lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_3/ConstЙ
lstm_cell_17/dropout_3/MulMullstm_cell_17/ones_like:output:0%lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/Mul
lstm_cell_17/dropout_3/ShapeShapelstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_3/Shape
3lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2џКп25
3lstm_cell_17/dropout_3/random_uniform/RandomUniform
%lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_3/GreaterEqual/yњ
#lstm_cell_17/dropout_3/GreaterEqualGreaterEqual<lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_3/GreaterEqualЌ
lstm_cell_17/dropout_3/CastCast'lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/CastЖ
lstm_cell_17/dropout_3/Mul_1Mullstm_cell_17/dropout_3/Mul:z:0lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_3/Mul_1~
lstm_cell_17/ones_like_1/ShapeShapezeros:output:0*
T0*
_output_shapes
:2 
lstm_cell_17/ones_like_1/Shape
lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2 
lstm_cell_17/ones_like_1/ConstР
lstm_cell_17/ones_like_1Fill'lstm_cell_17/ones_like_1/Shape:output:0'lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/ones_like_1
lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_4/ConstЛ
lstm_cell_17/dropout_4/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/Mul
lstm_cell_17/dropout_4/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_4/Shape
3lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2р25
3lstm_cell_17/dropout_4/random_uniform/RandomUniform
%lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_4/GreaterEqual/yњ
#lstm_cell_17/dropout_4/GreaterEqualGreaterEqual<lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_4/GreaterEqualЌ
lstm_cell_17/dropout_4/CastCast'lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/CastЖ
lstm_cell_17/dropout_4/Mul_1Mullstm_cell_17/dropout_4/Mul:z:0lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_4/Mul_1
lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_5/ConstЛ
lstm_cell_17/dropout_5/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/Mul
lstm_cell_17/dropout_5/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_5/Shapeџ
3lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ю;25
3lstm_cell_17/dropout_5/random_uniform/RandomUniform
%lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_5/GreaterEqual/yњ
#lstm_cell_17/dropout_5/GreaterEqualGreaterEqual<lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_5/GreaterEqualЌ
lstm_cell_17/dropout_5/CastCast'lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/CastЖ
lstm_cell_17/dropout_5/Mul_1Mullstm_cell_17/dropout_5/Mul:z:0lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_5/Mul_1
lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_6/ConstЛ
lstm_cell_17/dropout_6/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/Mul
lstm_cell_17/dropout_6/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_6/Shape
3lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЄЪ25
3lstm_cell_17/dropout_6/random_uniform/RandomUniform
%lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_6/GreaterEqual/yњ
#lstm_cell_17/dropout_6/GreaterEqualGreaterEqual<lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_6/GreaterEqualЌ
lstm_cell_17/dropout_6/CastCast'lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/CastЖ
lstm_cell_17/dropout_6/Mul_1Mullstm_cell_17/dropout_6/Mul:z:0lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_6/Mul_1
lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2
lstm_cell_17/dropout_7/ConstЛ
lstm_cell_17/dropout_7/MulMul!lstm_cell_17/ones_like_1:output:0%lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/Mul
lstm_cell_17/dropout_7/ShapeShape!lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2
lstm_cell_17/dropout_7/Shape
3lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform%lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2уйР25
3lstm_cell_17/dropout_7/random_uniform/RandomUniform
%lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2'
%lstm_cell_17/dropout_7/GreaterEqual/yњ
#lstm_cell_17/dropout_7/GreaterEqualGreaterEqual<lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:0.lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_cell_17/dropout_7/GreaterEqualЌ
lstm_cell_17/dropout_7/CastCast'lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/CastЖ
lstm_cell_17/dropout_7/Mul_1Mullstm_cell_17/dropout_7/Mul:z:0lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/dropout_7/Mul_1
lstm_cell_17/mulMulstrided_slice_2:output:0lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul
lstm_cell_17/mul_1Mulstrided_slice_2:output:0 lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_1
lstm_cell_17/mul_2Mulstrided_slice_2:output:0 lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_2
lstm_cell_17/mul_3Mulstrided_slice_2:output:0 lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_3~
lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_cell_17/split/split_dimВ
!lstm_cell_17/split/ReadVariableOpReadVariableOp*lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02#
!lstm_cell_17/split/ReadVariableOpл
lstm_cell_17/splitSplit%lstm_cell_17/split/split_dim:output:0)lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_cell_17/split
lstm_cell_17/MatMulMatMullstm_cell_17/mul:z:0lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul
lstm_cell_17/MatMul_1MatMullstm_cell_17/mul_1:z:0lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_1
lstm_cell_17/MatMul_2MatMullstm_cell_17/mul_2:z:0lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_2
lstm_cell_17/MatMul_3MatMullstm_cell_17/mul_3:z:0lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_3
lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2 
lstm_cell_17/split_1/split_dimД
#lstm_cell_17/split_1/ReadVariableOpReadVariableOp,lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02%
#lstm_cell_17/split_1/ReadVariableOpг
lstm_cell_17/split_1Split'lstm_cell_17/split_1/split_dim:output:0+lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_cell_17/split_1Ї
lstm_cell_17/BiasAddBiasAddlstm_cell_17/MatMul:product:0lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd­
lstm_cell_17/BiasAdd_1BiasAddlstm_cell_17/MatMul_1:product:0lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_1­
lstm_cell_17/BiasAdd_2BiasAddlstm_cell_17/MatMul_2:product:0lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_2­
lstm_cell_17/BiasAdd_3BiasAddlstm_cell_17/MatMul_3:product:0lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/BiasAdd_3
lstm_cell_17/mul_4Mulzeros:output:0 lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_4
lstm_cell_17/mul_5Mulzeros:output:0 lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_5
lstm_cell_17/mul_6Mulzeros:output:0 lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_6
lstm_cell_17/mul_7Mulzeros:output:0 lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_7 
lstm_cell_17/ReadVariableOpReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp
 lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2"
 lstm_cell_17/strided_slice/stack
"lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice/stack_1
"lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2$
"lstm_cell_17/strided_slice/stack_2Ъ
lstm_cell_17/strided_sliceStridedSlice#lstm_cell_17/ReadVariableOp:value:0)lstm_cell_17/strided_slice/stack:output:0+lstm_cell_17/strided_slice/stack_1:output:0+lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_sliceЇ
lstm_cell_17/MatMul_4MatMullstm_cell_17/mul_4:z:0#lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_4
lstm_cell_17/addAddV2lstm_cell_17/BiasAdd:output:0lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add
lstm_cell_17/SigmoidSigmoidlstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/SigmoidЄ
lstm_cell_17/ReadVariableOp_1ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_1
"lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2$
"lstm_cell_17/strided_slice_1/stack
$lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2&
$lstm_cell_17/strided_slice_1/stack_1
$lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_1/stack_2ж
lstm_cell_17/strided_slice_1StridedSlice%lstm_cell_17/ReadVariableOp_1:value:0+lstm_cell_17/strided_slice_1/stack:output:0-lstm_cell_17/strided_slice_1/stack_1:output:0-lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_1Љ
lstm_cell_17/MatMul_5MatMullstm_cell_17/mul_5:z:0%lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_5Ѕ
lstm_cell_17/add_1AddV2lstm_cell_17/BiasAdd_1:output:0lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_1
lstm_cell_17/Sigmoid_1Sigmoidlstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_1
lstm_cell_17/mul_8Mullstm_cell_17/Sigmoid_1:y:0zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_8Є
lstm_cell_17/ReadVariableOp_2ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_2
"lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2$
"lstm_cell_17/strided_slice_2/stack
$lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2&
$lstm_cell_17/strided_slice_2/stack_1
$lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_2/stack_2ж
lstm_cell_17/strided_slice_2StridedSlice%lstm_cell_17/ReadVariableOp_2:value:0+lstm_cell_17/strided_slice_2/stack:output:0-lstm_cell_17/strided_slice_2/stack_1:output:0-lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_2Љ
lstm_cell_17/MatMul_6MatMullstm_cell_17/mul_6:z:0%lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_6Ѕ
lstm_cell_17/add_2AddV2lstm_cell_17/BiasAdd_2:output:0lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_2x
lstm_cell_17/TanhTanhlstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh
lstm_cell_17/mul_9Mullstm_cell_17/Sigmoid:y:0lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_9
lstm_cell_17/add_3AddV2lstm_cell_17/mul_8:z:0lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_3Є
lstm_cell_17/ReadVariableOp_3ReadVariableOp$lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02
lstm_cell_17/ReadVariableOp_3
"lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2$
"lstm_cell_17/strided_slice_3/stack
$lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2&
$lstm_cell_17/strided_slice_3/stack_1
$lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2&
$lstm_cell_17/strided_slice_3/stack_2ж
lstm_cell_17/strided_slice_3StridedSlice%lstm_cell_17/ReadVariableOp_3:value:0+lstm_cell_17/strided_slice_3/stack:output:0-lstm_cell_17/strided_slice_3/stack_1:output:0-lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2
lstm_cell_17/strided_slice_3Љ
lstm_cell_17/MatMul_7MatMullstm_cell_17/mul_7:z:0%lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/MatMul_7Ѕ
lstm_cell_17/add_4AddV2lstm_cell_17/BiasAdd_3:output:0lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/add_4
lstm_cell_17/Sigmoid_2Sigmoidlstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Sigmoid_2|
lstm_cell_17/Tanh_1Tanhlstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/Tanh_1
lstm_cell_17/mul_10Mullstm_cell_17/Sigmoid_2:y:0lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_cell_17/mul_10
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2
TensorArrayV2_1/element_shapeИ
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
TensorArrayV2_1N
timeConst*
_output_shapes
: *
dtype0*
value	B : 2
time
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
while/maximum_iterationsj
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
while/loop_counterц
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0zeros:output:0zeros_1:output:0strided_slice_1:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0*lstm_cell_17_split_readvariableop_resource,lstm_cell_17_split_1_readvariableop_resource$lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*
bodyR
while_body_1216768*
condR
while_cond_1216767*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
whileЕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   22
0TensorArrayV2Stack/TensorListStack/element_shapeщ
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*,
_output_shapes
:џџџџџџџџџd*
element_dtype02$
"TensorArrayV2Stack/TensorListStack
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2
strided_slice_3StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
strided_slice_3y
transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
transpose_1/permІ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
transpose_1f
runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2	
runtimeМ
IdentityIdentitystrided_slice_3:output:0^lstm_cell_17/ReadVariableOp^lstm_cell_17/ReadVariableOp_1^lstm_cell_17/ReadVariableOp_2^lstm_cell_17/ReadVariableOp_3"^lstm_cell_17/split/ReadVariableOp$^lstm_cell_17/split_1/ReadVariableOp^while*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:џџџџџџџџџd: : : 2:
lstm_cell_17/ReadVariableOplstm_cell_17/ReadVariableOp2>
lstm_cell_17/ReadVariableOp_1lstm_cell_17/ReadVariableOp_12>
lstm_cell_17/ReadVariableOp_2lstm_cell_17/ReadVariableOp_22>
lstm_cell_17/ReadVariableOp_3lstm_cell_17/ReadVariableOp_32F
!lstm_cell_17/split/ReadVariableOp!lstm_cell_17/split/ReadVariableOp2J
#lstm_cell_17/split_1/ReadVariableOp#lstm_cell_17/split_1/ReadVariableOp2
whilewhile:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
ЙЫ
Я
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217450

inputs9
%embedding_17_embedding_lookup_1217189:
ЈУdE
2lstm_17_lstm_cell_17_split_readvariableop_resource:	dC
4lstm_17_lstm_cell_17_split_1_readvariableop_resource:	?
,lstm_17_lstm_cell_17_readvariableop_resource:	d9
'dense_17_matmul_readvariableop_resource:d6
(dense_17_biasadd_readvariableop_resource:
identityЂdense_17/BiasAdd/ReadVariableOpЂdense_17/MatMul/ReadVariableOpЂembedding_17/embedding_lookupЂ#lstm_17/lstm_cell_17/ReadVariableOpЂ%lstm_17/lstm_cell_17/ReadVariableOp_1Ђ%lstm_17/lstm_cell_17/ReadVariableOp_2Ђ%lstm_17/lstm_cell_17/ReadVariableOp_3Ђ)lstm_17/lstm_cell_17/split/ReadVariableOpЂ+lstm_17/lstm_cell_17/split_1/ReadVariableOpЂlstm_17/whilex
embedding_17/CastCastinputs*

DstT0*

SrcT0*(
_output_shapes
:џџџџџџџџџ2
embedding_17/CastС
embedding_17/embedding_lookupResourceGather%embedding_17_embedding_lookup_1217189embedding_17/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*8
_class.
,*loc:@embedding_17/embedding_lookup/1217189*,
_output_shapes
:џџџџџџџџџd*
dtype02
embedding_17/embedding_lookupЃ
&embedding_17/embedding_lookup/IdentityIdentity&embedding_17/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*8
_class.
,*loc:@embedding_17/embedding_lookup/1217189*,
_output_shapes
:џџџџџџџџџd2(
&embedding_17/embedding_lookup/IdentityШ
(embedding_17/embedding_lookup/Identity_1Identity/embedding_17/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2*
(embedding_17/embedding_lookup/Identity_1Д
spatial_dropout1d_17/IdentityIdentity1embedding_17/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
spatial_dropout1d_17/Identityt
lstm_17/ShapeShape&spatial_dropout1d_17/Identity:output:0*
T0*
_output_shapes
:2
lstm_17/Shape
lstm_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
lstm_17/strided_slice/stack
lstm_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
lstm_17/strided_slice/stack_1
lstm_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
lstm_17/strided_slice/stack_2
lstm_17/strided_sliceStridedSlicelstm_17/Shape:output:0$lstm_17/strided_slice/stack:output:0&lstm_17/strided_slice/stack_1:output:0&lstm_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
lstm_17/strided_slicel
lstm_17/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros/mul/y
lstm_17/zeros/mulMullstm_17/strided_slice:output:0lstm_17/zeros/mul/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros/mulo
lstm_17/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
lstm_17/zeros/Less/y
lstm_17/zeros/LessLesslstm_17/zeros/mul:z:0lstm_17/zeros/Less/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros/Lessr
lstm_17/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros/packed/1Ѓ
lstm_17/zeros/packedPacklstm_17/strided_slice:output:0lstm_17/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
lstm_17/zeros/packedo
lstm_17/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
lstm_17/zeros/Const
lstm_17/zerosFilllstm_17/zeros/packed:output:0lstm_17/zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/zerosp
lstm_17/zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros_1/mul/y
lstm_17/zeros_1/mulMullstm_17/strided_slice:output:0lstm_17/zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros_1/muls
lstm_17/zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
lstm_17/zeros_1/Less/y
lstm_17/zeros_1/LessLesslstm_17/zeros_1/mul:z:0lstm_17/zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros_1/Lessv
lstm_17/zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros_1/packed/1Љ
lstm_17/zeros_1/packedPacklstm_17/strided_slice:output:0!lstm_17/zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
lstm_17/zeros_1/packeds
lstm_17/zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
lstm_17/zeros_1/Const
lstm_17/zeros_1Filllstm_17/zeros_1/packed:output:0lstm_17/zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/zeros_1
lstm_17/transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
lstm_17/transpose/permГ
lstm_17/transpose	Transpose&spatial_dropout1d_17/Identity:output:0lstm_17/transpose/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
lstm_17/transposeg
lstm_17/Shape_1Shapelstm_17/transpose:y:0*
T0*
_output_shapes
:2
lstm_17/Shape_1
lstm_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
lstm_17/strided_slice_1/stack
lstm_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_1/stack_1
lstm_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_1/stack_2
lstm_17/strided_slice_1StridedSlicelstm_17/Shape_1:output:0&lstm_17/strided_slice_1/stack:output:0(lstm_17/strided_slice_1/stack_1:output:0(lstm_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
lstm_17/strided_slice_1
#lstm_17/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2%
#lstm_17/TensorArrayV2/element_shapeв
lstm_17/TensorArrayV2TensorListReserve,lstm_17/TensorArrayV2/element_shape:output:0 lstm_17/strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
lstm_17/TensorArrayV2Я
=lstm_17/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2?
=lstm_17/TensorArrayUnstack/TensorListFromTensor/element_shape
/lstm_17/TensorArrayUnstack/TensorListFromTensorTensorListFromTensorlstm_17/transpose:y:0Flstm_17/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type021
/lstm_17/TensorArrayUnstack/TensorListFromTensor
lstm_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
lstm_17/strided_slice_2/stack
lstm_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_2/stack_1
lstm_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_2/stack_2Ќ
lstm_17/strided_slice_2StridedSlicelstm_17/transpose:y:0&lstm_17/strided_slice_2/stack:output:0(lstm_17/strided_slice_2/stack_1:output:0(lstm_17/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
lstm_17/strided_slice_2
$lstm_17/lstm_cell_17/ones_like/ShapeShape lstm_17/strided_slice_2:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/ones_like/Shape
$lstm_17/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$lstm_17/lstm_cell_17/ones_like/Constи
lstm_17/lstm_cell_17/ones_likeFill-lstm_17/lstm_cell_17/ones_like/Shape:output:0-lstm_17/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/ones_like
&lstm_17/lstm_cell_17/ones_like_1/ShapeShapelstm_17/zeros:output:0*
T0*
_output_shapes
:2(
&lstm_17/lstm_cell_17/ones_like_1/Shape
&lstm_17/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2(
&lstm_17/lstm_cell_17/ones_like_1/Constр
 lstm_17/lstm_cell_17/ones_like_1Fill/lstm_17/lstm_cell_17/ones_like_1/Shape:output:0/lstm_17/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/lstm_cell_17/ones_like_1И
lstm_17/lstm_cell_17/mulMul lstm_17/strided_slice_2:output:0'lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mulМ
lstm_17/lstm_cell_17/mul_1Mul lstm_17/strided_slice_2:output:0'lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_1М
lstm_17/lstm_cell_17/mul_2Mul lstm_17/strided_slice_2:output:0'lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_2М
lstm_17/lstm_cell_17/mul_3Mul lstm_17/strided_slice_2:output:0'lstm_17/lstm_cell_17/ones_like:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_3
$lstm_17/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$lstm_17/lstm_cell_17/split/split_dimЪ
)lstm_17/lstm_cell_17/split/ReadVariableOpReadVariableOp2lstm_17_lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02+
)lstm_17/lstm_cell_17/split/ReadVariableOpћ
lstm_17/lstm_cell_17/splitSplit-lstm_17/lstm_cell_17/split/split_dim:output:01lstm_17/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_17/lstm_cell_17/splitЙ
lstm_17/lstm_cell_17/MatMulMatMullstm_17/lstm_cell_17/mul:z:0#lstm_17/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMulП
lstm_17/lstm_cell_17/MatMul_1MatMullstm_17/lstm_cell_17/mul_1:z:0#lstm_17/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_1П
lstm_17/lstm_cell_17/MatMul_2MatMullstm_17/lstm_cell_17/mul_2:z:0#lstm_17/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_2П
lstm_17/lstm_cell_17/MatMul_3MatMullstm_17/lstm_cell_17/mul_3:z:0#lstm_17/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_3
&lstm_17/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2(
&lstm_17/lstm_cell_17/split_1/split_dimЬ
+lstm_17/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4lstm_17_lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02-
+lstm_17/lstm_cell_17/split_1/ReadVariableOpѓ
lstm_17/lstm_cell_17/split_1Split/lstm_17/lstm_cell_17/split_1/split_dim:output:03lstm_17/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_17/lstm_cell_17/split_1Ч
lstm_17/lstm_cell_17/BiasAddBiasAdd%lstm_17/lstm_cell_17/MatMul:product:0%lstm_17/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/BiasAddЭ
lstm_17/lstm_cell_17/BiasAdd_1BiasAdd'lstm_17/lstm_cell_17/MatMul_1:product:0%lstm_17/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/BiasAdd_1Э
lstm_17/lstm_cell_17/BiasAdd_2BiasAdd'lstm_17/lstm_cell_17/MatMul_2:product:0%lstm_17/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/BiasAdd_2Э
lstm_17/lstm_cell_17/BiasAdd_3BiasAdd'lstm_17/lstm_cell_17/MatMul_3:product:0%lstm_17/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/BiasAdd_3Д
lstm_17/lstm_cell_17/mul_4Mullstm_17/zeros:output:0)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_4Д
lstm_17/lstm_cell_17/mul_5Mullstm_17/zeros:output:0)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_5Д
lstm_17/lstm_cell_17/mul_6Mullstm_17/zeros:output:0)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_6Д
lstm_17/lstm_cell_17/mul_7Mullstm_17/zeros:output:0)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_7И
#lstm_17/lstm_cell_17/ReadVariableOpReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02%
#lstm_17/lstm_cell_17/ReadVariableOpЅ
(lstm_17/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2*
(lstm_17/lstm_cell_17/strided_slice/stackЉ
*lstm_17/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2,
*lstm_17/lstm_cell_17/strided_slice/stack_1Љ
*lstm_17/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*lstm_17/lstm_cell_17/strided_slice/stack_2њ
"lstm_17/lstm_cell_17/strided_sliceStridedSlice+lstm_17/lstm_cell_17/ReadVariableOp:value:01lstm_17/lstm_cell_17/strided_slice/stack:output:03lstm_17/lstm_cell_17/strided_slice/stack_1:output:03lstm_17/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"lstm_17/lstm_cell_17/strided_sliceЧ
lstm_17/lstm_cell_17/MatMul_4MatMullstm_17/lstm_cell_17/mul_4:z:0+lstm_17/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_4П
lstm_17/lstm_cell_17/addAddV2%lstm_17/lstm_cell_17/BiasAdd:output:0'lstm_17/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add
lstm_17/lstm_cell_17/SigmoidSigmoidlstm_17/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/SigmoidМ
%lstm_17/lstm_cell_17/ReadVariableOp_1ReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02'
%lstm_17/lstm_cell_17/ReadVariableOp_1Љ
*lstm_17/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2,
*lstm_17/lstm_cell_17/strided_slice_1/stack­
,lstm_17/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2.
,lstm_17/lstm_cell_17/strided_slice_1/stack_1­
,lstm_17/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2.
,lstm_17/lstm_cell_17/strided_slice_1/stack_2
$lstm_17/lstm_cell_17/strided_slice_1StridedSlice-lstm_17/lstm_cell_17/ReadVariableOp_1:value:03lstm_17/lstm_cell_17/strided_slice_1/stack:output:05lstm_17/lstm_cell_17/strided_slice_1/stack_1:output:05lstm_17/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2&
$lstm_17/lstm_cell_17/strided_slice_1Щ
lstm_17/lstm_cell_17/MatMul_5MatMullstm_17/lstm_cell_17/mul_5:z:0-lstm_17/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_5Х
lstm_17/lstm_cell_17/add_1AddV2'lstm_17/lstm_cell_17/BiasAdd_1:output:0'lstm_17/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_1
lstm_17/lstm_cell_17/Sigmoid_1Sigmoidlstm_17/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/Sigmoid_1Џ
lstm_17/lstm_cell_17/mul_8Mul"lstm_17/lstm_cell_17/Sigmoid_1:y:0lstm_17/zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_8М
%lstm_17/lstm_cell_17/ReadVariableOp_2ReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02'
%lstm_17/lstm_cell_17/ReadVariableOp_2Љ
*lstm_17/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*lstm_17/lstm_cell_17/strided_slice_2/stack­
,lstm_17/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2.
,lstm_17/lstm_cell_17/strided_slice_2/stack_1­
,lstm_17/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2.
,lstm_17/lstm_cell_17/strided_slice_2/stack_2
$lstm_17/lstm_cell_17/strided_slice_2StridedSlice-lstm_17/lstm_cell_17/ReadVariableOp_2:value:03lstm_17/lstm_cell_17/strided_slice_2/stack:output:05lstm_17/lstm_cell_17/strided_slice_2/stack_1:output:05lstm_17/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2&
$lstm_17/lstm_cell_17/strided_slice_2Щ
lstm_17/lstm_cell_17/MatMul_6MatMullstm_17/lstm_cell_17/mul_6:z:0-lstm_17/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_6Х
lstm_17/lstm_cell_17/add_2AddV2'lstm_17/lstm_cell_17/BiasAdd_2:output:0'lstm_17/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_2
lstm_17/lstm_cell_17/TanhTanhlstm_17/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/TanhВ
lstm_17/lstm_cell_17/mul_9Mul lstm_17/lstm_cell_17/Sigmoid:y:0lstm_17/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_9Г
lstm_17/lstm_cell_17/add_3AddV2lstm_17/lstm_cell_17/mul_8:z:0lstm_17/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_3М
%lstm_17/lstm_cell_17/ReadVariableOp_3ReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02'
%lstm_17/lstm_cell_17/ReadVariableOp_3Љ
*lstm_17/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*lstm_17/lstm_cell_17/strided_slice_3/stack­
,lstm_17/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2.
,lstm_17/lstm_cell_17/strided_slice_3/stack_1­
,lstm_17/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2.
,lstm_17/lstm_cell_17/strided_slice_3/stack_2
$lstm_17/lstm_cell_17/strided_slice_3StridedSlice-lstm_17/lstm_cell_17/ReadVariableOp_3:value:03lstm_17/lstm_cell_17/strided_slice_3/stack:output:05lstm_17/lstm_cell_17/strided_slice_3/stack_1:output:05lstm_17/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2&
$lstm_17/lstm_cell_17/strided_slice_3Щ
lstm_17/lstm_cell_17/MatMul_7MatMullstm_17/lstm_cell_17/mul_7:z:0-lstm_17/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_7Х
lstm_17/lstm_cell_17/add_4AddV2'lstm_17/lstm_cell_17/BiasAdd_3:output:0'lstm_17/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_4
lstm_17/lstm_cell_17/Sigmoid_2Sigmoidlstm_17/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/Sigmoid_2
lstm_17/lstm_cell_17/Tanh_1Tanhlstm_17/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/Tanh_1И
lstm_17/lstm_cell_17/mul_10Mul"lstm_17/lstm_cell_17/Sigmoid_2:y:0lstm_17/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_10
%lstm_17/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2'
%lstm_17/TensorArrayV2_1/element_shapeи
lstm_17/TensorArrayV2_1TensorListReserve.lstm_17/TensorArrayV2_1/element_shape:output:0 lstm_17/strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
lstm_17/TensorArrayV2_1^
lstm_17/timeConst*
_output_shapes
: *
dtype0*
value	B : 2
lstm_17/time
 lstm_17/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2"
 lstm_17/while/maximum_iterationsz
lstm_17/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
lstm_17/while/loop_counterо
lstm_17/whileWhile#lstm_17/while/loop_counter:output:0)lstm_17/while/maximum_iterations:output:0lstm_17/time:output:0 lstm_17/TensorArrayV2_1:handle:0lstm_17/zeros:output:0lstm_17/zeros_1:output:0 lstm_17/strided_slice_1:output:0?lstm_17/TensorArrayUnstack/TensorListFromTensor:output_handle:02lstm_17_lstm_cell_17_split_readvariableop_resource4lstm_17_lstm_cell_17_split_1_readvariableop_resource,lstm_17_lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*&
bodyR
lstm_17_while_body_1217309*&
condR
lstm_17_while_cond_1217308*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
lstm_17/whileХ
8lstm_17/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2:
8lstm_17/TensorArrayV2Stack/TensorListStack/element_shape
*lstm_17/TensorArrayV2Stack/TensorListStackTensorListStacklstm_17/while:output:3Alstm_17/TensorArrayV2Stack/TensorListStack/element_shape:output:0*,
_output_shapes
:џџџџџџџџџd*
element_dtype02,
*lstm_17/TensorArrayV2Stack/TensorListStack
lstm_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
lstm_17/strided_slice_3/stack
lstm_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
lstm_17/strided_slice_3/stack_1
lstm_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_3/stack_2Ъ
lstm_17/strided_slice_3StridedSlice3lstm_17/TensorArrayV2Stack/TensorListStack:tensor:0&lstm_17/strided_slice_3/stack:output:0(lstm_17/strided_slice_3/stack_1:output:0(lstm_17/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
lstm_17/strided_slice_3
lstm_17/transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
lstm_17/transpose_1/permЦ
lstm_17/transpose_1	Transpose3lstm_17/TensorArrayV2Stack/TensorListStack:tensor:0!lstm_17/transpose_1/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
lstm_17/transpose_1v
lstm_17/runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2
lstm_17/runtimeЈ
dense_17/MatMul/ReadVariableOpReadVariableOp'dense_17_matmul_readvariableop_resource*
_output_shapes

:d*
dtype02 
dense_17/MatMul/ReadVariableOpЈ
dense_17/MatMulMatMul lstm_17/strided_slice_3:output:0&dense_17/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
dense_17/MatMulЇ
dense_17/BiasAdd/ReadVariableOpReadVariableOp(dense_17_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
dense_17/BiasAdd/ReadVariableOpЅ
dense_17/BiasAddBiasAdddense_17/MatMul:product:0'dense_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
dense_17/BiasAdd|
dense_17/SoftmaxSoftmaxdense_17/BiasAdd:output:0*
T0*'
_output_shapes
:џџџџџџџџџ2
dense_17/Softmaxй
IdentityIdentitydense_17/Softmax:softmax:0 ^dense_17/BiasAdd/ReadVariableOp^dense_17/MatMul/ReadVariableOp^embedding_17/embedding_lookup$^lstm_17/lstm_cell_17/ReadVariableOp&^lstm_17/lstm_cell_17/ReadVariableOp_1&^lstm_17/lstm_cell_17/ReadVariableOp_2&^lstm_17/lstm_cell_17/ReadVariableOp_3*^lstm_17/lstm_cell_17/split/ReadVariableOp,^lstm_17/lstm_cell_17/split_1/ReadVariableOp^lstm_17/while*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 2B
dense_17/BiasAdd/ReadVariableOpdense_17/BiasAdd/ReadVariableOp2@
dense_17/MatMul/ReadVariableOpdense_17/MatMul/ReadVariableOp2>
embedding_17/embedding_lookupembedding_17/embedding_lookup2J
#lstm_17/lstm_cell_17/ReadVariableOp#lstm_17/lstm_cell_17/ReadVariableOp2N
%lstm_17/lstm_cell_17/ReadVariableOp_1%lstm_17/lstm_cell_17/ReadVariableOp_12N
%lstm_17/lstm_cell_17/ReadVariableOp_2%lstm_17/lstm_cell_17/ReadVariableOp_22N
%lstm_17/lstm_cell_17/ReadVariableOp_3%lstm_17/lstm_cell_17/ReadVariableOp_32V
)lstm_17/lstm_cell_17/split/ReadVariableOp)lstm_17/lstm_cell_17/split/ReadVariableOp2Z
+lstm_17/lstm_cell_17/split_1/ReadVariableOp+lstm_17/lstm_cell_17/split_1/ReadVariableOp2
lstm_17/whilelstm_17/while:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs


.__inference_embedding_17_layer_call_fn_1217867

inputs
unknown:
ЈУd
identityЂStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_embedding_17_layer_call_and_return_conditional_losses_12162632
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:џџџџџџџџџ: 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs
ъ
И
)__inference_lstm_17_layer_call_fn_1217962
inputs_0
unknown:	d
	unknown_0:	
	unknown_1:	d
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputs_0unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *M
fHRF
D__inference_lstm_17_layer_call_and_return_conditional_losses_12156602
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:џџџџџџџџџџџџџџџџџџd: : : 22
StatefulPartitionedCallStatefulPartitionedCall:^ Z
4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџd
"
_user_specified_name
inputs/0
к
Ш
while_cond_1215914
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1215914___redundant_placeholder05
1while_while_cond_1215914___redundant_placeholder15
1while_while_cond_1215914___redundant_placeholder25
1while_while_cond_1215914___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:
Їб
Я
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217860

inputs9
%embedding_17_embedding_lookup_1217454:
ЈУdE
2lstm_17_lstm_cell_17_split_readvariableop_resource:	dC
4lstm_17_lstm_cell_17_split_1_readvariableop_resource:	?
,lstm_17_lstm_cell_17_readvariableop_resource:	d9
'dense_17_matmul_readvariableop_resource:d6
(dense_17_biasadd_readvariableop_resource:
identityЂdense_17/BiasAdd/ReadVariableOpЂdense_17/MatMul/ReadVariableOpЂembedding_17/embedding_lookupЂ#lstm_17/lstm_cell_17/ReadVariableOpЂ%lstm_17/lstm_cell_17/ReadVariableOp_1Ђ%lstm_17/lstm_cell_17/ReadVariableOp_2Ђ%lstm_17/lstm_cell_17/ReadVariableOp_3Ђ)lstm_17/lstm_cell_17/split/ReadVariableOpЂ+lstm_17/lstm_cell_17/split_1/ReadVariableOpЂlstm_17/whilex
embedding_17/CastCastinputs*

DstT0*

SrcT0*(
_output_shapes
:џџџџџџџџџ2
embedding_17/CastС
embedding_17/embedding_lookupResourceGather%embedding_17_embedding_lookup_1217454embedding_17/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*8
_class.
,*loc:@embedding_17/embedding_lookup/1217454*,
_output_shapes
:џџџџџџџџџd*
dtype02
embedding_17/embedding_lookupЃ
&embedding_17/embedding_lookup/IdentityIdentity&embedding_17/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*8
_class.
,*loc:@embedding_17/embedding_lookup/1217454*,
_output_shapes
:џџџџџџџџџd2(
&embedding_17/embedding_lookup/IdentityШ
(embedding_17/embedding_lookup/Identity_1Identity/embedding_17/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2*
(embedding_17/embedding_lookup/Identity_1
spatial_dropout1d_17/ShapeShape1embedding_17/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:2
spatial_dropout1d_17/Shape
(spatial_dropout1d_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2*
(spatial_dropout1d_17/strided_slice/stackЂ
*spatial_dropout1d_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2,
*spatial_dropout1d_17/strided_slice/stack_1Ђ
*spatial_dropout1d_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2,
*spatial_dropout1d_17/strided_slice/stack_2р
"spatial_dropout1d_17/strided_sliceStridedSlice#spatial_dropout1d_17/Shape:output:01spatial_dropout1d_17/strided_slice/stack:output:03spatial_dropout1d_17/strided_slice/stack_1:output:03spatial_dropout1d_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2$
"spatial_dropout1d_17/strided_sliceЂ
*spatial_dropout1d_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2,
*spatial_dropout1d_17/strided_slice_1/stackІ
,spatial_dropout1d_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2.
,spatial_dropout1d_17/strided_slice_1/stack_1І
,spatial_dropout1d_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2.
,spatial_dropout1d_17/strided_slice_1/stack_2ъ
$spatial_dropout1d_17/strided_slice_1StridedSlice#spatial_dropout1d_17/Shape:output:03spatial_dropout1d_17/strided_slice_1/stack:output:05spatial_dropout1d_17/strided_slice_1/stack_1:output:05spatial_dropout1d_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2&
$spatial_dropout1d_17/strided_slice_1
"spatial_dropout1d_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"spatial_dropout1d_17/dropout/Constт
 spatial_dropout1d_17/dropout/MulMul1embedding_17/embedding_lookup/Identity_1:output:0+spatial_dropout1d_17/dropout/Const:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2"
 spatial_dropout1d_17/dropout/MulЌ
3spatial_dropout1d_17/dropout/random_uniform/shape/1Const*
_output_shapes
: *
dtype0*
value	B :25
3spatial_dropout1d_17/dropout/random_uniform/shape/1Ж
1spatial_dropout1d_17/dropout/random_uniform/shapePack+spatial_dropout1d_17/strided_slice:output:0<spatial_dropout1d_17/dropout/random_uniform/shape/1:output:0-spatial_dropout1d_17/strided_slice_1:output:0*
N*
T0*
_output_shapes
:23
1spatial_dropout1d_17/dropout/random_uniform/shape
9spatial_dropout1d_17/dropout/random_uniform/RandomUniformRandomUniform:spatial_dropout1d_17/dropout/random_uniform/shape:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ*
dtype02;
9spatial_dropout1d_17/dropout/random_uniform/RandomUniform
+spatial_dropout1d_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+spatial_dropout1d_17/dropout/GreaterEqual/y
)spatial_dropout1d_17/dropout/GreaterEqualGreaterEqualBspatial_dropout1d_17/dropout/random_uniform/RandomUniform:output:04spatial_dropout1d_17/dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2+
)spatial_dropout1d_17/dropout/GreaterEqualЫ
!spatial_dropout1d_17/dropout/CastCast-spatial_dropout1d_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :џџџџџџџџџџџџџџџџџџ2#
!spatial_dropout1d_17/dropout/Castг
"spatial_dropout1d_17/dropout/Mul_1Mul$spatial_dropout1d_17/dropout/Mul:z:0%spatial_dropout1d_17/dropout/Cast:y:0*
T0*,
_output_shapes
:џџџџџџџџџd2$
"spatial_dropout1d_17/dropout/Mul_1t
lstm_17/ShapeShape&spatial_dropout1d_17/dropout/Mul_1:z:0*
T0*
_output_shapes
:2
lstm_17/Shape
lstm_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
lstm_17/strided_slice/stack
lstm_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
lstm_17/strided_slice/stack_1
lstm_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
lstm_17/strided_slice/stack_2
lstm_17/strided_sliceStridedSlicelstm_17/Shape:output:0$lstm_17/strided_slice/stack:output:0&lstm_17/strided_slice/stack_1:output:0&lstm_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
lstm_17/strided_slicel
lstm_17/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros/mul/y
lstm_17/zeros/mulMullstm_17/strided_slice:output:0lstm_17/zeros/mul/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros/mulo
lstm_17/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
lstm_17/zeros/Less/y
lstm_17/zeros/LessLesslstm_17/zeros/mul:z:0lstm_17/zeros/Less/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros/Lessr
lstm_17/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros/packed/1Ѓ
lstm_17/zeros/packedPacklstm_17/strided_slice:output:0lstm_17/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
lstm_17/zeros/packedo
lstm_17/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
lstm_17/zeros/Const
lstm_17/zerosFilllstm_17/zeros/packed:output:0lstm_17/zeros/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/zerosp
lstm_17/zeros_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros_1/mul/y
lstm_17/zeros_1/mulMullstm_17/strided_slice:output:0lstm_17/zeros_1/mul/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros_1/muls
lstm_17/zeros_1/Less/yConst*
_output_shapes
: *
dtype0*
value
B :ш2
lstm_17/zeros_1/Less/y
lstm_17/zeros_1/LessLesslstm_17/zeros_1/mul:z:0lstm_17/zeros_1/Less/y:output:0*
T0*
_output_shapes
: 2
lstm_17/zeros_1/Lessv
lstm_17/zeros_1/packed/1Const*
_output_shapes
: *
dtype0*
value	B :d2
lstm_17/zeros_1/packed/1Љ
lstm_17/zeros_1/packedPacklstm_17/strided_slice:output:0!lstm_17/zeros_1/packed/1:output:0*
N*
T0*
_output_shapes
:2
lstm_17/zeros_1/packeds
lstm_17/zeros_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
lstm_17/zeros_1/Const
lstm_17/zeros_1Filllstm_17/zeros_1/packed:output:0lstm_17/zeros_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/zeros_1
lstm_17/transpose/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
lstm_17/transpose/permГ
lstm_17/transpose	Transpose&spatial_dropout1d_17/dropout/Mul_1:z:0lstm_17/transpose/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
lstm_17/transposeg
lstm_17/Shape_1Shapelstm_17/transpose:y:0*
T0*
_output_shapes
:2
lstm_17/Shape_1
lstm_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
lstm_17/strided_slice_1/stack
lstm_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_1/stack_1
lstm_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_1/stack_2
lstm_17/strided_slice_1StridedSlicelstm_17/Shape_1:output:0&lstm_17/strided_slice_1/stack:output:0(lstm_17/strided_slice_1/stack_1:output:0(lstm_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
lstm_17/strided_slice_1
#lstm_17/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2%
#lstm_17/TensorArrayV2/element_shapeв
lstm_17/TensorArrayV2TensorListReserve,lstm_17/TensorArrayV2/element_shape:output:0 lstm_17/strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
lstm_17/TensorArrayV2Я
=lstm_17/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2?
=lstm_17/TensorArrayUnstack/TensorListFromTensor/element_shape
/lstm_17/TensorArrayUnstack/TensorListFromTensorTensorListFromTensorlstm_17/transpose:y:0Flstm_17/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type021
/lstm_17/TensorArrayUnstack/TensorListFromTensor
lstm_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
lstm_17/strided_slice_2/stack
lstm_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_2/stack_1
lstm_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_2/stack_2Ќ
lstm_17/strided_slice_2StridedSlicelstm_17/transpose:y:0&lstm_17/strided_slice_2/stack:output:0(lstm_17/strided_slice_2/stack_1:output:0(lstm_17/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
lstm_17/strided_slice_2
$lstm_17/lstm_cell_17/ones_like/ShapeShape lstm_17/strided_slice_2:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/ones_like/Shape
$lstm_17/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2&
$lstm_17/lstm_cell_17/ones_like/Constи
lstm_17/lstm_cell_17/ones_likeFill-lstm_17/lstm_cell_17/ones_like/Shape:output:0-lstm_17/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/ones_like
"lstm_17/lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2$
"lstm_17/lstm_cell_17/dropout/Constг
 lstm_17/lstm_cell_17/dropout/MulMul'lstm_17/lstm_cell_17/ones_like:output:0+lstm_17/lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/lstm_cell_17/dropout/Mul
"lstm_17/lstm_cell_17/dropout/ShapeShape'lstm_17/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2$
"lstm_17/lstm_cell_17/dropout/Shape
9lstm_17/lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform+lstm_17/lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Ее2;
9lstm_17/lstm_cell_17/dropout/random_uniform/RandomUniform
+lstm_17/lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2-
+lstm_17/lstm_cell_17/dropout/GreaterEqual/y
)lstm_17/lstm_cell_17/dropout/GreaterEqualGreaterEqualBlstm_17/lstm_cell_17/dropout/random_uniform/RandomUniform:output:04lstm_17/lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/lstm_cell_17/dropout/GreaterEqualО
!lstm_17/lstm_cell_17/dropout/CastCast-lstm_17/lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2#
!lstm_17/lstm_cell_17/dropout/CastЮ
"lstm_17/lstm_cell_17/dropout/Mul_1Mul$lstm_17/lstm_cell_17/dropout/Mul:z:0%lstm_17/lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout/Mul_1
$lstm_17/lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2&
$lstm_17/lstm_cell_17/dropout_1/Constй
"lstm_17/lstm_cell_17/dropout_1/MulMul'lstm_17/lstm_cell_17/ones_like:output:0-lstm_17/lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout_1/MulЃ
$lstm_17/lstm_cell_17/dropout_1/ShapeShape'lstm_17/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/dropout_1/Shape
;lstm_17/lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform-lstm_17/lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Цжљ2=
;lstm_17/lstm_cell_17/dropout_1/random_uniform/RandomUniformЃ
-lstm_17/lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2/
-lstm_17/lstm_cell_17/dropout_1/GreaterEqual/y
+lstm_17/lstm_cell_17/dropout_1/GreaterEqualGreaterEqualDlstm_17/lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:06lstm_17/lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+lstm_17/lstm_cell_17/dropout_1/GreaterEqualФ
#lstm_17/lstm_cell_17/dropout_1/CastCast/lstm_17/lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/lstm_cell_17/dropout_1/Castж
$lstm_17/lstm_cell_17/dropout_1/Mul_1Mul&lstm_17/lstm_cell_17/dropout_1/Mul:z:0'lstm_17/lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/lstm_cell_17/dropout_1/Mul_1
$lstm_17/lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2&
$lstm_17/lstm_cell_17/dropout_2/Constй
"lstm_17/lstm_cell_17/dropout_2/MulMul'lstm_17/lstm_cell_17/ones_like:output:0-lstm_17/lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout_2/MulЃ
$lstm_17/lstm_cell_17/dropout_2/ShapeShape'lstm_17/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/dropout_2/Shape
;lstm_17/lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform-lstm_17/lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Џђ2=
;lstm_17/lstm_cell_17/dropout_2/random_uniform/RandomUniformЃ
-lstm_17/lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2/
-lstm_17/lstm_cell_17/dropout_2/GreaterEqual/y
+lstm_17/lstm_cell_17/dropout_2/GreaterEqualGreaterEqualDlstm_17/lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:06lstm_17/lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+lstm_17/lstm_cell_17/dropout_2/GreaterEqualФ
#lstm_17/lstm_cell_17/dropout_2/CastCast/lstm_17/lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/lstm_cell_17/dropout_2/Castж
$lstm_17/lstm_cell_17/dropout_2/Mul_1Mul&lstm_17/lstm_cell_17/dropout_2/Mul:z:0'lstm_17/lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/lstm_cell_17/dropout_2/Mul_1
$lstm_17/lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2&
$lstm_17/lstm_cell_17/dropout_3/Constй
"lstm_17/lstm_cell_17/dropout_3/MulMul'lstm_17/lstm_cell_17/ones_like:output:0-lstm_17/lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout_3/MulЃ
$lstm_17/lstm_cell_17/dropout_3/ShapeShape'lstm_17/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/dropout_3/Shape
;lstm_17/lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform-lstm_17/lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Ѕй2=
;lstm_17/lstm_cell_17/dropout_3/random_uniform/RandomUniformЃ
-lstm_17/lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2/
-lstm_17/lstm_cell_17/dropout_3/GreaterEqual/y
+lstm_17/lstm_cell_17/dropout_3/GreaterEqualGreaterEqualDlstm_17/lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:06lstm_17/lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+lstm_17/lstm_cell_17/dropout_3/GreaterEqualФ
#lstm_17/lstm_cell_17/dropout_3/CastCast/lstm_17/lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/lstm_cell_17/dropout_3/Castж
$lstm_17/lstm_cell_17/dropout_3/Mul_1Mul&lstm_17/lstm_cell_17/dropout_3/Mul:z:0'lstm_17/lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/lstm_cell_17/dropout_3/Mul_1
&lstm_17/lstm_cell_17/ones_like_1/ShapeShapelstm_17/zeros:output:0*
T0*
_output_shapes
:2(
&lstm_17/lstm_cell_17/ones_like_1/Shape
&lstm_17/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2(
&lstm_17/lstm_cell_17/ones_like_1/Constр
 lstm_17/lstm_cell_17/ones_like_1Fill/lstm_17/lstm_cell_17/ones_like_1/Shape:output:0/lstm_17/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/lstm_cell_17/ones_like_1
$lstm_17/lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2&
$lstm_17/lstm_cell_17/dropout_4/Constл
"lstm_17/lstm_cell_17/dropout_4/MulMul)lstm_17/lstm_cell_17/ones_like_1:output:0-lstm_17/lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout_4/MulЅ
$lstm_17/lstm_cell_17/dropout_4/ShapeShape)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/dropout_4/Shape
;lstm_17/lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform-lstm_17/lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2щѓф2=
;lstm_17/lstm_cell_17/dropout_4/random_uniform/RandomUniformЃ
-lstm_17/lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2/
-lstm_17/lstm_cell_17/dropout_4/GreaterEqual/y
+lstm_17/lstm_cell_17/dropout_4/GreaterEqualGreaterEqualDlstm_17/lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:06lstm_17/lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+lstm_17/lstm_cell_17/dropout_4/GreaterEqualФ
#lstm_17/lstm_cell_17/dropout_4/CastCast/lstm_17/lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/lstm_cell_17/dropout_4/Castж
$lstm_17/lstm_cell_17/dropout_4/Mul_1Mul&lstm_17/lstm_cell_17/dropout_4/Mul:z:0'lstm_17/lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/lstm_cell_17/dropout_4/Mul_1
$lstm_17/lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2&
$lstm_17/lstm_cell_17/dropout_5/Constл
"lstm_17/lstm_cell_17/dropout_5/MulMul)lstm_17/lstm_cell_17/ones_like_1:output:0-lstm_17/lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout_5/MulЅ
$lstm_17/lstm_cell_17/dropout_5/ShapeShape)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/dropout_5/Shape
;lstm_17/lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform-lstm_17/lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ѓќѓ2=
;lstm_17/lstm_cell_17/dropout_5/random_uniform/RandomUniformЃ
-lstm_17/lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2/
-lstm_17/lstm_cell_17/dropout_5/GreaterEqual/y
+lstm_17/lstm_cell_17/dropout_5/GreaterEqualGreaterEqualDlstm_17/lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:06lstm_17/lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+lstm_17/lstm_cell_17/dropout_5/GreaterEqualФ
#lstm_17/lstm_cell_17/dropout_5/CastCast/lstm_17/lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/lstm_cell_17/dropout_5/Castж
$lstm_17/lstm_cell_17/dropout_5/Mul_1Mul&lstm_17/lstm_cell_17/dropout_5/Mul:z:0'lstm_17/lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/lstm_cell_17/dropout_5/Mul_1
$lstm_17/lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2&
$lstm_17/lstm_cell_17/dropout_6/Constл
"lstm_17/lstm_cell_17/dropout_6/MulMul)lstm_17/lstm_cell_17/ones_like_1:output:0-lstm_17/lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout_6/MulЅ
$lstm_17/lstm_cell_17/dropout_6/ShapeShape)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/dropout_6/Shape
;lstm_17/lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform-lstm_17/lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2їИТ2=
;lstm_17/lstm_cell_17/dropout_6/random_uniform/RandomUniformЃ
-lstm_17/lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2/
-lstm_17/lstm_cell_17/dropout_6/GreaterEqual/y
+lstm_17/lstm_cell_17/dropout_6/GreaterEqualGreaterEqualDlstm_17/lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:06lstm_17/lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+lstm_17/lstm_cell_17/dropout_6/GreaterEqualФ
#lstm_17/lstm_cell_17/dropout_6/CastCast/lstm_17/lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/lstm_cell_17/dropout_6/Castж
$lstm_17/lstm_cell_17/dropout_6/Mul_1Mul&lstm_17/lstm_cell_17/dropout_6/Mul:z:0'lstm_17/lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/lstm_cell_17/dropout_6/Mul_1
$lstm_17/lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2&
$lstm_17/lstm_cell_17/dropout_7/Constл
"lstm_17/lstm_cell_17/dropout_7/MulMul)lstm_17/lstm_cell_17/ones_like_1:output:0-lstm_17/lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/lstm_cell_17/dropout_7/MulЅ
$lstm_17/lstm_cell_17/dropout_7/ShapeShape)lstm_17/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2&
$lstm_17/lstm_cell_17/dropout_7/Shape
;lstm_17/lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform-lstm_17/lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2§џ2=
;lstm_17/lstm_cell_17/dropout_7/random_uniform/RandomUniformЃ
-lstm_17/lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>2/
-lstm_17/lstm_cell_17/dropout_7/GreaterEqual/y
+lstm_17/lstm_cell_17/dropout_7/GreaterEqualGreaterEqualDlstm_17/lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:06lstm_17/lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2-
+lstm_17/lstm_cell_17/dropout_7/GreaterEqualФ
#lstm_17/lstm_cell_17/dropout_7/CastCast/lstm_17/lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/lstm_cell_17/dropout_7/Castж
$lstm_17/lstm_cell_17/dropout_7/Mul_1Mul&lstm_17/lstm_cell_17/dropout_7/Mul:z:0'lstm_17/lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/lstm_cell_17/dropout_7/Mul_1З
lstm_17/lstm_cell_17/mulMul lstm_17/strided_slice_2:output:0&lstm_17/lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mulН
lstm_17/lstm_cell_17/mul_1Mul lstm_17/strided_slice_2:output:0(lstm_17/lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_1Н
lstm_17/lstm_cell_17/mul_2Mul lstm_17/strided_slice_2:output:0(lstm_17/lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_2Н
lstm_17/lstm_cell_17/mul_3Mul lstm_17/strided_slice_2:output:0(lstm_17/lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_3
$lstm_17/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$lstm_17/lstm_cell_17/split/split_dimЪ
)lstm_17/lstm_cell_17/split/ReadVariableOpReadVariableOp2lstm_17_lstm_cell_17_split_readvariableop_resource*
_output_shapes
:	d*
dtype02+
)lstm_17/lstm_cell_17/split/ReadVariableOpћ
lstm_17/lstm_cell_17/splitSplit-lstm_17/lstm_cell_17/split/split_dim:output:01lstm_17/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2
lstm_17/lstm_cell_17/splitЙ
lstm_17/lstm_cell_17/MatMulMatMullstm_17/lstm_cell_17/mul:z:0#lstm_17/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMulП
lstm_17/lstm_cell_17/MatMul_1MatMullstm_17/lstm_cell_17/mul_1:z:0#lstm_17/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_1П
lstm_17/lstm_cell_17/MatMul_2MatMullstm_17/lstm_cell_17/mul_2:z:0#lstm_17/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_2П
lstm_17/lstm_cell_17/MatMul_3MatMullstm_17/lstm_cell_17/mul_3:z:0#lstm_17/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_3
&lstm_17/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2(
&lstm_17/lstm_cell_17/split_1/split_dimЬ
+lstm_17/lstm_cell_17/split_1/ReadVariableOpReadVariableOp4lstm_17_lstm_cell_17_split_1_readvariableop_resource*
_output_shapes	
:*
dtype02-
+lstm_17/lstm_cell_17/split_1/ReadVariableOpѓ
lstm_17/lstm_cell_17/split_1Split/lstm_17/lstm_cell_17/split_1/split_dim:output:03lstm_17/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2
lstm_17/lstm_cell_17/split_1Ч
lstm_17/lstm_cell_17/BiasAddBiasAdd%lstm_17/lstm_cell_17/MatMul:product:0%lstm_17/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/BiasAddЭ
lstm_17/lstm_cell_17/BiasAdd_1BiasAdd'lstm_17/lstm_cell_17/MatMul_1:product:0%lstm_17/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/BiasAdd_1Э
lstm_17/lstm_cell_17/BiasAdd_2BiasAdd'lstm_17/lstm_cell_17/MatMul_2:product:0%lstm_17/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/BiasAdd_2Э
lstm_17/lstm_cell_17/BiasAdd_3BiasAdd'lstm_17/lstm_cell_17/MatMul_3:product:0%lstm_17/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/BiasAdd_3Г
lstm_17/lstm_cell_17/mul_4Mullstm_17/zeros:output:0(lstm_17/lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_4Г
lstm_17/lstm_cell_17/mul_5Mullstm_17/zeros:output:0(lstm_17/lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_5Г
lstm_17/lstm_cell_17/mul_6Mullstm_17/zeros:output:0(lstm_17/lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_6Г
lstm_17/lstm_cell_17/mul_7Mullstm_17/zeros:output:0(lstm_17/lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_7И
#lstm_17/lstm_cell_17/ReadVariableOpReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02%
#lstm_17/lstm_cell_17/ReadVariableOpЅ
(lstm_17/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2*
(lstm_17/lstm_cell_17/strided_slice/stackЉ
*lstm_17/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   2,
*lstm_17/lstm_cell_17/strided_slice/stack_1Љ
*lstm_17/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2,
*lstm_17/lstm_cell_17/strided_slice/stack_2њ
"lstm_17/lstm_cell_17/strided_sliceStridedSlice+lstm_17/lstm_cell_17/ReadVariableOp:value:01lstm_17/lstm_cell_17/strided_slice/stack:output:03lstm_17/lstm_cell_17/strided_slice/stack_1:output:03lstm_17/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2$
"lstm_17/lstm_cell_17/strided_sliceЧ
lstm_17/lstm_cell_17/MatMul_4MatMullstm_17/lstm_cell_17/mul_4:z:0+lstm_17/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_4П
lstm_17/lstm_cell_17/addAddV2%lstm_17/lstm_cell_17/BiasAdd:output:0'lstm_17/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add
lstm_17/lstm_cell_17/SigmoidSigmoidlstm_17/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/SigmoidМ
%lstm_17/lstm_cell_17/ReadVariableOp_1ReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02'
%lstm_17/lstm_cell_17/ReadVariableOp_1Љ
*lstm_17/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   2,
*lstm_17/lstm_cell_17/strided_slice_1/stack­
,lstm_17/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   2.
,lstm_17/lstm_cell_17/strided_slice_1/stack_1­
,lstm_17/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2.
,lstm_17/lstm_cell_17/strided_slice_1/stack_2
$lstm_17/lstm_cell_17/strided_slice_1StridedSlice-lstm_17/lstm_cell_17/ReadVariableOp_1:value:03lstm_17/lstm_cell_17/strided_slice_1/stack:output:05lstm_17/lstm_cell_17/strided_slice_1/stack_1:output:05lstm_17/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2&
$lstm_17/lstm_cell_17/strided_slice_1Щ
lstm_17/lstm_cell_17/MatMul_5MatMullstm_17/lstm_cell_17/mul_5:z:0-lstm_17/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_5Х
lstm_17/lstm_cell_17/add_1AddV2'lstm_17/lstm_cell_17/BiasAdd_1:output:0'lstm_17/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_1
lstm_17/lstm_cell_17/Sigmoid_1Sigmoidlstm_17/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/Sigmoid_1Џ
lstm_17/lstm_cell_17/mul_8Mul"lstm_17/lstm_cell_17/Sigmoid_1:y:0lstm_17/zeros_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_8М
%lstm_17/lstm_cell_17/ReadVariableOp_2ReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02'
%lstm_17/lstm_cell_17/ReadVariableOp_2Љ
*lstm_17/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   2,
*lstm_17/lstm_cell_17/strided_slice_2/stack­
,lstm_17/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  2.
,lstm_17/lstm_cell_17/strided_slice_2/stack_1­
,lstm_17/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2.
,lstm_17/lstm_cell_17/strided_slice_2/stack_2
$lstm_17/lstm_cell_17/strided_slice_2StridedSlice-lstm_17/lstm_cell_17/ReadVariableOp_2:value:03lstm_17/lstm_cell_17/strided_slice_2/stack:output:05lstm_17/lstm_cell_17/strided_slice_2/stack_1:output:05lstm_17/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2&
$lstm_17/lstm_cell_17/strided_slice_2Щ
lstm_17/lstm_cell_17/MatMul_6MatMullstm_17/lstm_cell_17/mul_6:z:0-lstm_17/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_6Х
lstm_17/lstm_cell_17/add_2AddV2'lstm_17/lstm_cell_17/BiasAdd_2:output:0'lstm_17/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_2
lstm_17/lstm_cell_17/TanhTanhlstm_17/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/TanhВ
lstm_17/lstm_cell_17/mul_9Mul lstm_17/lstm_cell_17/Sigmoid:y:0lstm_17/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_9Г
lstm_17/lstm_cell_17/add_3AddV2lstm_17/lstm_cell_17/mul_8:z:0lstm_17/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_3М
%lstm_17/lstm_cell_17/ReadVariableOp_3ReadVariableOp,lstm_17_lstm_cell_17_readvariableop_resource*
_output_shapes
:	d*
dtype02'
%lstm_17/lstm_cell_17/ReadVariableOp_3Љ
*lstm_17/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  2,
*lstm_17/lstm_cell_17/strided_slice_3/stack­
,lstm_17/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2.
,lstm_17/lstm_cell_17/strided_slice_3/stack_1­
,lstm_17/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2.
,lstm_17/lstm_cell_17/strided_slice_3/stack_2
$lstm_17/lstm_cell_17/strided_slice_3StridedSlice-lstm_17/lstm_cell_17/ReadVariableOp_3:value:03lstm_17/lstm_cell_17/strided_slice_3/stack:output:05lstm_17/lstm_cell_17/strided_slice_3/stack_1:output:05lstm_17/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2&
$lstm_17/lstm_cell_17/strided_slice_3Щ
lstm_17/lstm_cell_17/MatMul_7MatMullstm_17/lstm_cell_17/mul_7:z:0-lstm_17/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/MatMul_7Х
lstm_17/lstm_cell_17/add_4AddV2'lstm_17/lstm_cell_17/BiasAdd_3:output:0'lstm_17/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/add_4
lstm_17/lstm_cell_17/Sigmoid_2Sigmoidlstm_17/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/lstm_cell_17/Sigmoid_2
lstm_17/lstm_cell_17/Tanh_1Tanhlstm_17/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/Tanh_1И
lstm_17/lstm_cell_17/mul_10Mul"lstm_17/lstm_cell_17/Sigmoid_2:y:0lstm_17/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/lstm_cell_17/mul_10
%lstm_17/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2'
%lstm_17/TensorArrayV2_1/element_shapeи
lstm_17/TensorArrayV2_1TensorListReserve.lstm_17/TensorArrayV2_1/element_shape:output:0 lstm_17/strided_slice_1:output:0*
_output_shapes
: *
element_dtype0*

shape_type02
lstm_17/TensorArrayV2_1^
lstm_17/timeConst*
_output_shapes
: *
dtype0*
value	B : 2
lstm_17/time
 lstm_17/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2"
 lstm_17/while/maximum_iterationsz
lstm_17/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : 2
lstm_17/while/loop_counterо
lstm_17/whileWhile#lstm_17/while/loop_counter:output:0)lstm_17/while/maximum_iterations:output:0lstm_17/time:output:0 lstm_17/TensorArrayV2_1:handle:0lstm_17/zeros:output:0lstm_17/zeros_1:output:0 lstm_17/strided_slice_1:output:0?lstm_17/TensorArrayUnstack/TensorListFromTensor:output_handle:02lstm_17_lstm_cell_17_split_readvariableop_resource4lstm_17_lstm_cell_17_split_1_readvariableop_resource,lstm_17_lstm_cell_17_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*L
_output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *%
_read_only_resource_inputs
	
*&
bodyR
lstm_17_while_body_1217655*&
condR
lstm_17_while_cond_1217654*K
output_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : *
parallel_iterations 2
lstm_17/whileХ
8lstm_17/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2:
8lstm_17/TensorArrayV2Stack/TensorListStack/element_shape
*lstm_17/TensorArrayV2Stack/TensorListStackTensorListStacklstm_17/while:output:3Alstm_17/TensorArrayV2Stack/TensorListStack/element_shape:output:0*,
_output_shapes
:џџџџџџџџџd*
element_dtype02,
*lstm_17/TensorArrayV2Stack/TensorListStack
lstm_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:
џџџџџџџџџ2
lstm_17/strided_slice_3/stack
lstm_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
lstm_17/strided_slice_3/stack_1
lstm_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
lstm_17/strided_slice_3/stack_2Ъ
lstm_17/strided_slice_3StridedSlice3lstm_17/TensorArrayV2Stack/TensorListStack:tensor:0&lstm_17/strided_slice_3/stack:output:0(lstm_17/strided_slice_3/stack_1:output:0(lstm_17/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:џџџџџџџџџd*
shrink_axis_mask2
lstm_17/strided_slice_3
lstm_17/transpose_1/permConst*
_output_shapes
:*
dtype0*!
valueB"          2
lstm_17/transpose_1/permЦ
lstm_17/transpose_1	Transpose3lstm_17/TensorArrayV2Stack/TensorListStack:tensor:0!lstm_17/transpose_1/perm:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
lstm_17/transpose_1v
lstm_17/runtimeConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB
 *    2
lstm_17/runtimeЈ
dense_17/MatMul/ReadVariableOpReadVariableOp'dense_17_matmul_readvariableop_resource*
_output_shapes

:d*
dtype02 
dense_17/MatMul/ReadVariableOpЈ
dense_17/MatMulMatMul lstm_17/strided_slice_3:output:0&dense_17/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
dense_17/MatMulЇ
dense_17/BiasAdd/ReadVariableOpReadVariableOp(dense_17_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
dense_17/BiasAdd/ReadVariableOpЅ
dense_17/BiasAddBiasAdddense_17/MatMul:product:0'dense_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
dense_17/BiasAdd|
dense_17/SoftmaxSoftmaxdense_17/BiasAdd:output:0*
T0*'
_output_shapes
:џџџџџџџџџ2
dense_17/Softmaxй
IdentityIdentitydense_17/Softmax:softmax:0 ^dense_17/BiasAdd/ReadVariableOp^dense_17/MatMul/ReadVariableOp^embedding_17/embedding_lookup$^lstm_17/lstm_cell_17/ReadVariableOp&^lstm_17/lstm_cell_17/ReadVariableOp_1&^lstm_17/lstm_cell_17/ReadVariableOp_2&^lstm_17/lstm_cell_17/ReadVariableOp_3*^lstm_17/lstm_cell_17/split/ReadVariableOp,^lstm_17/lstm_cell_17/split_1/ReadVariableOp^lstm_17/while*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :џџџџџџџџџ: : : : : : 2B
dense_17/BiasAdd/ReadVariableOpdense_17/BiasAdd/ReadVariableOp2@
dense_17/MatMul/ReadVariableOpdense_17/MatMul/ReadVariableOp2>
embedding_17/embedding_lookupembedding_17/embedding_lookup2J
#lstm_17/lstm_cell_17/ReadVariableOp#lstm_17/lstm_cell_17/ReadVariableOp2N
%lstm_17/lstm_cell_17/ReadVariableOp_1%lstm_17/lstm_cell_17/ReadVariableOp_12N
%lstm_17/lstm_cell_17/ReadVariableOp_2%lstm_17/lstm_cell_17/ReadVariableOp_22N
%lstm_17/lstm_cell_17/ReadVariableOp_3%lstm_17/lstm_cell_17/ReadVariableOp_32V
)lstm_17/lstm_cell_17/split/ReadVariableOp)lstm_17/lstm_cell_17/split/ReadVariableOp2Z
+lstm_17/lstm_cell_17/split_1/ReadVariableOp+lstm_17/lstm_cell_17/split_1/ReadVariableOp2
lstm_17/whilelstm_17/while:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs
ъ
R
6__inference_spatial_dropout1d_17_layer_call_fn_1217892

inputs
identityд
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:џџџџџџџџџd* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *Z
fURS
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_12162712
PartitionedCallq
IdentityIdentityPartitionedCall:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:џџџџџџџџџd:T P
,
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
Є

Љ
I__inference_embedding_17_layer_call_and_return_conditional_losses_1216263

inputs,
embedding_lookup_1216257:
ЈУd
identityЂembedding_lookup^
CastCastinputs*

DstT0*

SrcT0*(
_output_shapes
:џџџџџџџџџ2
Cast
embedding_lookupResourceGatherembedding_lookup_1216257Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*+
_class!
loc:@embedding_lookup/1216257*,
_output_shapes
:џџџџџџџџџd*
dtype02
embedding_lookupя
embedding_lookup/IdentityIdentityembedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*+
_class!
loc:@embedding_lookup/1216257*,
_output_shapes
:џџџџџџџџџd2
embedding_lookup/IdentityЁ
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:џџџџџџџџџd2
embedding_lookup/Identity_1
IdentityIdentity$embedding_lookup/Identity_1:output:0^embedding_lookup*
T0*,
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:џџџџџџџџџ: 2$
embedding_lookupembedding_lookup:P L
(
_output_shapes
:џџџџџџџџџ
 
_user_specified_nameinputs


*__inference_dense_17_layer_call_fn_1219264

inputs
unknown:d
	unknown_0:
identityЂStatefulPartitionedCallѕ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:џџџџџџџџџ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12165422
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:џџџџџџџџџd: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs

ї
.__inference_lstm_cell_17_layer_call_fn_1219309

inputs
states_0
states_1
unknown:	d
	unknown_0:	
	unknown_1:	d
identity

identity_1

identity_2ЂStatefulPartitionedCallФ
StatefulPartitionedCallStatefulPartitionedCallinputsstates_0states_1unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *M
_output_shapes;
9:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_12158372
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_1

Identity_2Identity StatefulPartitionedCall:output:2^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*R
_input_shapesA
?:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd: : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/0:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/1
Џ?
Г
 __inference__traced_save_1219641
file_prefix6
2savev2_embedding_17_embeddings_read_readvariableop.
*savev2_dense_17_kernel_read_readvariableop,
(savev2_dense_17_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop:
6savev2_lstm_17_lstm_cell_17_kernel_read_readvariableopD
@savev2_lstm_17_lstm_cell_17_recurrent_kernel_read_readvariableop8
4savev2_lstm_17_lstm_cell_17_bias_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop=
9savev2_adam_embedding_17_embeddings_m_read_readvariableop5
1savev2_adam_dense_17_kernel_m_read_readvariableop3
/savev2_adam_dense_17_bias_m_read_readvariableopA
=savev2_adam_lstm_17_lstm_cell_17_kernel_m_read_readvariableopK
Gsavev2_adam_lstm_17_lstm_cell_17_recurrent_kernel_m_read_readvariableop?
;savev2_adam_lstm_17_lstm_cell_17_bias_m_read_readvariableop=
9savev2_adam_embedding_17_embeddings_v_read_readvariableop5
1savev2_adam_dense_17_kernel_v_read_readvariableop3
/savev2_adam_dense_17_bias_v_read_readvariableopA
=savev2_adam_lstm_17_lstm_cell_17_kernel_v_read_readvariableopK
Gsavev2_adam_lstm_17_lstm_cell_17_recurrent_kernel_v_read_readvariableop?
;savev2_adam_lstm_17_lstm_cell_17_bias_v_read_readvariableop
savev2_const

identity_1ЂMergeV2Checkpoints
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardІ
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*І
valueBB:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_namesР
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*K
valueBB@B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesЏ
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:02savev2_embedding_17_embeddings_read_readvariableop*savev2_dense_17_kernel_read_readvariableop(savev2_dense_17_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop6savev2_lstm_17_lstm_cell_17_kernel_read_readvariableop@savev2_lstm_17_lstm_cell_17_recurrent_kernel_read_readvariableop4savev2_lstm_17_lstm_cell_17_bias_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop9savev2_adam_embedding_17_embeddings_m_read_readvariableop1savev2_adam_dense_17_kernel_m_read_readvariableop/savev2_adam_dense_17_bias_m_read_readvariableop=savev2_adam_lstm_17_lstm_cell_17_kernel_m_read_readvariableopGsavev2_adam_lstm_17_lstm_cell_17_recurrent_kernel_m_read_readvariableop;savev2_adam_lstm_17_lstm_cell_17_bias_m_read_readvariableop9savev2_adam_embedding_17_embeddings_v_read_readvariableop1savev2_adam_dense_17_kernel_v_read_readvariableop/savev2_adam_dense_17_bias_v_read_readvariableop=savev2_adam_lstm_17_lstm_cell_17_kernel_v_read_readvariableopGsavev2_adam_lstm_17_lstm_cell_17_recurrent_kernel_v_read_readvariableop;savev2_adam_lstm_17_lstm_cell_17_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 **
dtypes 
2	2
SaveV2К
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesЁ
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*ж
_input_shapesФ
С: :
ЈУd:d:: : : : : :	d:	d:: : : : :
ЈУd:d::	d:	d::
ЈУd:d::	d:	d:: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
ЈУd:$ 

_output_shapes

:d: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :%	!

_output_shapes
:	d:%
!

_output_shapes
:	d:!

_output_shapes	
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :&"
 
_output_shapes
:
ЈУd:$ 

_output_shapes

:d: 

_output_shapes
::%!

_output_shapes
:	d:%!

_output_shapes
:	d:!

_output_shapes	
::&"
 
_output_shapes
:
ЈУd:$ 

_output_shapes

:d: 

_output_shapes
::%!

_output_shapes
:	d:%!

_output_shapes
:	d:!

_output_shapes	
::

_output_shapes
: 
к
Ш
while_cond_1218741
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice_15
1while_while_cond_1218741___redundant_placeholder05
1while_while_cond_1218741___redundant_placeholder15
1while_while_cond_1218741___redundant_placeholder25
1while_while_cond_1218741___redundant_placeholder3
while_identity
p

while/LessLesswhile_placeholderwhile_less_strided_slice_1*
T0*
_output_shapes
: 2

while/Less]
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: 2
while/Identity")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*S
_input_shapesB
@: : : : :џџџџџџџџџd:џџџџџџџџџd: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
:

ї
.__inference_lstm_cell_17_layer_call_fn_1219292

inputs
states_0
states_1
unknown:	d
	unknown_0:	
	unknown_1:	d
identity

identity_1

identity_2ЂStatefulPartitionedCallФ
StatefulPartitionedCallStatefulPartitionedCallinputsstates_0states_1unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *M
_output_shapes;
9:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *R
fMRK
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_12155772
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_1

Identity_2Identity StatefulPartitionedCall:output:2^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџd2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*R
_input_shapesA
?:џџџџџџџџџd:џџџџџџџџџd:џџџџџџџџџd: : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/0:QM
'
_output_shapes
:џџџџџџџџџd
"
_user_specified_name
states/1
џ
Н
lstm_17_while_body_1217655,
(lstm_17_while_lstm_17_while_loop_counter2
.lstm_17_while_lstm_17_while_maximum_iterations
lstm_17_while_placeholder
lstm_17_while_placeholder_1
lstm_17_while_placeholder_2
lstm_17_while_placeholder_3+
'lstm_17_while_lstm_17_strided_slice_1_0g
clstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensor_0M
:lstm_17_while_lstm_cell_17_split_readvariableop_resource_0:	dK
<lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0:	G
4lstm_17_while_lstm_cell_17_readvariableop_resource_0:	d
lstm_17_while_identity
lstm_17_while_identity_1
lstm_17_while_identity_2
lstm_17_while_identity_3
lstm_17_while_identity_4
lstm_17_while_identity_5)
%lstm_17_while_lstm_17_strided_slice_1e
alstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensorK
8lstm_17_while_lstm_cell_17_split_readvariableop_resource:	dI
:lstm_17_while_lstm_cell_17_split_1_readvariableop_resource:	E
2lstm_17_while_lstm_cell_17_readvariableop_resource:	dЂ)lstm_17/while/lstm_cell_17/ReadVariableOpЂ+lstm_17/while/lstm_cell_17/ReadVariableOp_1Ђ+lstm_17/while/lstm_cell_17/ReadVariableOp_2Ђ+lstm_17/while/lstm_cell_17/ReadVariableOp_3Ђ/lstm_17/while/lstm_cell_17/split/ReadVariableOpЂ1lstm_17/while/lstm_cell_17/split_1/ReadVariableOpг
?lstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*
valueB"џџџџd   2A
?lstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shape
1lstm_17/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemclstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensor_0lstm_17_while_placeholderHlstm_17/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*'
_output_shapes
:џџџџџџџџџd*
element_dtype023
1lstm_17/while/TensorArrayV2Read/TensorListGetItemР
*lstm_17/while/lstm_cell_17/ones_like/ShapeShape8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/ones_like/Shape
*lstm_17/while/lstm_cell_17/ones_like/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2,
*lstm_17/while/lstm_cell_17/ones_like/Const№
$lstm_17/while/lstm_cell_17/ones_likeFill3lstm_17/while/lstm_cell_17/ones_like/Shape:output:03lstm_17/while/lstm_cell_17/ones_like/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/ones_like
(lstm_17/while/lstm_cell_17/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2*
(lstm_17/while/lstm_cell_17/dropout/Constы
&lstm_17/while/lstm_cell_17/dropout/MulMul-lstm_17/while/lstm_cell_17/ones_like:output:01lstm_17/while/lstm_cell_17/dropout/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2(
&lstm_17/while/lstm_cell_17/dropout/MulБ
(lstm_17/while/lstm_cell_17/dropout/ShapeShape-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2*
(lstm_17/while/lstm_cell_17/dropout/ShapeЄ
?lstm_17/while/lstm_cell_17/dropout/random_uniform/RandomUniformRandomUniform1lstm_17/while/lstm_cell_17/dropout/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2аЈт2A
?lstm_17/while/lstm_cell_17/dropout/random_uniform/RandomUniformЋ
1lstm_17/while/lstm_cell_17/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>23
1lstm_17/while/lstm_cell_17/dropout/GreaterEqual/yЊ
/lstm_17/while/lstm_cell_17/dropout/GreaterEqualGreaterEqualHlstm_17/while/lstm_cell_17/dropout/random_uniform/RandomUniform:output:0:lstm_17/while/lstm_cell_17/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd21
/lstm_17/while/lstm_cell_17/dropout/GreaterEqualа
'lstm_17/while/lstm_cell_17/dropout/CastCast3lstm_17/while/lstm_cell_17/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2)
'lstm_17/while/lstm_cell_17/dropout/Castц
(lstm_17/while/lstm_cell_17/dropout/Mul_1Mul*lstm_17/while/lstm_cell_17/dropout/Mul:z:0+lstm_17/while/lstm_cell_17/dropout/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout/Mul_1
*lstm_17/while/lstm_cell_17/dropout_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2,
*lstm_17/while/lstm_cell_17/dropout_1/Constё
(lstm_17/while/lstm_cell_17/dropout_1/MulMul-lstm_17/while/lstm_cell_17/ones_like:output:03lstm_17/while/lstm_cell_17/dropout_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout_1/MulЕ
*lstm_17/while/lstm_cell_17/dropout_1/ShapeShape-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/dropout_1/ShapeЊ
Alstm_17/while/lstm_cell_17/dropout_1/random_uniform/RandomUniformRandomUniform3lstm_17/while/lstm_cell_17/dropout_1/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ьз2C
Alstm_17/while/lstm_cell_17/dropout_1/random_uniform/RandomUniformЏ
3lstm_17/while/lstm_cell_17/dropout_1/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>25
3lstm_17/while/lstm_cell_17/dropout_1/GreaterEqual/yВ
1lstm_17/while/lstm_cell_17/dropout_1/GreaterEqualGreaterEqualJlstm_17/while/lstm_cell_17/dropout_1/random_uniform/RandomUniform:output:0<lstm_17/while/lstm_cell_17/dropout_1/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1lstm_17/while/lstm_cell_17/dropout_1/GreaterEqualж
)lstm_17/while/lstm_cell_17/dropout_1/CastCast5lstm_17/while/lstm_cell_17/dropout_1/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/while/lstm_cell_17/dropout_1/Castю
*lstm_17/while/lstm_cell_17/dropout_1/Mul_1Mul,lstm_17/while/lstm_cell_17/dropout_1/Mul:z:0-lstm_17/while/lstm_cell_17/dropout_1/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*lstm_17/while/lstm_cell_17/dropout_1/Mul_1
*lstm_17/while/lstm_cell_17/dropout_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2,
*lstm_17/while/lstm_cell_17/dropout_2/Constё
(lstm_17/while/lstm_cell_17/dropout_2/MulMul-lstm_17/while/lstm_cell_17/ones_like:output:03lstm_17/while/lstm_cell_17/dropout_2/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout_2/MulЕ
*lstm_17/while/lstm_cell_17/dropout_2/ShapeShape-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/dropout_2/ShapeЊ
Alstm_17/while/lstm_cell_17/dropout_2/random_uniform/RandomUniformRandomUniform3lstm_17/while/lstm_cell_17/dropout_2/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2Ф2C
Alstm_17/while/lstm_cell_17/dropout_2/random_uniform/RandomUniformЏ
3lstm_17/while/lstm_cell_17/dropout_2/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>25
3lstm_17/while/lstm_cell_17/dropout_2/GreaterEqual/yВ
1lstm_17/while/lstm_cell_17/dropout_2/GreaterEqualGreaterEqualJlstm_17/while/lstm_cell_17/dropout_2/random_uniform/RandomUniform:output:0<lstm_17/while/lstm_cell_17/dropout_2/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1lstm_17/while/lstm_cell_17/dropout_2/GreaterEqualж
)lstm_17/while/lstm_cell_17/dropout_2/CastCast5lstm_17/while/lstm_cell_17/dropout_2/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/while/lstm_cell_17/dropout_2/Castю
*lstm_17/while/lstm_cell_17/dropout_2/Mul_1Mul,lstm_17/while/lstm_cell_17/dropout_2/Mul:z:0-lstm_17/while/lstm_cell_17/dropout_2/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*lstm_17/while/lstm_cell_17/dropout_2/Mul_1
*lstm_17/while/lstm_cell_17/dropout_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2,
*lstm_17/while/lstm_cell_17/dropout_3/Constё
(lstm_17/while/lstm_cell_17/dropout_3/MulMul-lstm_17/while/lstm_cell_17/ones_like:output:03lstm_17/while/lstm_cell_17/dropout_3/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout_3/MulЕ
*lstm_17/while/lstm_cell_17/dropout_3/ShapeShape-lstm_17/while/lstm_cell_17/ones_like:output:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/dropout_3/ShapeЊ
Alstm_17/while/lstm_cell_17/dropout_3/random_uniform/RandomUniformRandomUniform3lstm_17/while/lstm_cell_17/dropout_3/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЁжГ2C
Alstm_17/while/lstm_cell_17/dropout_3/random_uniform/RandomUniformЏ
3lstm_17/while/lstm_cell_17/dropout_3/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>25
3lstm_17/while/lstm_cell_17/dropout_3/GreaterEqual/yВ
1lstm_17/while/lstm_cell_17/dropout_3/GreaterEqualGreaterEqualJlstm_17/while/lstm_cell_17/dropout_3/random_uniform/RandomUniform:output:0<lstm_17/while/lstm_cell_17/dropout_3/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1lstm_17/while/lstm_cell_17/dropout_3/GreaterEqualж
)lstm_17/while/lstm_cell_17/dropout_3/CastCast5lstm_17/while/lstm_cell_17/dropout_3/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/while/lstm_cell_17/dropout_3/Castю
*lstm_17/while/lstm_cell_17/dropout_3/Mul_1Mul,lstm_17/while/lstm_cell_17/dropout_3/Mul:z:0-lstm_17/while/lstm_cell_17/dropout_3/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*lstm_17/while/lstm_cell_17/dropout_3/Mul_1Ї
,lstm_17/while/lstm_cell_17/ones_like_1/ShapeShapelstm_17_while_placeholder_2*
T0*
_output_shapes
:2.
,lstm_17/while/lstm_cell_17/ones_like_1/ShapeЁ
,lstm_17/while/lstm_cell_17/ones_like_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ?2.
,lstm_17/while/lstm_cell_17/ones_like_1/Constј
&lstm_17/while/lstm_cell_17/ones_like_1Fill5lstm_17/while/lstm_cell_17/ones_like_1/Shape:output:05lstm_17/while/lstm_cell_17/ones_like_1/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2(
&lstm_17/while/lstm_cell_17/ones_like_1
*lstm_17/while/lstm_cell_17/dropout_4/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2,
*lstm_17/while/lstm_cell_17/dropout_4/Constѓ
(lstm_17/while/lstm_cell_17/dropout_4/MulMul/lstm_17/while/lstm_cell_17/ones_like_1:output:03lstm_17/while/lstm_cell_17/dropout_4/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout_4/MulЗ
*lstm_17/while/lstm_cell_17/dropout_4/ShapeShape/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/dropout_4/ShapeЊ
Alstm_17/while/lstm_cell_17/dropout_4/random_uniform/RandomUniformRandomUniform3lstm_17/while/lstm_cell_17/dropout_4/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2шСБ2C
Alstm_17/while/lstm_cell_17/dropout_4/random_uniform/RandomUniformЏ
3lstm_17/while/lstm_cell_17/dropout_4/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>25
3lstm_17/while/lstm_cell_17/dropout_4/GreaterEqual/yВ
1lstm_17/while/lstm_cell_17/dropout_4/GreaterEqualGreaterEqualJlstm_17/while/lstm_cell_17/dropout_4/random_uniform/RandomUniform:output:0<lstm_17/while/lstm_cell_17/dropout_4/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1lstm_17/while/lstm_cell_17/dropout_4/GreaterEqualж
)lstm_17/while/lstm_cell_17/dropout_4/CastCast5lstm_17/while/lstm_cell_17/dropout_4/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/while/lstm_cell_17/dropout_4/Castю
*lstm_17/while/lstm_cell_17/dropout_4/Mul_1Mul,lstm_17/while/lstm_cell_17/dropout_4/Mul:z:0-lstm_17/while/lstm_cell_17/dropout_4/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*lstm_17/while/lstm_cell_17/dropout_4/Mul_1
*lstm_17/while/lstm_cell_17/dropout_5/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2,
*lstm_17/while/lstm_cell_17/dropout_5/Constѓ
(lstm_17/while/lstm_cell_17/dropout_5/MulMul/lstm_17/while/lstm_cell_17/ones_like_1:output:03lstm_17/while/lstm_cell_17/dropout_5/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout_5/MulЗ
*lstm_17/while/lstm_cell_17/dropout_5/ShapeShape/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/dropout_5/ShapeЊ
Alstm_17/while/lstm_cell_17/dropout_5/random_uniform/RandomUniformRandomUniform3lstm_17/while/lstm_cell_17/dropout_5/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ўпЊ2C
Alstm_17/while/lstm_cell_17/dropout_5/random_uniform/RandomUniformЏ
3lstm_17/while/lstm_cell_17/dropout_5/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>25
3lstm_17/while/lstm_cell_17/dropout_5/GreaterEqual/yВ
1lstm_17/while/lstm_cell_17/dropout_5/GreaterEqualGreaterEqualJlstm_17/while/lstm_cell_17/dropout_5/random_uniform/RandomUniform:output:0<lstm_17/while/lstm_cell_17/dropout_5/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1lstm_17/while/lstm_cell_17/dropout_5/GreaterEqualж
)lstm_17/while/lstm_cell_17/dropout_5/CastCast5lstm_17/while/lstm_cell_17/dropout_5/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/while/lstm_cell_17/dropout_5/Castю
*lstm_17/while/lstm_cell_17/dropout_5/Mul_1Mul,lstm_17/while/lstm_cell_17/dropout_5/Mul:z:0-lstm_17/while/lstm_cell_17/dropout_5/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*lstm_17/while/lstm_cell_17/dropout_5/Mul_1
*lstm_17/while/lstm_cell_17/dropout_6/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2,
*lstm_17/while/lstm_cell_17/dropout_6/Constѓ
(lstm_17/while/lstm_cell_17/dropout_6/MulMul/lstm_17/while/lstm_cell_17/ones_like_1:output:03lstm_17/while/lstm_cell_17/dropout_6/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout_6/MulЗ
*lstm_17/while/lstm_cell_17/dropout_6/ShapeShape/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/dropout_6/ShapeЊ
Alstm_17/while/lstm_cell_17/dropout_6/random_uniform/RandomUniformRandomUniform3lstm_17/while/lstm_cell_17/dropout_6/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ЧДе2C
Alstm_17/while/lstm_cell_17/dropout_6/random_uniform/RandomUniformЏ
3lstm_17/while/lstm_cell_17/dropout_6/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>25
3lstm_17/while/lstm_cell_17/dropout_6/GreaterEqual/yВ
1lstm_17/while/lstm_cell_17/dropout_6/GreaterEqualGreaterEqualJlstm_17/while/lstm_cell_17/dropout_6/random_uniform/RandomUniform:output:0<lstm_17/while/lstm_cell_17/dropout_6/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1lstm_17/while/lstm_cell_17/dropout_6/GreaterEqualж
)lstm_17/while/lstm_cell_17/dropout_6/CastCast5lstm_17/while/lstm_cell_17/dropout_6/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/while/lstm_cell_17/dropout_6/Castю
*lstm_17/while/lstm_cell_17/dropout_6/Mul_1Mul,lstm_17/while/lstm_cell_17/dropout_6/Mul:z:0-lstm_17/while/lstm_cell_17/dropout_6/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*lstm_17/while/lstm_cell_17/dropout_6/Mul_1
*lstm_17/while/lstm_cell_17/dropout_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2,
*lstm_17/while/lstm_cell_17/dropout_7/Constѓ
(lstm_17/while/lstm_cell_17/dropout_7/MulMul/lstm_17/while/lstm_cell_17/ones_like_1:output:03lstm_17/while/lstm_cell_17/dropout_7/Const:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2*
(lstm_17/while/lstm_cell_17/dropout_7/MulЗ
*lstm_17/while/lstm_cell_17/dropout_7/ShapeShape/lstm_17/while/lstm_cell_17/ones_like_1:output:0*
T0*
_output_shapes
:2,
*lstm_17/while/lstm_cell_17/dropout_7/ShapeЉ
Alstm_17/while/lstm_cell_17/dropout_7/random_uniform/RandomUniformRandomUniform3lstm_17/while/lstm_cell_17/dropout_7/Shape:output:0*
T0*'
_output_shapes
:џџџџџџџџџd*
dtype0*
seedБџх)*
seed2ћЛ~2C
Alstm_17/while/lstm_cell_17/dropout_7/random_uniform/RandomUniformЏ
3lstm_17/while/lstm_cell_17/dropout_7/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *ЭЬL>25
3lstm_17/while/lstm_cell_17/dropout_7/GreaterEqual/yВ
1lstm_17/while/lstm_cell_17/dropout_7/GreaterEqualGreaterEqualJlstm_17/while/lstm_cell_17/dropout_7/random_uniform/RandomUniform:output:0<lstm_17/while/lstm_cell_17/dropout_7/GreaterEqual/y:output:0*
T0*'
_output_shapes
:џџџџџџџџџd23
1lstm_17/while/lstm_cell_17/dropout_7/GreaterEqualж
)lstm_17/while/lstm_cell_17/dropout_7/CastCast5lstm_17/while/lstm_cell_17/dropout_7/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:џџџџџџџџџd2+
)lstm_17/while/lstm_cell_17/dropout_7/Castю
*lstm_17/while/lstm_cell_17/dropout_7/Mul_1Mul,lstm_17/while/lstm_cell_17/dropout_7/Mul:z:0-lstm_17/while/lstm_cell_17/dropout_7/Cast:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2,
*lstm_17/while/lstm_cell_17/dropout_7/Mul_1с
lstm_17/while/lstm_cell_17/mulMul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0,lstm_17/while/lstm_cell_17/dropout/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/while/lstm_cell_17/mulч
 lstm_17/while/lstm_cell_17/mul_1Mul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0.lstm_17/while/lstm_cell_17/dropout_1/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_1ч
 lstm_17/while/lstm_cell_17/mul_2Mul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0.lstm_17/while/lstm_cell_17/dropout_2/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_2ч
 lstm_17/while/lstm_cell_17/mul_3Mul8lstm_17/while/TensorArrayV2Read/TensorListGetItem:item:0.lstm_17/while/lstm_cell_17/dropout_3/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_3
*lstm_17/while/lstm_cell_17/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :2,
*lstm_17/while/lstm_cell_17/split/split_dimо
/lstm_17/while/lstm_cell_17/split/ReadVariableOpReadVariableOp:lstm_17_while_lstm_cell_17_split_readvariableop_resource_0*
_output_shapes
:	d*
dtype021
/lstm_17/while/lstm_cell_17/split/ReadVariableOp
 lstm_17/while/lstm_cell_17/splitSplit3lstm_17/while/lstm_cell_17/split/split_dim:output:07lstm_17/while/lstm_cell_17/split/ReadVariableOp:value:0*
T0*<
_output_shapes*
(:dd:dd:dd:dd*
	num_split2"
 lstm_17/while/lstm_cell_17/splitб
!lstm_17/while/lstm_cell_17/MatMulMatMul"lstm_17/while/lstm_cell_17/mul:z:0)lstm_17/while/lstm_cell_17/split:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_17/while/lstm_cell_17/MatMulз
#lstm_17/while/lstm_cell_17/MatMul_1MatMul$lstm_17/while/lstm_cell_17/mul_1:z:0)lstm_17/while/lstm_cell_17/split:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_1з
#lstm_17/while/lstm_cell_17/MatMul_2MatMul$lstm_17/while/lstm_cell_17/mul_2:z:0)lstm_17/while/lstm_cell_17/split:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_2з
#lstm_17/while/lstm_cell_17/MatMul_3MatMul$lstm_17/while/lstm_cell_17/mul_3:z:0)lstm_17/while/lstm_cell_17/split:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_3
,lstm_17/while/lstm_cell_17/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B : 2.
,lstm_17/while/lstm_cell_17/split_1/split_dimр
1lstm_17/while/lstm_cell_17/split_1/ReadVariableOpReadVariableOp<lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0*
_output_shapes	
:*
dtype023
1lstm_17/while/lstm_cell_17/split_1/ReadVariableOp
"lstm_17/while/lstm_cell_17/split_1Split5lstm_17/while/lstm_cell_17/split_1/split_dim:output:09lstm_17/while/lstm_cell_17/split_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:d:d:d:d*
	num_split2$
"lstm_17/while/lstm_cell_17/split_1п
"lstm_17/while/lstm_cell_17/BiasAddBiasAdd+lstm_17/while/lstm_cell_17/MatMul:product:0+lstm_17/while/lstm_cell_17/split_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/while/lstm_cell_17/BiasAddх
$lstm_17/while/lstm_cell_17/BiasAdd_1BiasAdd-lstm_17/while/lstm_cell_17/MatMul_1:product:0+lstm_17/while/lstm_cell_17/split_1:output:1*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/BiasAdd_1х
$lstm_17/while/lstm_cell_17/BiasAdd_2BiasAdd-lstm_17/while/lstm_cell_17/MatMul_2:product:0+lstm_17/while/lstm_cell_17/split_1:output:2*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/BiasAdd_2х
$lstm_17/while/lstm_cell_17/BiasAdd_3BiasAdd-lstm_17/while/lstm_cell_17/MatMul_3:product:0+lstm_17/while/lstm_cell_17/split_1:output:3*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/BiasAdd_3Ъ
 lstm_17/while/lstm_cell_17/mul_4Mullstm_17_while_placeholder_2.lstm_17/while/lstm_cell_17/dropout_4/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_4Ъ
 lstm_17/while/lstm_cell_17/mul_5Mullstm_17_while_placeholder_2.lstm_17/while/lstm_cell_17/dropout_5/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_5Ъ
 lstm_17/while/lstm_cell_17/mul_6Mullstm_17_while_placeholder_2.lstm_17/while/lstm_cell_17/dropout_6/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_6Ъ
 lstm_17/while/lstm_cell_17/mul_7Mullstm_17_while_placeholder_2.lstm_17/while/lstm_cell_17/dropout_7/Mul_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_7Ь
)lstm_17/while/lstm_cell_17/ReadVariableOpReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02+
)lstm_17/while/lstm_cell_17/ReadVariableOpБ
.lstm_17/while/lstm_cell_17/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        20
.lstm_17/while/lstm_cell_17/strided_slice/stackЕ
0lstm_17/while/lstm_cell_17/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    d   22
0lstm_17/while/lstm_cell_17/strided_slice/stack_1Е
0lstm_17/while/lstm_cell_17/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      22
0lstm_17/while/lstm_cell_17/strided_slice/stack_2
(lstm_17/while/lstm_cell_17/strided_sliceStridedSlice1lstm_17/while/lstm_cell_17/ReadVariableOp:value:07lstm_17/while/lstm_cell_17/strided_slice/stack:output:09lstm_17/while/lstm_cell_17/strided_slice/stack_1:output:09lstm_17/while/lstm_cell_17/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2*
(lstm_17/while/lstm_cell_17/strided_sliceп
#lstm_17/while/lstm_cell_17/MatMul_4MatMul$lstm_17/while/lstm_cell_17/mul_4:z:01lstm_17/while/lstm_cell_17/strided_slice:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_4з
lstm_17/while/lstm_cell_17/addAddV2+lstm_17/while/lstm_cell_17/BiasAdd:output:0-lstm_17/while/lstm_cell_17/MatMul_4:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
lstm_17/while/lstm_cell_17/addЉ
"lstm_17/while/lstm_cell_17/SigmoidSigmoid"lstm_17/while/lstm_cell_17/add:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2$
"lstm_17/while/lstm_cell_17/Sigmoidа
+lstm_17/while/lstm_cell_17/ReadVariableOp_1ReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02-
+lstm_17/while/lstm_cell_17/ReadVariableOp_1Е
0lstm_17/while/lstm_cell_17/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"    d   22
0lstm_17/while/lstm_cell_17/strided_slice_1/stackЙ
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    Ш   24
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_1Й
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      24
2lstm_17/while/lstm_cell_17/strided_slice_1/stack_2Њ
*lstm_17/while/lstm_cell_17/strided_slice_1StridedSlice3lstm_17/while/lstm_cell_17/ReadVariableOp_1:value:09lstm_17/while/lstm_cell_17/strided_slice_1/stack:output:0;lstm_17/while/lstm_cell_17/strided_slice_1/stack_1:output:0;lstm_17/while/lstm_cell_17/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2,
*lstm_17/while/lstm_cell_17/strided_slice_1с
#lstm_17/while/lstm_cell_17/MatMul_5MatMul$lstm_17/while/lstm_cell_17/mul_5:z:03lstm_17/while/lstm_cell_17/strided_slice_1:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_5н
 lstm_17/while/lstm_cell_17/add_1AddV2-lstm_17/while/lstm_cell_17/BiasAdd_1:output:0-lstm_17/while/lstm_cell_17/MatMul_5:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_1Џ
$lstm_17/while/lstm_cell_17/Sigmoid_1Sigmoid$lstm_17/while/lstm_cell_17/add_1:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/Sigmoid_1Ф
 lstm_17/while/lstm_cell_17/mul_8Mul(lstm_17/while/lstm_cell_17/Sigmoid_1:y:0lstm_17_while_placeholder_3*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_8а
+lstm_17/while/lstm_cell_17/ReadVariableOp_2ReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02-
+lstm_17/while/lstm_cell_17/ReadVariableOp_2Е
0lstm_17/while/lstm_cell_17/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"    Ш   22
0lstm_17/while/lstm_cell_17/strided_slice_2/stackЙ
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ,  24
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_1Й
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      24
2lstm_17/while/lstm_cell_17/strided_slice_2/stack_2Њ
*lstm_17/while/lstm_cell_17/strided_slice_2StridedSlice3lstm_17/while/lstm_cell_17/ReadVariableOp_2:value:09lstm_17/while/lstm_cell_17/strided_slice_2/stack:output:0;lstm_17/while/lstm_cell_17/strided_slice_2/stack_1:output:0;lstm_17/while/lstm_cell_17/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2,
*lstm_17/while/lstm_cell_17/strided_slice_2с
#lstm_17/while/lstm_cell_17/MatMul_6MatMul$lstm_17/while/lstm_cell_17/mul_6:z:03lstm_17/while/lstm_cell_17/strided_slice_2:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_6н
 lstm_17/while/lstm_cell_17/add_2AddV2-lstm_17/while/lstm_cell_17/BiasAdd_2:output:0-lstm_17/while/lstm_cell_17/MatMul_6:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_2Ђ
lstm_17/while/lstm_cell_17/TanhTanh$lstm_17/while/lstm_cell_17/add_2:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2!
lstm_17/while/lstm_cell_17/TanhЪ
 lstm_17/while/lstm_cell_17/mul_9Mul&lstm_17/while/lstm_cell_17/Sigmoid:y:0#lstm_17/while/lstm_cell_17/Tanh:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/mul_9Ы
 lstm_17/while/lstm_cell_17/add_3AddV2$lstm_17/while/lstm_cell_17/mul_8:z:0$lstm_17/while/lstm_cell_17/mul_9:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_3а
+lstm_17/while/lstm_cell_17/ReadVariableOp_3ReadVariableOp4lstm_17_while_lstm_cell_17_readvariableop_resource_0*
_output_shapes
:	d*
dtype02-
+lstm_17/while/lstm_cell_17/ReadVariableOp_3Е
0lstm_17/while/lstm_cell_17/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"    ,  22
0lstm_17/while/lstm_cell_17/strided_slice_3/stackЙ
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        24
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_1Й
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      24
2lstm_17/while/lstm_cell_17/strided_slice_3/stack_2Њ
*lstm_17/while/lstm_cell_17/strided_slice_3StridedSlice3lstm_17/while/lstm_cell_17/ReadVariableOp_3:value:09lstm_17/while/lstm_cell_17/strided_slice_3/stack:output:0;lstm_17/while/lstm_cell_17/strided_slice_3/stack_1:output:0;lstm_17/while/lstm_cell_17/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes

:dd*

begin_mask*
end_mask2,
*lstm_17/while/lstm_cell_17/strided_slice_3с
#lstm_17/while/lstm_cell_17/MatMul_7MatMul$lstm_17/while/lstm_cell_17/mul_7:z:03lstm_17/while/lstm_cell_17/strided_slice_3:output:0*
T0*'
_output_shapes
:џџџџџџџџџd2%
#lstm_17/while/lstm_cell_17/MatMul_7н
 lstm_17/while/lstm_cell_17/add_4AddV2-lstm_17/while/lstm_cell_17/BiasAdd_3:output:0-lstm_17/while/lstm_cell_17/MatMul_7:product:0*
T0*'
_output_shapes
:џџџџџџџџџd2"
 lstm_17/while/lstm_cell_17/add_4Џ
$lstm_17/while/lstm_cell_17/Sigmoid_2Sigmoid$lstm_17/while/lstm_cell_17/add_4:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2&
$lstm_17/while/lstm_cell_17/Sigmoid_2І
!lstm_17/while/lstm_cell_17/Tanh_1Tanh$lstm_17/while/lstm_cell_17/add_3:z:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_17/while/lstm_cell_17/Tanh_1а
!lstm_17/while/lstm_cell_17/mul_10Mul(lstm_17/while/lstm_cell_17/Sigmoid_2:y:0%lstm_17/while/lstm_cell_17/Tanh_1:y:0*
T0*'
_output_shapes
:џџџџџџџџџd2#
!lstm_17/while/lstm_cell_17/mul_10
2lstm_17/while/TensorArrayV2Write/TensorListSetItemTensorListSetItemlstm_17_while_placeholder_1lstm_17_while_placeholder%lstm_17/while/lstm_cell_17/mul_10:z:0*
_output_shapes
: *
element_dtype024
2lstm_17/while/TensorArrayV2Write/TensorListSetIteml
lstm_17/while/add/yConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_17/while/add/y
lstm_17/while/addAddV2lstm_17_while_placeholderlstm_17/while/add/y:output:0*
T0*
_output_shapes
: 2
lstm_17/while/addp
lstm_17/while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
lstm_17/while/add_1/y
lstm_17/while/add_1AddV2(lstm_17_while_lstm_17_while_loop_counterlstm_17/while/add_1/y:output:0*
T0*
_output_shapes
: 2
lstm_17/while/add_1
lstm_17/while/IdentityIdentitylstm_17/while/add_1:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity­
lstm_17/while/Identity_1Identity.lstm_17_while_lstm_17_while_maximum_iterations*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity_1
lstm_17/while/Identity_2Identitylstm_17/while/add:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity_2С
lstm_17/while/Identity_3IdentityBlstm_17/while/TensorArrayV2Write/TensorListSetItem:output_handle:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*
_output_shapes
: 2
lstm_17/while/Identity_3Е
lstm_17/while/Identity_4Identity%lstm_17/while/lstm_cell_17/mul_10:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/while/Identity_4Д
lstm_17/while/Identity_5Identity$lstm_17/while/lstm_cell_17/add_3:z:0*^lstm_17/while/lstm_cell_17/ReadVariableOp,^lstm_17/while/lstm_cell_17/ReadVariableOp_1,^lstm_17/while/lstm_cell_17/ReadVariableOp_2,^lstm_17/while/lstm_cell_17/ReadVariableOp_30^lstm_17/while/lstm_cell_17/split/ReadVariableOp2^lstm_17/while/lstm_cell_17/split_1/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџd2
lstm_17/while/Identity_5"9
lstm_17_while_identitylstm_17/while/Identity:output:0"=
lstm_17_while_identity_1!lstm_17/while/Identity_1:output:0"=
lstm_17_while_identity_2!lstm_17/while/Identity_2:output:0"=
lstm_17_while_identity_3!lstm_17/while/Identity_3:output:0"=
lstm_17_while_identity_4!lstm_17/while/Identity_4:output:0"=
lstm_17_while_identity_5!lstm_17/while/Identity_5:output:0"P
%lstm_17_while_lstm_17_strided_slice_1'lstm_17_while_lstm_17_strided_slice_1_0"j
2lstm_17_while_lstm_cell_17_readvariableop_resource4lstm_17_while_lstm_cell_17_readvariableop_resource_0"z
:lstm_17_while_lstm_cell_17_split_1_readvariableop_resource<lstm_17_while_lstm_cell_17_split_1_readvariableop_resource_0"v
8lstm_17_while_lstm_cell_17_split_readvariableop_resource:lstm_17_while_lstm_cell_17_split_readvariableop_resource_0"Ш
alstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensorclstm_17_while_tensorarrayv2read_tensorlistgetitem_lstm_17_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*K
_input_shapes:
8: : : : :џџџџџџџџџd:џџџџџџџџџd: : : : : 2V
)lstm_17/while/lstm_cell_17/ReadVariableOp)lstm_17/while/lstm_cell_17/ReadVariableOp2Z
+lstm_17/while/lstm_cell_17/ReadVariableOp_1+lstm_17/while/lstm_cell_17/ReadVariableOp_12Z
+lstm_17/while/lstm_cell_17/ReadVariableOp_2+lstm_17/while/lstm_cell_17/ReadVariableOp_22Z
+lstm_17/while/lstm_cell_17/ReadVariableOp_3+lstm_17/while/lstm_cell_17/ReadVariableOp_32b
/lstm_17/while/lstm_cell_17/split/ReadVariableOp/lstm_17/while/lstm_cell_17/split/ReadVariableOp2f
1lstm_17/while/lstm_cell_17/split_1/ReadVariableOp1lstm_17/while/lstm_cell_17/split_1/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :-)
'
_output_shapes
:џџџџџџџџџd:-)
'
_output_shapes
:џџџџџџџџџd:

_output_shapes
: :

_output_shapes
: "ЬL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*Т
serving_defaultЎ
R
embedding_17_input<
$serving_default_embedding_17_input:0џџџџџџџџџ<
dense_170
StatefulPartitionedCall:0џџџџџџџџџtensorflow/serving/predict:р
/
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
	optimizer
	variables
regularization_losses
trainable_variables
		keras_api


signatures
c__call__
d_default_save_signature
*e&call_and_return_all_conditional_losses"Ф,
_tf_keras_sequentialЅ,{"name": "sequential_17", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "class_name": "Sequential", "config": {"name": "sequential_17", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 150]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "embedding_17_input"}}, {"class_name": "Embedding", "config": {"name": "embedding_17", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 150]}, "dtype": "float32", "input_dim": 25000, "output_dim": 100, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}}, "embeddings_regularizer": null, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": 150}}, {"class_name": "SpatialDropout1D", "config": {"name": "spatial_dropout1d_17", "trainable": true, "dtype": "float32", "rate": 0.2, "noise_shape": null, "seed": null}}, {"class_name": "LSTM", "config": {"name": "lstm_17", "trainable": true, "dtype": "float32", "return_sequences": false, "return_state": false, "go_backwards": false, "stateful": false, "unroll": false, "time_major": false, "units": 100, "activation": "tanh", "recurrent_activation": "sigmoid", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 4}, "recurrent_initializer": {"class_name": "Orthogonal", "config": {"gain": 1.0, "seed": null}, "shared_object_id": 5}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 6}, "unit_forget_bias": true, "kernel_regularizer": null, "recurrent_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "recurrent_constraint": null, "bias_constraint": null, "dropout": 0.2, "recurrent_dropout": 0.2, "implementation": 1}}, {"class_name": "Dense", "config": {"name": "dense_17", "trainable": true, "dtype": "float32", "units": 2, "activation": "softmax", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}, "shared_object_id": 12, "build_input_shape": {"class_name": "TensorShape", "items": [null, 150]}, "is_graph_network": true, "save_spec": {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 150]}, "float32", "embedding_17_input"]}, "keras_version": "2.5.0", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential_17", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 150]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "embedding_17_input"}, "shared_object_id": 0}, {"class_name": "Embedding", "config": {"name": "embedding_17", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 150]}, "dtype": "float32", "input_dim": 25000, "output_dim": 100, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 1}, "embeddings_regularizer": null, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": 150}, "shared_object_id": 2}, {"class_name": "SpatialDropout1D", "config": {"name": "spatial_dropout1d_17", "trainable": true, "dtype": "float32", "rate": 0.2, "noise_shape": null, "seed": null}, "shared_object_id": 3}, {"class_name": "LSTM", "config": {"name": "lstm_17", "trainable": true, "dtype": "float32", "return_sequences": false, "return_state": false, "go_backwards": false, "stateful": false, "unroll": false, "time_major": false, "units": 100, "activation": "tanh", "recurrent_activation": "sigmoid", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 4}, "recurrent_initializer": {"class_name": "Orthogonal", "config": {"gain": 1.0, "seed": null}, "shared_object_id": 5}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 6}, "unit_forget_bias": true, "kernel_regularizer": null, "recurrent_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "recurrent_constraint": null, "bias_constraint": null, "dropout": 0.2, "recurrent_dropout": 0.2, "implementation": 1}, "shared_object_id": 8}, {"class_name": "Dense", "config": {"name": "dense_17", "trainable": true, "dtype": "float32", "units": 2, "activation": "softmax", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 9}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 10}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 11}]}}, "training_config": {"loss": "categorical_crossentropy", "metrics": [[{"class_name": "MeanMetricWrapper", "config": {"name": "accuracy", "dtype": "float32", "fn": "categorical_accuracy"}, "shared_object_id": 13}]], "weighted_metrics": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0010000000474974513, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
о

embeddings
	variables
regularization_losses
trainable_variables
	keras_api
f__call__
*g&call_and_return_all_conditional_losses"П
_tf_keras_layerЅ{"name": "embedding_17", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, 150]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "embedding_17", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 150]}, "dtype": "float32", "input_dim": 25000, "output_dim": 100, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 1}, "embeddings_regularizer": null, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": 150}, "shared_object_id": 2, "build_input_shape": {"class_name": "TensorShape", "items": [null, 150]}}
У
	variables
regularization_losses
trainable_variables
	keras_api
h__call__
*i&call_and_return_all_conditional_losses"Д
_tf_keras_layer{"name": "spatial_dropout1d_17", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "SpatialDropout1D", "config": {"name": "spatial_dropout1d_17", "trainable": true, "dtype": "float32", "rate": 0.2, "noise_shape": null, "seed": null}, "shared_object_id": 3, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 3, "max_ndim": null, "min_ndim": null, "axes": {}}, "shared_object_id": 14}}
З
cell

state_spec
	variables
regularization_losses
trainable_variables
	keras_api
j__call__
*k&call_and_return_all_conditional_losses"
_tf_keras_rnn_layer№
{"name": "lstm_17", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "LSTM", "config": {"name": "lstm_17", "trainable": true, "dtype": "float32", "return_sequences": false, "return_state": false, "go_backwards": false, "stateful": false, "unroll": false, "time_major": false, "units": 100, "activation": "tanh", "recurrent_activation": "sigmoid", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 4}, "recurrent_initializer": {"class_name": "Orthogonal", "config": {"gain": 1.0, "seed": null}, "shared_object_id": 5}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 6}, "unit_forget_bias": true, "kernel_regularizer": null, "recurrent_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "recurrent_constraint": null, "bias_constraint": null, "dropout": 0.2, "recurrent_dropout": 0.2, "implementation": 1}, "shared_object_id": 8, "input_spec": [{"class_name": "InputSpec", "config": {"dtype": null, "shape": {"class_name": "__tuple__", "items": [null, null, 100]}, "ndim": 3, "max_ndim": null, "min_ndim": null, "axes": {}}, "shared_object_id": 15}], "build_input_shape": {"class_name": "TensorShape", "items": [null, 150, 100]}}
е

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
l__call__
*m&call_and_return_all_conditional_losses"А
_tf_keras_layer{"name": "dense_17", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Dense", "config": {"name": "dense_17", "trainable": true, "dtype": "float32", "units": 2, "activation": "softmax", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 9}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 10}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 11, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 100}}, "shared_object_id": 16}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 100]}}
П
 iter

!beta_1

"beta_2
	#decay
$learning_ratemWmXmY%mZ&m['m\v]v^v_%v`&va'vb"
	optimizer
J
0
%1
&2
'3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
J
0
%1
&2
'3
4
5"
trackable_list_wrapper
Ъ
	variables

(layers
)layer_regularization_losses
*metrics
regularization_losses
trainable_variables
+non_trainable_variables
,layer_metrics
c__call__
d_default_save_signature
*e&call_and_return_all_conditional_losses
&e"call_and_return_conditional_losses"
_generic_user_object
,
nserving_default"
signature_map
+:)
ЈУd2embedding_17/embeddings
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
­
	variables

-layers
.layer_regularization_losses
/metrics
regularization_losses
trainable_variables
0non_trainable_variables
1layer_metrics
f__call__
*g&call_and_return_all_conditional_losses
&g"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
­
	variables

2layers
3layer_regularization_losses
4metrics
regularization_losses
trainable_variables
5non_trainable_variables
6layer_metrics
h__call__
*i&call_and_return_all_conditional_losses
&i"call_and_return_conditional_losses"
_generic_user_object
	
7
state_size

%kernel
&recurrent_kernel
'bias
8	variables
9regularization_losses
:trainable_variables
;	keras_api
o__call__
*p&call_and_return_all_conditional_losses"Ь
_tf_keras_layerВ{"name": "lstm_cell_17", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "LSTMCell", "config": {"name": "lstm_cell_17", "trainable": true, "dtype": "float32", "units": 100, "activation": "tanh", "recurrent_activation": "sigmoid", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 4}, "recurrent_initializer": {"class_name": "Orthogonal", "config": {"gain": 1.0, "seed": null}, "shared_object_id": 5}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 6}, "unit_forget_bias": true, "kernel_regularizer": null, "recurrent_regularizer": null, "bias_regularizer": null, "kernel_constraint": null, "recurrent_constraint": null, "bias_constraint": null, "dropout": 0.2, "recurrent_dropout": 0.2, "implementation": 1}, "shared_object_id": 7}
 "
trackable_list_wrapper
5
%0
&1
'2"
trackable_list_wrapper
 "
trackable_list_wrapper
5
%0
&1
'2"
trackable_list_wrapper
Й
	variables

<layers
=layer_regularization_losses
>metrics
regularization_losses

?states
trainable_variables
@non_trainable_variables
Alayer_metrics
j__call__
*k&call_and_return_all_conditional_losses
&k"call_and_return_conditional_losses"
_generic_user_object
!:d2dense_17/kernel
:2dense_17/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
­
	variables

Blayers
Clayer_regularization_losses
Dmetrics
regularization_losses
trainable_variables
Enon_trainable_variables
Flayer_metrics
l__call__
*m&call_and_return_all_conditional_losses
&m"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
.:,	d2lstm_17/lstm_cell_17/kernel
8:6	d2%lstm_17/lstm_cell_17/recurrent_kernel
(:&2lstm_17/lstm_cell_17/bias
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
.
G0
H1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
5
%0
&1
'2"
trackable_list_wrapper
 "
trackable_list_wrapper
5
%0
&1
'2"
trackable_list_wrapper
­
8	variables

Ilayers
Jlayer_regularization_losses
Kmetrics
9regularization_losses
:trainable_variables
Lnon_trainable_variables
Mlayer_metrics
o__call__
*p&call_and_return_all_conditional_losses
&p"call_and_return_conditional_losses"
_generic_user_object
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
д
	Ntotal
	Ocount
P	variables
Q	keras_api"
_tf_keras_metric{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}, "shared_object_id": 17}

	Rtotal
	Scount
T
_fn_kwargs
U	variables
V	keras_api"а
_tf_keras_metricЕ{"class_name": "MeanMetricWrapper", "name": "accuracy", "dtype": "float32", "config": {"name": "accuracy", "dtype": "float32", "fn": "categorical_accuracy"}, "shared_object_id": 13}
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
:  (2total
:  (2count
.
N0
O1"
trackable_list_wrapper
-
P	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
R0
S1"
trackable_list_wrapper
-
U	variables"
_generic_user_object
0:.
ЈУd2Adam/embedding_17/embeddings/m
&:$d2Adam/dense_17/kernel/m
 :2Adam/dense_17/bias/m
3:1	d2"Adam/lstm_17/lstm_cell_17/kernel/m
=:;	d2,Adam/lstm_17/lstm_cell_17/recurrent_kernel/m
-:+2 Adam/lstm_17/lstm_cell_17/bias/m
0:.
ЈУd2Adam/embedding_17/embeddings/v
&:$d2Adam/dense_17/kernel/v
 :2Adam/dense_17/bias/v
3:1	d2"Adam/lstm_17/lstm_cell_17/kernel/v
=:;	d2,Adam/lstm_17/lstm_cell_17/recurrent_kernel/v
-:+2 Adam/lstm_17/lstm_cell_17/bias/v
2
/__inference_sequential_17_layer_call_fn_1216564
/__inference_sequential_17_layer_call_fn_1217168
/__inference_sequential_17_layer_call_fn_1217185
/__inference_sequential_17_layer_call_fn_1217086Р
ЗВГ
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
ь2щ
"__inference__wrapped_model_1215376Т
В
FullArgSpec
args 
varargsjargs
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *2Ђ/
-*
embedding_17_inputџџџџџџџџџ
і2ѓ
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217450
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217860
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217106
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217126Р
ЗВГ
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
и2е
.__inference_embedding_17_layer_call_fn_1217867Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
ѓ2№
I__inference_embedding_17_layer_call_and_return_conditional_losses_1217877Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
2
6__inference_spatial_dropout1d_17_layer_call_fn_1217882
6__inference_spatial_dropout1d_17_layer_call_fn_1217887
6__inference_spatial_dropout1d_17_layer_call_fn_1217892
6__inference_spatial_dropout1d_17_layer_call_fn_1217897Д
ЋВЇ
FullArgSpec)
args!
jself
jinputs

jtraining
varargs
 
varkw
 
defaults
p 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
2
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217902
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217924
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217929
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217951Д
ЋВЇ
FullArgSpec)
args!
jself
jinputs

jtraining
varargs
 
varkw
 
defaults
p 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
2
)__inference_lstm_17_layer_call_fn_1217962
)__inference_lstm_17_layer_call_fn_1217973
)__inference_lstm_17_layer_call_fn_1217984
)__inference_lstm_17_layer_call_fn_1217995е
ЬВШ
FullArgSpecB
args:7
jself
jinputs
jmask

jtraining
jinitial_state
varargs
 
varkw
 
defaults

 
p 

 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
ѓ2№
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218246
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218625
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218876
D__inference_lstm_17_layer_call_and_return_conditional_losses_1219255е
ЬВШ
FullArgSpecB
args:7
jself
jinputs
jmask

jtraining
jinitial_state
varargs
 
varkw
 
defaults

 
p 

 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
д2б
*__inference_dense_17_layer_call_fn_1219264Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
я2ь
E__inference_dense_17_layer_call_and_return_conditional_losses_1219275Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
зBд
%__inference_signature_wrapper_1217151embedding_17_input"
В
FullArgSpec
args 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
Є2Ё
.__inference_lstm_cell_17_layer_call_fn_1219292
.__inference_lstm_cell_17_layer_call_fn_1219309О
ЕВБ
FullArgSpec3
args+(
jself
jinputs
jstates

jtraining
varargs
 
varkw
 
defaults
p 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
к2з
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1219391
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1219537О
ЕВБ
FullArgSpec3
args+(
jself
jinputs
jstates

jtraining
varargs
 
varkw
 
defaults
p 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 Ё
"__inference__wrapped_model_1215376{%'&<Ђ9
2Ђ/
-*
embedding_17_inputџџџџџџџџџ
Њ "3Њ0
.
dense_17"
dense_17џџџџџџџџџЅ
E__inference_dense_17_layer_call_and_return_conditional_losses_1219275\/Ђ,
%Ђ"
 
inputsџџџџџџџџџd
Њ "%Ђ"

0џџџџџџџџџ
 }
*__inference_dense_17_layer_call_fn_1219264O/Ђ,
%Ђ"
 
inputsџџџџџџџџџd
Њ "џџџџџџџџџЎ
I__inference_embedding_17_layer_call_and_return_conditional_losses_1217877a0Ђ-
&Ђ#
!
inputsџџџџџџџџџ
Њ "*Ђ'
 
0џџџџџџџџџd
 
.__inference_embedding_17_layer_call_fn_1217867T0Ђ-
&Ђ#
!
inputsџџџџџџџџџ
Њ "џџџџџџџџџdХ
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218246}%'&OЂL
EЂB
41
/,
inputs/0џџџџџџџџџџџџџџџџџџd

 
p 

 
Њ "%Ђ"

0џџџџџџџџџd
 Х
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218625}%'&OЂL
EЂB
41
/,
inputs/0џџџџџџџџџџџџџџџџџџd

 
p

 
Њ "%Ђ"

0џџџџџџџџџd
 Ж
D__inference_lstm_17_layer_call_and_return_conditional_losses_1218876n%'&@Ђ=
6Ђ3
%"
inputsџџџџџџџџџd

 
p 

 
Њ "%Ђ"

0џџџџџџџџџd
 Ж
D__inference_lstm_17_layer_call_and_return_conditional_losses_1219255n%'&@Ђ=
6Ђ3
%"
inputsџџџџџџџџџd

 
p

 
Њ "%Ђ"

0џџџџџџџџџd
 
)__inference_lstm_17_layer_call_fn_1217962p%'&OЂL
EЂB
41
/,
inputs/0џџџџџџџџџџџџџџџџџџd

 
p 

 
Њ "џџџџџџџџџd
)__inference_lstm_17_layer_call_fn_1217973p%'&OЂL
EЂB
41
/,
inputs/0џџџџџџџџџџџџџџџџџџd

 
p

 
Њ "џџџџџџџџџd
)__inference_lstm_17_layer_call_fn_1217984a%'&@Ђ=
6Ђ3
%"
inputsџџџџџџџџџd

 
p 

 
Њ "џџџџџџџџџd
)__inference_lstm_17_layer_call_fn_1217995a%'&@Ђ=
6Ђ3
%"
inputsџџџџџџџџџd

 
p

 
Њ "џџџџџџџџџdЫ
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1219391§%'&Ђ}
vЂs
 
inputsџџџџџџџџџd
KЂH
"
states/0џџџџџџџџџd
"
states/1џџџџџџџџџd
p 
Њ "sЂp
iЂf

0/0џџџџџџџџџd
EB

0/1/0џџџџџџџџџd

0/1/1џџџџџџџџџd
 Ы
I__inference_lstm_cell_17_layer_call_and_return_conditional_losses_1219537§%'&Ђ}
vЂs
 
inputsџџџџџџџџџd
KЂH
"
states/0џџџџџџџџџd
"
states/1џџџџџџџџџd
p
Њ "sЂp
iЂf

0/0џџџџџџџџџd
EB

0/1/0џџџџџџџџџd

0/1/1џџџџџџџџџd
  
.__inference_lstm_cell_17_layer_call_fn_1219292э%'&Ђ}
vЂs
 
inputsџџџџџџџџџd
KЂH
"
states/0џџџџџџџџџd
"
states/1џџџџџџџџџd
p 
Њ "cЂ`

0џџџџџџџџџd
A>

1/0џџџџџџџџџd

1/1џџџџџџџџџd 
.__inference_lstm_cell_17_layer_call_fn_1219309э%'&Ђ}
vЂs
 
inputsџџџџџџџџџd
KЂH
"
states/0џџџџџџџџџd
"
states/1џџџџџџџџџd
p
Њ "cЂ`

0џџџџџџџџџd
A>

1/0џџџџџџџџџd

1/1џџџџџџџџџdУ
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217106u%'&DЂA
:Ђ7
-*
embedding_17_inputџџџџџџџџџ
p 

 
Њ "%Ђ"

0џџџџџџџџџ
 У
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217126u%'&DЂA
:Ђ7
-*
embedding_17_inputџџџџџџџџџ
p

 
Њ "%Ђ"

0џџџџџџџџџ
 З
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217450i%'&8Ђ5
.Ђ+
!
inputsџџџџџџџџџ
p 

 
Њ "%Ђ"

0џџџџџџџџџ
 З
J__inference_sequential_17_layer_call_and_return_conditional_losses_1217860i%'&8Ђ5
.Ђ+
!
inputsџџџџџџџџџ
p

 
Њ "%Ђ"

0џџџџџџџџџ
 
/__inference_sequential_17_layer_call_fn_1216564h%'&DЂA
:Ђ7
-*
embedding_17_inputџџџџџџџџџ
p 

 
Њ "џџџџџџџџџ
/__inference_sequential_17_layer_call_fn_1217086h%'&DЂA
:Ђ7
-*
embedding_17_inputџџџџџџџџџ
p

 
Њ "џџџџџџџџџ
/__inference_sequential_17_layer_call_fn_1217168\%'&8Ђ5
.Ђ+
!
inputsџџџџџџџџџ
p 

 
Њ "џџџџџџџџџ
/__inference_sequential_17_layer_call_fn_1217185\%'&8Ђ5
.Ђ+
!
inputsџџџџџџџџџ
p

 
Њ "џџџџџџџџџЛ
%__inference_signature_wrapper_1217151%'&RЂO
Ђ 
HЊE
C
embedding_17_input-*
embedding_17_inputџџџџџџџџџ"3Њ0
.
dense_17"
dense_17џџџџџџџџџо
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217902IЂF
?Ђ<
63
inputs'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
p 
Њ ";Ђ8
1.
0'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 о
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217924IЂF
?Ђ<
63
inputs'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
p
Њ ";Ђ8
1.
0'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 Л
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217929f8Ђ5
.Ђ+
%"
inputsџџџџџџџџџd
p 
Њ "*Ђ'
 
0џџџџџџџџџd
 Л
Q__inference_spatial_dropout1d_17_layer_call_and_return_conditional_losses_1217951f8Ђ5
.Ђ+
%"
inputsџџџџџџџџџd
p
Њ "*Ђ'
 
0џџџџџџџџџd
 Е
6__inference_spatial_dropout1d_17_layer_call_fn_1217882{IЂF
?Ђ<
63
inputs'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
p 
Њ ".+'џџџџџџџџџџџџџџџџџџџџџџџџџџџЕ
6__inference_spatial_dropout1d_17_layer_call_fn_1217887{IЂF
?Ђ<
63
inputs'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
p
Њ ".+'џџџџџџџџџџџџџџџџџџџџџџџџџџџ
6__inference_spatial_dropout1d_17_layer_call_fn_1217892Y8Ђ5
.Ђ+
%"
inputsџџџџџџџџџd
p 
Њ "џџџџџџџџџd
6__inference_spatial_dropout1d_17_layer_call_fn_1217897Y8Ђ5
.Ђ+
%"
inputsџџџџџџџџџd
p
Њ "џџџџџџџџџd