# Gender Stereotypes

## Overview

### Datasets

In this project you will work with product reviews written on amazon.com.

### Research goal

The task is to predict gender of the author from the review text only. Existing results suggest that classifiers are very adept at this task unlike humans who perform much worse. In fact, humans tend to stereotype by classifying the author as well if they can guess that the review is related to Electronics similarly for female when related to beauty products. The idea would be to check if the models also pick up on this stereotyping. One way could be by removing product related terms from the reviews and then retraining these models to see if the accuracy drops.

### Sources

- Information on dataset - <https://nijianmo.github.io/amazon/index.html>.
- You can work with smaller per-category datasets in case the computing resources are an issue. You can consider Electronics category as the one dominated by males while Clothing, Shoes and Jewelry as the one dominated by females. However, you are free to take up any other as well given they make intuitive sense.
- Download links are available in the link mentioned above.
- Related paper - <https://arxiv.org/pdf/2001.09955.pdf>

### Additional Research Links

- Could be useful when developing own model to identify gender - <https://www.researchgate.net/publication/220346182_Author_gender_identification_from_text>

## Presentation

The mid-term presentation should contain

### Goal

- What is the purpose of your endeavor? What questions are you trying to answer?

### Approach

- How are you going to tackle your task? How are you going to answer the question(s)?

### Preliminary results

- What does the data you expect to work with look like?
- What progress have you made so far, which roadblocks did you already encounter?
- Are there unsolved roadblocks that maybe others in this meeting can help you with?
- Also think of potential roadblocks that might come up!

### Evaluation strategy

- How are you going to access whether you have been able to answer your question/made significant progress on your journey?

### What’s up next?

- What it the next things you are going to tackle?
- Also give a rough schedule of which task’s you plan to complete until when. Make sure you also factor in time for writing.

Presentation should be **<10 min**
