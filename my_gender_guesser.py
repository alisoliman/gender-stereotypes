# Find probability that name has a certain gender

import pandas as pd
import numpy as np

import glob, os
import re


def find_firstname(name, pattern):
    if not isinstance(name, str):
        # shouldn't happen
        return "-"
        # filter out something like Greg and Kristin
    if ("&" in name) or (" and " in name):
        return "-"

    matches = re.findall(pattern, name)
    if matches == []:
        # print(name)
        return "-"
    return matches[0].lower()


class Detector:
    def __init__(self, min_proba=0.97, min_occurrence=10000):
        name_probs = pd.read_csv("name_probas.csv")

        name_probs = name_probs[(name_probs.F + name_probs.M) > min_occurrence]
        name_probs = name_probs[(name_probs.gender_prob > min_proba) | (name_probs.gender_prob < 1-min_proba)]

        name_probs["gender"] = name_probs.gender_prob.apply(lambda x: "male" if x > 0.5 else "female")


        #name_probs.Name = name_probs.Name.apply(lambda x: x.lower())
        name_probs.Name = name_probs.Name.apply(lambda fname: fname.lower())
        name_probs = name_probs.set_index("Name")
        self.gender_dict = name_probs["gender"].to_dict()

        self.pattern_firstname = re.compile("[a-zA-Z]+")

    def get_gender(self, name):
        firstname = find_firstname(name, self.pattern_firstname)
        if firstname == "-":
            return firstname
        if firstname in self.gender_dict:
            return self.gender_dict[firstname]
        else:
            return "-"


if __name__ == '__main__':

    names = pd.DataFrame()

    os.chdir(".\\names")
    for file in glob.glob("*.txt"):
        if int(re.findall("\d+", file)[0]) > 1900:
            names = pd.concat([names, pd.read_csv(file, sep=",", header=None)], axis=0, ignore_index=True)
    os.chdir(".\\..")

    names.columns = ["name", "gender", "occs"]

    sumnames =names.groupby(["name", "gender"]).sum()
    sumnames = sumnames.reset_index()

    female_names = sumnames[sumnames.gender == "F"].drop(columns=["gender"])
    female_names.columns = ["name", "F"]
    female_names = female_names.set_index("name")
    male_names = sumnames[sumnames.gender == "M"].drop(columns=["gender"])
    male_names.columns = ["name", "M"]
    male_names = male_names.set_index("name")

    name_probs = male_names.join(female_names).fillna(0)
    name_probs["gender_prob"] = name_probs.M / (name_probs.M +name_probs.F)
    print(len(name_probs))
    all = name_probs

    # Zwischen 0.3 und 0.7 Prozent, dass Name männlich ist
    # Mindestens 1000 Vorkommen des Namen

    name_probs.to_csv("name_probas.csv", index_label="Name", header=True)








