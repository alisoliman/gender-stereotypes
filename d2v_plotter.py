import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import os
import itertools

# keywords = True
# gender = True
debug = False

for gender, keywords in itertools.product([True, False], [True, False]):

    if keywords:
        csv_file = f"results_keyword"
    else:
        csv_file = f"results_random"

    csv_path = os.path.join("results", csv_file + ".csv")

    df = pd.read_csv(csv_path, sep=",")
    if gender:
        df = df[df.variant == "gender"]
    else:
        df = df[df.variant == "category"]
    # df = df.rename(columns={df.columns[0]: "min_error"})
    df_cols = [col for col in df.columns if "MBWK-b" not in col]

    # df = df[df_cols]

    for model in ["BB", "BC", "BE", "BF", "BM"]:
        cols = [col for col in df.columns if model in col]
        df_model = df[cols]
        # df_model = df_model.rename(columns={f"('{model}', 'tp')": "tp",
        #                                     f"('{model}', 'tn')": "tn",
        #                                     f"('{model}', 'fp')": "fp",
        #                                     f"('{model}', 'fn')": "fn"})
        metrics_p = [f"{model}-tp", f"{model}-fn"]
        df[metrics_p] = df_model[metrics_p].div(df_model[metrics_p].sum(axis=1), axis=0)

        metrics_n = [f"{model}-tn", f"{model}-fp"]
        df[metrics_n] = df_model[metrics_n].div(df_model[metrics_n].sum(axis=1), axis=0)

    # fig, ax = plt.subplots()
    #
    # ax.plot([1,2,3,4], [3,4,5,6])
    # plt.show()

    for x in ["min_error", "vocab_size"]:
        for variant in [0, 1]:
            for metric in ["tp", "fp", "tn", "fn", "acc"]:
                if keywords is False and x == "min_error":
                    continue
                fig, ax = plt.subplots()
                df_var = df[df["gender/category"] == variant]
                cols = [col for col in df.columns if metric in col]

                if len(df_var[cols[0]]) == 0:
                    continue

                ax.plot(df_var[x], df_var[cols[0]], color="grey", label="Both categories")
                if gender or metric == "acc":
                    ax.plot(df_var[x], df_var[cols[1]], color="pink", label="Clothing")
                    ax.plot(df_var[x], df_var[cols[2]], color="aqua", label="Electronics")
                if not gender or metric == "acc":
                    ax.plot(df_var[x], df_var[cols[3]], color="tomato", label="Female")
                    ax.plot(df_var[x], df_var[cols[4]], color="cornflowerblue", label="Male")
                ax.legend()

                #df_var = df_var.melt(id_vars=x, value_name=metric, var_name='model_type')

                # sns.lineplot(x=x, y=metric, hue='model_type', data=df_var, ax=ax, palette=["grey", "pink", "aqua"])
                if keywords:
                    if x == "vocab_size":
                        ax.set_xlim(3000, 16000)
                        ax.set_xlabel("Number of words in vocabulary (Removal of keywords)")
                    else:
                        ax.set_xlim(0, 0.5)
                        ax.set_xlabel("Minimum Error")
                else:
                        ax.set_xlim(3000, 16000)
                        ax.set_xlabel("Number of words in vocabulary (Random Removal)")

                if metric == "acc":
                    ax.set_ylim(0.5, 1)
                    ax.set_ylabel("Accuracy")
                else:
                    ax.set_ylim(0, 1)
                    if metric == "tp":
                        ax.set_ylabel("True Positive Rate (True Male Rate)")
                    if metric == "tn":
                        ax.set_ylabel("True Female Rate (True Female Rate)")
                    if metric == "fn":
                        ax.set_ylabel("False Negative Rate (False Female Rate)")
                    if metric == "fp":
                        ax.set_ylabel("False Positive Rate (False Male Rate)")

                ax.set_title(f"Predicting {'Gender' if gender else 'Category'}{' - Minimal Preprocessing' if variant == 0 else 'Heavy Preprocessing'}")

                directory = ""
                if keywords:
                    directory += "keywords"
                else:
                    directory += "random"
                if gender:
                    directory += "_gender"
                else:
                    directory += "_category"
                fig_name = f"NEW_{metric}_{variant}_{x.replace(' ', '')}"
                plt.savefig(os.path.join("figures", "d2v_all", directory, fig_name+csv_file)+".png", dpi=1200)
                plt.close('all')



