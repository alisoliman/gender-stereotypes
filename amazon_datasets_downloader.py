import pandas as pd
import httplib2
from bs4 import BeautifulSoup

from urllib.request import urlopen
import time
import secrets
import urllib.request
from os.path import isfile, isdir, join, splitext
from os import makedirs, listdir
import os
import json
import gzip
import re

# extracts firstname from username

def find_firstname(name):
    if not isinstance(name, str):
        # shouldn't happen
        return None
    # filter out something like Greg and Kristin
    if ("&" in name) or (" and " in name):
        return None

    matches = re.findall("[a-zA-Z]+", name)
    if matches == []:
        #print(name)
        return None
    return matches[0].lower()

dataset_gz = "amazon-datasets-gz"
dataset_csv = "amazon-datasets-csv"


# ----------- download gz files if necessary

http = httplib2.Http()
status, response = http.request('https://nijianmo.github.io/amazon/index.html')

soup = BeautifulSoup(response, 'html.parser')
gz_links = [link["href"] for link in soup.find_all("a", string="5-core")]
gz_links = [link for link in gz_links if ("Clothing" in link) or ("Elec" in link)]

for i in gz_links:
    print(i)



input1 = input(f"Do you want to download the files above in {dataset_gz}? Press y: ")
if input1 != "y":
    exit()


if not isdir(dataset_gz):
    makedirs(dataset_gz)


for link in gz_links:
    file_base_name = filename = splitext(splitext(os.path.basename(link))[0])[0]
    file_name = os.path.basename(link)
    file_path = os.path.join(dataset_gz, file_name)

    if isfile(file_path):
        print(file_base_name, "already exists. Skip download.")
        continue

    print("Downloading ", file_base_name, "...")
    urllib.request.urlretrieve(link, os.path.join(dataset_gz, file_name))

# ----------------- csv files ----------------


def write_csv(df_dict, csv_file_path, append):
    df = pd.DataFrame.from_dict(df_dict, orient='index')
    df = df[["reviewerID", "reviewerName", "asin", "verified", "unixReviewTime", "reviewText"]]
    # df = df.dropna(subset=["reviewText", "reviewerName"])
    # df = df.drop_duplicates(subset=["reviewText"])

    if not append:
        df.to_csv(csv_file_path, index=False)
    else:
        df.to_csv(csv_file_path, index=False, mode="a", header=False)


def parse(path):
    g = gzip.open(path, 'rb')
    for l in g:

        yield json.loads(l)


if not isdir(dataset_csv):
    makedirs(dataset_csv)

gz_files = [join(dataset_gz, f) for f in listdir(dataset_gz) if isfile(join(dataset_gz, f)) and f.endswith("gz")]


print(f"gzip to csv in : {dataset_csv}")


for path_to_file in gz_files:
    filename = splitext(splitext(os.path.basename(path_to_file))[0])[0]
    csv_file_path = os.path.join(dataset_csv, filename + ".csv")
    try:
        print("start", filename)

        i = 0
        write_n = 500000
        df_dict = {}
        for d in parse(path_to_file):
            df_dict[i] = d
            i += 1
            if i % write_n == 0:
                write_csv(df_dict, csv_file_path, i>write_n)
                df_dict = {}
        if df_dict:
            write_csv(df_dict, csv_file_path, i>write_n)


        #firstnames = df.reviewerName.apply(lambda x: find_firstname(x))
        # df["gender"] = firstnames.apply(lambda fname: gd.get_gender(fname))
        #df.to_csv(join(dataset_csv, filename + ".csv"))
    except:
        print(f"Could not unpack {filename}")














