import time
import itertools

import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense

from pipeline import *

debug = False
if debug:
    FEMALE_CATEGORY = os.path.join("amazon-datasets-csv", "AMAZON_FASHION_5.csv")
    MALE_CATEGORY = os.path.join("amazon-datasets-csv", "Arts_Crafts_and_Sewing_5.csv")
    # FEMALE_CATEGORY_1 = os.path.join("amazon-datasets-csv", "Kindle_Store_5.csv")
    # MALE_CATEGORY_1 = os.path.join("amazon-datasets-csv", "Sports_and_Outdoors_5.csv")
    # FEMALE_CATEGORY_2 = os.path.join("amazon-datasets-csv", "Luxury_Beauty_5.csv")
    # MALE_CATEGORY_2 = os.path.join("amazon-datasets-csv", "Automotive_5.csv")
    # FEMALE_CATEGORY_LIST = [FEMALE_CATEGORY_1, FEMALE_CATEGORY_2]
    # MALE_CATEGORY_LIST = [MALE_CATEGORY_1, MALE_CATEGORY_2]
    FEMALE_CATEGORY_LIST = [FEMALE_CATEGORY]
    MALE_CATEGORY_LIST = [MALE_CATEGORY]

else:

    FEMALE_CATEGORY = os.path.join("amazon-datasets-csv", "Clothing_Shoes_and_Jewelry_5.csv")
    MALE_CATEGORY = os.path.join("amazon-datasets-csv", "Electronics_5.csv")
    FEMALE_CATEGORY_LIST = [FEMALE_CATEGORY]
    MALE_CATEGORY_LIST = [MALE_CATEGORY]

vector_size = 150
number_words = 150
d2v_epochs = 20

max_vocab = 30000
min_count = 5
train_split = 0.8

# ------------------------
keywords = False
# gender = False

# if gender:
#     y = "gender_is_male"
# else:
#     y = "category_is_male"

filename = os.path.join("results", f"d2v_(kw_{keywords})__(gen+cat)__(real_{not debug})")
# ------------------------


def test_model(X1, y1, train_index_n1, test_index_n1, *args):
    """args is (df_all, test_index)"""
    error_list = []
    X_train_1 = X1.loc[train_index_n1]
    X_test_1 = X1.loc[test_index_n1]

    t = time.time()
    d2v_model, X_train_1 = encode_reviews_d2v(X_train_1, vector_size=vector_size, epochs=d2v_epochs, min_count=min_count, max_vocab=max_vocab)
    _, X_test_1 = encode_reviews_d2v(X_test_1, d2v_model=d2v_model, vector_size=vector_size, epochs=d2v_epochs, min_count=min_count, max_vocab=max_vocab)
    print("d2v: ", time.time()-t)

    model = Sequential()
    model.add(Dense(100, input_dim=150, activation='relu'))
    model.add(Dense(70, activation='relu'))
    model.add(Dense(50, activation='relu'))
    model.add(Dense(25, activation='relu'))
    model.add(Dense(12, activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    history = model.fit(X_train_1, y1.loc[train_index_n1], batch_size=200, validation_split=0.1,
                        epochs=20, verbose=False)

    error_list += get_metrics(model, X_test_1, y1.loc[test_index_n1])

    for test_index2 in args:
        X_test2 = X1.loc[test_index2]
        y2 = y1.loc[test_index2]

        _, X_test2 = encode_reviews_d2v(X_test2, d2v_model=d2v_model)

        error_list += get_metrics(model, X_test2, y2)

    return error_list


# Dataset with male and female category
df_all_c_e = get_df_all(male_categories=MALE_CATEGORY_LIST, female_categories=FEMALE_CATEGORY_LIST)

t0 = time.time()
metrics_list_gender = []
metrics_list_category = []
df_metrics = pd.DataFrame()

for variant in [0, 1]:

    # Dataset with male and female category
    balanced_index_c_e = balance(df_all_c_e, balance_gender=True, balance_categories=True)
    df_all_c_e = df_all_c_e.loc[balanced_index_c_e]
    df_all_c_e["review"] = prepare_reviews(df_all_c_e.reviewText, number_words=number_words, variant=variant)
    train_index_c_e = np.random.choice(balanced_index_c_e, int(len(balanced_index_c_e) * train_split), replace=False)
    test_index_c_e = list(set(train_index_c_e) & set(balanced_index_c_e))

    if keywords:
        min_errors = np.linspace(0, 0.499, 10)
    else:
        min_errors = np.linspace(1000, 16000, 10)

    for min_error in min_errors:

        # Find Keyword tokenizer
        tokenizer_index = np.random.choice(train_index_c_e, int(len(train_index_c_e) * 0.9), replace=False)
        if keywords:
            remove_tokenizer = get_remove_keyword_tokenizer(df_all_c_e.loc[tokenizer_index], min_error=min_error)
        else:
            remove_tokenizer = get_remove_random_tokenizer(df_all_c_e.loc[tokenizer_index].review,
                                                           vocab_size=int(min_error))

        X_c_e = remove_keywords(df_all_c_e.review, remove_tokenizer)

        cols = ["variant", "gender/category", "min_error", "vocab_size"]
        metrics_error_list_gender = ["gender", variant, min_error, len(remove_tokenizer.word_index)]
        metrics_error_list_category = ["category", variant, min_error, len(remove_tokenizer.word_index)]

        # Model from Male and Female category
        clothing_index = df_all_c_e[(df_all_c_e.category_is_male == 0) & (df_all_c_e.index.isin(test_index_c_e))].index
        electronics_index = df_all_c_e[(df_all_c_e.category_is_male == 1) & (df_all_c_e.index.isin(test_index_c_e))].index

        male_index = df_all_c_e[(df_all_c_e.gender_is_male == 0) & (df_all_c_e.index.isin(test_index_c_e))].index
        female_index = df_all_c_e[
            (df_all_c_e.gender_is_male == 1) & (df_all_c_e.index.isin(test_index_c_e))].index

        metrics_error_list_gender += test_model(X_c_e, df_all_c_e["gender_is_male"], train_index_c_e, test_index_c_e, *(clothing_index, electronics_index, female_index, male_index))
        metrics_error_list_category += test_model(X_c_e, df_all_c_e["category_is_male"], train_index_c_e, test_index_c_e,
                                                *(clothing_index, electronics_index, female_index, male_index))
        metrics_list_gender.append(metrics_error_list_gender)
        cols += [e[0]+"-"+e[1] for e in list(itertools.product(["BB", "BC", "BE", "BF", "BM"], ["acc", "tp", "tn", "fn", "fp"]))]

        if len(df_metrics) == 0:
            df_metrics[cols] = None

        df_metrics.loc[len(df_metrics)] = pd.Series(metrics_error_list_gender, index=cols)
        df_metrics.loc[len(df_metrics)] = pd.Series(metrics_error_list_category, index=cols)
        df_metrics.to_csv(filename + "NEW.csv", index=False)
        df_metrics.to_html(filename + "NEW.html", index=False)
        print(variant, min_error, "done")

df_metrics.to_csv(filename + "NEW.csv", index=False)
df_metrics.to_html(filename + "NEW.html", index=False)


print(time.time()-t0)


