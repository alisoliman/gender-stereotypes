from os.path import isfile, isdir, join, splitext
from os import makedirs, listdir
import os
import re
import time
from operator import itemgetter

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split as train_test_split_sklearn
import my_gender_guesser as gender

from collections import Counter
from keras.preprocessing.text import Tokenizer

from gensim.models.doc2vec import Doc2Vec, TaggedLineDocument

from keras.preprocessing.sequence import pad_sequences

from gensim.models import Word2Vec
from gensim.models import Phrases


import nltk

nltk.download('stopwords')
from nltk.corpus import stopwords


def get_metrics(model, X_test, y_test):
    test_predict = (model.predict(X_test) > 0.5).flatten()
    tp = ((test_predict + y_test) == 2).sum()
    tn = ((test_predict + y_test) == 0).sum()
    fn = ((test_predict - y_test) == -1).sum()
    fp = ((test_predict - y_test) == 1).sum()
    acc = (tp+tn)/(fn+fp+tn+tp)
    return acc, tp, tn, fn, fp


def _insert(dictionary, word):
    if word in dictionary:
        dictionary[word] += 1
    else:
        dictionary[word] = 1


def _create_idf_dict(series):
    dictionary = dict()
    for bag_of_words in series:
        no_duplicates = list(dict.fromkeys(bag_of_words))
        # print(no_duplicates)
        for word in no_duplicates:
            _insert(dictionary, word)
    return dictionary


def _get_vectors_for_review(review, word2vec, size):
    vector_list = []
    for word in review:
        if word in word2vec.wv.key_to_index:
            vector_list.append(word2vec.wv.get_vector(word, norm=True))
        else:
            vector_list.append(np.zeros(size, dtype="float32"))
    len_review = len(vector_list)
    if len_review < 150:
        vector_list += [np.zeros(size, dtype="float32")] * (150 - len_review)
    return np.array(vector_list)


def get_df_all(gender_guesser=None, male_categories=[], female_categories=[]):
    if gender_guesser is None:
        gender_guesser = gender.Detector()

    df_all = pd.DataFrame()

    for category_is_male, csv_files in [(1, male_categories), (0, female_categories)]:

        for c_id, file_path in enumerate(csv_files):
            filename = splitext(os.path.basename(file_path))[0]

            df = pd.read_csv(file_path, usecols=["reviewerID", "reviewerName", "asin", "reviewText"])

            df = df.dropna(subset=["reviewerID", "asin", "reviewText"])
            df = df.drop_duplicates(subset=["reviewText"])

            df["gender_is_male"] = df.reviewerName.apply(lambda username: gender_guesser.get_gender(username))
            df = df[df.gender_is_male.isin(["male", "female"])]
            df.gender_is_male = df.gender_is_male.replace("male", 1).replace("female", 0)

            # df["category"] = filename.split("_")[0]
            df["category_is_male"] = category_is_male

            df = df.sample(frac=1, random_state=5)
            df_all = pd.concat([df_all, df], axis=0).reset_index(drop=True)
            del df

    df_all = df_all.reset_index(drop=True).sample(frac=1)
    return df_all


def balance(df_all, balance_categories=True, balance_gender=True):

    if balance_categories and balance_gender:
        min_num = np.inf
        for category_is_male in [0, 1]:
            for g in [1, 0]:
                num = len(df_all[(df_all.category_is_male == category_is_male) & (df_all.gender_is_male == g)])
                # print(category, g, num)
                if num < min_num:
                    min_num = num

        df = pd.DataFrame()
        for category_is_male in [0, 1]:
            for g in [1, 0]:
                df = pd.concat([df, (df_all[(df_all.category_is_male == category_is_male) & (df_all.gender_is_male == g)].sample(n=min_num, random_state=13))], axis=0)

    elif balance_gender:
        min_num = np.inf
        for g in [1, 0]:
            num = len(df_all[df_all.gender_is_male == g])
            if num < min_num:
                min_num = num

        df = pd.DataFrame()

        for g in [1, 0]:
            df = pd.concat([df, df_all[df_all.gender_is_male == g].sample(n=min_num, random_state=13)],
                           axis=0)

    elif balance_categories:
        min_num = np.inf
        for category_is_male in [0, 1]:
            num = len(df_all[df_all.category_is_male == category_is_male])
            if num < min_num:
                min_num = num

        df = pd.DataFrame()
        for category_is_male in [0, 1]:
            df = pd.concat([df, df_all[df_all.category_is_male == category_is_male].sample(n=min_num, random_state=13)],
                           axis=0)

    else:
        raise Exception("Nothing to balance")

    return df.index


def prepare_reviews(review_text, stop_words=None, lemmatization=False, bad_symbols=None, tokenizer=None, variant=0, number_words=150):
    from nltk.tokenize import regexp_tokenize, wordpunct_tokenize, blankline_tokenize

    if variant == 0:
        # keep = re.compile("[.]+|[A-Za-z]+|[!]+|[&]+|[\n]+|[12345]+")
        #
        rep = {"\"": "", "'": ""}
        rep = dict((re.escape(k), v) for k, v in rep.items())
        pattern = re.compile("|".join(rep.keys()))

        pattern_all = re.compile("[.]+|[A-Za-z]+|[!]+|[&]+|[\n]+|[$]+")

        def on_review(review):
            review = pattern.sub(lambda k: rep[re.escape(k.group(0))], review)
            review = re.findall(pattern_all, review)
            review = review[:number_words]
            return review

        reviews = review_text.apply(on_review)
        #
        # #nwords = self.df_all.review.apply(lambda text: len(text))
        # df_all.review = df_all.review.apply(lambda text: text[:number_words])
        # reviews = reviews.apply(lambda review: regexp_tokenize(review, pattern="[.]+|[A-Za-z]+|[!]+|[&]+|[\n]+|[$]+"))
        # reviews = reviews.apply(lambda review: review[:number_words])

        # Bigrams:
        # phrases = Phrases(self.df_all.review)
        #
        # self.df_all.review = list(phrases[self.df_all.review])

    else:
        from nltk.stem.snowball import SnowballStemmer

        # REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
        # BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
        STOPWORDS = set(stopwords.words('english'))

        stemmer = nltk.stem.SnowballStemmer('english')

        def clean_text(text):

            text = text.lower()
            text = regexp_tokenize(text, pattern="[a-z]+")
            text = text[:number_words]
            text = [stemmer.stem(word) for word in text if word not in STOPWORDS]

            return text

        reviews = review_text.apply(clean_text)

    return reviews


def remove_keywords(reviews, tokenizer):
    S = tokenizer.texts_to_sequences(reviews.values)
    reviews_tmp = tokenizer.sequences_to_texts(S)
    reviews = pd.Series([review.split() for review in reviews_tmp], index=reviews.index)
    return reviews



def _gender_table(df_train, gender=0):
    # computes in how many docs word occur (just once per doc)
    # for clothing:

    col_female_category = f"female_category_{gender}_gender"

    index = df_train.index.intersection(df_train[(df_train.gender_is_male == gender) & (df_train.category_is_male == 0)].index)
    X_train = df_train.review[index]

    count_word_dict_clothing = _create_idf_dict(X_train)
    idf_scores_clothing = pd.DataFrame.from_dict(count_word_dict_clothing, orient="index")
    idf_scores_clothing.columns = [col_female_category]
    idf_scores_clothing = idf_scores_clothing.sort_values(by=col_female_category, ascending=False)[:50000]

    # for electronics
    col_male_category = f"male_category_{gender}_gender"

    index = df_train.index.intersection(df_train[(df_train.gender_is_male == gender) & (df_train.category_is_male == 1)].index)
    X_train = df_train.review[index]

    count_word_dict_elec = _create_idf_dict(X_train)
    idf_scores_elec = pd.DataFrame.from_dict(count_word_dict_elec, orient="index")
    idf_scores_elec.columns = [col_male_category]
    idf_scores_elec = idf_scores_elec.sort_values(by=col_male_category, ascending=False)[:50000]

    # inner join and calculate ratio
    idf_scores = idf_scores_clothing.join(idf_scores_elec, how="inner")
    #idf_scores = (idf_scores / len(X_train)) * 100
    idf_scores[f"ratio_{gender}"] = idf_scores[col_female_category] / (idf_scores[col_male_category] + idf_scores[col_female_category])
    # idf_scores[f"ratio_{gender}"] = idf_scores[f"ratio_{gender}"].apply(lambda x: 1/x if x > 1 else x)

    return np.round(idf_scores, 4)


def get_remove_keyword_tokenizer(df_train, min_error=0.3, MAX_WORDS=50000):

    keywords_female = _gender_table(df_train, gender=0)
    keywords_male = _gender_table(df_train, gender=1)
    keywords = keywords_female.join(keywords_male, how="inner")
    #keywords["diff"] = np.abs(keywords.ratio_1 - keywords.ratio_0)

    without_clothing = keywords[(keywords.ratio_1 < (1 - min_error)) | (keywords.ratio_0 < (1 - min_error))]
    without_elec = keywords[(keywords.ratio_1 > min_error) | (keywords.ratio_0 > min_error)]

    without_keywords = keywords[(keywords.index.isin(without_clothing.index)) & (keywords.index.isin(without_elec.index))]

    # The maximum number of words to be used. (most frequent)

    tokenizer = Tokenizer(num_words=MAX_WORDS, filters="", lower=False)
    tokenizer.fit_on_texts(list(without_keywords.index))
    # word_index = tokenizer.word_index

    return tokenizer


def get_remove_random_tokenizer(review, vocab_size):
    tokenizer = Tokenizer(filters="", lower=False)
    tokenizer.fit_on_texts(review.values)
    n = len(tokenizer.index_word)

    if vocab_size >= n:
        vocab_size = n

    index = list(np.random.choice(range(1, n + 1), vocab_size, replace=False))

    random_words = list(itemgetter(*index)(tokenizer.index_word))

    tokenizer = Tokenizer(filters="", lower=False)
    tokenizer.fit_on_texts(random_words)
    return tokenizer


def encode_reviews_w2v(X_train, X_test):
    # auf X_train finden
    # word 2vec
    # für jedes review einen vektor aus wordtovec
    #
    # train on X_train
    w2v = Word2Vec(X_train, min_count=2, vector_size=3)

    X_train_w2v = X_train.apply(lambda list_review: _get_vectors_for_review(list_review, w2v, 3))
    X_test_w2v = X_test.apply(lambda list_review: _get_vectors_for_review(list_review, w2v, 3))

    X_train_w2v = np.array([np.array(X_train_w2v.to_numpy()[i]) for i in range(len(X_train_w2v))])
    X_test_w2v = np.array([np.array(X_test_w2v.to_numpy()[i]) for i in range(len(X_test_w2v))])

    return X_train_w2v, X_test_w2v


def encode_reviews_d2v(X, epochs=5, d2v_model=None, vector_size=150, min_count=0, max_vocab=50000):

    # train on X_train
    import gensim
    import multiprocessing

    if d2v_model is None:

        train_corpus = [gensim.models.doc2vec.TaggedDocument(review, [i]) for i, review in enumerate(X)]
        cores = multiprocessing.cpu_count()

        d2v_model = gensim.models.doc2vec.Doc2Vec(vector_size=vector_size, min_count=min_count, epochs=epochs,
                                                  workers=cores - 1, max_final_vocab=max_vocab)
        d2v_model.build_vocab(train_corpus)
        d2v_model.train(train_corpus, total_examples=d2v_model.corpus_count, epochs=d2v_model.epochs)

    X = np.array([d2v_model.infer_vector(review) for review in X])

    return d2v_model, X


def encode_reviews_d2v_tagged_line(X, epochs=5, d2v_model=None, vector_size=150, min_count=0, max_vocab=50000):

    # train on X_train
    import gensim
    import multiprocessing

    if d2v_model is None:

        gensim.utils.save_as_line_sentence(X.values, "x_values_gensim")

        train_corpus = gensim.models.doc2vec.TaggedLineDocument("x_values_gensim")
        cores = multiprocessing.cpu_count()

        d2v_model = gensim.models.doc2vec.Doc2Vec(vector_size=vector_size, min_count=min_count, epochs=epochs,
                                                  workers=cores - 1, max_final_vocab=max_vocab)
        d2v_model.build_vocab(train_corpus)
        d2v_model.train(train_corpus, total_examples=d2v_model.corpus_count, epochs=d2v_model.epochs)

    X = np.array([d2v_model.infer_vector(review) for review in X])

    return d2v_model, X


def encode_reviews_seq(self, vector_size=150, max_vocab=50000):

    tokenizer = Tokenizer(num_words=max_vocab, filters="", lower=True)
    tokenizer.fit_on_texts(self.X_train)

    self.X_train = tokenizer.texts_to_sequences(self.X_train)
    self.X_train = pad_sequences(self.X_train, maxlen=vector_size)
    self.X_test = tokenizer.texts_to_sequences(self.X_test)
    self.X_test = pad_sequences(self.X_test, maxlen=vector_size)


def _build_feature_matrix(self, reviews, f_words):
    fm = list()
    for review in reviews:
        counter = Counter(review)
        fm.append([counter[word] for word in f_words.index])
    fm_df = pd.DataFrame(data=fm, index=reviews.index, columns=f_words.index)
    nwords = reviews.apply(lambda text: len(text))
    fm_df = fm_df.div(nwords, axis=0)
    # for i in range(len(data)):
    #     counter = Counter(data.X.iloc[i])
    #     fm.iloc[i] = [counter[word] for word in f_words]
    fm_df = fm_df * f_words.scores
    return fm_df


def encode_reviews_tf_idf(self, vector_size=1000, idf_scores=None):

    if idf_scores is None:
        count_word_dict_clothing = _create_idf_dict(self.X_train)
        idf_scores = pd.DataFrame.from_dict(count_word_dict_clothing, orient="index")
        print("idf_scores", len(idf_scores))
        if len(idf_scores) < vector_size:
            print("Vector Size greater than possible idf_scores")
        idf_scores = np.log2(len(self.X_train) / idf_scores).sort_values(by=0)[:vector_size]
        idf_scores.columns = ["scores"]
    self.X_train = self._build_feature_matrix(self.X_train, idf_scores).to_numpy()
    self.X_test = self._build_feature_matrix(self.X_test, idf_scores).to_numpy()

    return idf_scores

#
# if __name__ == '__main__':
#     import my_gender_guesser as gender
#
#     gd = gender.Detector()
#     d = DataStore(male_categories=["All"], female_categories=["AMAZON"])
#     d.prepare_reviews()
#     d.train_test_split()
#     d.remove_product_keywords()
#     d.encode_reviews_tf_idf()
#
#     # ganz normales Model CNN mit wordTo Vec
